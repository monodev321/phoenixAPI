package store

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
)

// 生成 rsa 密钥对
func GenerateRsaKey(keySize int) (priKey string, pubKey string) {
	/***************** 私钥 **********************/
	// 1 生产私钥
	privateKey, err := rsa.GenerateKey(rand.Reader, keySize)
	if err != nil {
		panic(err)
	}
	// fmt.Printf("%X", *privateKey)

	// 2 通过x509标准将得到的rsa私钥序列化为ASN.1的DER编码字符串
	derText := x509.MarshalPKCS1PrivateKey(privateKey)

	// 3 将私钥字符串设置到pem格式块中，组织一个pem.Block
	block := pem.Block{
		Type:  "rsa private key",
		Bytes: derText,
	}

	// 4 pem编码
	buffer := new(bytes.Buffer)
	pem.Encode(buffer, &block)
	priKey = buffer.String()

	/***************** 公钥**********************/
	// 1 从私钥结构体中取出公钥
	publicKey := privateKey.PublicKey

	// 2 使用x509标准序列化
	derpText, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		panic(err)
	}

	// 3 组织一个pem.Block
	block = pem.Block{
		Type:  "rsa public key",
		Bytes: derpText,
	}

	// 4 pem编码
	buffer = new(bytes.Buffer)
	pem.Encode(buffer, &block)
	pubKey = buffer.String()
	return
}

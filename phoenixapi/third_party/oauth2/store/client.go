package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"phoenix/pkg/db"

	coinHash "phoenix/pkg/hash"

	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/models"
	"github.com/jmoiron/sqlx"
)

type ClientStore struct {
	db        *sqlx.DB
	tableName string

	initTableDisabled bool
	maxLifetime       time.Duration
	maxOpenConns      int
	maxIdleConns      int
}

// NewClientStore creates PostgreSQL store instance
func NewClientStore() (*ClientStore, error) {

	store := &ClientStore{
		db:           db.DBCli,
		tableName:    "oauth2_clients",
		maxLifetime:  time.Hour * 2,
		maxOpenConns: 50,
		maxIdleConns: 25,
	}

	return store, nil
}

func (s *ClientStore) toClientInfo(data models.Client) (oauth2.ClientInfo, error) {
	return &data, nil
}

// GetByID retrieves and returns client information by id
func (s *ClientStore) GetByID(ctx context.Context, id string) (oauth2.ClientInfo, error) {
	if id == "" {
		return nil, nil
	}

	var item models.Client
	err := s.db.QueryRowx(fmt.Sprintf("SELECT * FROM %s WHERE id = ?", s.tableName), id).StructScan(&item)
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}
	return s.toClientInfo(item)
}

// Create creates and stores the new client information
func (s *ClientStore) Create(info oauth2.ClientInfo) error {
	now := time.Now().Unix()
	priKey, pubKey := GenerateRsaKey(4096)
	username := info.GetUsername()
	secret := coinHash.GenerateHashCode(username)
	_, err := s.db.Exec(fmt.Sprintf("INSERT INTO %s (uid, secret, domain, username,redirect_url,private_key,publick_key,type,scope,create_time,update_time) VALUES (?,?,?,?,?,?,?,?,?,?,?)", s.tableName),
		info.GetUserID(),
		secret,
		info.GetDomain(),
		username,
		info.GetRedirectUrl(),
		priKey,
		pubKey,
		info.GetType(),
		info.GetScope(),
		now,
		now,
	)
	if err != nil {
		return err
	}
	return nil
}

// Create creates and stores the new client information
func (s *ClientStore) CreateDappClient(info oauth2.ClientInfo) (int64, error) {
	now := time.Now().Unix()
	priKey, pubKey := GenerateRsaKey(4096)
	username := info.GetUsername()
	secret := coinHash.GenerateHashCode(username)
	result, err := s.db.Exec(fmt.Sprintf("INSERT INTO %s (uid, secret, domain, username,redirect_url,private_key,publick_key,type,scope,create_time,update_time) VALUES (?,?,?,?,?,?,?,?,?,?,?)", s.tableName),
		info.GetUserID(),
		secret,
		info.GetDomain(),
		username,
		info.GetRedirectUrl(),
		priKey,
		pubKey,
		info.GetType(),
		info.GetScope(),
		now,
		now,
	)
	if err != nil {
		return 0, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return 0, errors.New("获取数据库结果错误")
	}

	if affected == 0 {
		return 0, errors.New("获取登陆信息失败")
	}

	id, err := result.LastInsertId()
	return id, err
}

// GetByID retrieves and returns client information by id
func (s *ClientStore) GetByClientID(id int64) (oauth2.ClientInfo, error) {
	var item models.Client
	err := s.db.QueryRowx(fmt.Sprintf("SELECT * FROM %s WHERE id = ?", s.tableName), id).StructScan(&item)
	switch {
	case err == sql.ErrNoRows:
		return nil, nil
	case err != nil:
		return nil, err
	}
	return s.toClientInfo(item)
}

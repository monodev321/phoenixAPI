package models

// Client client model
type Client struct {
	ID          int64  `db:"id"`
	Secret      string `db:"secret"`
	Domain      string `db:"domain"`
	UserID      int64  `db:"uid"`
	Username    string `db:"username"`
	RedirectUrl string `db:"redirect_url"`
	PrivateKey  string `db:"private_key"`
	PublickKey  string `db:"publick_key"`
	Type        string `db:"type"`
	Scope       string `db:"scope"`
	CreateTime  int64  `db:"create_time"`
	UpdateTime  int64  `db:"update_time"`
}

// GetID client id
func (c *Client) GetID() int64 {
	return c.ID
}

// GetSecret client secret
func (c *Client) GetSecret() string {
	return c.Secret
}

// GetDomain client domain
func (c *Client) GetDomain() string {
	return c.Domain
}

// GetUserID user id
func (c *Client) GetUserID() int64 {
	return c.UserID
}

// GetUserID user id
func (c *Client) GetUsername() string {
	return c.Username
}

// GetUserID user id
func (c *Client) GetRedirectUrl() string {
	return c.RedirectUrl
}

// GetUserID user id
func (c *Client) GetPrivateKey() string {
	return c.PrivateKey
}

// GetUserID user id
func (c *Client) GetPublickKey() string {
	return c.PublickKey
}

// GetUserID user id
func (c *Client) GetType() string {
	return c.Type
}

// GetUserID user id
func (c *Client) GetScope() string {
	return c.Scope
}

// GetUserID user id
func (c *Client) GetCreateTime() int64 {
	return c.CreateTime
}

// GetUserID user id
func (c *Client) GetUpdateTime() int64 {
	return c.UpdateTime
}

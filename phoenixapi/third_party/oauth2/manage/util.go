package manage

import (
	"net/url"
)

type (
	// ValidateURIHandler validates that redirectURI is contained in baseURI
	ValidateURIHandler func(baseURI, redirectURI string) error
)

// DefaultValidateURI validates that redirectURI is contained in baseURI
func DefaultValidateURI(baseURI string, redirectURI string) error {
	_, err := url.Parse(baseURI)
	if err != nil {
		return err
	}

	_, err = url.Parse(redirectURI)
	if err != nil {
		return err
	}
	// if !strings.HasSuffix(redirect.Host, base.Host) {
	// 	return errors.ErrInvalidRedirectURI
	// }
	return nil
}

package send

import (
	"fmt"

	ts "go.biyong.com/telesign/rest"
)

func GetVerification() {
	ts.SetCustomerID("Your Customer ID")
	ts.SetSecretKey("Your Secret Key")

	var EXTERNAL_ID string = "Your transactions external id"

	// Empty params dictionary
	var params map[string]string
	status_response := ts.MessageStatus(string(EXTERNAL_ID), params)
	fmt.Println(string(status_response.Body))

}

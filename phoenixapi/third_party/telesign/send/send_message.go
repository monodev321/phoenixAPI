package send

import (
	"encoding/json"
	"errors"

	ts "go.biyong.com/telesign/rest"

	jp "github.com/buger/jsonparser"
)

func SendMessage(mumber string, mssg string, clientId string, clientSecretKey string) error {
	// ts.SetCustomerID("EE70557E-8886-4C95-88DD-8E0F72AFF264")
	// ts.SetSecretKey("fzESdUrtH4Poi6PbPnemQU1X7H9RBUVPY5UyDbZ8+N1aRftWBJzdJMq+cVBO0RjZMRG30g+b3ZlTvUZr+2Rj6Q==")
	// println(mumber)
	ts.SetCustomerID(clientId)
	ts.SetSecretKey(clientSecretKey)

	var params map[string]string
	message_response := ts.Message(mumber, mssg, "OTP", params)
	ref_id, _, _, err := jp.Get(message_response.Body, "reference_id")
	// fmt.Printf("%+v", err)
	if err != nil || ref_id == nil {
		return errors.New("发送错误，请重试")
	}
	status_response := ts.MessageStatus(string(ref_id), params)
	var res map[string]interface{}
	err = json.Unmarshal(status_response.Body, &res)
	if err != nil {
		return errors.New("不支持您所在的国家")
	}
	// fmt.Printf("%+v", res)
	codeMap := res["status"].(map[string]interface{})
	code := int(codeMap["code"].(float64))
	if code == 203 {
		return nil
	}
	if code == 209 {
		return errors.New("不支持您所在的国家")
	}
	return errors.New("发送错误，请重试")

}

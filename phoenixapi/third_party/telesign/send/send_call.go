package send

import (
	ts "go.biyong.com/telesign/rest"
)

func GetCall() {
	ts.SetCustomerID("Your Customer ID")
	ts.SetSecretKey("Your Secret Key")

	// var PHONE_NUMBER2 = "asas"

	// // Empty params dictionary
	// var params map[string]string

	// // Start a call to listed phone_number
	// fmt.Printf("\nSend call request------ \n")
	// message_response := ts.Call(PHONE_NUMBER2, "", "ARN", params)
	// fmt.Println(string(message_response.Body))

	// // Parse out the transaction reference_id from payload
	// ref_id, _, _, err := jp.Get(message_response.Body, "reference_id")
	// if err != nil || ref_id == nil {
	// 	fmt.Printf("Failed to retrieve a valid reference_id or nil\n")
	// }
	// fmt.Printf("ref_id: %s", string(ref_id))

	// // Get call status
	// fmt.Printf("\nGet status ------ \n")
	// status_response := ts.CallStatus(string(ref_id), params)
	// fmt.Println(string(status_response.Body))

}

package send

import (
	"fmt"

	ts "go.biyong.com/telesign/rest"

	jp "github.com/buger/jsonparser"
)

func GetScore() {
	ts.SetCustomerID("Your Customer ID")
	ts.SetSecretKey("Your Secret Key")

	var PHONE_NUMBER1 = "Phone Number to test"
	var ACCOUNT_LIFECYCLE_EVENT = "create"

	// Empty params dictionary
	var params map[string]string

	// Send a Score request
	fmt.Printf("\nSend score request ------ \n")
	message_response := ts.Score(PHONE_NUMBER1, ACCOUNT_LIFECYCLE_EVENT, params)
	fmt.Println(string(message_response.Body))

	// Retrive reference_id from the transaction JSON response
	ref_id, _, _, err := jp.Get(message_response.Body, "reference_id")
	if err != nil || ref_id == nil {
		fmt.Printf("Failed to retrieve a valid reference_id or nil\n")
	}
	fmt.Printf("ref_id: %s", string(ref_id))

}

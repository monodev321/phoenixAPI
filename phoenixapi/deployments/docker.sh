#!/bin/bash

echo ${BUILD_ID}
echo ${WORKSPACE}
ls -al
ls ./dist 
if [ "${EVN}" = "pre" ];then
    docker build -f ./deployments/Dockerfile -t pre-phoenix-api:${VERSION}-${BUILD_ID} .
else
    docker build -f ./deployments/Dockerfile -t phoenix-api:${VERSION}-${BUILD_ID} .
fi


### 微软云

## api

sudo docker pull docker.biyong001.com/coin/coin-api:1.0.0-73
sudo docker rm -f coin-api

/home/azureuser/data/uploads

sudo docker run -d --name coin-api --network coinuse -p 9001:9000 -p 9000:9000 --restart=always -v ~/data/uploads/html:/data/site -v ~/data/leveldb:/data/database docker.biyong001.com/coin/coin-api:1.0.0-73 /var/app/app --m api --config /var/app/prod/

## task

sudo docker rm -f coin-task
sudo rm -rf data/leveldb/task-income-db
sudo docker run -d --name coin-task --network coinuse --restart=always -v ~/data/leveldb:/data/database docker.biyong001.com/coin/coin-api:1.0.0-69 /var/app/app --m task --config /var/app/prod/

## conn

sudo docker rm -f coin-connet
sudo docker run -d --name coin-connet --network coinuse --restart=always -p 6666:6666 -p 7000:7000 -v ~/data/leveldb:/data/database docker.biyong001.com/coin/coin-api:1.0.0-69 /var/app/app --m conn --config /var/app/prod/

## rpc

sudo docker rm -f coin-rpc
sudo docker run -d --name coin-rpc --network coinuse --restart=always -p 9888:9888 docker.biyong001.com/coin/coin-api:1.0.0-53 /var/app/app --m rpc --config /var/app/prod/

## develop

sudo docker rm -f coin-develop
sudo docker run \
--name coin-develop \
--restart=always \
--network coinuse \
-d \
docker.biyong001.com/coin/coin-develop:1.0.0-5

sudo docker rm -f coin-admin
sudo docker run \
--name coin-admin \
--restart=always \
--network coinuse \
-d \
docker.biyong001.com/coin/coin-admin:1.0.0-7

## coin 网关

sudo docker login -u admin -p Fh7FSgEQ6hMn8MB7Q4URnh76eEDdIXAg https://docker.biyong001.com/
docker pull docker.biyong001.com/gateway/gateway-coin:1.0.0-48
docker rm -f gateway
sudo docker run \
--name gateway \
--restart=always \
--network coinuse \
-d -p 443:443 -p 80:80 \
-v ~/data/gateway:/var/log/nginx \
docker.biyong001.com/gateway/gateway-coin:1.0.0-56

## coin 图片

sudo docker rm -f coin-image
sudo docker run -d \
--name coin-image \
--restart=always \
--network coinuse \
-p 8001:80 \
-v ~/data/uploads/log:/var/log/nginx \
-v ~/data/uploads/html:/usr/share/nginx/html \
nginx:stable

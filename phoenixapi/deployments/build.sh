#!/bin/bash
if [ "${EVN}" = "pre" ];then
    cp -f ./main-api.go2 ./main.go
    rm -f ./build/phoenixServer; 
    go mod tidy
    go build -o ./build/phoenixServer main.go ;
else
    echo "pro"
    # cp -f ./site/${SITE}/.env.production ./.env.production
    # cat ./.env.production
    # cp -f ./site/${SITE}/index.js ./src/router
    # cat ./src/router/index.js
    npm run build:prod
fi

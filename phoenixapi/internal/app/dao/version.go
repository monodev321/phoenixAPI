package dao

import (
	"time"

	"phoenix/internal/app/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type versionDao struct{}

var VersionDao = new(versionDao)

func (*versionDao) Add(version *model.Version) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `app_version` ( `code`, `name`, `channel`, `tables`, `url`, `update_comment`, `alert_comment`, `must`, `create_time`) values(?,?,?,?,?,?,?,?,?)",
		version.Code, version.Name, version.Channel, version.Tables, version.Url, version.UpdateComment, version.AlertComment, version.Must, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "获取版本失败")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加版本失败")
	}

	return id, nil
}

func (*versionDao) Last(channel string) (*model.Version, error) {
	var versions []model.Version
	err := db.DBCli.Select(&versions, "select id,code,name,channel,tables,url,update_comment,alert_comment,must,create_time from app_version where channel = ? ORDER BY id DESC limit 0,1 ", channel)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	if len(versions) > 0 {
		return &versions[0], nil
	}
	return nil, nil
}

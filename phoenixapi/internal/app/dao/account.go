package dao

import (
	"time"

	"phoenix/internal/app/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type systemAccountDao struct{}

var SystemAccountDao = new(systemAccountDao)

func (*systemAccountDao) Add(account *model.SystemAccount) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `system_accounts` ( `name`, `avatar`, `type`, `uid`, `status`, `comment`, `extra`, `create_time`) values(?,?,?,?,?,?,?,?)",
		account.Name, account.Avatar, account.Type, account.Uid, account.Status, account.Comment, account.Extra, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "获取版本失败")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加版本失败")
	}

	return id, nil
}

func (*systemAccountDao) Account(accountType string) (*model.SystemAccount, error) {
	var user model.SystemAccount
	err := db.DBCli.Get(&user, "select 	id,name,avatar,type,uid,status,extra,create_time from system_accounts where type = ? ", accountType)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &user, nil
}

package model

type Version struct {
	Id            int64  `json:"id"`                                // 代币id
	Name          string `json:"name"  db:"name"`                   // 代币名
	Code          int64  `json:"code"  db:"code"`                   // 代币英文符号
	Channel       string `json:"channel" db:"channel"`              // 代币图标url
	Url           string `json:"url" db:"url"`                      // 代币图标url
	Tables        string `json:"tables" db:"tables"`                // 代币说明
	UpdateComment string `json:"updateComment" db:"update_comment"` // 审核说明
	AlertComment  string `json:"alertComment"  db:"alert_comment"`  // 最大数量
	Must          int    `json:"must"  db:"must"`                   // 代币发行到哪个账户
	CreateTime    int64  `json:"createTime"  db:"create_time"`      // 创建时间
}

type SystemAccount struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Avatar     string `json:"avatar"`
	Type       string `json:"type"`
	Uid        int64  `json:"uid"`
	Status     int    `json:"status"`
	Comment    string `json:"comment"`
	Extra      string `json:"extra"`
	CreateTime int64  `json:"createTime" db:"create_time"`
}

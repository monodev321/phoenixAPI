package service

import (
	"phoenix/internal/app/dao"
	"phoenix/internal/app/model"
)

type versionService struct{}

var VersionService = new(versionService)

func (*versionService) AddVersion(version *model.Version) (int64, error) {

	id, err := dao.VersionDao.Add(version)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*versionService) Last(channel string) (*model.Version, error) {

	version, err := dao.VersionDao.Last(channel)
	if err != nil {
		return nil, err
	}
	return version, err
}

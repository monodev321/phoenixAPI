package service

import (
	"phoenix/internal/app/dao"
	"phoenix/internal/app/model"
)

type accountService struct{}

var AccountService = new(accountService)

func (*accountService) Add(account *model.SystemAccount) (int64, error) {

	id, err := dao.SystemAccountDao.Add(account)
	if err != nil {
		return 0, err
	}

	return id, err
}

func (*accountService) Account(accountType string) (*model.SystemAccount, error) {

	account, err := dao.SystemAccountDao.Account(accountType)
	if err != nil {
		return nil, err
	}
	return account, err
}

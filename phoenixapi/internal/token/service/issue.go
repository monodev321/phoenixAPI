package service

import (
	"encoding/json"
	"strconv"
	"time"

	"phoenix/internal/im/message"
	"phoenix/internal/im/tools"
	"phoenix/internal/token/constant"
	"phoenix/internal/token/dao"
	"phoenix/internal/token/model"
	"phoenix/pkg/db"
	coinHash "phoenix/pkg/hash"

	"phoenix/pkg/werror"
)

type issueService struct{}

var IssueService = new(issueService)

// @todo 这三个需要增加权限判断，是否有操作权限，uid是否等于开发者id
func (*issueService) Issue(tokenLog *model.TokenIssueLogs) (int64, error) {
	tokenid := tokenLog.TokenId
	token, err := dao.TokenDao.GetTokenById(tokenid)
	if err != nil {
		return 0, err
	}
	if token.Status != 1 {
		return 0, werror.New(100, "该代币未激活")
	}
	tokenLog.TokenName = token.Name
	tokenLog.TokenSymbol = token.Symbol
	tokenLog.TokenIcon = token.Icon
	tokenLog.TokenIcon = token.Icon
	// 添加到好友数据库
	id, err := dao.IssueDao.Add(tokenLog)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*issueService) Approve(tokenLog *model.TokenIssueLogs) (int64, error) {

	// 需要添加事务，添加的时候，同时添加流水记录，和用户代币，与转账相同
	// 查询发行用户

	id := tokenLog.Id
	res, err := dao.IssueDao.GetIssueLogById(id)
	if err != nil {
		return 0, err
	}

	if res.Status != 0 {
		return 0, werror.New(100, "已经审核过了")
	}
	tokenid := res.TokenId
	token, err := dao.TokenDao.GetTokenById(tokenid)
	if err != nil {
		return 0, err
	}
	issueUid := token.IssueUid

	// 如果是拒绝 ，就直接更新记录表就可以
	if tokenLog.Status == 2 {
		tx := db.DBCli.MustBegin()
		dao.IssueDao.Approve(tx, tokenLog)
		err = tx.Commit()

		if err != nil {
			return 0, err
		}
		return id, err
	}

	//审核通过的处理
	userToken, err := dao.UserTokenDao.GetUserTokenByUid(issueUid, tokenid)
	if err != nil {
		return 0, err
	}
	isNew := false
	quantity := uint64(0)
	gross := uint64(0)
	if userToken == nil {
		isNew = true
		quantity = res.Quantity
		gross = res.Quantity
	} else {
		quantity = userToken.AvailableQuantity + res.Quantity
		gross = userToken.GrossQuantity + res.Quantity
	}

	// 如果通过还需要判断是否超过了总的发行量
	if (res.Quantity + token.IssueQuantity) > token.QuantityLimit {
		return id, werror.New(200, "发行数量超过总代币限制")
	}

	orderId := coinHash.GetOrderSequence("1")
	flowLog := &model.TokenFlowLogs{
		TokenId:   tokenid,
		Uid:       issueUid,
		FUid:      int64(0),
		FUsername: "0",
		FNickname: "0",
		FAvatar:   "0",
		Quantity:  res.Quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_ISSUE,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_ADD,
		Comment:   "发行代币",
		Balance:   gross,
		OrderId:   orderId,
		Charge:    0,
		TranId:    "issue" + strconv.FormatInt(id, 10),
		ProceId:   "issue" + strconv.FormatInt(id, 10),
	}
	tx := db.DBCli.MustBegin()
	if isNew {
		userToken = new(model.UserTokens)
		userToken.Uid = issueUid
		userToken.AvailableQuantity = quantity
		userToken.GrossQuantity = quantity
		userToken.TokenId = tokenid
		dao.UserTokenDao.Add(tx, userToken)
	} else {
		dao.UserTokenDao.UpdateQuantity(tx, quantity, gross, userToken.Id)
	}
	issueQuantity := token.IssueQuantity + res.Quantity
	issueNum := token.IssueNum + 1
	result := dao.FlowRecordDao.Add(tx, flowLog)
	dao.IssueDao.Approve(tx, tokenLog)
	dao.TokenDao.ApproveIssue(tx, issueQuantity, issueNum, token.Id)
	err = tx.Commit()

	if err != nil {
		return 0, err
	}

	// 发送转账通知
	flowId, _ := (*result).LastInsertId()
	m := map[string]interface{}{
		"sysbol":   token.Symbol,
		"quantity": res.Quantity,
		"tokenId":  token.Id,
		"flowId":   flowId,
		"charge":   0,
		"uid":      token.IssueUid,
		"nickname": "-",
		"time":     time.Now().Unix(),
		"type":     constant.TOKEN_FLOW_LOG_TYPE_ISSUE,
	}
	mjson, _ := json.Marshal(m)
	msg := string(mjson)
	msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	message.SendSingleMassage("payaccount", msg, uint64(token.OwnerUid), uint64(token.IssueUid), msgid2, 0)

	return id, err
}

func (*issueService) TokenIssueList(tokenId int64) (*[]model.TokenIssueLogs, error) {

	// 添加到好友数据库
	var issues *[]model.TokenIssueLogs
	issues, err := dao.IssueDao.GetIssueLogByTokenId(tokenId)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return issues, err
}

func (*issueService) IssueFlow(tokenId int64) (*[]model.TokenIssueFlow, error) {

	issues, err := dao.IssueDao.IssueFlow(tokenId)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return issues, err
}

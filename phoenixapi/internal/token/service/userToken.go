package service

import (
	"phoenix/internal/token/dao"
	"phoenix/internal/token/model"
	"phoenix/pkg/werror"
)

type userTokenService struct{}

var UserTokenService = new(userTokenService)

func (*userTokenService) UserTokenList(uid int64, offset int64) (*[]model.UserTokens, error) {

	// 添加到好友数据库
	var tokens *[]model.UserTokens
	tokens, err := dao.UserTokenDao.GetTokenByUid(uid, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	if offset == 0 && len(*tokens) == 0 {
		tokenArr, err := dao.TokenDao.Get2Coin()
		if err != nil {
			return nil, werror.FunWrap(err, 200, "查询失败")
		}
		userTokens := make([]model.UserTokens, 0, len(*tokenArr))
		for _, token := range *tokenArr {
			mapToken := model.UserTokens{
				Weight:            0,
				GrossQuantity:     0,
				AvailableQuantity: 0,
				FrozenQuantity:    0,
			}
			mapToken.TokenId = token.Id
			mapToken.Icon = token.Icon
			mapToken.Symbol = token.Symbol
			mapToken.Name = token.Name
			userTokens = append(userTokens, mapToken)
		}
		return &userTokens, err
	}
	ids := make([]int64, 0, len(*tokens))
	tokenMap := map[int64]model.UserTokens{}
	for _, token := range *tokens {
		ids = append(ids, token.TokenId)
		tokenMap[token.TokenId] = token
	}
	tokenArr, err := dao.TokenDao.ListInArr(&ids)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "获取dapp详情失败")
	}
	for _, token := range *tokenArr {

		if _, ok := tokenMap[token.Id]; ok {
			mapToken := tokenMap[token.Id]
			mapToken.Icon = token.Icon
			mapToken.Symbol = token.Symbol
			mapToken.Name = token.Name

			tokenMap[token.Id] = mapToken
		}
	}
	userTokens := make([]model.UserTokens, 0, len(ids))
	for _, id := range ids {
		userTokens = append(userTokens, tokenMap[id])
	}

	return &userTokens, err
}

func (*userTokenService) UserToken(uid int64, tokenId int64) (*model.UserTokens, error) {

	// 添加到好友数据库
	var token *model.UserTokens
	token, err := dao.UserTokenDao.GetUserTokenByUid(uid, tokenId)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	coinToken, err := dao.TokenDao.GetTokenById(tokenId)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "获取token失败")
	}
	token.Icon = coinToken.Icon
	token.Symbol = coinToken.Symbol
	token.Name = coinToken.Name
	return token, err
}

func (*userTokenService) FlowRecord(uid int64, tokenId int64, id int64, operator int, isPre int, rows int) (*[]model.TokenFlowLogs, error) {

	var records *[]model.TokenFlowLogs
	records, err := dao.FlowRecordDao.GetFlowRecordByUidAndTokenId(uid, tokenId, id, operator, isPre, rows)
	if err != nil {
		return nil, err
	}

	return records, err
}

func (*userTokenService) UserFlow(flowId int64) (*model.TokenFlowLogs, error) {

	record, err := dao.FlowRecordDao.Flow(flowId)
	if err != nil {
		return nil, err
	}

	return record, err
}

func (*userTokenService) FrozenRecord(uid int64, tokenId int64, id int64, isPre int, rows int) (*[]model.TokenFrozen, error) {

	var records *[]model.TokenFrozen
	records, err := dao.TokenFrozenDao.UserFrozenRecord(uid, tokenId, id, isPre, rows)
	if err != nil {
		return nil, err
	}

	return records, err
}

func (*userTokenService) UnFrozenRecord(uid int64, frozenId int64, id int64, rows int) (*[]model.TokenUnFrozen, error) {

	var records *[]model.TokenUnFrozen
	records, err := dao.TokenUnFrozenDao.UserUnFrozenRecord(uid, frozenId, id, rows)
	if err != nil {
		return nil, err
	}

	return records, err
}

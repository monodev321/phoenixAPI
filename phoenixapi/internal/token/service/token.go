package service

import (
	"encoding/json"
	"math"
	"strconv"
	"time"

	AppDao "phoenix/internal/app/dao"
	"phoenix/internal/im/message"
	"phoenix/internal/im/tools"
	"phoenix/internal/token/cache"
	"phoenix/internal/token/constant"
	"phoenix/internal/token/dao"
	"phoenix/internal/token/model"
	UserDao "phoenix/internal/user/dao"
	"phoenix/pkg/db"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/werror"
)

type tokenService struct{}

var TokenService = new(tokenService)

func (*tokenService) GenerateToken(token *model.CoinToken) (int64, error) {

	// 添加到好友数据库
	id, err := dao.TokenDao.Add(token)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*tokenService) ApproveToken(token *model.CoinToken) (int64, error) {

	// 添加到好友数据库
	id, err := dao.TokenDao.Approve(token)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*tokenService) TokenList() (*[]model.ListToken, error) {

	// 添加到好友数据库
	var tokens *[]model.ListToken
	tokens, err := dao.TokenDao.List()
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return tokens, err
}

func (*tokenService) Token(id int64) (*model.ListToken, error) {

	token, err := dao.TokenDao.GetUserTokenById(id)
	if err != nil {
		return nil, err
	}

	return token, err
}

func (*tokenService) Transfer(log1 *model.TokenFlowLogs, log2 *model.TokenFlowLogs, tranPassword string, sysbol string) (int64, error) {
	// 判断金额够吗
	userToken1, err := dao.UserTokenDao.GetUserTokenByUid(log1.Uid, log1.TokenId)
	if err != nil {
		return 0, err
	}
	quantity1 := uint64(0)
	if userToken1 == nil {
		return 0, werror.New(100, "余额不足")
	}
	if userToken1.AvailableQuantity < log1.Quantity {
		return 0, werror.New(100, "余额不足")
	}

	// 判断转账密码是否正确
	user, err := UserDao.UserDao.GetTranPassword(log1.Uid)
	if err != nil {
		return 0, err
	}
	//账户被禁用
	if user.Active != 1 {
		return 0, werror.New(100, "账户被禁用")
	}
	//锁定账户
	isLook, _ := cache.UserCache.IsLocked(log1.Uid)
	defer cache.UserCache.Del(log1.Uid)
	if isLook == true {
		return 0, werror.New(100, "账户被锁定")
	}
	_tranPassword := coinHash.GetSha256ByConf(tranPassword)
	if user.TranPassword == "" || user.TranPassword != _tranPassword {
		return 0, werror.New(100, "转账密码不正确")
	}

	// 判断token是否存在
	_, err = dao.TokenDao.GetTokenById(log1.TokenId)
	if err != nil {
		return 0, werror.New(100, "转账代币非法")
	}

	// 判断转账密码是否正确
	println(log1.FUid)
	_, err = UserDao.UserDao.GetUserInfo(log1.FUid)
	if err != nil {
		return 0, werror.New(100, "转出账户不存在")
	}

	quantity1 = userToken1.AvailableQuantity - log1.Quantity
	gross1 := userToken1.GrossQuantity - log1.Quantity

	// 判断金额够吗
	userToken2, err := dao.UserTokenDao.GetUserTokenByUid(log2.Uid, log2.TokenId)
	if err != nil {
		return 0, err
	}
	isNew2 := false
	quantity2 := uint64(0)
	gross2 := uint64(0)
	if userToken2 == nil {
		isNew2 = true
		quantity2 = log1.Quantity
	} else {
		quantity2 = userToken2.AvailableQuantity + log2.Quantity
		gross2 = userToken2.GrossQuantity + log2.Quantity
	}

	orderId := coinHash.GetOrderSequence("1")
	log1.Balance = gross1
	log1.OrderId = orderId
	log2.Balance = gross2
	log2.OrderId = orderId
	tx := db.DBCli.MustBegin()
	dao.UserTokenDao.UpdateQuantity(tx, quantity1, gross1, userToken1.Id)
	if isNew2 {
		userToken2 = new(model.UserTokens)
		userToken2.Uid = log2.Uid
		userToken2.AvailableQuantity = quantity2
		userToken2.GrossQuantity = quantity2
		userToken2.TokenId = log2.TokenId
		dao.UserTokenDao.Add(tx, userToken2)
	} else {
		dao.UserTokenDao.UpdateQuantity(tx, quantity2, gross2, userToken2.Id)
	}

	dao.FlowRecordDao.Add(tx, log2)
	result := dao.FlowRecordDao.Add(tx, log1)
	err = tx.Commit()
	if err != nil {
		return 0, err
	}
	flowId, _ := (*result).LastInsertId()

	// 发送信息
	flowId2 := flowId - 1
	m := map[string]interface{}{
		"sysbol":   sysbol,
		"quantity": log1.Quantity,
		"tokenId":  log1.TokenId,
		"flowId":   flowId2,
		"charge":   0,
		"uid":      log2.FUid,
		"nickname": log2.FNickname,
		"time":     time.Now().Unix(),
		"type":     1,
	}
	mjson, _ := json.Marshal(m)
	msg := string(mjson)
	msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	message.SendSingleMassage("payaccount", msg, uint64(log1.Uid), uint64(log1.FUid), msgid2, 0)

	return flowId, nil
}
func (*tokenService) Pay(log1 *model.TokenFlowLogs, log2 *model.TokenFlowLogs, tranPassword string, sysbol string) (int64, error) {
	// 转账方
	userToken1, err := dao.UserTokenDao.GetUserTokenByUid(log1.Uid, log1.TokenId)
	if err != nil {
		return 0, err
	}
	quantity1 := uint64(0)
	if userToken1 == nil {
		return 0, werror.New(100, "余额不足")
	}
	if userToken1.AvailableQuantity < log1.Quantity {
		return 0, werror.New(100, "余额不足")
	}

	// 判断转账密码是否正确
	user, err := UserDao.UserDao.GetTranPassword(log1.Uid)
	if err != nil {
		return 0, err
	}
	//账户被禁用
	if user.Active == 2 {
		return 0, werror.New(100, "账户被禁用")
	}
	//锁定账户
	isLook, _ := cache.UserCache.IsLocked(log1.Uid)
	defer cache.UserCache.Del(log1.Uid)
	if isLook == true {
		return 0, werror.New(100, "账户被锁定")
	}
	_tranPassword := coinHash.GetSha256ByConf(tranPassword)
	if user.TranPassword == "" || user.TranPassword != _tranPassword {
		return 0, werror.New(100, "转账密码不正确")
	}

	// 判断token是否存在
	_, err = dao.TokenDao.GetTokenById(log1.TokenId)
	if err != nil {
		return 0, werror.New(100, "转账代币非法")
	}

	// 判断转账密码是否正确
	_, err = UserDao.UserDao.GetUserInfo(log1.FUid)
	if err != nil {
		return 0, werror.New(100, "转出账户不存在")
	}

	quantity1 = userToken1.AvailableQuantity - log1.Quantity
	gross1 := userToken1.GrossQuantity - log1.Quantity

	// 收款方
	userToken2, err := dao.UserTokenDao.GetUserTokenByUid(log2.Uid, log2.TokenId)
	if err != nil {
		return 0, err
	}
	isNew2 := false
	quantity2 := uint64(0)
	gross2 := uint64(0)
	charge := uint64(math.Ceil(float64(log1.Quantity*3) / 1000))
	if userToken2 == nil {
		isNew2 = true
		quantity2 = log1.Quantity - charge
		gross2 = log1.Quantity - charge
	} else {
		quantity2 = userToken2.AvailableQuantity + log2.Quantity - charge
		gross2 = userToken2.GrossQuantity + log2.Quantity - charge
	}

	// 手续费
	uid3 := int64(1)
	chargeAccount, err := AppDao.SystemAccountDao.Account("paycharge")
	if err != nil {
		return 0, err
	}
	chargeUser, err := UserDao.UserDao.GetUserInfo(chargeAccount.Uid)
	if err != nil {
		return 0, err
	}
	uid3 = chargeUser.Uid
	userToken3, err := dao.UserTokenDao.GetUserTokenByUid(uid3, log2.TokenId)
	if err != nil {
		return 0, err
	}
	isNew3 := false
	quantity3 := charge
	gross3 := charge
	if userToken3 == nil {
		isNew3 = true
	} else {
		quantity3 = userToken3.AvailableQuantity + charge
		gross3 = userToken3.GrossQuantity + charge
	}
	log3 := &model.TokenFlowLogs{
		TokenId:   log2.TokenId,
		Uid:       uid3,
		FUid:      log1.FUid,
		FUsername: log1.FUsername,
		FNickname: log1.FNickname,
		FAvatar:   log1.FAvatar,
		Quantity:  charge,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_CHARGE_IN,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_ADD,
		Comment:   "手续费: " + log2.TranId,
		TranId:    log2.TranId,
		ProceId:   "0",
	}
	log4 := &model.TokenFlowLogs{
		TokenId:   log2.TokenId,
		Uid:       log1.FUid,
		FUid:      uid3,
		FUsername: chargeUser.Username,
		FNickname: chargeUser.Nickname,
		FAvatar:   chargeUser.AvatarUrl,
		Quantity:  charge,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_CHARGE_OUT,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_DECREASE,
		Comment:   "手续费: " + log2.TranId,
		TranId:    log2.TranId,
		ProceId:   "0",
	}

	// 开始支付处理
	orderId := coinHash.GetOrderSequence("1")
	log1.Balance = gross1
	log1.OrderId = orderId
	log1.Charge = charge
	log2.Balance = gross2 + charge
	log2.OrderId = orderId
	log2.Charge = charge
	log3.Balance = gross3
	log3.OrderId = orderId
	log4.Balance = gross2
	log4.OrderId = orderId

	tx := db.DBCli.MustBegin()

	dao.UserTokenDao.UpdateQuantity(tx, quantity1, gross1, userToken1.Id)
	if isNew2 {
		userToken2 = new(model.UserTokens)
		userToken2.Uid = log2.Uid
		userToken2.AvailableQuantity = quantity2
		userToken2.GrossQuantity = quantity2
		userToken2.TokenId = log2.TokenId
		dao.UserTokenDao.Add(tx, userToken2)
	} else {
		dao.UserTokenDao.UpdateQuantity(tx, quantity2, gross2, userToken2.Id)
	}

	if isNew3 {
		userToken3 = new(model.UserTokens)
		userToken3.Uid = log3.Uid
		userToken3.AvailableQuantity = quantity3
		userToken3.GrossQuantity = quantity3
		userToken3.TokenId = log3.TokenId
		dao.UserTokenDao.Add(tx, userToken3)
	} else {
		dao.UserTokenDao.UpdateQuantity(tx, quantity3, gross3, userToken3.Id)
	}

	dao.FlowRecordDao.Add(tx, log3)
	dao.FlowRecordDao.Add(tx, log2)
	dao.FlowRecordDao.Add(tx, log4)
	result := dao.FlowRecordDao.Add(tx, log1)
	err = tx.Commit()
	if err != nil {
		return 0, err
	}

	//发送信息
	flowId, _ := (*result).LastInsertId()
	flowId2 := flowId - 2
	m := map[string]interface{}{
		"sysbol":   sysbol,
		"quantity": log1.Quantity,
		"tokenId":  log1.TokenId,
		"flowId":   flowId2,
		"charge":   charge,
		"uid":      log2.FUid,
		"nickname": log2.FNickname,
		"time":     time.Now().Unix(),
		"type":     log2.LogType,
	}
	mjson, _ := json.Marshal(m)
	msg := string(mjson)
	msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	message.SendSingleMassage("payaccount", msg, uint64(log1.Uid), uint64(log1.FUid), msgid2, 0)

	return flowId, nil
}

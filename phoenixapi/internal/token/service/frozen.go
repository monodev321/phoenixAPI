package service

import (
	"database/sql"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"

	AppDao "phoenix/internal/app/dao"
	DappDao "phoenix/internal/dapp/dao"
	"phoenix/internal/token/cache"
	"phoenix/internal/token/constant"
	"phoenix/internal/token/dao"
	"phoenix/internal/token/model"
	UserDao "phoenix/internal/user/dao"
	UserModel "phoenix/internal/user/model"

	"phoenix/pkg/db"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/werror"
)

type frozenService struct{}

var FrozenService = new(frozenService)

func (*frozenService) Frozen(frozen *model.TokenFrozen, tokenSymbol string, tranPassword string) (map[string]interface{}, error) {
	frozen2, _ := dao.TokenFrozenDao.GetFrozenByTranId(frozen.TranId)
	if frozen2 != nil {
		data := map[string]interface{}{
			"frozenId": frozen2.Id,
			"tokenId":  frozen.TokenId,
			"isNew":    0,
		}
		return data, nil
	}
	if frozen.ChargeRate < 10 {
		return nil, werror.New(100, "手续费不能低余千分一")
	}
	// 判断token是否存在
	token, err := dao.TokenDao.GetTokenBySymbol(tokenSymbol)
	if err != nil {
		return nil, werror.New(100, "转账代币非法")
	}
	frozen.TokenId = token.Id

	userToken, err := dao.UserTokenDao.GetUserTokenByUid(frozen.Uid, frozen.TokenId)
	if err != nil {
		return nil, err
	}
	quantity := uint64(0)
	if userToken == nil {
		return nil, werror.New(100, "余额不足")
	}
	if userToken.AvailableQuantity < frozen.Quantity {
		return nil, werror.New(100, "余额不足")
	}

	// 判断转账密码是否正确
	user, err := UserDao.UserDao.GetTranPassword(frozen.Uid)
	if err != nil {
		return nil, err
	}
	if user.Active == 2 {
		return nil, werror.New(100, "账户被禁用")
	}
	//锁定账户
	isLook, _ := cache.UserCache.IsLocked(frozen.Uid)
	defer cache.UserCache.Del(frozen.Uid)
	if isLook == true {
		return nil, werror.New(100, "账户被锁定")
	}
	_tranPassword := coinHash.GetSha256ByConf(tranPassword)
	if user.TranPassword == "" || user.TranPassword != _tranPassword {
		return nil, werror.New(100, "转账密码不正确")
	}

	// 判断转账密码是否正确
	user, err = UserDao.UserDao.GetUserInfo(frozen.FUid)
	if err != nil {
		return nil, werror.New(100, "转出账户不存在")
	}
	frozen.FUsername = user.Username
	frozen.FNickname = user.Nickname

	dapp, err := DappDao.DappDao.GetDevDappById(frozen.DappId)
	if err != nil {
		return nil, werror.New(100, "dapp信息不正确")
	}
	frozen.DappName = dapp.Name
	frozen.DappSymbol = dapp.Symbol

	quantity = userToken.AvailableQuantity - frozen.Quantity
	frozenQuantity := userToken.FrozenQuantity + frozen.Quantity

	unfrozen := &model.TokenUnFrozen{
		TokenId:        frozen.TokenId,
		DappId:         frozen.DappId,
		OrderId:        frozen.TranId,
		OpName:         0,
		Uid:            frozen.Uid,
		FUid:           frozen.FUid,
		FUsername:      frozen.FUsername,
		FNickname:      frozen.FNickname,
		Quantity:       frozen.Quantity,
		FrozenQuantity: frozen.FrozenQuantity,
		Charge:         0,
		ChargeRate:     0,
		Status:         1,
		Comment:        frozen.Comment,
		TranId:         frozen.TranId,
	}

	tx := db.DBCli.MustBegin()
	dao.UserTokenDao.UpdateFrozenQuantity(tx, quantity, frozenQuantity, userToken.Id)
	result := dao.TokenFrozenDao.Add(tx, frozen)
	frozenId, _ := (*result).LastInsertId()
	unfrozen.FrozenId = frozenId
	dao.TokenUnFrozenDao.Add(tx, unfrozen)
	err = tx.Commit()
	if err != nil {
		return nil, err
	}
	data := map[string]interface{}{
		"frozenId": frozenId,
		"tokenId":  frozen.TokenId,
		"isNew":    1,
	}
	return data, nil
}

func (*frozenService) FrozenTran(frozenId int64, fuid int64, tranQuantity uint64, tranPassword string, tranId string, uid int64) error {
	unfrozen2, err := dao.TokenUnFrozenDao.GetFrozenByOrderIdAndType(tranId, 1)
	if unfrozen2 != nil {
		return nil
	}

	frozen, err := dao.TokenFrozenDao.GetFrozenById(frozenId)
	if err != nil {
		return werror.New(100, "冻结订单不存在")
	}
	if frozen.OrderQuantity < tranQuantity || tranQuantity <= 0 {
		return werror.New(100, "交易金额不对")
	}
	dapp, err := DappDao.DappDao.GetDevDappById(frozen.DappId)
	if err != nil {
		return err
	}
	if frozen.Uid != uid && dapp.DeveloperId != uid {
		return werror.New(100, "您没有权限")
	}
	// 判断转账密码是否正确
	user, err := UserDao.UserDao.GetUserInfo(uid)
	if err != nil {
		return err
	}
	if user.Active == 2 {
		return werror.New(100, "账户被禁用")
	}
	//锁定账户
	isLook, _ := cache.UserCache.IsLocked(uid)
	defer cache.UserCache.Del(uid)
	if isLook == true {
		return werror.New(100, "账户被锁定")
	}
	_tranPassword := coinHash.GetSha256ByConf(tranPassword)
	if user.TranPassword == "" || user.TranPassword != _tranPassword {
		return werror.New(100, "转账密码不正确")
	}

	// 获取dapp账户
	dappAccount, err := DappDao.PaymentDao.GetPaymentByDappid(frozen.DappId)
	if err != nil {
		return err
	}
	dappUser, err := UserDao.UserDao.GetUserInfo(dappAccount.Uid)
	if err != nil {
		return err
	}
	// 获取系统账户
	chargeAccount, err := AppDao.SystemAccountDao.Account("frozencharge")
	if err != nil {
		return err
	}
	chargeUser, err := UserDao.UserDao.GetUserInfo(chargeAccount.Uid)
	if err != nil {
		return err
	}
	payUser, err := UserDao.UserDao.GetUserInfo(frozen.Uid)
	if err != nil {
		return err
	}
	payeeUser, err := UserDao.UserDao.GetUserInfo(fuid)
	if err != nil {
		return err
	}
	payTranQua1, payTranQua2, payeeTranQua1, payeeTranQua2, _, dappTranQua2, systemTranQua, charge := calculateCharge(tranQuantity, uint64(frozen.ChargeRate), frozen.ChargeUser, frozen.OrderQuantity, frozen.Charge)

	unfrozen := &model.TokenUnFrozen{}
	if payTranQua2 == uint64(0) {
		unfrozen = getFrozenTokenLog(frozen, payeeUser, tranId, payTranQua1, charge)
	} else {
		unfrozen = getFrozenTokenLog(frozen, payeeUser, tranId, payTranQua2, charge)
	}
	payQuaG := uint64(0)
	payQuaF := uint64(0)
	payeeQuaG := uint64(0)
	payeeQuaA := uint64(0)
	payUserTokenId := int64(0)
	payeeUserTokenId := int64(0)
	if payTranQua2 == uint64(0) {
		payQuaG, payQuaF, payUserTokenId, err = changeUserFrozenToken(frozen.Uid, frozen.TokenId, payTranQua1)
		if err != nil {
			return err
		}
	} else {
		payQuaG, payQuaF, payUserTokenId, err = changeUserFrozenToken(frozen.Uid, frozen.TokenId, payTranQua2)
		if err != nil {
			return err
		}
	}

	if payeeTranQua2 == uint64(0) {
		payeeQuaG, payeeQuaA, payeeUserTokenId, err = addUserToken(fuid, frozen.TokenId, payeeTranQua1)
		if err != nil {
			return err
		}
	} else {
		payeeQuaG, payeeQuaA, payeeUserTokenId, err = addUserToken(fuid, frozen.TokenId, payeeTranQua2)
		if err != nil {
			return err
		}
	}
	dappQuaG, dappQuaA, dappUserTokenId, err := addUserToken(dappUser.Uid, frozen.TokenId, dappTranQua2)
	if err != nil {
		return err
	}
	chargeQuaG, chargeQuaA, chargeUserTokenId, err := addUserToken(chargeUser.Uid, frozen.TokenId, systemTranQua)
	if err != nil {
		return err
	}

	tx := db.DBCli.MustBegin()
	// 改冻结表金额 ，如果订单金额等于冻结金额，关闭订单 （减少）
	if frozen.OrderQuantity == tranQuantity {
		dao.TokenFrozenDao.FinishFrozenQuantity(tx, frozenId)
	} else {
		frozenOrderQuan := frozen.OrderQuantity - tranQuantity
		frozenChargeQuan := frozen.Charge - charge
		dao.TokenFrozenDao.UpdateFrozenQuantity(tx, frozenId, unfrozen.Quantity, frozenOrderQuan, frozenChargeQuan)
	}
	// 冻结流水表
	dao.TokenUnFrozenDao.Add(tx, unfrozen)
	// 减少冻结方的冻结金额，冻结方流水记录，
	dao.UserTokenDao.TranFrozenQuantity(tx, payQuaG, payQuaF, payUserTokenId)
	if payTranQua2 != uint64(0) {
		payLog1 := getTranLogOut(frozen, frozen.Uid, payeeUser, payTranQua1, payQuaG+charge, dapp.Name+"交易", charge, tranId)
		dao.FlowRecordDao.Add(tx, payLog1)
		payLog2 := getTranLogOut(frozen, frozen.Uid, dappUser, charge, payQuaG, dapp.Name+"交易手续费", uint64(0), tranId)
		dao.FlowRecordDao.Add(tx, payLog2)
	} else {
		payLog1 := getTranLogOut(frozen, frozen.Uid, payeeUser, payTranQua1, payQuaG, dapp.Name+"交易", uint64(0), tranId)
		dao.FlowRecordDao.Add(tx, payLog1)
	}

	addUserTokenDb(tx, payeeQuaG, payeeQuaA, payeeUserTokenId, frozen.TokenId, fuid)
	if payeeTranQua2 != uint64(0) {
		payeeLog1 := getTranLogIn(frozen, fuid, payUser, payeeTranQua1, payeeQuaG+charge, dapp.Name+"交易", charge, tranId)
		dao.FlowRecordDao.Add(tx, payeeLog1)
		payeeLog2 := getTranLogOut(frozen, fuid, dappUser, charge, payeeQuaG, dapp.Name+"交易手续费", uint64(0), tranId)
		dao.FlowRecordDao.Add(tx, payeeLog2)

	} else {
		payeeLog1 := getTranLogIn(frozen, fuid, payUser, payeeTranQua1, payeeQuaG, dapp.Name+"交易", uint64(0), tranId)
		dao.FlowRecordDao.Add(tx, payeeLog1)
	}

	addUserTokenDb(tx, dappQuaG, dappQuaA, dappUserTokenId, frozen.TokenId, dappUser.Uid)
	payeeBalance := dappQuaG + systemTranQua
	if payeeTranQua2 != uint64(0) {
		payeeBalance = payeeBalance + charge
	}
	if payTranQua2 != uint64(0) {
		adppLog2 := getTranLogIn(frozen, dappUser.Uid, payUser, charge, payeeBalance, dapp.Name+"交易手续费", uint64(0), tranId)
		dao.FlowRecordDao.Add(tx, adppLog2)
	}
	if payeeTranQua2 != uint64(0) {
		adppLog2 := getTranLogIn(frozen, dappUser.Uid, payeeUser, charge, dappQuaG+systemTranQua, dapp.Name+"交易手续费", uint64(0), tranId)
		dao.FlowRecordDao.Add(tx, adppLog2)
	}

	adppLog3 := getTranLogOut(frozen, dappUser.Uid, chargeUser, systemTranQua, dappQuaG, dapp.Name+"交易手续费", uint64(0), tranId)
	dao.FlowRecordDao.Add(tx, adppLog3)

	addUserTokenDb(tx, chargeQuaG, chargeQuaA, chargeUserTokenId, frozen.TokenId, chargeUser.Uid)
	chargeLog2 := getTranLogIn(frozen, chargeUser.Uid, dappUser, systemTranQua, chargeQuaG, dapp.Name+"交易手续费", uint64(0), tranId)
	dao.FlowRecordDao.Add(tx, chargeLog2)
	err = tx.Commit()
	if err != nil {
		return err
	}
	//todo 通知双方金额变化
	return nil
}

func (*frozenService) FrozenBack(frozenId int64, frozenQuantity uint64, tranPassword string, uid int64) error {
	frozen, err := dao.TokenFrozenDao.GetFrozenById(frozenId)
	if err != nil {
		return werror.New(100, "冻结订单不存在")
	}
	unfrozen2, err := dao.TokenUnFrozenDao.GetFrozenByOrderIdAndType(frozen.TranId, 2)
	if unfrozen2 != nil {
		return nil
	}
	if frozen.FrozenQuantity != frozenQuantity {
		return werror.New(100, "解冻金额不对")
	}
	dapp, err := DappDao.DappDao.GetDevDappById(frozen.DappId)
	if err != nil {
		return err
	}
	if frozen.FUid != uid && dapp.DeveloperId != uid {
		now := time.Now().Unix()
		if (frozen.CreateTime+frozen.CancelInterval) > now || frozen.Uid != uid {
			return werror.New(100, "您没有权限")
		}

	}
	userToken, err := dao.UserTokenDao.GetUserTokenByUid(frozen.Uid, frozen.TokenId)
	if err != nil {
		return err
	}
	quantity := uint64(0)

	// 判断转账密码是否正确
	user, err := UserDao.UserDao.GetTranPassword(uid)
	if err != nil {
		return err
	}
	//账户被禁用
	if user.Active == 2 {
		return werror.New(100, "账户被禁用")
	}
	//锁定账户
	isLook, _ := cache.UserCache.IsLocked(uid)
	defer cache.UserCache.Del(uid)
	if isLook == true {
		return werror.New(100, "账户被锁定")
	}
	_tranPassword := coinHash.GetSha256ByConf(tranPassword)
	if user.TranPassword == "" || user.TranPassword != _tranPassword {
		return werror.New(100, "转账密码不正确")
	}

	quantity = userToken.AvailableQuantity + frozen.FrozenQuantity
	fQuantity := userToken.FrozenQuantity - frozen.FrozenQuantity

	unfrozen := &model.TokenUnFrozen{
		TokenId:        frozen.TokenId,
		FrozenId:       frozen.Id,
		DappId:         frozen.DappId,
		OrderId:        frozen.TranId,
		OpName:         2,
		Uid:            frozen.Uid,
		FUid:           frozen.FUid,
		FUsername:      frozen.FUsername,
		FNickname:      frozen.FNickname,
		Quantity:       0,
		FrozenQuantity: frozen.FrozenQuantity,
		Charge:         0,
		ChargeRate:     0,
		Status:         1,
		Comment:        "解冻",
		TranId:         frozen.TranId,
	}

	tx := db.DBCli.MustBegin()

	dao.UserTokenDao.UpdateFrozenQuantity(tx, quantity, fQuantity, userToken.Id)
	dao.TokenFrozenDao.FinishFrozenQuantity(tx, frozenId)
	dao.TokenUnFrozenDao.Add(tx, unfrozen)
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

func (*frozenService) UserFrozen(id int64) (*model.TokenFrozen, error) {
	frozen, err := dao.TokenFrozenDao.GetFrozenById(id)
	return frozen, err
}

func getFrozenTokenLog(frozen *model.TokenFrozen, user *UserModel.User, tranId string, quantity uint64, charge uint64) *model.TokenUnFrozen {
	return &model.TokenUnFrozen{
		TokenId:        frozen.TokenId,
		DappId:         frozen.DappId,
		FrozenId:       frozen.Id,
		OrderId:        tranId,
		OpName:         1,
		Uid:            frozen.Uid,
		FUid:           user.Uid,
		FUsername:      user.Username,
		FNickname:      user.Nickname,
		Quantity:       frozen.Quantity - quantity,
		FrozenQuantity: quantity,
		Charge:         charge,
		ChargeRate:     frozen.ChargeRate,
		Status:         1,
		Comment:        "交易",
		TranId:         frozen.TranId,
	}
}

func changeUserFrozenToken(uid int64, tokenId int64, quantity uint64) (uint64, uint64, int64, error) {
	userToken, err := dao.UserTokenDao.GetUserTokenByUid(uid, tokenId)
	if err != nil {
		return 0, 0, 0, err
	}
	gQuantity := userToken.GrossQuantity - quantity
	fQuantity := userToken.FrozenQuantity - quantity
	return gQuantity, fQuantity, userToken.Id, nil
}

func addUserToken(uid int64, tokenId int64, quantity uint64) (uint64, uint64, int64, error) {
	userToken, err := dao.UserTokenDao.GetUserTokenByUid(uid, tokenId)
	if err != nil {
		return 0, 0, 0, err
	}
	if userToken == nil {
		return quantity, quantity, 0, nil
	}
	gQuantity := userToken.GrossQuantity + quantity
	aQuantity := userToken.AvailableQuantity + quantity
	return gQuantity, aQuantity, userToken.Id, nil
}

func addUserTokenDb(tx *sqlx.Tx, QuaG uint64, QuaA uint64, UserTokenId int64, tokenId int64, uid int64) *sql.Result {
	if UserTokenId == 0 {
		userToken := &model.UserTokens{
			Uid:               uid,
			TokenId:           tokenId,
			AvailableQuantity: QuaA,
			GrossQuantity:     QuaG,
		}
		return dao.UserTokenDao.Add(tx, userToken)
	} else {
		return dao.UserTokenDao.UpdateQuantity(tx, QuaA, QuaG, UserTokenId)

	}
}

func reduceUserToken(uid int64, tokenId int64, quantity uint64) (uint64, uint64, int64, error) {
	userToken, err := dao.UserTokenDao.GetUserTokenByUid(uid, tokenId)
	if err != nil {
		return 0, 0, 0, err
	}
	gQuantity := userToken.GrossQuantity - quantity
	aQuantity := userToken.AvailableQuantity - quantity
	return gQuantity, aQuantity, userToken.Id, nil
}

func getTranLogOut(frozen *model.TokenFrozen, uid int64, user *UserModel.User, quantity uint64, balance uint64, comment string, charge uint64, tranId string) *model.TokenFlowLogs {
	return &model.TokenFlowLogs{
		TokenId:   frozen.TokenId,
		Uid:       uid,
		FUid:      user.Uid,
		FUsername: user.Username,
		FNickname: user.Nickname,
		FAvatar:   user.AvatarUrl,
		Quantity:  quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_OUT,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_DECREASE,
		Comment:   comment,
		TranId:    frozen.TranId,
		ProceId:   "frozen" + strconv.FormatInt(frozen.Id, 10),
		Charge:    charge,
		Balance:   balance,
		OrderId:   tranId,
	}
}

func getTranLogIn(frozen *model.TokenFrozen, uid int64, user *UserModel.User, quantity uint64, balance uint64, comment string, charge uint64, tranId string) *model.TokenFlowLogs {
	return &model.TokenFlowLogs{
		TokenId:   frozen.TokenId,
		Uid:       uid,
		FUid:      user.Uid,
		FUsername: user.Username,
		FNickname: user.Nickname,
		FAvatar:   user.AvatarUrl,
		Quantity:  quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_IN,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_ADD,
		Comment:   comment,
		TranId:    frozen.TranId,
		ProceId:   "frozen" + strconv.FormatInt(frozen.Id, 10),
		Charge:    charge,
		Balance:   balance,
		OrderId:   tranId,
	}
}

func calculateCharge(quantity uint64, ChargeRate uint64, chargeUser int, orderQuantity uint64, frozenCharge uint64) (uint64, uint64, uint64, uint64, uint64, uint64, uint64, uint64) {
	charge := uint64(0)
	if quantity == orderQuantity && frozenCharge != 0 {
		charge = frozenCharge
	} else {
		charge = uint64(ChargeRate) * quantity / 10000
	}

	chargeSystem := uint64(10) * quantity / 10000
	switch chargeUser {
	case 0:
		payTranQua1 := quantity
		payTranQua2 := uint64(0)
		payeeTranQua1 := quantity
		payeeTranQua2 := quantity - charge
		dappTranQua1 := charge
		dappTranQua2 := charge - chargeSystem
		systemTranQua := chargeSystem
		return payTranQua1, payTranQua2, payeeTranQua1, payeeTranQua2, dappTranQua1, dappTranQua2, systemTranQua, charge
	case 1:
		payTranQua1 := quantity
		payTranQua2 := quantity + charge
		payeeTranQua1 := quantity
		payeeTranQua2 := uint64(0)
		dappTranQua1 := charge
		dappTranQua2 := charge - chargeSystem
		systemTranQua := chargeSystem
		return payTranQua1, payTranQua2, payeeTranQua1, payeeTranQua2, dappTranQua1, dappTranQua2, systemTranQua, charge
	case 2:
		payTranQua1 := quantity
		payTranQua2 := quantity + charge
		payeeTranQua1 := quantity
		payeeTranQua2 := quantity - charge
		dappTranQua1 := charge + charge
		dappTranQua2 := charge + charge - chargeSystem - chargeSystem
		systemTranQua := chargeSystem + chargeSystem
		return payTranQua1, payTranQua2, payeeTranQua1, payeeTranQua2, dappTranQua1, dappTranQua2, systemTranQua, charge
	}
	return 0, 0, 0, 0, 0, 0, 0, 0
}

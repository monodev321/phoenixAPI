package model

type CoinToken struct {
	Id              int64  `json:"id"`                                     // 代币id
	Name            string `json:"name"  db:"name"`                        // 代币名
	Symbol          string `json:"symbol"  db:"symbol"`                    // 代币英文符号
	Icon            string `json:"icon" db:"icon"`                         // 代币图标url
	Comment         string `json:"comment" db:"comment"`                   // 代币说明
	ApplyComment    string `json:"applyComment" db:"apply_comment"`        // 审核说明
	QuantityLimit   uint64 `json:"quantityLimit"  db:"quantity_limit"`     // 最大数量
	IssueNum        int    `json:"issueNum"  db:"issue_num"`               // 最大数量
	Weight          int    `json:"weight"  db:"weight"`                    // 最大数量
	IssueQuantity   uint64 `json:"issueQuantity"  db:"issue_quantity"`     // 最大数量
	Website         string `json:"website"  db:"website"`                  // 最大数量
	OwnerUid        int64  `json:"owner_uid"  db:"owner_uid"`              // 申请人uid，只有这个id可以issue
	IssueUid        int64  `json:"issue_uid"  db:"issue_uid"`              // 代币发行到哪个账户
	IssueUsername   string `json:"issueUsername"  db:"issue_username"`     // 代币发行到哪个账户
	ApproveUid      int64  `json:"approve_uid"  db:"approve_uid"`          // 审核人id
	ApproveUsername string `json:"approveUsername"  db:"approve_username"` // 代币发行到哪个账户
	Status          int    `json:"status"  db:"status"`                    // 代币状态，0表示申请 1表示正常 2表示停止
	CreateTime      int64  `json:"create_time"  db:"create_time"`          // 创建时间
	ApproveTime     int64  `json:"approve_time"  db:"approve_time"`        // 审核时间
}

type ListToken struct {
	Id            int64  `json:"id"`                                 // 代币id
	Name          string `json:"name"  db:"name"`                    // 代币名
	Symbol        string `json:"symbol"  db:"symbol"`                // 代币英文符号
	Icon          string `json:"icon" db:"icon"`                     // 代币图标url
	QuantityLimit uint64 `json:"quantityLimit"  db:"quantity_limit"` // 最大数量
	IssueNum      int    `json:"issueNum"  db:"issue_num"`           // 最大数量
	Weight        int    `json:"weight"  db:"weight"`                // 最大数量
	IssueQuantity uint64 `json:"issueQuantity"  db:"issue_quantity"` // 最大数量
	Website       string `json:"website"  db:"website"`              // 最大数量
	Status        int    `json:"status"  db:"status"`                // 代币状态，0表示申请 1表示正常 2表示停止
	CreateTime    int64  `json:"createTime"  db:"create_time"`       // 创建时间l
}

type TokenIssueLogs struct {
	Id          int64  `json:"id"`                              // id
	TokenId     int64  `json:"token_id"  db:"token_id"`         // 代币id
	TokenName   string `json:"token_name"  db:"token_name"`     // 代币名
	TokenSymbol string `json:"token_symbol" db:"token_symbol"`  // 代币英文符号
	TokenIcon   string `json:"token_icon" db:"token_icon"`      // 代币图标url
	Quantity    uint64 `json:"quantity" db:"quantity"`          // 发行数量
	Comment     string `json:"comment" db:"comment"`            // 发行说明
	ApproveUid  int64  `json:"approve_uid"  db:"approve_uid"`   // 审核人id
	Status      int    `json:"status"  db:"status"`             // 是否允许发行
	CreateTime  int64  `json:"create_time"  db:"create_time"`   // 创建时间
	ApproveTime int64  `json:"approve_time"  db:"approve_time"` // 审核时间
}

type TokenIssueFlow struct {
	Id         int64  `json:"id"`                            // id
	TokenId    int64  `json:"token_id"  db:"token_id"`       // 代币id
	Quantity   uint64 `json:"quantity" db:"quantity"`        // 发行数量
	Comment    string `json:"comment" db:"comment"`          // 发行说明
	Status     int    `json:"status"  db:"status"`           // 是否允许发行
	CreateTime int64  `json:"create_time"  db:"create_time"` // 创建时间
}

type TokenIssueCount struct {
	Total uint64 `json:"total" db:"total"` // 发行数量
}

type TokenFlowLogs struct {
	Id         int64  `json:"id"`                           // id
	OrderId    string `json:"orderId" db:"order_id"`        // 帐单id，方便转账双方和手续费的统计
	TokenId    int64  `json:"tokenId"  db:"token_id"`       // 代币id
	Uid        int64  `json:"uid" db:"uid"`                 // 用户
	FUid       int64  `json:"fuid" db:"f_uid"`              // 流水相关用户
	FUsername  string `json:"fusername" db:"f_username"`    // 流水相关用户
	FNickname  string `json:"fnickname" db:"f_nickname"`    // 流水相关用户
	FAvatar    string `json:"favatar" db:"f_avatar"`        // 流水相关用户
	Quantity   uint64 `json:"quantity" db:"quantity"`       // 数量
	Balance    uint64 `json:"balance" db:"balance"`         // 数量
	Charge     uint64 `json:"charge" db:"charge"`           // 数量
	LogType    int    `json:"logType" db:"log_type"`        // 流水类型
	Operator   int    `json:"operator" db:"operator"`       // 1增加或者2减少
	Comment    string `json:"comment" db:"comment"`         // 流水说明
	TranId     string `json:"tranId" db:"tran_id"`          // 流水说明，第三方的交易id
	ProceId    string `json:"proceId" db:"proce_id"`        // 关联事务id，比如红包，冻结，发行
	CreateTime int64  `json:"createTime"  db:"create_time"` // 创建时间
}

type UserTokens struct {
	Id                int64  `json:"id"`                                        // id
	TokenId           int64  `json:"tokenId"  db:"token_id"`                    // 代币id
	Name              string `json:"name"  db:"name"`                           // 代币名
	Symbol            string `json:"symbol"  db:"symbol"`                       // 代币英文符号
	Icon              string `json:"icon" db:"icon"`                            // 代币图标url
	Uid               int64  `json:"uid" db:"uid"`                              // 用户
	GrossQuantity     uint64 `json:"grossQuantity" db:"gross_quantity"`         // 数量
	AvailableQuantity uint64 `json:"availableQuantity" db:"available_quantity"` // 数量
	FrozenQuantity    uint64 `json:"frozenQuantity" db:"frozen_quantity"`       // 抵押中的数量
	Weight            int    `json:"weight" db:"weight"`                        // 抵押中的数量
}

type TokenFrozen struct {
	Id             int64  `json:"id"`
	TokenId        int64  `json:"tokenId" db:"token_id" `
	Uid            int64  `json:"uid"`
	FUid           int64  `json:"fuid" db:"f_uid"`
	FUsername      string `json:"fusername" db:"f_username"`
	FNickname      string `json:"fnickname" db:"f_nickname"`
	DappId         int64  `json:"dappId" db:"dapp_id"`
	DappSymbol     string `json:"dappSymbol" db:"dapp_symbol"`
	DappName       string `json:"dappName" db:"dapp_name"`
	Quantity       uint64 `json:"quantity"`
	FrozenQuantity uint64 `json:"frozenQuantity" db:"frozen_quantity"`
	OrderQuantity  uint64 `json:"orderQuantity" db:"order_quantity"`
	Charge         uint64 `json:"charge"`
	ChargeRate     int    `json:"chargeRate" db:"charge_rate"`
	ChargeUser     int    `json:"chargeUser" db:"charge_user"`
	IsAuto         int    `json:"isAuto" db:"is_auto"`
	Status         int    `json:"status"`
	TranId         string `json:"tranId" db:"tran_id"`
	Comment        string `json:"comment"`
	CancelInterval int64  `json:"cancelInterval" db:"cancel_interval"`
	CreateTime     int64  `json:"createTime" db:"create_time"`
	FinishTime     int64  `json:"finishTime" db:"finish_time"`
}

type TokenUnFrozen struct {
	Id             int64  `json:"id"`
	TokenId        int64  `json:"tokenId" db:"token_id"`
	DappId         int64  `json:"dappId" db:"dapp_id"`
	FrozenId       int64  `json:"frozenId" db:"frozen_id"`
	OrderId        string `json:"orderId" db:"order_id"`
	Uid            int64  `json:"uid"`
	FUid           int64  `json:"fuid" db:"f_uid"`
	FUsername      string `json:"fusername" db:"f_username"`
	FNickname      string `json:"fnickname" db:"f_nickname"`
	Quantity       uint64 `json:"quantity"`
	OpName         int64  `json:"opName" db:"op_name"`
	FrozenQuantity uint64 `json:"frozenQuantity" db:"frozen_quantity"`
	Charge         uint64 `json:"charge"`
	ChargeRate     int    `json:"chargeRate" db:"charge_rate"`
	Status         int    `json:"status"`
	Comment        string `json:"comment"`
	TranId         string `json:"tranId" db:"tran_id"`
	CreateTime     int64  `json:"createTime" db:"create_time"`
	FinishTime     int64  `json:"finishTime" db:"finish_time"`
}

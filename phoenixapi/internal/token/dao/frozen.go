package dao

import (
	"database/sql"
	"time"

	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"

	"phoenix/internal/token/model"

	"phoenix/pkg/db"
)

type tokenFrozenDao struct{}

var TokenFrozenDao = new(tokenFrozenDao)

func (*tokenFrozenDao) Add(tx *sqlx.Tx, frozen *model.TokenFrozen) *sql.Result {
	createTime := time.Now().Unix()
	result := tx.MustExec("INSERT IGNORE INTO `token_frozen` (`token_id`, `uid`, `f_uid`, `f_username`, `f_nickname`, `dapp_id`, `dapp_symbol`, `dapp_name`, `quantity`, `order_quantity`, `frozen_quantity`, `charge`, `charge_rate`,`charge_user`, `status`,  `tran_id`, `comment`,`cancel_interval`, `create_time`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
		frozen.TokenId, frozen.Uid, frozen.FUid, frozen.FUsername, frozen.FNickname, frozen.DappId, frozen.DappSymbol, frozen.DappName, frozen.Quantity, frozen.OrderQuantity, frozen.FrozenQuantity, frozen.Charge, frozen.ChargeRate, frozen.ChargeUser, 0, frozen.TranId, frozen.Comment, frozen.CancelInterval, createTime)
	return &result
}

func (*tokenFrozenDao) FinishFrozenQuantity(tx *sqlx.Tx, id int64) *sql.Result {
	now := time.Now().Unix()
	result := tx.MustExec("UPDATE token_frozen SET  frozen_quantity = 0,order_quantity = 0,charge = 0,status = 1,finish_time=? WHERE id= ? ",
		now, id)
	return &result
}

func (*tokenFrozenDao) UpdateFrozenQuantity(tx *sqlx.Tx, id int64, quantity uint64, orderQuantity uint64, charge uint64) *sql.Result {
	result := tx.MustExec("UPDATE token_frozen SET  frozen_quantity = ?,order_quantity = ?,charge = ? WHERE id= ? ",
		quantity, orderQuantity, charge, id)
	return &result
}

func (*tokenFrozenDao) GetFrozenById(id int64) (*model.TokenFrozen, error) {
	var frozen model.TokenFrozen
	err := db.DBCli.Get(&frozen, "select id,token_id,uid,f_uid,f_username,f_nickname,dapp_id,dapp_symbol,dapp_name,quantity,order_quantity,frozen_quantity,charge,charge_rate,charge_user,status,is_auto,tran_id,comment,create_time,finish_time,cancel_interval from token_frozen where id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &frozen, nil
}
func (*tokenFrozenDao) GetFrozenByTranId(tranId string) (*model.TokenFrozen, error) {
	var frozen model.TokenFrozen
	err := db.DBCli.Get(&frozen, "select id,token_id,uid,f_uid,f_username,f_nickname,dapp_id,dapp_symbol,dapp_name,quantity,order_quantity,frozen_quantity,charge,charge_rate,charge_user,status,is_auto,tran_id,comment,create_time,finish_time from token_frozen where tran_id = ?",
		tranId)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &frozen, nil
}

func (*tokenFrozenDao) UserFrozenRecord(uid int64, tokenId int64, id int64, isPre int, rows int) (*[]model.TokenFrozen, error) {

	var flowLogs []model.TokenFrozen
	var err error
	if isPre == 1 {
		if id == 0 {
			err = db.DBCli.Select(&flowLogs, "select id,token_id,uid,dapp_id,dapp_symbol,dapp_name,	quantity,	frozen_quantity	,is_auto,status	,tran_id,comment,create_time,finish_time from token_frozen where uid = ? and token_id = ? and id > ? ORDER BY id desc LIMIT 0,?",
				uid, tokenId, id, rows)
		} else {
			err = db.DBCli.Select(&flowLogs, "select id,token_id,uid,dapp_id,dapp_symbol,dapp_name,	quantity,	frozen_quantity	,status,is_auto	,tran_id,comment,create_time,finish_time from token_frozen where uid = ? and token_id = ? and id > ? ORDER BY id  LIMIT 0,?",
				uid, tokenId, id, rows)
		}

	} else {

		err = db.DBCli.Select(&flowLogs, "select id,token_id,uid,dapp_id,dapp_symbol,dapp_name,	quantity,	frozen_quantity	,status	,is_auto,tran_id,comment,create_time,finish_time from token_frozen where uid = ? and token_id = ? and id < ? ORDER BY id desc LIMIT 0,?",
			uid, tokenId, id, rows)

	}

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &flowLogs, nil
}

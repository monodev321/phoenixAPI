package dao

import (
	"database/sql"
	"fmt"

	"phoenix/pkg/werror"

	"phoenix/internal/token/model"

	"phoenix/pkg/db"

	"github.com/jmoiron/sqlx"
)

type userTokenDao struct{}

var UserTokenDao = new(userTokenDao)

// 通过好友请求
func (*userTokenDao) Add(tx *sqlx.Tx, userToken *model.UserTokens) *sql.Result {
	result := tx.MustExec("INSERT  INTO `user_tokens` (`uid`, `token_id`, `available_quantity`,`gross_quantity`) values(?,?,?,?)",
		userToken.Uid, userToken.TokenId, userToken.AvailableQuantity, userToken.GrossQuantity)
	return &result
}

// 审核代币
func (*userTokenDao) UpdateQuantity(tx *sqlx.Tx, quantity uint64, gross uint64, id int64) *sql.Result {
	result := tx.MustExec("UPDATE user_tokens SET  available_quantity = ?,gross_quantity = ? WHERE id= ? ",
		quantity, gross, id)
	return &result
}

func (*userTokenDao) UpdateFrozenQuantity(tx *sqlx.Tx, quantity uint64, frozen uint64, id int64) *sql.Result {
	result := tx.MustExec("UPDATE user_tokens SET  available_quantity = ?,frozen_quantity = ? WHERE id= ? ",
		quantity, frozen, id)
	return &result
}

func (*userTokenDao) TranFrozenQuantity(tx *sqlx.Tx, gross uint64, frozen uint64, id int64) *sql.Result {
	result := tx.MustExec("UPDATE user_tokens SET gross_quantity = ?,frozen_quantity = ? WHERE id= ? ",
		gross, frozen, id)
	return &result
}

// 获取用户的好友请求列表
func (*userTokenDao) List(uid int64) (*[]model.UserTokens, error) {

	var userTokens []model.UserTokens
	err := db.DBCli.Select(&userTokens, "select id,token_id,gross_quantity,available_quantity,frozen_quantity,weight from user_tokens where uid = ?  order by weight desc",
		uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &userTokens, nil
}

// 获取用户的好友请求列表
func (*userTokenDao) GetUserTokenByUid(uid int64, tokenId int64) (*model.UserTokens, error) {

	var usertoken model.UserTokens
	err := db.DBCli.Get(&usertoken, "select id,token_id,gross_quantity,available_quantity,frozen_quantity,weight from user_tokens where uid = ? and token_id = ? ",
		uid, tokenId)
	if err != nil {
		if fmt.Sprintf("%T", err) == "*errors.errorString" {
			return nil, nil
		} else {
			return nil, werror.FunWrap(err, 200, "查询失败")
		}

	}
	return &usertoken, nil
}

// 获取用户的好友请求列表
func (*userTokenDao) GetTokenByUid(uid int64, offset int64) (*[]model.UserTokens, error) {

	var tokens []model.UserTokens
	err := db.DBCli.Select(&tokens, "select id,token_id,gross_quantity,available_quantity,frozen_quantity,weight from user_tokens where uid = ?  order by weight desc limit ?,100", uid, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &tokens, nil
}

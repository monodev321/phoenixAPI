package dao

import (
	"database/sql"
	"time"

	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"

	"phoenix/internal/token/model"

	"phoenix/pkg/db"
)

type tokenUnFrozenDao struct{}

var TokenUnFrozenDao = new(tokenUnFrozenDao)

func (*tokenUnFrozenDao) Add(tx *sqlx.Tx, unfrozen *model.TokenUnFrozen) *sql.Result {
	createTime := time.Now().Unix()
	result := tx.MustExec("INSERT IGNORE INTO `token_unfrozen` (`token_id`, `dapp_id`, `frozen_id`, `order_id`, `op_name`, `uid`, `f_uid`, `f_username`, `f_nickname`, `quantity`, `frozen_quantity`, `charge`, `charge_rate`, `status`, `comment`, `tran_id`, `create_time`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
		unfrozen.TokenId, unfrozen.DappId, unfrozen.FrozenId, unfrozen.OrderId, unfrozen.OpName, unfrozen.Uid, unfrozen.FUid, unfrozen.FUsername, unfrozen.FNickname, unfrozen.Quantity, unfrozen.FrozenQuantity, unfrozen.Charge, unfrozen.ChargeRate, unfrozen.Status, unfrozen.Comment, unfrozen.TranId, createTime)
	return &result
}

func (*tokenUnFrozenDao) UserUnFrozenRecord(uid int64, frozenId int64, id int64, rows int) (*[]model.TokenUnFrozen, error) {

	var flowLogs []model.TokenUnFrozen
	var err error

	err = db.DBCli.Select(&flowLogs, "select id,token_id,dapp_id,frozen_id,order_id,op_name,f_uid,f_username,f_nickname,quantity,frozen_quantity,charge,charge_rate,status,comment,tran_id,create_time,finish_time from token_unfrozen where frozen_id = ? and uid = ? and id > ? ORDER BY id  LIMIT 0,?", frozenId, uid, id, rows)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &flowLogs, nil
}

func (*tokenUnFrozenDao) GetFrozenByOrderIdAndType(orderId string, opName int) (*model.TokenUnFrozen, error) {
	var frozen model.TokenUnFrozen
	err := db.DBCli.Get(&frozen, "select id,order_id from token_unfrozen where order_id = ? and op_name = ?",
		orderId, opName)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &frozen, nil
}

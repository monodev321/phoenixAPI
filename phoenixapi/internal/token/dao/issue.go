package dao

import (
	"database/sql"
	"time"

	"phoenix/pkg/werror"

	"phoenix/internal/token/model"

	"phoenix/pkg/db"

	"github.com/jmoiron/sqlx"
)

type issueDao struct{}

var IssueDao = new(issueDao)

// 添加代币
func (*issueDao) Add(issue *model.TokenIssueLogs) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT INTO `token_issue_logs` ( `token_id`, `token_name`, `token_symbol`, `token_icon`, `quantity`, `comment`, `create_time`) values(?,?,?,?,?,?,?)",
		issue.TokenId, issue.TokenName, issue.TokenSymbol, issue.TokenIcon, issue.Quantity, issue.Comment, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "代币已经存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加代币失败")
	}

	return id, nil
}

// 审核发行代币
func (*issueDao) Approve(tx *sqlx.Tx, issue *model.TokenIssueLogs) *sql.Result {
	approveTime := time.Now().Unix()
	result := tx.MustExec("UPDATE token_issue_logs SET  approve_uid = ?, status =?, approve_time = ?  WHERE id= ? ",
		issue.ApproveUid, issue.Status, approveTime, issue.Id)
	return &result
}

// 获取用户的好友请求列表
func (*issueDao) GetIssueLogById(id int64) (*model.TokenIssueLogs, error) {

	var issueLog model.TokenIssueLogs
	err := db.DBCli.Get(&issueLog, "select token_id,quantity,status from token_issue_logs where id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &issueLog, nil
}

func (*issueDao) GetIssueLogByTokenId(tokenId int64) (*[]model.TokenIssueLogs, error) {

	var issues []model.TokenIssueLogs
	err := db.DBCli.Select(&issues, "select id,token_id,token_name,token_symbol,token_icon,quantity,comment,status,create_time,approve_time from token_issue_logs where token_id = ? order by create_time desc", tokenId)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &issues, nil

}

func (*issueDao) IssueFlow(tokenId int64) (*[]model.TokenIssueFlow, error) {

	var issues []model.TokenIssueFlow
	err := db.DBCli.Select(&issues, "select id,token_id,quantity,comment,status,create_time from token_issue_logs where token_id = ? and status = 1 order by create_time desc", tokenId)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &issues, nil

}

func (*issueDao) GetIssueCount(tokenId int64) (*model.TokenIssueCount, error) {

	var issue model.TokenIssueCount
	err := db.DBCli.Get(&issue, "select SUM(quantity) as 'total' from token_issue_logs  where token_id = ? ", tokenId)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &issue, nil

}

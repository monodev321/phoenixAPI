package dao

import (
	"database/sql"
	"time"

	"phoenix/internal/token/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"
)

type flowRecordDao struct{}

var FlowRecordDao = new(flowRecordDao)

func (*flowRecordDao) Add(tx *sqlx.Tx, flowLog *model.TokenFlowLogs) *sql.Result {
	createTime := time.Now().Unix()
	result := tx.MustExec("INSERT IGNORE INTO `token_flow_logs` (`token_id`, `uid`, `f_uid`, `f_username`, `f_nickname`, `f_avatar`, `quantity`, `log_type`, `operator`, `comment`, `create_time`,`balance`,`tran_id`,`charge`,`proce_id`,`order_id`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
		flowLog.TokenId, flowLog.Uid, flowLog.FUid, flowLog.FUsername, flowLog.FNickname, flowLog.FAvatar, flowLog.Quantity, flowLog.LogType, flowLog.Operator, flowLog.Comment, createTime, flowLog.Balance, flowLog.TranId, flowLog.Charge, flowLog.ProceId, flowLog.OrderId)
	return &result
}

func (*flowRecordDao) List(uid int64) (*[]model.TokenFlowLogs, error) {

	var flowLogs []model.TokenFlowLogs
	err := db.DBCli.Select(&flowLogs, "select id,token_id,f_uid,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,tran_id,create_time from token_flow_logs where uid = ?  ORDER BY id desc LIMIT 0,200",
		uid)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &flowLogs, nil
}

func (*flowRecordDao) Flow(flowId int64) (*model.TokenFlowLogs, error) {

	var flowLog model.TokenFlowLogs
	err := db.DBCli.Get(&flowLog, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where id =?",
		flowId)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &flowLog, nil
}

func (*flowRecordDao) GetFlowRecordByUidAndTokenId(uid int64, tokenId int64, id int64, operator int, isPre int, rows int) (*[]model.TokenFlowLogs, error) {

	var flowLogs []model.TokenFlowLogs
	var err error
	if isPre == 1 {
		if id == 0 {
			if operator < 2 {
				err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and operator = ? and id > ?  ORDER BY id desc LIMIT 0, ?",
					uid, tokenId, operator, id, rows)
			} else {
				err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and id > ? ORDER BY id desc LIMIT 0,?",
					uid, tokenId, id, rows)
			}
		} else {
			if operator < 2 {
				err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and operator = ? and id > ?  ORDER BY id  LIMIT 0, ?",
					uid, tokenId, operator, id, rows)
			} else {
				err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and id > ? ORDER BY id  LIMIT 0,?",
					uid, tokenId, id, rows)
			}
		}

	} else {
		if operator < 2 {
			err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and operator = ?  and id < ? ORDER BY id desc LIMIT 0,?",
				uid, tokenId, operator, id, rows)
		} else {
			err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and id < ? ORDER BY id desc LIMIT 0,?",
				uid, tokenId, id, rows)
		}
	}

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &flowLogs, nil
}

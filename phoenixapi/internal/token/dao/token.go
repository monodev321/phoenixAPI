package dao

import (
	"database/sql"
	"fmt"
	"time"

	"phoenix/internal/token/model"
	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"

	"phoenix/pkg/db"
)

type tokenDao struct{}

var TokenDao = new(tokenDao)

// 添加代币
func (*tokenDao) Add(token *model.CoinToken) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `coin_tokens` ( `name`, `symbol`, `icon`, `website`, `comment`,`quantity_limit`, `owner_uid`, `issue_uid`,`issue_username`,  `create_time`) values(?,?,?,?,?,?,?,?,?,?)",
		token.Name, token.Symbol, token.Icon, token.Website, token.Comment, token.QuantityLimit, token.OwnerUid, token.IssueUid, token.IssueUsername, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "代币已经存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加代币失败")
	}

	return id, nil
}

// 审核代币
func (*tokenDao) Approve(token *model.CoinToken) (int64, error) {
	approveTime := time.Now().Unix()
	result, err := db.DBCli.Exec("UPDATE coin_tokens SET apply_comment = ?, approve_uid = ?, status =?, approve_time = ?  WHERE id= ? ",
		token.ApplyComment, token.ApproveUid, token.Status, approveTime, token.Id)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "更新数据库失败")
	}

	return token.Id, nil
}

// 审核发行代币
func (*tokenDao) ApproveIssue(tx *sqlx.Tx, quantity uint64, num int, tokenId int64) *sql.Result {
	result := tx.MustExec("UPDATE coin_tokens SET  issue_quantity = ?, issue_num =?  WHERE id= ? ",
		quantity, num, tokenId)
	return &result
}

// 获取用户的好友请求列表
func (*tokenDao) List() (*[]model.ListToken, error) {

	var tokens []model.ListToken
	err := db.DBCli.Select(&tokens, "select id,name,symbol,icon,status,website,quantity_limit,issue_quantity,issue_num,weight,create_time from coin_tokens where status = 1 order by weight desc")
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &tokens, nil
}

func (*tokenDao) ListInArr(ids *[]int64) (*[]model.ListToken, error) {

	var tokens []model.ListToken
	sql := "select id,name,symbol,icon from coin_tokens where id IN (?)"
	sql, args, err := sqlx.In(sql, *ids)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "构建查询语句失败")
	}
	err = db.DBCli.Select(&tokens, sql, args...)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &tokens, nil
}

func (*tokenDao) Get2Coin() (*[]model.ListToken, error) {

	var tokens []model.ListToken
	err := db.DBCli.Select(&tokens, "select id,name,symbol,icon from coin_tokens where status = 1 order by weight desc limit 0,2")
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &tokens, nil
}

func (*tokenDao) GetTokenByDevelopUid(uid int64) (*[]model.CoinToken, error) {

	var tokens []model.CoinToken
	err := db.DBCli.Select(&tokens, "select id,name,symbol,icon,create_time,comment,apply_comment,quantity_limit,issue_uid,issue_username,status from coin_tokens where owner_uid = ?", uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &tokens, nil

}

// 获取用户的好友请求列表
func (*tokenDao) GetTokenById(id int64) (*model.CoinToken, error) {

	var token model.CoinToken
	err := db.DBCli.Get(&token, "select id,name,symbol,icon,issue_uid,status,website,quantity_limit,issue_quantity,issue_num,weight,create_time from coin_tokens where id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &token, nil
}

// 获取用户的好友请求列表
func (*tokenDao) GetUserTokenById(id int64) (*model.ListToken, error) {

	var token model.ListToken
	err := db.DBCli.Get(&token, "select id,name,symbol,icon,status,website,quantity_limit,issue_quantity,issue_num,weight,create_time from coin_tokens where id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &token, nil
}

// 获取用户的好友请求列表
func (*tokenDao) GetTokenBySymbol(symbol string) (*model.CoinToken, error) {

	var token model.CoinToken
	err := db.DBCli.Get(&token, "select id,name,symbol,icon,issue_uid,quantity_limit from coin_tokens where symbol = ?",
		symbol)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &token, nil
}

func (*tokenDao) TokenList(status int, symbol string, offset int64, limit int) (*[]model.CoinToken, error) {
	sql := "select id,name,symbol,icon,comment,apply_comment,quantity_limit,owner_uid,approve_uid,status,create_time,approve_time,issue_username,issue_quantity,weight,issue_num,website from coin_tokens where id > 0 "

	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if symbol != "" {
		sql = sql + "and symbol = '%s' "
		sql = fmt.Sprintf(sql, symbol)
	}

	sql = sql + "order by id limit %d, %d"
	sql = fmt.Sprintf(sql, offset, limit)

	var orders []model.CoinToken
	err := db.DBCli.Select(&orders, sql)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

func (*tokenDao) TokenListCount(status int, symbol string) (int64, error) {
	sql := "select count(1) from coin_tokens where id > 0 "
	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if symbol != "" {
		sql = sql + "and symbol = '%s' "
		sql = fmt.Sprintf(sql, symbol)
	}
	var num int64
	err := db.DBCli.Get(&num, sql)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	return num, nil
}

func (*tokenDao) ChangeField(id int64, field string, value int) error {
	sql := fmt.Sprintf("UPDATE coin_tokens SET %s = ?  WHERE id= ?", field)
	_, err := db.DBCli.Exec(sql, value, id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

package cache

import (
	"strconv"
	"time"

	"phoenix/pkg/db"
	"phoenix/pkg/werror"

	"github.com/syndtr/goleveldb/leveldb"
)

const (
	UserKey    = "user_token_lock_"
	UserExpire = 30 * time.Second
)

type userCache struct{}

var UserCache = new(userCache)

func (*userCache) Key(userId int64) string {
	return UserKey + strconv.FormatInt(userId, 10)
}

// Get 获取用户缓存
func (c *userCache) IsLocked(uid int64) (bool, error) {

	// _, err := db.RedisCli1.Get(c.Key(uid)).Result()
	now := time.Now().Unix()
	lockerTime, err := db.ApiLevelDB.Get(c.Key(uid))
	lockerTimeInt, _ := strconv.ParseInt(lockerTime, 10, 64)
	if err != nil && err != leveldb.ErrNotFound {
		return true, werror.New(100, "账户锁定出错，请稍后操作")
	}
	if err == leveldb.ErrNotFound || (lockerTimeInt+30) < now {
		err = c.Set(uid)
		if err == nil {
			return false, nil
		}
		return true, werror.New(100, "账户锁定中，请稍后操作")
	}
	return true, werror.New(100, "账户锁定中，请稍后操作")
}

// Set 设置用户缓存
func (c *userCache) Set(uid int64) error {
	now := strconv.FormatInt(time.Now().Unix(), 10)
	// _, err := db.RedisCli1.Set(c.Key(uid), now, UserExpire).Result()
	err := db.ApiLevelDB.Add(c.Key(uid), []byte(now))
	if err != nil {
		return werror.New(100, "锁定账户失败")
	}
	return nil
}

// Del 删除用户缓存
func (c *userCache) Del(uid int64) error {
	// _, err := db.RedisCli1.Del(c.Key(uid)).Result()
	err := db.ApiLevelDB.Del(c.Key(uid))
	if err != nil {
		return werror.New(100, "解锁账户失败")
	}
	return nil
}

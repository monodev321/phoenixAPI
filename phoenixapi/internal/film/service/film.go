package service

import (
	models "phoenix/app/model"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/internal/film/dao"
	"phoenix/internal/film/model"
	"phoenix/pkg/werror"
)

type filmService struct {
	comment models.CommentModel
	user    models.UserModel
}

var FilmService = new(filmService)

func (*filmService) TopRecommedList() (*[]model.Film, error) {
	var films *[]model.Film

	films, err := dao.FilmDao.TopRecommedList()

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (*filmService) RankingList() (*[]model.Film, error) {
	var films *[]model.Film

	films, err := dao.FilmDao.RankingList()

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (*filmService) NewArrivalsList() (*[]model.Film, error) {
	var films *[]model.Film

	films, err := dao.FilmDao.NewArrivalsList()

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (*filmService) RecommedList(offset, limit int) (*[]model.Film, error) {
	var films *[]model.Film

	films, err := dao.FilmDao.RecommedList(offset, limit)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (*filmService) FilmById(filmId uint) (*types.Film, error) {
	var film *types.Film

	film, err := dao.FilmDao.GetFilm(filmId)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return film, err
}

func (*filmService) SamePlotList(plotId string) (*[]model.Film, error) {
	var films *[]model.Film

	films, err := dao.FilmDao.SamePlotList(plotId)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (*filmService) LikeFilm(filmId uint) error {
	err := dao.FilmDao.LikeFilm(filmId)

	if err != nil {
		return werror.FunWrap(err, 200, "查询失败")
	}

	return nil
}

func (*filmService) DislikeFilm(filmId uint) error {
	err := dao.FilmDao.DislikeFilm(filmId)

	if err != nil {
		return werror.FunWrap(err, 200, "查询失败")
	}

	return nil
}

func (*filmService) SearchByName(offset, limit int, movieName string) (*[]types.Film, error) {
	var films *[]types.Film

	films, err := dao.FilmDao.SearchByName(offset, limit, movieName)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (*filmService) QuickFilter(offset, limit int, req proto.QuickFilterReq) (*[]types.Film, error) {
	var films *[]types.Film

	films, err := dao.FilmDao.QuickFilter(offset, limit, req)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return films, err
}

func (s filmService) ListComments(film_id int, uid uint, offset, limit int) ([]*proto.UserComment, int, error) {
	var comments []*types.Comment

	comments, count, err := s.comment.GetCommentsByFilm(uid, film_id, offset, limit)
	if err != nil {
		return nil, 0, werror.FunWrap(err, 200, "查询失败")
	}

	uids := make([]uint, 0, len(comments))
	for _, v := range comments {
		uids = append(uids, v.UserID)
	}

	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, 0, err
	}

	list := make([]*proto.UserComment, 0, len(comments))
	for k, v := range comments {
		list = append(list, &proto.UserComment{
			UserBaseInfo: userMap[v.UserID],
			Comment:      comments[k],
		})
	}
	err = s.comment.SetReplies(uid, list)
	if err != nil {
		return nil, 0, err
	}
	return list, count, nil
}

func (s filmService) GetCommentById(id uint) (*types.Comment, error) {
	comment, err := s.comment.GetOne(id)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return comment, nil
}

func (s filmService) CreateComment(comment *types.Comment) error {
	return s.comment.Save(comment)
}

func (s filmService) StatsComment(filmId int) error {
	err := dao.FilmDao.StatsComment(filmId)

	if err != nil {
		return werror.FunWrap(err, 200, "查询失败")
	}

	return nil
}

func (s filmService) LikeComment(uid, cid uint, isLike bool) error {
	return s.comment.Like(uid, cid, isLike)
}

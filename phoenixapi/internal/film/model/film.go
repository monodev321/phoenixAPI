package model

type Film struct {
	Id       int64  `json:"id"`
	Title    string `json:"title"`
	Cover    string `json:"cover"`
	Cover2   string `json:"cover2"`
	Desc     string `json:"desc"`
	Views    int    `json:"views"`
	Likes    int    `json:"likes"`
	Times    string `json:"times"`
	Duration int    `json:"duration"`
}

package dao

import (
	"fmt"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/internal/film/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
	"strconv"
)

type filmDao struct{}

var FilmDao = new(filmDao)

// Get top recommendation film list
func (*filmDao) TopRecommedList() (*[]model.Film, error) {
	var films []model.Film

	err := db.DBCli.Select(&films, "select id,title,cover,cover2,`desc` from films where is_top=true order by id limit 8")

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

// Get top ranking film list
func (*filmDao) RankingList() (*[]model.Film, error) {
	var films []model.Film

	err := db.DBCli.Select(&films, "select id,title,cover,cover2,`desc`,views,likes,times from films order by views limit 50")

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

// Get top ranking film list
func (*filmDao) NewArrivalsList() (*[]model.Film, error) {
	var films []model.Film

	err := db.DBCli.Select(&films, "select id,title,cover,cover2,`desc` from films order by update_time limit 8")

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

// Get recommended film list
func (*filmDao) RecommedList(offset, limit int) (*[]model.Film, error) {
	var films []model.Film

	sql := "select id,title,cover,cover2,`desc` from films order by sort, grade, update_time limit %d, %d"
	sql = fmt.Sprintf(sql, offset, limit)

	err := db.DBCli.Select(&films, sql)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

// Get film list by plot_id
func (*filmDao) SamePlotList(plotId string) (*[]model.Film, error) {
	var films []model.Film

	sql := "select id,title,cover,cover2,duration,views from films where plot_id %s order by sort, grade, update_time limit 12"
	sql = fmt.Sprintf(sql, "like '%"+plotId+"%'")

	err := db.DBCli.Select(&films, sql)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

func (*filmDao) GetFilm(id uint) (film *types.Film, err error) {
	film = &types.Film{}
	err = db.Orm.Model(film).Take(film, id).Error
	if err != nil {
		film = nil
	}
	return
}

// like film by id
func (*filmDao) LikeFilm(id uint) error {
	sql := "UPDATE films SET likes = likes + 1 WHERE id= ? "

	result, err := db.DBCli.Exec(sql, id)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil
}

// like film by id
func (*filmDao) DislikeFilm(id uint) error {
	sql := "UPDATE films SET likes = likes - 1 WHERE id= ? "

	result, err := db.DBCli.Exec(sql, id)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil
}

func (*filmDao) SearchByName(offset, limit int, movieName string) (*[]types.Film, error) {
	var films []types.Film

	sql := "select * from films where title like %s limit %d, %d"
	sql = fmt.Sprintf(sql, "'%"+movieName+"%'", offset, limit)

	err := db.DBCli.Select(&films, sql)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

func (*filmDao) QuickFilter(offset, limit int, req proto.QuickFilterReq) (*[]types.Film, error) {
	var films []types.Film

	sql := "select * from films "
	conditions := ""
	if req.KindId != 0 {
		conditions += " kind_id=" + strconv.Itoa(req.KindId)
	}
	if req.PlotId != "" {
		if conditions != "" {
			conditions += " and "
		}
		conditions += " plot_id like '" + req.PlotId + "'"
		// conditions += " plot_id='1_'"
	}
	if req.RegionId != 0 {
		if conditions != "" {
			conditions += " and "
		}
		conditions += " region_id=" + strconv.Itoa(req.RegionId)
	}
	if req.LanguageId != 0 {
		if conditions != "" {
			conditions += " and "
		}
		conditions += " language_id=" + strconv.Itoa(req.LanguageId)
	}
	if req.TimeId != 0 {
		if conditions != "" {
			conditions += " and "
		}
		conditions += " time_id=" + strconv.Itoa(req.TimeId)
	}
	if req.Name != "" {
		if conditions != "" {
			conditions += " and "
		}

		conditions += " title like '%%" + req.Name + "%%' "
	}
	if conditions != "" {
		sql += " where " + conditions
	}
	sql += " limit %d, %d "
	sql = fmt.Sprintf(sql, offset, limit)

	err := db.DBCli.Select(&films, sql)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &films, nil
}

func (*filmDao) StatsComment(id int) (err error) {
	sql := "UPDATE films SET comments = comments + 1 WHERE id= ? "

	result, err := db.DBCli.Exec(sql, id)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil
}

package model

type Upgrade struct {
	Id         int64  `json:"id"`                          // id
	Name       string `json:"name"`                        // 代币id
	Extra      string `json:"extra"`                       // 代币id
	Status     int    `json:"status"`                      // 0表示禁用，1表示正常
	FinishTime int64  `json:"finishTime" db:"finish_time"` // 创建时间
}

package service

import (
	"fmt"
	"strconv"

	"phoenix/internal/upgrade/dao"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/werror"
)

type userService struct{}

var UserService = new(userService)

// 用户注册
func (*userService) UpdateEmail() error {
	upgrade, err := dao.UpgradeDao.GetUpgrade("md5email")
	if err != nil {
		return werror.New(100, "获取更新信息错误，请检查")
	}
	if upgrade.Status == 1 {
		return werror.New(100, "已更新完成")
	}
	uid, _ := strconv.ParseInt(upgrade.Extra, 10, 64)

	users, err := dao.UserDao.UserList(uid)
	if err != nil {
		return werror.New(100, "更新控制表出错，请检查")
	}

	len := len(*users)

	for _, user := range *users {
		email := user.Username
		username := coinHash.BsonId()
		email2 := coinHash.Md5WithSalt(email, email)
		println(len, email, username, email2)
		uid = user.Uid
		err := dao.UserDao.UpdateUsernameAndEmail(username, email2, user.Uid)
		fmt.Printf("%+v", err)
		if err != nil {
			return werror.New(100, "更新用户出错")
		}
	}

	if len < 10 {
		err := dao.UpgradeDao.Finished(upgrade.Id)
		if err != nil {
			return werror.New(100, "更新控制表出错，请检查")
		}
	} else {
		err := dao.UpgradeDao.UpdateStatus(0, upgrade.Id, strconv.FormatInt(uid, 10))
		if err != nil {
			return werror.New(100, "更新控制表出错，请检查")
		}
		return werror.New(100, "本次更新完成，请继续")
	}
	return werror.New(100, "已更新完成")
}

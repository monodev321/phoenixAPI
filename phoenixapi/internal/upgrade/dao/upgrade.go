package dao

import (
	"database/sql"
	"time"

	"phoenix/internal/upgrade/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type upgradeDao struct{}

var UpgradeDao = new(upgradeDao)

// SetActive 激活用户
func (*upgradeDao) Add(name string, extra string, status int) error {

	_, err := db.DBCli.Exec("insert IGNORE into upgrades( `name`,`extra`, `status`) values(?,?,?)",
		name, extra, status)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

// SetActive 激活用户
func (*upgradeDao) GetUpgrade(name string) (*model.Upgrade, error) {
	upgrade := model.Upgrade{}
	//查询单个Get
	err := db.DBCli.Get(&upgrade, "select id,name,extra,status,finish_time from upgrades where name = ? ",
		name)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &upgrade, nil
}

// SetActive 激活用户
func (*upgradeDao) UpdateStatus(status int, id int64, extra string) error {
	createTime := time.Now().Unix()
	_, err := db.DBCli.Exec("UPDATE upgrades SET status=?, extra = ?, finish_time = ? where id = ?", status, extra, createTime, id)
	if err != nil {
		return werror.FunWrap(err, 200, "更新状态失败")
	}
	return nil
}

func (*upgradeDao) Finished(id int64) error {
	createTime := time.Now().Unix()
	_, err := db.DBCli.Exec("UPDATE upgrades SET status=1,  finish_time = ? where id = ?", createTime, id)
	if err != nil {
		return werror.FunWrap(err, 200, "更新状态失败")
	}
	return nil
}

package dao

import (
	"phoenix/internal/user/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type userDao struct{}

var UserDao = new(userDao)

func (*userDao) UserList(uid int64) (*[]model.User, error) {
	sql := "select uid,username from users where uid < ? order by uid desc limit 0, 10"

	var orders []model.User
	err := db.DBCli.Select(&orders, sql, uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

// Get 获取用户信息
func (*userDao) UpdateUsernameAndEmail(username string, email string, uid int64) error {

	_, err := db.DBCli.Exec("UPDATE users SET username=? ,email = ? where uid=?", username, email, uid)
	if err != nil {
		return werror.FunWrap(err, 200, "更新active状态失败")
	}
	return nil
}

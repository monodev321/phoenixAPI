package dao

import (
	"fmt"
	"phoenix/internal/blog/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
	"time"
	// slugify "github.com/gosimple/slug"
)

type blogDao struct{}

var BlogDao = new(blogDao)

func (*blogDao) Add(blog *model.Blog) (*model.Blog, error) {
	blog.CreatedAt = time.Now().Unix()
	blog.UpdatedAt = time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT INTO `blogs` (`title`, `content`, `cover_url`, `user_id`, `nickname`,`category`,`category_id`,`language`,`language_id`, `is_top`,`is_recommend`,`sort`,`views_num`, `is_status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", blog.Title, blog.Content, blog.CoverUrl, blog.UserID, blog.Nickname, blog.Category, blog.CategoryId, blog.Language, blog.LanguageId, blog.IsTop, blog.IsRecommend, blog.Sort, blog.ViewsNum, blog.IsStatus, blog.CreatedAt, blog.UpdatedAt)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "Blog Insert Failed")
	}
	id, _ := result.LastInsertId()
	blog.ID = id
	return blog, nil
}

func (*blogDao) BlogList(category string, offset int64, limit int) (*[]model.Blog, error) {
	var blogs []model.Blog
	sql := "select * from `blogs` Order by created_at desc LIMIT %d OFFSET %d"
	if category == "" {
		sql = fmt.Sprintf(sql, limit, offset)
		err := db.DBCli.Select(&blogs, sql)
		if err != nil {
			return nil, werror.FunWrap(err, 200, "something went wrong,unable to fetch blogs")
		}
		return &blogs, nil
	} else {
		sql = "select * from `blogs` where category = ? Order by created_at desc LIMIT %d OFFSET %d"
		sql = fmt.Sprintf(sql, limit, offset)
	}
	err := db.DBCli.Select(&blogs, sql, category)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "something went wrong,unable to fetch blogs")
	}
	return &blogs, nil
}
func (*blogDao) BlogListCount(category string) (int64, int64, error) {
	sql := "select count(1) from blogs where id > 0 "

	var total_num int64
	err := db.DBCli.Get(&total_num, sql)
	if err != nil {
		return 0, 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	var num int64
	err1 := db.DBCli.Get(&num, "select count(1) from blogs where id > 0 and category =?", category)
	if err1 != nil {
		return 0, 0, werror.FunWrap(err1, 200, "查询列表失败")
	}
	if category == "" {
		num = total_num
	}
	return total_num, num, nil
}
func (*blogDao) GetBlogInfo(id int64) (*model.Blog, error) {
	blog := model.Blog{}
	err := db.DBCli.Get(&blog, "select id,title, content,cover_url, user_id, nickname, category, language, is_top, is_recommend, sort, views_num,is_status,created_at,updated_at from blogs where id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &blog, nil
}

func (*blogDao) SearchByName(offset, limit int, blogName string, category string) (*[]model.Blog, int, error) {
	var blogs []model.Blog
	var count int
	if category == "" {
		sql := "select * from blogs where title like %s order by created_at desc limit %d, %d"
		sql1 := "select count(1) from blogs where id > 0 and title like %s"
		sql = fmt.Sprintf(sql, "'%"+blogName+"%'", offset, limit)
		sql1 = fmt.Sprintf(sql1, "'%"+blogName+"%'")
		db.DBCli.Get(&count, sql1)
		err := db.DBCli.Select(&blogs, sql)
		if err != nil {
			return nil, 0, werror.FunWrap(err, 200, "查询列表失败")
		}
	} else {
		sql := "select * from blogs where title like %s and category =? order by created_at desc limit %d, %d"
		sql1 := "select count(1) from blogs where id > 0 and title like %s and category = ?"
		sql = fmt.Sprintf(sql, "'%"+blogName+"%'", offset, limit)
		sql1 = fmt.Sprintf(sql1, "'%"+blogName+"%'")
		err := db.DBCli.Select(&blogs, sql, category)
		if err != nil {
			return nil, 0, werror.FunWrap(err, 200, "查询列表失败")
		}
		db.DBCli.Get(&count, sql1, category)
	}
	return &blogs, count, nil
}

func (*blogDao) DeleteBlog(id int64) error {
	_, err := db.DBCli.Exec("DELETE FROM `blogs` WHERE id=? ", id)
	return err
}

func (*blogDao) EditBlog(blog *model.Blog) (*model.Blog, error) {
	// title := slugify.Make(blog.Title)
	result, err := db.DBCli.Exec("UPDATE `blogs` SET title = ?, content = ?, cover_url =?, category = ?, category_id = ?, language = ?, language_id = ?, updated_at = ?  WHERE id= ? ", blog.Title, blog.Content, blog.CoverUrl, blog.Category, blog.CategoryId, blog.Language, blog.LanguageId, blog.UpdatedAt, blog.ID)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "我们无法更新此博客")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return nil, werror.FunWrap(err, 200, "找不到博客")
	}

	if affected == 0 {
		return nil, werror.New(300, "博客未更新")
	}

	updatedBlog, err := BlogDao.GetBlogInfo(blog.ID)

	if err != nil {
		return nil, werror.New(300, "找不到博客")
	}

	return updatedBlog, nil
}

func (*blogDao) BlogChangeField(blog *model.Blog) (*model.Blog, error) {
	// title := slugify.Make(blog.Title)
	result, err := db.DBCli.Exec("UPDATE `blogs` SET is_top = ?, is_status = ?, sort =?, updated_at = ?  WHERE id= ? ", blog.IsTop, blog.IsStatus, blog.Sort, blog.UpdatedAt, blog.ID)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "我们无法更新此博客")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return nil, werror.FunWrap(err, 200, "找不到博客")
	}

	if affected == 0 {
		return nil, werror.New(300, "博客未更新")
	}

	updatedBlog, err := BlogDao.GetBlogInfo(blog.ID)

	if err != nil {
		return nil, werror.New(300, "找不到博客")
	}

	return updatedBlog, nil
}

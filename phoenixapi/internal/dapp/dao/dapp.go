package dao

import (
	"fmt"
	"time"

	"phoenix/internal/dapp/model"
	dappModel "phoenix/internal/dapp/model"
	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"

	"phoenix/pkg/db"
)

type dappDao struct{}

var DappDao = new(dappDao)

// 添加代币
func (*dappDao) Apply(dapp *model.Dapp) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `dapps` (  `name`, `symbol`, `icon`, `domain`, `callback_url`, `intro`,`comment`, `developer_id`, `create_time`) values(?,?,?,?,?,?,?,?,?)",
		dapp.Name, dapp.Symbol, dapp.Icon, dapp.Domain, dapp.CallbackUrl, dapp.Intro, dapp.Comment, dapp.DeveloperId, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "Dapp已存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "获取dappId失败")
	}

	return id, nil
}

func (*dappDao) GetDappsByDevelopUid(uid int64) (*[]model.Dapp, error) {

	var dapps []model.Dapp
	err := db.DBCli.Select(&dapps, "select id,name,symbol,icon,domain,callback_url,create_time,IFNULL(comment, '') as comment,IFNULL(intro, '') as intro,IFNULL(approve_comment, '') as approve_comment,developer_id,is_publish,approve_time,status,client_id,follower from dapps where developer_id = ?", uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &dapps, nil
}

func (*dappDao) MarketDappList() (*[]model.Dapp, error) {

	var dapps []model.Dapp
	err := db.DBCli.Select(&dapps, "select id,name,symbol,icon,domain,IFNULL(comment, '') as comment,IFNULL(intro, '') as intro,weight,follower,create_time from dapps where is_publish = 1 and status = 1 and is_recommend = 1 order by weight desc")
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &dapps, nil
}

func (*dappDao) DappListInArr(ids *[]int64) (*[]model.Dapp, error) {

	var dapps []model.Dapp

	sql := "select id,name,symbol,icon,domain,IFNULL(comment, '') as comment,IFNULL(intro, '') as intro,weight,follower,create_time from dapps where id IN (?)  and status = 1 and is_publish = 1"
	sql, args, err := sqlx.In(sql, *ids)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "构建查询语句失败")
	}
	err = db.DBCli.Select(&dapps, sql, args...)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &dapps, nil
}

func (*dappDao) GetDappById(id int64) (*model.Dapp, error) {

	var dapp model.Dapp
	err := db.DBCli.Get(&dapp, "select id,name,symbol,icon,IFNULL(comment, '') as comment,IFNULL(intro, '') as intro,domain,follower,weight,status,client_id,create_time from dapps where id = ? and status = 1 and is_publish = 1",
		id)
	if err != nil {
		if fmt.Sprintf("%T", err) == "*errors.errorString" {
			return nil, nil
		} else {
			return nil, werror.FunWrap(err, 200, "查询失败")
		}

	}
	return &dapp, nil
}

func (*dappDao) GetDappBySymbol(symbol string) (*model.Dapp, error) {

	var dapp model.Dapp
	err := db.DBCli.Get(&dapp, "select id,name,symbol,icon,IFNULL(intro, '') as intro,domain,status,create_time from dapps where symbol = ? and status = 1 and is_publish = 1",
		symbol)
	if err != nil {
		if fmt.Sprintf("%T", err) == "*errors.errorString" {
			return nil, nil
		} else {
			return nil, werror.FunWrap(err, 200, "查询失败")
		}

	}
	return &dapp, nil
}

func (*dappDao) GetDevDappById(id int64) (*model.Dapp, error) {

	var dapp model.Dapp
	err := db.DBCli.Get(&dapp, "select id,name,symbol,icon,IFNULL(comment, '') as comment,IFNULL(intro, '') as intro,domain,callback_url,developer_id,follower,weight,status,client_id,create_time from dapps where id = ? ",
		id)
	if err != nil {
		if fmt.Sprintf("%T", err) == "*errors.errorString" {
			return nil, nil
		} else {
			return nil, werror.FunWrap(err, 200, "查询失败")
		}

	}
	return &dapp, nil
}

func (*dappDao) EditDapp(dapp *model.Dapp) error {

	result, err := db.DBCli.Exec("UPDATE dapps SET name = ?, symbol = ?,icon = ?,domain = ?,callback_url = ?,comment = ?,intro = ?, status=0  WHERE id= ? ",
		dapp.Name, dapp.Symbol, dapp.Icon, dapp.Domain, dapp.CallbackUrl, dapp.Comment, dapp.Intro, dapp.Id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "您的数据未改变")
	}

	return nil
}

func (*dappDao) Publish(id int64) error {

	result, err := db.DBCli.Exec("UPDATE dapps SET is_publish = 1  WHERE id= ? ",
		id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}

// 审核代币
func (*dappDao) Approve(dapp *model.Dapp) (int64, error) {
	approveTime := time.Now().Unix()
	result, err := db.DBCli.Exec("UPDATE dapps SET approve_comment = ?, approve_uid = ?, status =?, approve_time = ?,client_id=?  WHERE id= ? ",
		dapp.ApproveComment, dapp.ApproveUid, dapp.Status, approveTime, dapp.ClientId, dapp.Id)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "更新数据库失败")
	}

	return dapp.Id, nil
}

func (*dappDao) Follow(id int64) error {

	_, err := db.DBCli.Exec("UPDATE dapps SET `follower` = `follower` +1  WHERE id= ? ",
		id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

func (*dappDao) UnFollow(id int64) error {

	_, err := db.DBCli.Exec("UPDATE dapps SET `follower` = `follower` -1  WHERE id= ? ",
		id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

func (*dappDao) DappList(status int, isRecommend, isPublishint int, symbol string, offset int64, limit int) (*[]dappModel.Dapp, error) {
	sql := "select id,name,symbol,icon,domain,callback_url,intro,comment,approve_comment,developer_id,approve_uid,status,is_publish,weight,create_time,approve_time,client_id,follower,is_recommend from dapps where id > 0 "

	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}
	if isRecommend > -1 {
		sql = sql + "and is_recommend = %d "
		sql = fmt.Sprintf(sql, isRecommend)
	}
	if isPublishint > -1 {
		sql = sql + "and is_publish = %d "
		sql = fmt.Sprintf(sql, isPublishint)
	}

	if symbol != "" {
		sql = sql + "and symbol = '%s' "
		sql = fmt.Sprintf(sql, symbol)
	}

	sql = sql + "order by id limit %d, %d"
	sql = fmt.Sprintf(sql, offset, limit)

	var orders []dappModel.Dapp
	err := db.DBCli.Select(&orders, sql)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

func (*dappDao) DappListCount(status int, isRecommend, isPublishint int, symbol string) (int64, error) {
	sql := "select count(1) from dapps where id > 0 "
	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}
	if isRecommend > -1 {
		sql = sql + "and is_recommend = %d "
		sql = fmt.Sprintf(sql, isRecommend)
	}
	if isPublishint > -1 {
		sql = sql + "and is_publish = %d "
		sql = fmt.Sprintf(sql, isPublishint)
	}

	if symbol != "" {
		sql = sql + "and symbol = '%s' "
		sql = fmt.Sprintf(sql, symbol)
	}
	var num int64
	err := db.DBCli.Get(&num, sql)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	return num, nil
}

func (*dappDao) ChangeField(id int64, field string, value int) error {
	sql := fmt.Sprintf("UPDATE dapps SET %s = ?  WHERE id= ?", field)
	_, err := db.DBCli.Exec(sql, value, id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

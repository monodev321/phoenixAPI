package dao

import (
	"time"

	"phoenix/internal/dapp/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type paymentDao struct{}

var PaymentDao = new(paymentDao)

// 添加代币
func (*paymentDao) AddDappPayment(payment *model.DappPayment) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `dapp_payments` (  `dapp_id`, `uid`, `username`, `token_id`, `token_symbol`, `status`, `create_time`) values(?,?,?,?,?,?,?)",
		payment.DappId, payment.Uid, payment.Username, payment.TokenId, payment.TokenSymbol, payment.Status, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "获取登陆信息失败")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加登陆信息失败")
	}

	return id, nil
}

func (*paymentDao) EditDappPayment(payment *model.DappPayment, id int64) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("UPDATE dapp_payments SET uid = ?, username = ?,token_id = ?,token_symbol = ?,status = ?,create_time = ? WHERE id= ? ",
		payment.Uid, payment.Username, payment.TokenId, payment.TokenSymbol, payment.Status, createTime, id)

	if err != nil {
		return id, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return id, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return id, werror.New(300, "更新数据库失败")
	}

	return id, nil
}

func (*paymentDao) GetPaymentByDappid(dappId int64) (*model.DappPayment, error) {

	var payment model.DappPayment
	err := db.DBCli.Get(&payment, "select id,dapp_id,uid,username,token_id,token_symbol,status,create_time from dapp_payments where dapp_id = ?", dappId)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &payment, nil
}

package dao

import (
	"time"

	"phoenix/pkg/werror"

	"phoenix/internal/dapp/model"

	"phoenix/pkg/db"
)

type loginDao struct{}

var LoginDao = new(loginDao)

// 添加代币
func (*loginDao) Add(login *model.DappLogin) (int64, error) {

	result, err := db.DBCli.Exec("INSERT IGNORE INTO `dapp_login` ( `login_uuid`, `create_time`) values(?,?)",
		login.LoginUuid, login.CreateTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "获取登陆信息失败")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加登陆信息失败")
	}

	return id, nil
}

// 添加代币
func (*loginDao) GetLoginByUuid(uuid string) (*model.DappLogin, error) {

	var login model.DappLogin
	err := db.DBCli.Get(&login, "select id,login_uuid,IFNULL(uid, 0) as uid,IFNULL(username, '') as username,IFNULL(scope, '') as scope,IFNULL(token_type, '') as token_type,IFNULL(access, '') as access,IFNULL(refresh, '') as refresh,IFNULL(expired_at, 0) as expired_at from dapp_login where login_uuid = ?",
		uuid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	if login.Access == "" {
		return nil, werror.New(200, "还未授权登陆")
	}
	return &login, nil
}

func (*loginDao) Login(login *model.DappLogin) error {

	result, err := db.DBCli.Exec("UPDATE dapp_login SET uid = ?, username = ?, scope =?, token_type = ?, access = ?, refresh = ?, expired_at = ?  WHERE login_uuid= ? ",
		login.Uid, login.Username, login.Scope, login.TokenType, login.Access, login.Refresh, login.ExpiredAt, login.LoginUuid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}

func (s *loginDao) Clean() error {
	now := time.Now().Unix() - 600
	_, err := db.DBCli.Exec("DELETE FROM dapp_login WHERE create_time<=? ", now)
	return err

}

package dao

import (
	"fmt"
	"time"

	"phoenix/internal/dapp/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type followerDao struct{}

var FollowerDao = new(followerDao)

// 添加代币
func (*followerDao) Follow(follower *model.DappFollower) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("REPLACE INTO `dapp_followers` ( `dapp_id`, `uid`, `username`, `referrer_uid`,  `referrer_path`, `status`, `create_time`) values(?,?,?,?,?,?,?)",
		follower.DappId, follower.Uid, follower.Username, follower.ReferrerUid, follower.ReferrerPath, follower.Status, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "获取关注信息失败")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "获取关注失败")
	}

	return id, nil
}

func (*followerDao) GetFollowByUid(uid int64, dappId int64) (*model.DappFollower, error) {

	var follower model.DappFollower
	err := db.DBCli.Get(&follower, "select id,dapp_id,uid,username,referrer_uid,referrer_path,referrer_count,create_time,status from dapp_followers where uid = ? and dapp_id = ?",
		uid, dappId)
	if err != nil {
		if fmt.Sprintf("%T", err) == "*errors.errorString" {
			return nil, nil
		} else {
			return nil, werror.FunWrap(err, 200, "查询失败")
		}

	}
	return &follower, nil
}

func (*followerDao) GetDappsByFollowerUid(uid int64) (*[]model.Dapp, error) {

	var followerDapps []model.DappFollower
	err := db.DBCli.Select(&followerDapps, "select id,dapp_id,create_time from dapp_followers where uid = ? and status = 1 order by create_time desc", uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	if len(followerDapps) == 0 {
		return nil, nil
	}
	ids := make([]int64, 0, len(followerDapps))
	dappMap := map[int64]model.Dapp{}
	for _, followerDapp := range followerDapps {
		ids = append(ids, followerDapp.DappId)
		dappMap[followerDapp.DappId] = model.Dapp{CreateTime: followerDapp.CreateTime}
	}
	dappArr, err := DappDao.DappListInArr(&ids)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "获取dapp详情失败")
	}
	for _, dapp := range *dappArr {
		if _, ok := dappMap[dapp.Id]; ok {
			dapp.CreateTime = dappMap[dapp.Id].CreateTime
			dappMap[dapp.Id] = dapp
		}
	}
	dapps := make([]model.Dapp, 0, len(followerDapps))
	for _, id := range ids {
		dapps = append(dapps, dappMap[id])
	}
	return &dapps, nil
}

func (*followerDao) UnFollow(dappid int64, uid int64) error {

	result, err := db.DBCli.Exec("UPDATE dapp_followers SET status = 2 WHERE  uid = ? and dapp_id = ? ",
		uid, dappid)

	if err != nil {
		return werror.FunWrap(err, 200, "操作失败重试")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "操作结果错误")
	}

	if affected == 0 {
		return werror.New(300, "操作未成功")
	}

	return nil
}

func (*followerDao) HaveFollow(dappid int64, uid int64) error {

	_, err := db.DBCli.Exec("UPDATE dapp_followers SET status = 1 WHERE  uid = ? and dapp_id = ? ",
		uid, dappid)

	if err != nil {
		return werror.FunWrap(err, 200, "操作失败重试")
	}

	return nil
}

func (*followerDao) AddFollowCount(uid int64) error {

	_, err := db.DBCli.Exec("UPDATE dapp_followers SET `referrer_count` = `referrer_count` +1  WHERE uid= ? ",
		uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

package dao

import (
	"fmt"
	"time"

	"phoenix/pkg/werror"

	dappModel "phoenix/internal/dapp/model"

	"phoenix/pkg/db"
)

type developerDao struct{}

var DevelopDao = new(developerDao)

// 添加代币
func (*developerDao) Apply(applyInfo map[string]interface{}) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `developers` ( `uid`, `username`, `nickname`, `avatar`, `apply_comment`, `create_time`) values(?,?,?,?,?,?)",
		applyInfo["uid"], applyInfo["username"], applyInfo["nickname"], applyInfo["avatar"], applyInfo["comment"], createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "获取登陆信息失败")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加登陆信息失败")
	}

	return id, nil
}

func (*developerDao) GetDeveloperInfo(uid int64) (*dappModel.Developer, error) {

	var developer dappModel.Developer
	err := db.DBCli.Get(&developer, "select id,uid,username,nickname,avatar,apply_comment,status,create_time,client_id,approve_time from developers where uid = ?", uid)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &developer, nil

}

func (*developerDao) ApproveDeveloper(comment string, uid int64, id int64, status int) error {

	approveTime := time.Now().Unix()
	result, err := db.DBCli.Exec("UPDATE developers SET approve_comment = ?, approve_uid = ?, status =?, approve_time = ?  WHERE id= ? ",
		comment, uid, status, approveTime, id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil

}

func (*developerDao) DeveloperList(status int, username string, offset int64, limit int) (*[]dappModel.Developer, error) {
	sql := "select id,uid,username,nickname,avatar,apply_comment,approve_comment,approve_uid,status,create_time,approve_time from developers where id > 0 "

	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if username != "" {
		sql = sql + "and username = '%s' "
		sql = fmt.Sprintf(sql, username)
	}

	sql = sql + "order by id limit %d, %d"
	sql = fmt.Sprintf(sql, offset, limit)

	var orders []dappModel.Developer
	err := db.DBCli.Select(&orders, sql)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

func (*developerDao) DeveloperListCount(status int, username string) (int64, error) {
	sql := "select count(1) from developers where id > 0 "
	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if username != "" {
		sql = sql + "and username = '%s' "
		sql = fmt.Sprintf(sql, username)
	}
	var num int64
	err := db.DBCli.Get(&num, sql)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	return num, nil
}

func (*developerDao) ChangeDeveloperField(id int64, field string, value int) error {
	sql := fmt.Sprintf("UPDATE developers SET %s = ?  WHERE id= ?", field)
	_, err := db.DBCli.Exec(sql, value, id)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

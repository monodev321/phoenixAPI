package service

import (
	DappDao "phoenix/internal/dapp/dao"
	"phoenix/internal/dapp/model"
	"phoenix/internal/token/dao"
	UserDao "phoenix/internal/user/dao"
	"phoenix/pkg/werror"
)

type paymentService struct{}

var PaymentService = new(paymentService)

func (*paymentService) EditDappPayment(id int64, dappId int64, status int, symbol string, username string, developUid int64) (int64, error) {

	dapp, err := DappDao.DappDao.GetDevDappById(dappId)
	if err != nil {
		return 0, nil
	}

	if (*dapp).DeveloperId != developUid {
		return 0, werror.New(100, "您没有编辑权限")
	}

	user, err := UserDao.UserDao.GetUserByUsername(username)
	if err != nil {
		return 0, werror.New(200, "收款用户不存在")
	}
	uid := (*user).Uid
	// if !ok {
	// 	return 0, werror.New(200, "获取收款用户id错误")
	// }
	payUsername := (*user).Username
	// if !ok {
	// 	return 0, werror.New(200, "获取收款用户名错误")
	// }

	token, err := dao.TokenDao.GetTokenBySymbol(symbol)
	if err != nil {
		return 0, werror.New(200, "收款代币不存在")
	}

	payment := &model.DappPayment{
		DappId:      dappId,
		Uid:         uid,
		Username:    payUsername,
		TokenId:     token.Id,
		TokenSymbol: token.Symbol,
		Status:      status,
	}
	if id > 0 {
		id, err = DappDao.PaymentDao.EditDappPayment(payment, id)
		return id, err
	}

	// 添加
	id, err = DappDao.PaymentDao.AddDappPayment(payment)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*paymentService) GetDappPaymeny(dappId int64) (*model.DappPayment, error) {

	payment, err := DappDao.PaymentDao.GetPaymentByDappid(dappId)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return payment, nil
}

package service

import (
	"strconv"
	"strings"

	"phoenix/internal/dapp/dao"
	"phoenix/internal/dapp/model"
	"phoenix/internal/im/message"
	"phoenix/internal/im/tools"
	"phoenix/pkg/db"
)

type dappService struct{}

var DappService = new(dappService)

func (*dappService) DappInfo(id int64, uid int64, username string, referrer int64) (*model.Dapp, error) {

	dapp, err := dao.DappDao.GetDappById(id)
	if err != nil {
		return nil, err
	}

	follow, err := dao.FollowerDao.GetFollowByUid(uid, id)
	if err != nil {
		return nil, err
	}

	if follow == nil {
		//添加关注
		dappFollow := &model.DappFollower{
			DappId:      id,
			Uid:         uid,
			Username:    username,
			ReferrerUid: referrer,
			Status:      0,
		}
		dappFollow.ReferrerPath = "0"
		if referrer != 0 {
			referreerFollow, err := dao.FollowerDao.GetFollowByUid(referrer, id)
			if referreerFollow != nil && err == nil {
				dappFollow.ReferrerPath = referreerFollow.ReferrerPath + "_" + strconv.FormatInt(referrer, 10)
			} else {
				dappFollow.ReferrerPath = strconv.FormatInt(referrer, 10)
			}
		}
		dao.FollowerDao.Follow(dappFollow)
		dao.FollowerDao.AddFollowCount(referrer)
	}

	dapp.IsFollow = 0
	if follow != nil && follow.Status == 1 {
		dapp.IsFollow = 1
	}

	payment, err := dao.PaymentDao.GetPaymentByDappid(id)
	if err != nil {
		dapp.TokenId = 0
		dapp.PaymentUid = 0
		dapp.PaymentTokenSymbol = ""
		dapp.PaymentUsername = ""
	} else {
		dapp.TokenId = payment.TokenId
		dapp.PaymentUid = payment.Uid
		dapp.PaymentTokenSymbol = payment.TokenSymbol
		dapp.PaymentUsername = payment.Username
	}

	return dapp, err

}
func (*dappService) UserDappBySymbol(symbol string) (*model.Dapp, error) {

	dapp, err := dao.DappDao.GetDappBySymbol(symbol)
	if err != nil {
		return nil, err
	}

	return dapp, err

}

// 通过或者拒绝好友请求
func (*dappService) DappRecommend(members []int64, msg string, uid int64) error {
	uids := make([]string, 0, len(members))
	for _, member := range members {
		uids = append(uids, strconv.FormatInt(member, 10))
	}
	identifier := strings.Join(uids, ",")

	toid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	//把uid放入到redis
	field := strconv.FormatUint(toid, 10)
	key := "push_members_" + field
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}
	err = message.SendPushMassage("recommendDapp", msg, uint64(uid), toid, "push")
	return err
}

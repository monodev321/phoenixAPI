package service

import (
	"phoenix/internal/token/cache"
	"phoenix/internal/token/dao"
	"phoenix/internal/token/model"
	UserDao "phoenix/internal/user/dao"
	"phoenix/pkg/db"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/werror"
)

type tokenService struct{}

var TokenService = new(tokenService)

func (*tokenService) GenerateToken(token *model.CoinToken, issueUsername string) (int64, error) {
	user, err := UserDao.UserDao.GetUserByUsername(issueUsername)
	if err != nil {
		return 0, err
	}
	uid := (*user).Uid
	// if !ok {
	// 	return 0, werror.New(200, "获取用户issue 用户id错误")
	// }
	token.IssueUid = uid
	token.IssueUsername = issueUsername
	// 添加到好友数据库
	id, err := dao.TokenDao.Add(token)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*tokenService) ApproveToken(token *model.CoinToken) (int64, error) {

	// 添加到好友数据库
	id, err := dao.TokenDao.Approve(token)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*tokenService) DevelopTokenList(uid int64) (*[]model.CoinToken, error) {

	// 添加到好友数据库
	var tokens *[]model.CoinToken
	tokens, err := dao.TokenDao.GetTokenByDevelopUid(uid)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器
	return tokens, err
}

func (*tokenService) TokenList() (*[]model.ListToken, error) {

	// 添加到好友数据库
	var tokens *[]model.ListToken
	tokens, err := dao.TokenDao.List()
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return tokens, err
}

func (*tokenService) Transfer(log1 *model.TokenFlowLogs, log2 *model.TokenFlowLogs, tranPassword string) error {
	// 判断金额够吗
	userToken1, err := dao.UserTokenDao.GetUserTokenByUid(log1.Uid, log1.TokenId)
	if err != nil {
		return err
	}
	quantity1 := uint64(0)
	if userToken1 == nil {
		return werror.New(100, "余额不足")
	}
	if userToken1.AvailableQuantity < log1.Quantity {
		return werror.New(100, "余额不足")
	}

	// 判断转账密码是否正确
	user, err := UserDao.UserDao.GetTranPassword(log1.Uid)
	if err != nil {
		return err
	}
	//账户被禁用
	if user.Active == 2 {
		return werror.New(100, "账户被禁用")
	}
	//锁定账户
	isLook, _ := cache.UserCache.IsLocked(log1.Uid)
	defer cache.UserCache.Del(log1.Uid)
	if isLook == true {
		return werror.New(100, "账户被锁定")
	}
	_tranPassword := coinHash.GetSha256ByConf(tranPassword)
	if user.TranPassword == "" || user.TranPassword != _tranPassword {
		return werror.New(100, "转账密码不正确")
	}

	// 判断token是否存在
	// _, err = dao.TokenDao.GetTokenById(log1.TokenId)
	// if err != nil {
	// 	return werror.New(100, "转账代币非法")
	// }

	// 判断转账密码是否正确
	_, err = UserDao.UserDao.GetUserInfo(log1.FUid)
	if err != nil {
		return werror.New(100, "转出账户不存在")
	}

	quantity1 = userToken1.AvailableQuantity - log1.Quantity

	// 判断金额够吗
	userToken2, err := dao.UserTokenDao.GetUserTokenByUid(log2.Uid, log2.TokenId)
	if err != nil {
		return err
	}
	isNew2 := false
	quantity2 := uint64(0)
	if userToken2 == nil {
		isNew2 = true
		quantity2 = log1.Quantity
	} else {
		quantity2 = userToken2.AvailableQuantity + log2.Quantity
	}
	log1.Balance = quantity1
	log2.Balance = quantity2
	tx := db.DBCli.MustBegin()
	dao.UserTokenDao.UpdateQuantity(tx, quantity1, 0, userToken1.Id)
	if isNew2 {
		userToken2 = new(model.UserTokens)
		userToken2.Uid = log2.Uid
		userToken2.AvailableQuantity = quantity2
		userToken2.TokenId = log2.TokenId
		dao.UserTokenDao.Add(tx, userToken2)
	} else {
		dao.UserTokenDao.UpdateQuantity(tx, quantity2, 0, userToken2.Id)
	}
	dao.FlowRecordDao.Add(tx, log1)
	dao.FlowRecordDao.Add(tx, log2)
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

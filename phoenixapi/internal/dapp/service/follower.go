package service

import (
	"strconv"

	"phoenix/internal/dapp/dao"
	"phoenix/internal/dapp/model"
	"phoenix/pkg/werror"
)

type followService struct{}

var FollowerService = new(followService)

func (*followService) Follow(dappFollower *model.DappFollower) (int64, error) {
	// 获取是否关注
	dappFollow2, err := dao.FollowerDao.GetFollowByUid(dappFollower.Uid, dappFollower.DappId)
	if dappFollow2 != nil && dappFollow2.Status == 1 {
		return 0, werror.New(300, "您已经关注过该app")
	}
	if err != nil {
		return 0, werror.New(300, "系统错误")
	}

	if dappFollow2 != nil {
		err := dao.FollowerDao.HaveFollow(dappFollower.DappId, dappFollower.Uid)
		println(dappFollower.DappId, dappFollower.Uid)
		dao.DappDao.Follow(dappFollower.DappId)
		if err != nil {
			return 0, err
		}
		return dappFollow2.Id, err
	}
	// 如果之前数据不存在
	dappFollower.ReferrerPath = "0"
	if dappFollower.ReferrerUid != 0 {
		referreerFollow, err := dao.FollowerDao.GetFollowByUid(dappFollower.ReferrerUid, dappFollower.DappId)
		if referreerFollow != nil && err == nil {
			dappFollower.ReferrerPath = referreerFollow.ReferrerPath + "_" + strconv.FormatInt(dappFollower.ReferrerUid, 10)
		} else {
			dappFollower.ReferrerPath = strconv.FormatInt(dappFollower.ReferrerUid, 10)
		}
	}

	// 添加到好友数据库
	dappFollower.Status = 1
	id, err := dao.FollowerDao.Follow(dappFollower)
	dao.FollowerDao.AddFollowCount(dappFollower.ReferrerUid)
	dao.DappDao.Follow(dappFollower.DappId)
	if err != nil {
		return 0, err
	}
	return id, err
}

func (*followService) UnFollow(id int64, uid int64) error {
	follow, err := dao.FollowerDao.GetFollowByUid(uid, id)
	if err != nil {
		return werror.New(300, "取消失败，请重试")
	}

	if follow.Uid != uid {
		return werror.New(300, "您无权该操作")
	}
	// 添加到好友数据库
	err = dao.FollowerDao.UnFollow(id, uid)
	dao.DappDao.UnFollow(follow.DappId)
	if err != nil {
		return err
	}
	// 推送好友消息到好友申请服务器

	return err
}

func (*followService) FollowDappList(uid int64) (*[]model.Dapp, error) {

	dapps, err := dao.FollowerDao.GetDappsByFollowerUid(uid)

	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return dapps, err
}

func (*followService) UserReferrer(uid int64, dappId int64) (*model.ReferrerInfo, error) {

	follower1, err := dao.FollowerDao.GetFollowByUid(uid, dappId)

	if err != nil && follower1 == nil {
		return nil, err
	}
	referrer := &model.ReferrerInfo{
		ReferrerUid:    follower1.ReferrerUid,
		ReferrerPath:   follower1.ReferrerPath,
		ReferrerCount:  follower1.ReferrerCount,
		ReferrerCount2: 0,
	}
	if follower1.ReferrerUid != 0 {
		follower2, err := dao.FollowerDao.GetFollowByUid(follower1.ReferrerUid, dappId)
		if err == nil && follower2 != nil {
			referrer.ReferrerCount2 = follower2.ReferrerCount
		}
	}
	return referrer, err
}

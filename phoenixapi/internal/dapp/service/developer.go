package service

import (
	"context"
	"strconv"
	"time"

	"phoenix/pkg/werror"

	"phoenix/internal/dapp/dao"
	"phoenix/internal/dapp/model"
	userService "phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/hash"
	"phoenix/pkg/util"

	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/models"
	"github.com/go-oauth2/oauth2/v4/store"
)

type developerService struct{}

var DeveloperService = new(developerService)

func (*developerService) GenerateLogin() (*model.DappLogin, error) {
	createTime := time.Now().Unix()
	lUuid := hash.GetUuid()
	login := &model.DappLogin{
		CreateTime: createTime,
		LoginUuid:  lUuid,
	}
	id, err := dao.LoginDao.Add(login)
	login.Id = id
	if err != nil {
		return login, err
	}
	// 推送好友消息到好友申请服务器

	return login, nil
}

func (*developerService) LoopLogin(uuid string) (*model.DappLogin, error) {

	dappLogin, err := dao.LoginDao.GetLoginByUuid(uuid)
	if err != nil {
		return dappLogin, err
	}
	// 推送好友消息到好友申请服务器
	return dappLogin, nil
}

func (*developerService) Login(uuid string, code string, uid int64, username string) error {

	//获取token
	data := map[string]string{
		"code":          code,
		"grant_type":    "authorization_code",
		"redirect_uri":  config.AppCfg.DappOauth2.RedirectUri,
		"client_id":     strconv.FormatInt(config.AppCfg.DappOauth2.ID, 10),
		"client_secret": config.AppCfg.DappOauth2.Secret,
	}
	res, err := util.Post("/v1/oauth2/token", data, "user", true)
	// fmt.Printf("%+v", res)
	// fmt.Printf("%+v", err)
	if err != nil {
		return err
	}
	// 写入数据库
	now := time.Now().Unix()
	expire := strconv.FormatFloat(res["expires_in"].(float64), 'g', -1, 64)
	expire2, err := strconv.ParseInt(expire, 10, 64)
	if err != nil {
		return err
	}
	login := &model.DappLogin{
		LoginUuid: uuid,
		Uid:       uid,
		Username:  username,
		Scope:     res["scope"].(string),
		TokenType: res["token_type"].(string),
		Access:    res["access_token"].(string),
		Refresh:   res["refresh_token"].(string),
		ExpiredAt: now + expire2,
	}
	err = dao.LoginDao.Login(login)
	// fmt.Printf("%+v", err)
	if err != nil {
		return err
	}
	return nil
}

func (*developerService) ApplyDeveloper(comment string, uid int64) error {

	user, err := userService.UserService.GetUserInfo(uid)
	if err != nil {
		return err
	}
	data := map[string]interface{}{
		"uid":      user.Uid,
		"username": user.Username,
		"nickname": user.Nickname,
		"avatar":   user.AvatarUrl,
		"comment":  comment,
	}

	_, err = dao.DevelopDao.Apply(data)
	// fmt.Printf("%+v", err)
	if err != nil {
		return err
	}
	return nil
}

func (*developerService) DeveloperInfo(uid int64) (map[string]interface{}, error) {

	developer, err := dao.DevelopDao.GetDeveloperInfo(uid)
	if err != nil {
		return nil, err
	}

	var client oauth2.ClientInfo
	if developer.ClientId > 0 {
		ctx := context.Background()
		clientId := strconv.FormatInt(developer.ClientId, 10)
		clientStore, err := store.NewClientStore()
		client, err = clientStore.GetByID(ctx, clientId)

		if err != nil {
			return nil, err
		}

	}

	data := map[string]interface{}{
		"developer": developer,
		"client":    client,
	}
	return data, nil
}

func (*developerService) ApproveDeveloper(comment string, uid int64, id int64, status int) error {

	err := dao.DevelopDao.ApproveDeveloper(comment, uid, id, status)
	if err != nil {
		return err
	}
	return nil

}

/*
***
dapp
**
*/
func (*developerService) ApplyDapp(dapp *model.Dapp) (int64, error) {

	id, err := dao.DappDao.Apply(dapp)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*developerService) EditDapp(dapp *model.Dapp) (int64, error) {

	err := dao.DappDao.EditDapp(dapp)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return dapp.Id, err
}

func (*developerService) DevelopDappList(uid int64) (*[]model.Dapp, error) {

	// 添加到好友数据库
	var dapps *[]model.Dapp
	dapps, err := dao.DappDao.GetDappsByDevelopUid(uid)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return dapps, err
}

func (*developerService) MarketDappList() (*[]model.Dapp, error) {

	// 添加到好友数据库
	var dapps *[]model.Dapp
	dapps, err := dao.DappDao.MarketDappList()
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return dapps, err
}

func (*developerService) DappPublish(id int64, uid int64, username string) error {
	dapp, err := dao.DappDao.GetDevDappById(id)
	if err != nil {
		return err
	}
	if (*dapp).DeveloperId != uid {
		return werror.New(100, "您无权操作该dapp")
	}
	if (*dapp).Status != 1 {
		return werror.New(100, "该dapp未审核或者被禁用")
	}
	_, err = dao.PaymentDao.GetPaymentByDappid(id)
	if err != nil {
		return werror.New(100, "请先设置支付方式")
	}
	// 添加到好友数据库
	dappFollower := &model.DappFollower{
		DappId:       id,
		Uid:          uid,
		Username:     username,
		ReferrerUid:  0,
		ReferrerPath: "0",
		Status:       1,
	}
	_, err = dao.FollowerDao.Follow(dappFollower)
	if err != nil {
		return werror.New(100, "关注失败")
	}
	dao.DappDao.Follow(id)

	err = dao.DappDao.Publish(id)
	if err != nil {
		return err
	}
	// 推送好友消息到好友申请服务器

	return nil
}

func (*developerService) DappDevelopInfo(id string) (oauth2.ClientInfo, error) {

	ctx := context.Background()
	clientStore, err := store.NewClientStore()
	client, err := clientStore.GetByID(ctx, id)

	if err != nil {
		return nil, err
	}
	return client, nil

}

func (*developerService) ApproveDapp(dapp *model.Dapp) (int64, error) {

	_dapp, err := dao.DappDao.GetDevDappById(dapp.Id)
	if err != nil {
		return 0, err
	}
	if (*_dapp).Status != 0 {
		return 0, werror.New(100, "该dapp已经审核了")
	}
	if _dapp == nil {
		dapp.ClientId = 0
	} else {
		dapp.ClientId = (*_dapp).ClientId
	}

	//审核不通过
	if dapp.Status != 1 {
		id, err := dao.DappDao.Approve(dapp)
		if err != nil {
			return 0, err
		}
		return id, err
	}

	//审核通过
	if dapp.Status == 1 {
		if dapp.ClientId == 0 {
			user, err := userService.UserService.GetUserInfo(_dapp.DeveloperId)
			if err != nil {
				return 0, err
			}
			info := &models.Client{
				Domain:      _dapp.Domain,
				UserID:      user.Uid,
				Username:    user.Username,
				RedirectUrl: _dapp.CallbackUrl,
				Type:        "dapp",
				Scope:       "dapp",
			}
			clientStore, err := store.NewClientStore()
			id, err := clientStore.CreateDappClient(info)
			if err != nil {
				return 0, err
			}
			dapp.ClientId = id
		}
		id, err := dao.DappDao.Approve(dapp)
		if err != nil {
			return 0, err
		}
		return id, err
	}

	return 0, werror.New(100, "审核状态不对")

}

func (*developerService) CleanLoginCode() error {
	err := dao.LoginDao.Clean()
	return err
}

func (*developerService) DappInfo(id int64) (*model.Dapp, error) {

	dapp, err := dao.DappDao.GetDevDappById(id)
	if err != nil {
		return nil, err
	}

	payment, err := dao.PaymentDao.GetPaymentByDappid(id)
	if err == nil {
		dapp.TokenId = payment.TokenId
		dapp.PaymentUid = payment.Uid
	}

	return dapp, nil

}

package model

type DappLogin struct {
	Id         int64  `json:"id"`                           // 代币id
	LoginUuid  string `json:"loginUuid"  db:"login_uuid"`   // 代币名
	Uid        int64  `json:"uid"  db:"uid"`                // 代币英文符号
	Username   string `json:"username" db:"username"`       // 代币图标url
	Scope      string `json:"scope" db:"scope"`             // 代币说明
	TokenType  string `json:"tokenType" db:"token_type"`    // 审核说明
	Access     string `json:"access"  db:"access"`          // 最大数量
	Refresh    string `json:"refresh"  db:"refresh"`        // 申请人uid，只有这个id可以issue
	ExpiredAt  int64  `json:"expiredAt"  db:"expired_at"`   // 代币发行到哪个账户
	CreateTime int64  `json:"createTime"  db:"create_time"` // 创建时间
}

type Developer struct {
	Id             int64  `json:"id"`                                  // 代币id
	Uid            int64  `json:"uid"  db:"uid"`                       // 申请者id
	Username       string `json:"username" db:"username"`              // 申请者用户名
	Nickname       string `json:"nickname" db:"nickname"`              // 申请者昵称
	Avatar         string `json:"avatar" db:"avatar"`                  // 申请者头像
	ApplyComment   string `json:"applyComment" db:"apply_comment"`     // 申请理由
	ApproveComment string `json:"approveComment" db:"approve_comment"` // 审核理由
	ApproveUid     int64  `json:"approveUid"   db:"approve_uid"`       // 代币id
	Status         int    `json:"status"  db:"status"`                 // 状态
	ClientId       int64  `json:"clientId"  db:"client_id"`            // 客户端id
	ApproveTime    int64  `json:"approveTime"  db:"approve_time"`      // 审核时间
	CreateTime     int64  `json:"createTime"  db:"create_time"`        // 创建时间
}

type Dapp struct {
	Id                 int64  `json:"id"`                                  // 代币id
	Name               string `json:"name"  db:"name"`                     // 代币名
	Symbol             string `json:"symbol"  db:"symbol"`                 // 代币英文符号
	Icon               string `json:"icon" db:"icon"`                      // 代币图标url
	Domain             string `json:"domain" db:"domain"`                  // 代币图标url
	CallbackUrl        string `json:"callbackUrl" db:"callback_url"`       // 代币图标url
	Comment            string `json:"comment" db:"comment"`                // 代币说明
	Intro              string `json:"intro" db:"intro"`                    // 申请理由
	ApproveComment     string `json:"approveComment" db:"approve_comment"` // 审核理由
	Status             int    `json:"status"  db:"status"`                 // 状态
	IsPublish          int    `json:"isPublish"  db:"is_publish"`          // 状态
	IsRecommend        int    `json:"isRecommend"  db:"is_recommend"`      // 状态
	Weight             int    `json:"weight"  db:"weight"`                 // 状态
	Follower           int64  `json:"follower"  db:"follower"`             // 关注者
	DeveloperId        int64  `json:"developerId"   db:"developer_id"`     // 代币id
	ApproveUid         int64  `json:"approveUid"   db:"approve_uid"`       // 代币id
	ClientId           int64  `json:"clientId"  db:"client_id"`            // 客户端id
	TokenId            int64  `json:"tokenId" `                            // 客户端id
	IsFollow           int    `json:"isFollow" `                           // 客户端id
	PaymentUid         int64  `json:"paymentUid" `                         // 客户端id
	PaymentUsername    string `json:"paymentUsername" `                    // 客户端id
	PaymentTokenSymbol string `json:"paymentTokenSymbol" `                 // 客户端id
	ApproveTime        int64  `json:"approveTime"  db:"approve_time"`      // 审核时间
	CreateTime         int64  `json:"createTime"  db:"create_time"`        // 创建时间
}

type DappFollower struct {
	Id            int64  `json:"id"`                                 // 代币id
	DappId        int64  `json:"dappId" db:"dapp_id"`                // 代币id
	Uid           int64  `json:"uid"  db:"uid"`                      // 状态
	Username      string `json:"username"  db:"username"`            // 状态
	ReferrerUid   int64  `json:"referrerUid"  db:"referrer_uid"`     // 状态
	ReferrerPath  string `json:"referrerPath"  db:"referrer_path"`   // 审核时间
	ReferrerCount int    `json:"referrerCount"  db:"referrer_count"` // 审核时间
	Status        int    `json:"status"  db:"status"`                // 状态
	CreateTime    int64  `json:"createTime"  db:"create_time"`       // 创建时间
}

type DappPayment struct {
	Id          int64  `json:"id"`                             // 代币id
	DappId      int64  `json:"dappId" db:"dapp_id"`            // 代币id
	Uid         int64  `json:"uid"  db:"uid"`                  // 状态
	Username    string `json:"username"  db:"username"`        // 状态
	TokenId     int64  `json:"tokenId"  db:"token_id"`         // 状态
	TokenSymbol string `json:"tokenSymbol"  db:"token_symbol"` // 审核时间
	Status      int    `json:"status"  db:"status"`            // 状态
	CreateTime  int64  `json:"createTime"  db:"create_time"`   // 创建时间
}

type ReferrerInfo struct {
	ReferrerUid    int64  `json:"referrerUid"  db:"referrer_uid"`     // 状态
	ReferrerPath   string `json:"referrerPath"  db:"referrer_path"`   // 审核时间
	ReferrerCount  int    `json:"referrerCount"  db:"referrer_count"` // 审核时间
	ReferrerCount2 int    `json:"referrerCount2"`                     // 审核时间
}

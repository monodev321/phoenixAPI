package message

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"

	"phoenix/internal/im/model"
	pb "phoenix/internal/im/proto"
	"phoenix/internal/im/tools"
	"phoenix/pkg/config"
	taskProto "phoenix/pkg/proto/task"
	"phoenix/pkg/rpc"
)

func SendGroupMassage(msgType string, msg string, fromuid uint64, touid uint64, sendid uint64) error {
	groupMsg := &model.GroupMsg{
		Type: msgType,
		Msg:  msg,
	}
	//发送群成立消息
	jsonBytes, err := json.Marshal(groupMsg)
	if err != nil {
		return err
	}
	jsonMsg := string(jsonBytes)
	msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	if sendid == uint64(0) {
		sendid = msgid
	}
	header := tools.Message.GenerateHeader(pb.MessageType_MT_MSG, msgid, sendid, fromuid, touid, pb.DeviceType_DEVICE_APP, pb.ContentType_CT_CUSTOM, pb.DispatchType_DT_GROUP, pb.PongType_PT_NULL)
	body := tools.Message.GenerateText(jsonMsg)
	msgBytes, err := tools.Message.Encode(header, body)
	PushToTask(msgBytes[4:])
	return nil
}

func SendPushMassage(msgType string, msg string, fromuid uint64, touid uint64, toType string) error {
	groupMsg := &model.GroupMsg{
		Type: msgType,
		Msg:  msg,
	}
	//发送群成立消息
	jsonBytes, err := json.Marshal(groupMsg)
	if err != nil {
		return err
	}
	jsonMsg := string(jsonBytes)
	msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	_pushType := uint64(0)
	if toType == "group" {
		_pushType = 0
	}
	if toType == "single" {
		_pushType = 1
	}
	if toType == "push" {
		_pushType = 2
	}
	header := tools.Message.GenerateHeader(pb.MessageType_MT_MSG, _pushType, msgid, fromuid, touid, pb.DeviceType_DEVICE_APP, pb.ContentType_CT_CUSTOM, pb.DispatchType_DT_PUSH, pb.PongType_PT_NULL)
	body := tools.Message.GenerateText(jsonMsg)
	msgBytes, err := tools.Message.Encode(header, body)
	PushToTask(msgBytes[4:])
	return nil
}

func SendSingleMassage(msgType string, msg string, fromuid uint64, touid uint64, msgid uint64, sendid uint64) error {
	groupMsg := &model.GroupMsg{
		Type: msgType,
		Msg:  msg,
	}
	//发送群成立消息
	jsonBytes, err := json.Marshal(groupMsg)
	if err != nil {
		return err
	}
	if sendid == uint64(0) {
		sendid = msgid
	}
	jsonMsg := string(jsonBytes)
	header := tools.Message.GenerateHeader(pb.MessageType_MT_MSG, msgid, msgid, fromuid, touid, pb.DeviceType_DEVICE_APP, pb.ContentType_CT_CUSTOM, pb.DispatchType_DT_SINGLE, pb.PongType_PT_NULL)
	body := tools.Message.GenerateText(jsonMsg)
	msgBytes, err := tools.Message.Encode(header, body)
	PushToTask(msgBytes[4:])
	return nil
}

func PushToTask(msg []byte) {
	apiRpcClient := rpc.GetRpcClient(config.AppCfg.Taskservice[0].Tasks[0])
	args := &taskProto.Msg{
		Msg: msg,
	}

	reply := &taskProto.Reply{}
	err := apiRpcClient.Call(context.Background(), "TaskMessage", "Receive", args, reply)
	if err != nil {
		fmt.Printf("failed to call: %v", err)
	}
	fmt.Printf("resulte = %d", reply.C)
}

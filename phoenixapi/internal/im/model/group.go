package model

type Group struct {
	Id            int64  `json:"id"`                                 // client id
	Name          string `json:"name"`                               // 群名称
	Identifier    string `json:"identifier"`                         // 群标示
	Avatar        string `json:"avatar"`                             // 群头像
	Creator       int64  `json:"creator"`                            // 创建者
	Owner         int64  `json:"owner"`                              // 创建者
	Members       int    `json:"members"`                            // 创建者
	Comment       string `json:"comment"`                            // 群介绍
	JoinType      int    `json:"joinType" db:"join_type"`            // 0表示正常，1表示被禁言
	Status        int    `json:"status"`                             // 0表示正常，1表示被封禁，2表示被解散
	IsMute        int    `json:"isMute" db:"is_mute"`                // 0表示正常，1表示被禁言
	IsPrivateChat int    `json:"isPrivateChat" db:"is_private_chat"` // 0表示正常，1表示禁止私聊
	CreateTime    int64  `json:"createTime" db:"create_time"`        // 创建时间
}

type GroupMember struct {
	Id            int64  `json:"id"`                                 // client id
	Gid           int64  `json:"gid"`                                // 群id
	Uid           int64  `json:"uid"`                                // 申请用户id
	Nickname      string `json:"nickname"`                           // 用户昵称
	Username      string `json:"username"`                           // 用户昵称
	Markname      string `json:"markname"`                           // 群里备注名
	Avatar        string `json:"avatar"`                             // 成员头像
	Referrer      int64  `json:"referrer"`                           // 拉进群的人
	Status        int    `json:"status"`                             // 0表示正常，1表示黑名单
	Role          int    `json:"role"`                               // 0表示普通成员，1表示群主，2表示管理员
	JoinType      int    `json:"joinType" db:"join_type"`            // 0表示拉进群，1表示扫码进群
	IsMute        int    `json:"isMute" db:"is_mute"`                // 0表示正常，1表示禁言
	IsNoNotify    int    `json:"isNoNotify" db:"is_no_notify"`       // 0表示正常，1表示不打扰我
	IsTop         int    `json:"isTop" db:"is_top"`                  // 0表示正常，1表示置顶
	IsAddressBook int    `json:"isAddressBook" db:"is_address_book"` // 0表示正常，1表示加入通讯录
	JoinTime      int64  `json:"joinTime" db:"join_time"`            // 加入时间
}

type Gid struct {
	Gid int64 `json:"gid"` // 群id
}

type GroupMemberApply struct {
	Id         int64  `json:"id"`                          // id
	Gid        int64  `json:"gid"`                         // 群id
	Owner      int64  `json:"owner"`                       // 群主uid
	Uid        int64  `json:"uid"`                         // 申请用户id
	Nickname   string `json:"nickname"`                    // 用户昵称
	Username   string `json:"username"`                    // 用户昵称
	Markname   string `json:"markname"`                    // 群里备注名
	Avatar     string `json:"avatar"`                      // 成员头像
	Referrer   int64  `json:"referrer"`                    // 拉进群的人
	JoinType   int    `json:"joinType" db:"join_type"`     // 0表示拉进群，1表示扫码进群
	Comment    string `json:"comment"`                     // 申请备注
	CreateTime int64  `json:"createTime" db:"create_time"` // 申请时间
}

type GroupMsg struct {
	Type string `json:"type"` // client id
	Msg  string `json:"msg"`
}

package model

type Friend struct {
	Id               int64  `json:"id"`                                        // 申请id
	Uid              int64  `json:"uid"`                                       // 用户id
	FriendUid        int64  `json:"friend_uid" db:"friend_uid"`                // 申请人
	FriendUsername   string `json:"friend_username"  db:"friend_username"`     // 用户名
	FriendNickname   string `json:"nickname"  db:"friend_nikcname"`            // 用户昵称
	FriendRemarkname string `json:"friend_remarkname"  db:"friend_remarkname"` // 用户昵称
	FriendAvatar     string `json:"friend_avatar" db:"friend_avatar"`          // 用户头像
	Tel              string `json:"tel" db:"tel"`                              // 用户头像
	Status           int    `json:"status"`                                    // 添加方式
	IsNoNotify       int    `json:"isNoNotify" db:"is_no_notify"`              // 0表示正常，1表示不打扰我
	IsTop            int    `json:"isTop" db:"is_top"`                         // 0表示正常，1表示置顶
	IsAllowWatch     int    `json:"isAllowWatch" db:"is_allow_watch"`          // 0表示正常，1表示置顶
	IsWatch          int    `json:"isWatch" db:"is_watch"`                     // 0表示正常，1表示置顶
	Relation         int    `json:"relation" db:"relation"`                    // 0表示正常，1表示置顶
	Comment          string `json:"comment"`                                   // 申请备注
	CreateTime       int64  `json:"create_time"  db:"create_time"`             // 申请时间
}

type FriendRequest struct {
	Id           int64  `json:"id"`                                // 申请id
	ToUid        int64  `json:"to_uid" db:"to_uid"`                // 申请人
	ToUsername   string `json:"to_username"  db:"to_username"`     // 用户名
	ToNickname   string `json:"to_nickname"  db:"to_nickname"`     // 用户昵称
	ToAvatar     string `json:"to_avatar" db:"to_avatar"`          // 用户头像
	FromUid      int64  `json:"from_uid" db:"from_uid"`            // 申请人
	FromUsername string `json:"from_username"  db:"from_username"` // 用户名
	FromNickname string `json:"from_nickname"  db:"from_nickname"` // 用户昵称
	FromAvatar   string `json:"from_avatar" db:"from_avatar"`      // 用户头像
	AddType      int    `json:"add_type" db:"add_type"`            // 添加方式
	Comment      string `json:"comment"`                           // 申请备注
	CreateTime   int64  `json:"create_time"  db:"create_time"`     // 申请时间
}

type SnsRelation struct {
	ID        int64  `json:"id"`
	CreatedAt uint   `json:"created_at" db:"created_at"`
	UserID    int64  `json:"user_id" db:"user_id"`     //用户ID
	FollowID  int64  `json:"follow_id" db:"follow_id"` //关注的人
	Relation  int64  `json:"relation" db:"relation"`   //关系 0:None, 1:关注, 2: 粉丝, 3:相互关注
	Remark    string `json:"remark" db:"remark"`       //备注名
	IsTop     bool   `json:"is_top" db:"is_top"`       //置顶
	SeeTa     bool   `json:"see_ta" db:"see_ta"`       //看他
	SeeMe     bool   `json:"see_me" db:"see_me"`       //让他看
}

type Feed struct {
	ID         uint   `json:"id" db:"id"`
	CreatedAt  int64  `json:"created_at" db:"created_at"`
	UpdatedAt  int64  `json:"updated_at" db:"updated_at"`
	DeletedAt  int64  `json:"deleted_at" db:"deleted_at"`
	UserID     int64  `json:"user_id" db:"user_id"`
	LikeNum    uint   `json:"like_num" db:"like_num"`
	StarNum    uint   `json:"star_num" db:"star_num"`
	CommentNum uint   `json:"comment_num" db:"comment_num"`
	ShareNum   uint   `json:"share_num" db:"share_num"`
	WatchNum   uint   `json:"watch_num" db:"watch_num"`
	Visible    uint8  `json:"visible" db:"visible"` //public = 0, friend = 1, self = 2
	Cate       string `json:"cate" db:"cate"`
	Tag        string `json:"tag" db:"tag"`
	Location   string `json:"location" db:"location"`
	Intro      string `json:"intro" db:"intro"`
	FeedUrl    string `json:"feed_url" db:"feed_url"`
	CoverUrl   string `json:"cover_url" db:"cover_url"`
	BgmUrl     string `json:"bgm_url" db:"bgm_url"`
}

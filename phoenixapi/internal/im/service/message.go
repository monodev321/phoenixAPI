package service

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"phoenix/internal/im/message"
	"phoenix/internal/im/tools"
	"phoenix/pkg/config"
	"phoenix/pkg/hash"
	taskProto "phoenix/pkg/proto/task"
	"phoenix/pkg/rpc"
	"phoenix/pkg/werror"

	dappDao "phoenix/internal/dapp/dao"
	// pb "phoenix/internal/im/proto"

	"github.com/go-oauth2/oauth2/v4/store"
)

type messageService struct{}

var MessageService = new(messageService)

func (*messageService) Pull(uid string) (*map[string]string, error) {
	taskRpcPath := config.AppCfg.Taskservice[0].Tasks[0]
	rpcClient := rpc.GetRpcClient(taskRpcPath)
	userID, _ := strconv.ParseUint(uid, 10, 64)
	pushMsgReq := &taskProto.HttpPullMessage{
		Uid: userID,
	}
	reply := &taskProto.HttpPullMessageReply{}
	err := rpcClient.Call(context.Background(), "TaskMessage", "HttpPullMessage", pushMsgReq, reply)
	// fmt.Printf("%+v %+v \n", pushMsgReq, reply)
	if err != nil || reply.Code == 500 {
		return nil, werror.New(500, "远程rpc错误")
	}
	if reply.Code == 201 {
		return nil, werror.New(500, "稍后处理")
	}
	return &reply.Messages, nil
}

// 获取好友请求
func (*messageService) Ack(ids []string, uid string) error {
	taskRpcPath := config.AppCfg.Taskservice[0].Tasks[0]
	rpcClient := rpc.GetRpcClient(taskRpcPath)
	userID, _ := strconv.ParseUint(uid, 10, 64)
	pushMsgReq := &taskProto.HttpPullMessageAck{
		Uid: userID,
		Ids: ids,
	}
	reply := &taskProto.HttpPullMessageAckReply{}
	err := rpcClient.Call(context.Background(), "TaskMessage", "HttpPullAck", pushMsgReq, reply)
	if err != nil || reply.Code == 500 {
		return werror.New(500, "远程rpc错误")
	}
	return nil
}

// 获取好友请求
func (*messageService) Push(msgs []string, uid uint64) (map[uint64]uint64, error) {
	data := map[uint64]uint64{}
	for _, msg := range msgs {
		msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		msgByte, err := base64.StdEncoding.DecodeString(msg) // flutter模式
		// msgByte, err := base64.URLEncoding.DecodeString(msg)
		// fmt.Printf("%+v push msg", msgByte)
		header, body, err := tools.Message.Decode(msgByte[4:])
		data[header.GetSendid()] = msgid
		if uid != header.GetFromuid() {
			return map[uint64]uint64{}, werror.New(100, "您没有该权限")
		}
		if err != nil {
			// logrus.Errorf("readPump ReadMessage err:%s", err.Error())
			return map[uint64]uint64{}, err
		}
		header.Msgid = msgid
		msgBytes, err := tools.Message.Encode(header, body)
		message.PushToTask(msgBytes[4:])

	}
	return data, nil
}

// dapp订单消息
func (*messageService) PushSingleMessage(clientId int64, uid int64, dappId int64, msgId int, secret string, title string, detail string) error {
	dapp, err := dappDao.DappDao.GetDevDappById(dappId)
	// 获取 client id,对比权限 secret
	if dapp.ClientId != clientId {
		return werror.New(100, "应用ID不匹配")
	}

	clientStore, err := store.NewClientStore()
	client, err := clientStore.GetByClientID(clientId)
	if hash.Md5(client.GetSecret()) != secret {
		return werror.New(100, "应用权限不对")
	}

	msgs := map[int]string{
		1: "订单金额:%s%s,订单状态:%s,订单号:%s",
	}
	detailJson := make(map[string]string)
	err = json.Unmarshal([]byte(detail), &detailJson)
	if err != nil {
		return werror.New(100, "消息体编译不正确")
	}
	var msgDetail string
	switch msgId {
	case 1:
		msgDetail = fmt.Sprintf(msgs[1], detailJson["amount"], detailJson["symbol"], detailJson["status"], detailJson["orderId"])
	default:
		return werror.New(100, "消息类型不存在")
	}
	msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	var msgJson = map[string]interface{}{
		"dappId":   dapp.Id,
		"dappName": dapp.Name,
		"dappIcon": dapp.Icon,
		"title":    title,
		"detail":   msgDetail,
		"time":     time.Now().Unix(),
		"type":     25,
	}
	jsonBytes, err := json.Marshal(msgJson)
	if err != nil {
		return err
	}
	msg := string(jsonBytes)
	message.SendSingleMassage("dappPushMsg", msg, uint64(dapp.DeveloperId), uint64(uid), msgid, 0)

	return nil
}

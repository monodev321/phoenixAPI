package service

import (
	"phoenix/internal/im/dao"
	"phoenix/internal/im/message"
	"phoenix/internal/im/model"
	"phoenix/pkg/werror"
)

type groupApplyService struct{}

var GroupApplyService = new(groupApplyService)

func (*groupApplyService) GroupApplyList(uid int64) (*[]model.GroupMemberApply, error) {

	applys, err := dao.MemberRequestDao.List(uid)
	if err != nil {
		return nil, err
	}
	return applys, nil
}
func (*groupApplyService) GroupApplyDeal(uid int64, id int64, status int) error {

	apply, err := dao.MemberRequestDao.Apply(id)
	if err != nil {
		return err
	}

	if apply.Owner != uid {
		return werror.New(100, "您没有权限")
	}

	if status == 0 {
		dao.MemberRequestDao.Delete(id)
	}
	member := &model.GroupMember{
		Uid:      apply.Uid,
		Nickname: apply.Nickname,
		Username: apply.Username,
		Avatar:   apply.Avatar,
	}
	members := []*model.GroupMember{member}
	msg, err := MemberService.AddMembersToGroup(members, apply.Gid, apply.Referrer, apply.JoinType)
	if err != nil {
		return err
	}
	message.SendGroupMassage("allowGroup", msg, uint64(uid), uint64(apply.Gid), 0)
	dao.MemberRequestDao.Delete(id)
	return nil
}

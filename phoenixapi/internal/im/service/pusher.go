package service

import (
	"strconv"

	"phoenix/pkg/db"
)

type pusherService struct{}

var PusherService = new(pusherService)

func (*pusherService) GetUidsForPush(gid int64) (string, error) {

	field := strconv.FormatInt(gid, 10)
	key := "push_members_" + field

	uidsStr, err := db.ApiLevelDB.Get(key)
	if uidsStr != "" && err == nil {
		db.ApiLevelDB.Del(key)
		return uidsStr, nil
	}
	return "", err
}

package service

import (
	"sort"
	"strconv"
	"strings"
	"time"

	"phoenix/pkg/db"
	"phoenix/pkg/hash"
	"phoenix/pkg/werror"

	"phoenix/internal/im/dao"
	"phoenix/internal/im/message"
	"phoenix/internal/im/model"
	"phoenix/internal/im/tools"
)

type memberService struct{}

var MemberService = new(memberService)

// 添加好友请求
func (*memberService) RemoveGroupMembers(uids []int64, gid int64, uid int64) error {
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}
	if group.Owner != uid {
		return werror.New(100, "您不是群管理")
	}

	err = dao.GroupDao.ReduceGroupMembers(gid, len(uids))
	if err != nil {
		return werror.New(100, "减少群成员失败")
	}

	err = dao.MemberDao.Remove(gid, uids)
	if err != nil {
		return werror.New(100, "删除失败")
	}

	members, err := dao.MemberDao.GetUidsByGid(gid)
	if err != nil {
		return err
	}
	memberUids := make([]string, 0, len(*members))
	for _, member := range *members {
		memberUids = append(memberUids, strconv.FormatInt(member.Uid, 10))
	}
	sort.Sort(sort.StringSlice(memberUids))
	identifier := strings.Join(memberUids, ",")
	field := strconv.FormatInt(gid, 10)
	key := "group_members_" + field
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	if err != nil {
		return err
	}

	err = dao.GroupDao.Setting(gid, "identifier", hash.Md5(identifier))
	if err != nil {
		return err
	}

	// todo 发送群消息
	message.SendGroupMassage("removeGroup", strconv.FormatInt(gid, 10), uint64(uid), uint64(gid), 0)

	msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	for _, memberuid := range uids {
		message.SendSingleMassage("removeYouGroup", strconv.FormatInt(gid, 10), uint64(uid), uint64(memberuid), msgid2, 0)
	}

	return nil
}

// 添加好友请求
func (*memberService) ExitGroupMember(gid int64, uid int64) error {
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}

	err = dao.GroupDao.ReduceGroupMembers(gid, 1)
	if err != nil {
		return werror.New(100, "减少群成员失败")
	}

	uids := []int64{uid}
	err = dao.MemberDao.Remove(gid, uids)
	if err != nil {
		return werror.New(100, "退出失败")
	}

	members, err := dao.MemberDao.GetUidsByGid(gid)
	if err != nil {
		return err
	}
	memberUids := make([]string, 0, len(*members))
	for _, member := range *members {
		memberUids = append(memberUids, strconv.FormatInt(member.Uid, 10))
	}
	sort.Sort(sort.StringSlice(memberUids))
	identifier := strings.Join(memberUids, ",")
	field := strconv.FormatInt(gid, 10)
	key := "group_members_" + field
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	if err != nil {
		return err
	}

	err = dao.GroupDao.Setting(gid, "identifier", hash.Md5(identifier))
	if err != nil {
		return err
	}
	msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	message.SendSingleMassage("exitOwnerGroup", strconv.FormatInt(gid, 10), uint64(uid), uint64(group.Owner), msgid2, 0)
	// todo 发送群消息
	message.SendGroupMassage("exitGroup", strconv.FormatInt(gid, 10), uint64(group.Owner), uint64(gid), 0)
	return nil
}

// 添加好友请求
func (*memberService) AddGroupMembers(members []*model.GroupMember, gid int64, uid int64) error {
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}
	// 判断邀请者是否在群里
	_, err = dao.MemberDao.MemberByGidAndUid(gid, uid)
	if err != nil {
		return werror.New(100, "您不再该群")
	}

	if group.Owner == uid || group.JoinType == 1 {
		// 直接拉人入群
		msg, err := MemberService.AddMembersToGroup(members, gid, uid, 2)
		if err != nil {
			return err
		}
		message.SendGroupMassage("addGroup", msg, uint64(uid), uint64(gid), 0)
		return nil
	}

	if group.JoinType == 2 {
		return werror.New(100, "该群仅允许群主加人")
	}
	if group.JoinType == 0 {
		// 加入申请列表
		valueStrings := make([]string, 0, len(members))
		valueArgs := make([]interface{}, 0, len(members)*10)
		createTime := time.Now().Unix()
		for _, member := range members {
			valueStrings = append(valueStrings, "(?, ?,?, ?,?, ?,?, ?,?,?)")
			valueArgs = append(valueArgs, gid)
			valueArgs = append(valueArgs, member.Uid) //
			valueArgs = append(valueArgs, member.Nickname)
			valueArgs = append(valueArgs, member.Username)
			valueArgs = append(valueArgs, member.Avatar)
			valueArgs = append(valueArgs, uid) //referrer
			valueArgs = append(valueArgs, 2)
			valueArgs = append(valueArgs, "好友邀请")
			valueArgs = append(valueArgs, group.Owner)
			valueArgs = append(valueArgs, createTime)
		}
		err = dao.MemberRequestDao.Add(valueStrings, valueArgs)
		if err != nil {
			return err
		}

		// 给群主 发送通知，有人申请
		msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		if err != nil {
			return err
		}
		message.SendSingleMassage("applyGroup", strconv.FormatInt(group.Id, 10), uint64(uid), uint64(group.Owner), msgid, 0)
		return nil
	}

	return nil
}

// 添加好友请求
func (*memberService) JoinGroupMembers(members []*model.GroupMember, gid int64, referrer int64, uid int64) error {
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}

	if group.Owner == referrer || group.JoinType == 1 {
		// 直接拉人入群
		msg, err := MemberService.AddMembersToGroup(members, gid, referrer, 1)
		if err != nil {
			return err
		}
		message.SendGroupMassage("addGroup", msg, uint64(uid), uint64(gid), 0)
		return nil
	}

	if group.JoinType == 2 {
		return werror.New(100, "该群仅允许群主加人")
	}
	if group.JoinType == 0 {
		// 加入申请列表
		valueStrings := make([]string, 0, len(members))
		valueArgs := make([]interface{}, 0, len(members)*10)
		createTime := time.Now().Unix()
		for _, member := range members {
			valueStrings = append(valueStrings, "(?, ?,?, ?,?, ?,?, ?,?,?)")
			valueArgs = append(valueArgs, gid)
			valueArgs = append(valueArgs, member.Uid) //
			valueArgs = append(valueArgs, member.Nickname)
			valueArgs = append(valueArgs, member.Username)
			valueArgs = append(valueArgs, member.Avatar)
			valueArgs = append(valueArgs, referrer) //referrer
			valueArgs = append(valueArgs, 1)
			valueArgs = append(valueArgs, "扫码加入")
			valueArgs = append(valueArgs, group.Owner)
			valueArgs = append(valueArgs, createTime)
		}
		err = dao.MemberRequestDao.Add(valueStrings, valueArgs)
		if err != nil {
			return err
		}

		// 给群主 发送通知，有人申请
		msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		if err != nil {
			return err
		}
		message.SendSingleMassage("applyGroup", strconv.FormatInt(group.Id, 10), uint64(uid), uint64(group.Owner), msgid, 0)
		return nil
	}

	return nil
}

func (*memberService) AddMembersToGroup(members []*model.GroupMember, gid int64, referrer int64, joinType int) (string, error) {
	// 直接拉人入群
	mlen := len(members)
	valueStrings := make([]string, 0, mlen)
	valueArgs := make([]interface{}, 0, mlen*10)
	createTime := time.Now().Unix()
	msg := ""
	for _, member := range members {
		role := 0
		isAddressBook := 0
		msg = msg + member.Nickname + ","
		valueStrings = append(valueStrings, "(?, ?,?, ?,?, ?,?, ?,?,?)")
		valueArgs = append(valueArgs, gid)
		valueArgs = append(valueArgs, member.Uid) //
		valueArgs = append(valueArgs, member.Nickname)
		valueArgs = append(valueArgs, member.Username)
		valueArgs = append(valueArgs, member.Avatar)
		valueArgs = append(valueArgs, referrer) //referrer
		valueArgs = append(valueArgs, role)
		valueArgs = append(valueArgs, joinType) //join_type 0创建，1扫码，2邀请
		valueArgs = append(valueArgs, isAddressBook)
		valueArgs = append(valueArgs, createTime)
	}
	err := dao.MemberDao.Add(valueStrings, valueArgs)
	if err != nil {
		return msg, err
	}
	//修改群成员
	dao.GroupDao.AddGroupMembers(gid, mlen)
	members2, err := dao.MemberDao.GetUidsByGid(gid)
	if err != nil {
		return msg, err
	}
	memberUids := make([]string, 0, len(*members2))
	for _, member := range *members2 {
		memberUids = append(memberUids, strconv.FormatInt(member.Uid, 10))
	}
	sort.Sort(sort.StringSlice(memberUids))
	identifier := strings.Join(memberUids, ",")

	//把uid放入到redis
	field := strconv.FormatInt(gid, 10)
	key := "group_members_" + field
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	if err != nil {
		return msg, err
	}

	msg = msg[0:(len(msg) - 1)]
	return msg, nil
}

func (*memberService) MuteMember(isMute string, uid int64, gid int64, ownwer int64) error {
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}
	if group.Owner != ownwer {
		return werror.New(100, "您不是群主，不能转让群")
	}
	err = dao.MemberDao.MuteMember(isMute, uid, gid)
	msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	if err != nil {
		return err
	}
	msg := "您被群主禁言"
	if isMute == "0" {
		msg = "群主取消您的禁言"
	}
	message.SendSingleMassage("muteMemberGroup", msg, uint64(group.Id), uint64(uid), msgid, 0)

	return nil
}

func (*memberService) MarknameMember(marknme string, gid int64, uid int64) error {

	err := dao.MemberDao.MarknameMember(marknme, uid, gid)
	if err != nil {
		return err
	}
	message.SendGroupMassage("updateMemberGroup", "", uint64(uid), uint64(gid), 0)

	return nil
}

func (*memberService) MemberSetting(gid int64, field string, val string, uid int64) error {

	err := dao.MemberDao.Setting(gid, field, val, uid)
	if err != nil {
		return err
	}
	return nil
}

func (*memberService) GroupMember(gid int64, uid int64) (*model.GroupMember, error) {

	member, err := dao.MemberDao.MemberByGidAndUid(gid, uid)
	if err != nil {
		return nil, err
	}
	return member, nil
}

package service

import (
	"strconv"
	"strings"

	"phoenix/internal/im/dao"
	"phoenix/internal/im/message"
	"phoenix/internal/im/model"
	"phoenix/internal/im/tools"
	userDao "phoenix/internal/user/dao"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type friendService struct{}

var FriendService = new(friendService)

// 添加好友请求
func (*friendService) AddFriendRequest(freq *model.FriendRequest) (int64, error) {
	friend, _ := dao.FriendDao.FriendByUid(freq.FromUid, freq.ToUid)
	if friend != nil && friend.Status == 0 {
		return 0, nil
	}
	// 添加到好友数据库
	id, err := dao.FriendRequestDao.Add(freq)
	if err != nil {
		return 0, err
	}

	if id == -1 {
		res, err := dao.FriendRequestDao.GetFriendRequestByUid(freq.ToUid, freq.FromUid)
		if err != nil {
			return 0, err
		}
		id = res.Id
	}
	// 推送好友消息到好友申请服务器
	return id, err
}

// 获取好友请求
func (*friendService) FriendRequestList(uid int64) (*[]model.FriendRequest, error) {

	// 添加到好友数据库
	var freqs *[]model.FriendRequest
	freqs, err := dao.FriendRequestDao.List(uid)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return freqs, err
}

// 获取好友请求
func (*friendService) FriendList(uid int64) (*[]model.Friend, error) {

	// 添加到好友数据库
	var friends *[]model.Friend
	friends, err := dao.FriendDao.List(uid)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return friends, err
}

// 获取好友请求
func (*friendService) FriendByUid(fuid int64, uid int64) (*model.Friend, error) {

	friend, err := dao.FriendDao.FriendByUid(fuid, uid)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return friend, err
}

// 添加好友请求
func (*friendService) Setting(fid int64, field string, val string, uid int64) error {
	if field == "friend_remarkname" {
		friend, err := userDao.UserDao.GetUserInfo(fid)
		if err != nil {
			return err
		}
		err = dao.FriendDao.UpdateMarkname(fid, uid, friend.AvatarUrl, friend.Nickname, val)
		if err != nil {
			return err
		}
		return nil
	}
	err := dao.FriendDao.Setting(fid, uid, field, val)
	if err != nil {
		return err
	}
	return nil
}

// 添加好友请求
func (*friendService) Recover(fid int64, uid int64) error {
	tx := db.DBCli.MustBegin()
	dao.FriendDao.Status(tx, fid, uid, 0)
	dao.FriendDao.Status(tx, uid, fid, 0)
	err := tx.Commit()

	if err != nil {
		return err
	}
	msg := strconv.FormatInt(uid, 10)
	err = message.SendPushMassage("recoverFriend", msg, uint64(uid), uint64(fid), "single")
	if err != nil {
		return err
	}
	return nil
}

// 添加好友请求
func (*friendService) Delete(fid int64, uid int64) error {
	tx := db.DBCli.MustBegin()
	dao.FriendDao.Status(tx, fid, uid, 3)
	dao.FriendDao.Status(tx, uid, fid, 4)
	err := tx.Commit()
	if err != nil {
		return err
	}
	msg := strconv.FormatInt(uid, 10)
	err = message.SendPushMassage("deleteFriend", msg, uint64(uid), uint64(fid), "single")
	if err != nil {
		return err
	}
	return nil
}

// 通过或者拒绝好友请求
func (*friendService) HandleFriendRequest(uid int64, freq_id int64, isPass bool) error {
	freq, err := dao.FriendRequestDao.GetFriendRequest(freq_id)
	if err != nil {
		return err
	}
	if freq == nil {
		return nil
	}
	if freq.ToUid != uid {
		return werror.New(200, "该用户没有权限")
	}
	// 删除请求
	err = dao.FriendRequestDao.Delete(freq_id)
	if err != nil {
		return werror.New(200, "添加好友失败")
	}
	if isPass {
		friend1 := &model.Friend{
			Uid:              freq.ToUid,
			FriendUid:        freq.FromUid,
			FriendUsername:   freq.FromUsername,
			FriendRemarkname: freq.FromNickname,
			FriendNickname:   freq.FromNickname,
			FriendAvatar:     freq.FromAvatar,
		}
		_, err := dao.FriendDao.Add(friend1)
		if err != nil {
			return err
		}
		friend2 := &model.Friend{
			Uid:              freq.FromUid,
			FriendUid:        freq.ToUid,
			FriendUsername:   freq.ToUsername,
			FriendRemarkname: freq.ToNickname,
			FriendNickname:   freq.ToNickname,
			FriendAvatar:     freq.ToAvatar,
		}
		_, err = dao.FriendDao.Add(friend2)
		if err != nil {
			return err
		}
	}
	return nil
}

// 通过或者拒绝好友请求
func (*friendService) RecommendFriend(members []int64, msg string, uid int64) error {
	uids := make([]string, 0, len(members))
	for _, member := range members {
		uids = append(uids, strconv.FormatInt(member, 10))
	}
	identifier := strings.Join(uids, ",")

	toid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	//把uid放入到redis
	field := strconv.FormatUint(toid, 10)
	key := "push_members_" + field
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	if err != nil {
		return err
	}

	if err != nil {
		return err
	}
	err = message.SendPushMassage("recommendUser", msg, uint64(uid), toid, "push")
	return err
}

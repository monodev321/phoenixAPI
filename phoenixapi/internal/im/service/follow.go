package service

import (
	"context"
	"errors"
	"fmt"
	"phoenix/app/types/proto"
	"phoenix/internal/im/dao"
	"phoenix/internal/im/model"
	userDao "phoenix/internal/user/dao"
	"phoenix/pkg/db"
	"strconv"
)

type followService struct{}

var FollowService = new(followService)

// 添加好友请求
func (*followService) Follow(uid int64, fuid int64, undo bool) error {
	// 推送好友消息到好友申请服务器
	user, _ := userDao.UserDao.GetUserInfo(fuid)
	if user == nil {
		return errors.New("好友不存在")
	}
	friend, _ := dao.FriendDao.FriendByUid(fuid, uid)
	//取消关注
	if undo {
		if friend == nil {
			return errors.New("还没有关注")
		}
		dao.FriendDao.Remove(friend.Id)
		friend2, _ := dao.FriendDao.FriendByUid(uid, fuid)
		//更新好友关系
		if friend2 != nil && friend2.Relation == 2 {
			_ = dao.FollowDao.UpdateRelation(friend2.Id, 1)
		}
	} else {
		if friend != nil {
			return errors.New("您已经关注该用户")
		}
		friend2, err := dao.FriendDao.FriendByUid(uid, fuid)
		if err != nil {
			return err
		}
		var relation int = 1
		if friend2 != nil && friend2.Relation > 0 {
			relation = 2
		}
		f := &model.Friend{
			Uid:              uid,
			FriendUid:        fuid,
			FriendUsername:   user.Username,
			FriendRemarkname: user.Nickname,
			FriendNickname:   user.Nickname,
			FriendAvatar:     user.AvatarUrl,
			Relation:         relation,
		}
		_, err = dao.FollowDao.Follow(f)
		if err != nil {
			return err
		}
		//更新好友关系
		if friend2 != nil && friend2.Relation == 1 {
			_ = dao.FollowDao.UpdateRelation(friend2.Id, 2)
		}
	}

	//增加关注人数

	var change int = 1
	if undo {
		change = -1
	}
	userDao.UserDao.UpdateColumn(uid, "follow_num", change)
	userDao.UserDao.UpdateColumn(fuid, "follower_num", change)
	return nil
}

func (*followService) Settings(fid int64, sqlstr string, uid int64) error {
	err := dao.FollowDao.Settings(fid, uid, sqlstr)
	if err != nil {
		return err
	}
	return nil
}

func (*followService) FollowList(uid uint, limit int, offset int) (*[]model.SnsRelation, error) {
	var followlist *[]model.SnsRelation
	followlist, err := dao.FollowDao.FollowList(uid, limit, offset)
	if err != nil {
		return nil, err
	}
	return followlist, err
}

func (*followService) FollowerList(uid uint, limit int, offset int) (*[]model.SnsRelation, error) {
	var followerlist *[]model.SnsRelation
	followerlist, err := dao.FollowDao.FollowerList(uid, limit, offset)
	if err != nil {
		return nil, err
	}
	return followerlist, err
}

func (*followService) GetFollowNewest(selfId int64, limit int, offset int) (*[]model.Feed, error) {
	var follownewest *[]model.Feed
	follownewest, err := dao.FollowDao.GetFollowNewest(selfId, limit, offset)
	if err != nil {
		return nil, err
	}
	return follownewest, err
}
func (*followService) FillUserStared(selfId int64, list []*proto.UserFeed1) error {
	if selfId == 0 || len(list) == 0 {
		return nil
	}
	ctx := context.Background()
	key := fmt.Sprintf("feed_star:%d", selfId)
	members := make([]string, 0, len(list))
	for _, v := range list {
		members = append(members, strconv.Itoa(int(v.Feed.ID)))
	}
	rst := db.Redis.ZMScore(ctx, key, members...)
	if rst.Err() != nil {
		return rst.Err()
	}
	for k, v := range rst.Val() {
		if v > 0 {
			list[k].Stared = true
		} else {
			list[k].Stared = false
		}
	}
	return nil
}

func (*followService) FillUserLiked(selfId int64, list []*proto.UserFeed1) error {
	if selfId == 0 || len(list) == 0 {
		return nil
	}
	ctx := context.Background()
	key := fmt.Sprintf("feed_like:%d", selfId)
	members := make([]string, 0, len(list))
	for _, v := range list {
		members = append(members, strconv.Itoa(int(v.Feed.ID)))
	}
	rst := db.Redis.ZMScore(ctx, key, members...)
	if rst.Err() != nil {
		return rst.Err()
	}
	for k, v := range rst.Val() {
		if v > 0 {
			list[k].Liked = true
		} else {
			list[k].Liked = false
		}
	}
	return nil
}

func (*followService) SearchUserFollow(uid uint, keywd string, offset int, limit int) (*[]model.SnsRelation, error) {
	var searchuser *[]model.SnsRelation
	searchuser, err := dao.FollowDao.SearchUserFollow(uid, keywd, offset, limit)
	if err != nil {
		return nil, err
	}
	return searchuser, err
}

func (*followService) GetFriendNewest(userId uint, limit int, offset int) (*[]model.Feed, error) {
	var friendnewest *[]model.Feed
	friendnewest, err := dao.FollowDao.GetFriendNewest(userId, limit, offset)
	if err != nil {
		return nil, err
	}
	return friendnewest, err
}

func (*followService) GetUserLike(userId uint, start int64, stop int64) ([]*model.Feed, error) {
	ctx := context.Background()
	key := fmt.Sprintf("feed_like:%d", userId)
	rst, err := db.Redis.ZRange(ctx, key, start, stop).Result()
	fmt.Println(rst)
	if err != nil {
		return nil, err
	}
	var userlike []*model.Feed
	if len(rst) > 0 {
		for i := range rst {
			id, err := strconv.Atoi(rst[i])
			if err != nil {
				return nil, err
			}
			req, err := dao.FollowDao.GetUserLike(id)
			userlike = append(userlike, req)
		}
	}
	return userlike, err
}

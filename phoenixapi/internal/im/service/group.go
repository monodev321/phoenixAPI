package service

import (
	"encoding/json"
	"math/rand"
	"sort"
	"strconv"
	"strings"
	"time"

	"phoenix/pkg/db"
	"phoenix/pkg/hash"
	"phoenix/pkg/werror"

	"phoenix/internal/im/dao"
	"phoenix/internal/im/message"
	"phoenix/internal/im/model"
	pb "phoenix/internal/im/proto"
	"phoenix/internal/im/tools"
	"phoenix/pkg/config"
)

type groupService struct{}

var GroupService = new(groupService)

// 添加好友请求
func (*groupService) CreateGroup(members []*model.GroupMember, uid int64) (*model.Group, error) {
	//@todo 先判断是否存在一样的群
	isOwer := false
	uids := make([]string, 0, len(members))
	owerName := ""
	for _, member := range members {
		if member.Uid == uid {
			isOwer = true
			owerName = member.Nickname
		}
		uids = append(uids, strconv.FormatInt(member.Uid, 10))
	}
	sort.Sort(sort.StringSlice(uids))
	if !isOwer {
		return nil, werror.New(100, "群主不在群成员列表")
	}
	identifier := strings.Join(uids, ",")
	// 3-40 4-74 9-36
	avatar := ""
	if len(members) > 6 {
		avatar = "9-" + strconv.Itoa(rand.Intn(36)+1)
	}

	if 3 < len(members) && len(members) < 7 {
		avatar = "4-" + strconv.Itoa(rand.Intn(74)+1)
	}

	if 3 == len(members) {
		avatar = "3-" + strconv.Itoa(rand.Intn(40)+1)
	}
	createTime := time.Now().Unix()
	group, err := dao.GroupDao.GetGroupByidentifier(hash.Md5(identifier))
	if group != nil {
		return group, err
	}

	group = &model.Group{
		Name:          "群聊",
		Identifier:    hash.Md5(identifier),
		Creator:       uid,
		Owner:         uid,
		Comment:       config.AppCfg.Signature,
		Status:        0,
		IsPrivateChat: 1,
		JoinType:      0,
		IsMute:        0,
		Members:       len(members),
		CreateTime:    createTime,
		Avatar:        config.AppCfg.AvatarGroupBase + avatar + ".jpg",
	}
	gid, err := dao.GroupDao.Add(group)
	if err != nil && gid != -1 {
		return nil, err
	}
	group.Id = gid

	valueStrings := make([]string, 0, len(members))
	valueArgs := make([]interface{}, 0, len(members)*10)

	for _, member := range members {
		role := 0
		isAddressBook := 0
		if member.Uid == uid {
			role = 1
			isAddressBook = 1
		}
		valueStrings = append(valueStrings, "(?, ?,?, ?,?, ?,?, ?,?,?)")
		valueArgs = append(valueArgs, gid)
		valueArgs = append(valueArgs, member.Uid) //
		valueArgs = append(valueArgs, member.Nickname)
		valueArgs = append(valueArgs, member.Username)
		valueArgs = append(valueArgs, member.Avatar)
		valueArgs = append(valueArgs, uid) //referrer
		valueArgs = append(valueArgs, role)
		valueArgs = append(valueArgs, 0) //join_type 0创建，1扫码，2群主邀请，3他人邀请
		valueArgs = append(valueArgs, isAddressBook)
		valueArgs = append(valueArgs, createTime)
	}
	err = dao.MemberDao.Add(valueStrings, valueArgs)
	if err != nil {
		return nil, err
	}

	//把uid放入到redis
	field := strconv.FormatInt(gid, 10)
	key := "group_members_" + field
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	if err != nil {
		return nil, err
	}
	groupMsg := &model.GroupMsg{
		Type: "createGroup",
		Msg:  owerName,
	}
	//发送群成立消息
	jsonBytes, err := json.Marshal(groupMsg)
	if err != nil {
		return nil, err
	}
	jsonMsg := string(jsonBytes)
	msgid, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
	header := tools.Message.GenerateHeader(pb.MessageType_MT_MSG, msgid, msgid, uint64(uid), uint64(gid), pb.DeviceType_DEVICE_APP, pb.ContentType_CT_CUSTOM, pb.DispatchType_DT_GROUP, pb.PongType_PT_NULL)
	body := tools.Message.GenerateText(jsonMsg)
	msgBytes, err := tools.Message.Encode(header, body)
	message.PushToTask(msgBytes[4:])
	return group, nil
}

// 添加好友请求
func (*groupService) GetUidsByGid(gid int64) (string, error) {

	//@todo 先判断是否存在一样的群
	field := strconv.FormatInt(gid, 10)
	key := "group_members_" + field
	uidsStr, err := db.ApiLevelDB.Get(key)
	if uidsStr != "" && err == nil {
		return uidsStr, nil
	}

	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return "", err
	}
	if group.Status == 2 {
		// _, err = db.RedisCli.HSet(key, field, "-").Result()
		err = db.ApiLevelDB.Add(key, []byte("-"))
		return "-", nil
	}

	members, err := dao.MemberDao.GetUidsByGid(gid)
	if err != nil {
		return "", err
	}
	uids := make([]string, 0, len(*members))
	for _, member := range *members {
		uids = append(uids, strconv.FormatInt(member.Uid, 10))
	}
	sort.Sort(sort.StringSlice(uids))
	identifier := strings.Join(uids, ",")
	// _, err = db.RedisCli.HSet(key, field, identifier).Result()
	err = db.ApiLevelDB.Add(key, []byte(identifier))
	return identifier, nil
}

func (*groupService) GetUidsForPush(gid int64) (string, error) {

	members, err := dao.MemberDao.GetUidsByGid(gid)
	if err != nil {
		return "", err
	}
	uids := make([]string, 0, len(*members))
	for _, member := range *members {
		uids = append(uids, strconv.FormatInt(member.Uid, 10))
	}
	identifier := strings.Join(uids, ",")
	return identifier, nil
}

func (*groupService) GroupMembersByGid(gid int64, uid int64) (*map[string]interface{}, error) {
	// member, err := dao.MemberDao.MemberByGidAndUid(gid, uid)

	// if err != nil || member == nil {
	// 	return &map[string]interface{}{}, werror.New(100, "您不在该群组")
	// }
	members, err := dao.MemberDao.MembersByGid(gid)
	if err != nil {
		return &map[string]interface{}{}, err
	}
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return &map[string]interface{}{}, err
	}
	data := map[string]interface{}{"group": group, "members": members}
	return &data, nil
}

func (*groupService) GroupMembersByGid2(gid int64, uid int64) (*map[string]interface{}, error) {
	// member, err := dao.MemberDao.MemberByGidAndUid(gid, uid)

	// if err != nil || member == nil {
	// 	return &map[string]interface{}{}, werror.New(100, "您不在该群组")
	// }
	members, err := dao.MemberDao.MembersByGid(gid)
	if err != nil {
		return nil, err
	}

	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return nil, err
	}
	isUpdate := 0
	if group.Members != len(*members) {
		dao.GroupDao.UpdateMembers(gid, len(*members))
		isUpdate = 1
	}
	data := map[string]interface{}{"isUpdate": isUpdate, "members": members}
	return &data, nil
}

func (*groupService) DismissGroup(gid int64, uid int64) error {
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}
	if group.Owner != uid {
		return werror.New(100, "您不是群管理")
	}
	err = dao.GroupDao.Setting(gid, "status", "2")
	if err != nil {
		return err
	}
	field := strconv.FormatInt(gid, 10)
	key := "group_members_" + field
	// _, err = db.RedisCli.HSet(key, field, "-").Result()
	err = db.ApiLevelDB.Add(key, []byte("-"))
	if err != nil {
		return err
	}
	msg := "群主解散了本群"
	err = message.SendPushMassage("dismissGroup", msg, uint64(uid), uint64(gid), "group")
	if err != nil {
		return err
	}
	return nil
}

func (*groupService) GroupByGid(gid int64, uid int64) (*model.Group, error) {
	// member, err := dao.MemberDao.MemberByGidAndUid(gid, uid)
	// if err != nil || member == nil {
	// 	return nil, werror.New(100, "您不在该群组")
	// }
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return nil, err
	}
	return group, nil
}

func (*groupService) GroupByGid2(gid int64, uid int64) (*map[string]interface{}, error) {
	member, err := dao.MemberDao.MemberByGidAndUid(gid, uid)
	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return nil, err
	}
	g := map[string]interface{}{
		"gid":           group.Id,
		"name":          group.Name,
		"identifier":    group.Identifier,
		"avatar":        group.Avatar,
		"comment":       group.Comment,
		"joinType":      group.JoinType,
		"owner":         group.Owner,
		"status":        group.Status,
		"isMute":        group.IsMute,
		"isPrivateChat": group.IsPrivateChat,
		"createTime":    group.CreateTime,
		"memberNum":     group.Members,
		"isAddressBook": member.IsAddressBook,
		"isTop":         member.IsTop,
		"isMeMute":      member.IsMute,
		"isMeNoNotify":  member.IsNoNotify,
	}
	return &g, nil
}

// 添加好友请求
func (*groupService) GroupSetting(gid int64, field string, val string, uid int64) error {

	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}
	if group.Owner != uid {
		return werror.New(100, "您不是群管理")
	}
	err = dao.GroupDao.Setting(gid, field, val)
	if err != nil {
		return err
	}
	msg := "群主修改了群设置"
	switch field {
	case "join_type":
		msg = "群主修改了群加群方式"
	case "avatar":
		msg = "群主修改了群头像"
	case "comment":
		msg = "群主修改了群简介"
	case "name":
		msg = "群主修改了群名称"
	case "is_private_chat":
		if val == "1" {
			msg = "群主打开了私聊"
			break
		}
		msg = "群主设置了禁止私聊"
	case "is_mute":
		if val == "0" {
			msg = "群主取消了禁言"
			break
		}
		msg = "群主设置了禁言"
	}
	err = message.SendGroupMassage("settingGroup", msg, uint64(uid), uint64(gid), 0)
	if err != nil {
		return err
	}
	return nil
}

// 添加好友请求
func (*groupService) AssiganGroup(gid int64, owner int64, nickname string, uid int64) error {

	group, err := dao.GroupDao.GroupByGid(gid)
	if err != nil {
		return werror.New(100, "设置的群组不存在")
	}
	if group.Owner != uid {
		return werror.New(100, "您不是群主，不能转让群")
	}
	// err = dao.GroupDao.AssiganGroup(gid, owner)
	tx := db.DBCli.MustBegin()
	dao.GroupDao.Assigan(tx, owner, gid)
	dao.MemberDao.Assigan(tx, 1, owner, gid)
	dao.MemberDao.Assigan(tx, 0, uid, gid)
	err = tx.Commit()
	if err != nil {
		return err
	}
	msg := nickname + "成为新群主"
	err = message.SendGroupMassage("assignGroup", msg, uint64(uid), uint64(gid), 0)
	if err != nil {
		return err
	}
	return nil
}

// 添加好友请求
func (*groupService) UserGroups(uid int64) (*[]model.Gid, error) {

	gids, err := dao.MemberDao.UserGroups(uid)
	if err != nil {
		return nil, werror.New(100, "查询错误")
	}

	return gids, nil
}

// 添加好友请求
func (*groupService) UserGroups2(uid int64) (*[]map[string]interface{}, error) {

	gounpMembers, err := dao.MemberDao.UserAllGroups(uid)
	if err != nil {
		return nil, werror.New(100, "查询错误")
	}
	glen := len(*gounpMembers)
	gounps := make([]map[string]interface{}, 0, glen)
	gounpsMap := map[int64]model.GroupMember{}

	if glen > 0 {
		gids := make([]int64, 0, glen)
		for _, memberGroup := range *gounpMembers {
			gids = append(gids, memberGroup.Gid)
			gounpsMap[memberGroup.Gid] = memberGroup
		}
		userGroups, err := dao.GroupDao.GroupsByGids(gids)
		if err != nil {
			return nil, werror.New(100, "获取群组消息失败")
		}

		for _, group := range *userGroups {
			g := map[string]interface{}{
				"gid":           group.Id,
				"name":          group.Name,
				"identifier":    group.Identifier,
				"avatar":        group.Avatar,
				"comment":       group.Comment,
				"joinType":      group.JoinType,
				"owner":         group.Owner,
				"status":        group.Status,
				"isMute":        group.IsMute,
				"isPrivateChat": group.IsPrivateChat,
				"createTime":    group.CreateTime,
				"memberNum":     group.Members,
				"isAddressBook": gounpsMap[group.Id].IsAddressBook,
				"isTop":         gounpsMap[group.Id].IsTop,
				"isMeMute":      gounpsMap[group.Id].IsMute,
				"isMeNoNotify":  gounpsMap[group.Id].IsNoNotify,
			}
			gounps = append(gounps, g)
		}
		return &gounps, nil
	} else {
		return nil, nil
	}
}

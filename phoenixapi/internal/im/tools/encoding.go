package tools

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"time"

	pb "phoenix/internal/im/proto"

	"github.com/golang/protobuf/proto"
	"github.com/pkg/errors"
)

type Msg struct{}

var Message = new(Msg)

func (m *Msg) Encode(header *pb.Header, body proto.Message) ([]byte, error) {
	headByte, err := proto.Marshal(header)
	if err != nil {
		return []byte{}, err
	}
	var buf bytes.Buffer
	headLen := uint32(len(headByte))
	bodyByte, err := proto.Marshal(body)
	if err != nil {
		return []byte{}, err
	}
	bodylen := uint32(len(bodyByte))
	packageLen := bodylen + headLen + 8

	headerLen := headLen
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, packageLen)
	buf.Write(b)
	binary.BigEndian.PutUint32(b, headerLen)
	buf.Write(b)
	buf.Write(headByte)
	buf.Write(bodyByte)
	b = buf.Bytes()
	return b, nil
}

func (m *Msg) Decode(msg []byte) (*pb.Header, proto.Message, error) {
	head := msg[:4]
	var headLen uint32
	headLen = binary.BigEndian.Uint32(head)
	headByte := msg[4 : headLen+4]
	bodyByte := msg[headLen+4:]
	header := &pb.Header{}
	err := proto.Unmarshal(headByte, header)
	if err != nil {
		return header, nil, err
	}
	var body proto.Message
	if len(bodyByte) > 0 {
		body, err = m.DecodeBody(header.Ctype, bodyByte)
	} else {
		body = nil
	}

	return header, body, err
}

func (m *Msg) EncodeHeader(header *pb.Header) ([]byte, error) {
	headByte, err := proto.Marshal(header)
	if err != nil {
		return []byte{}, err
	}
	var buf bytes.Buffer
	headLen := uint32(len(headByte))
	packageLen := headLen + 8
	headerLen := headLen
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, packageLen)
	buf.Write(b)
	binary.BigEndian.PutUint32(b, headerLen)
	buf.Write(b)
	buf.Write(headByte)
	b = buf.Bytes()
	return b, nil
}

func (m *Msg) DecodeHeader(msg []byte) (*pb.Header, error) {
	head := msg[:4]
	var headLen uint32
	headLen = binary.BigEndian.Uint32(head)
	if uint32(len(msg)) < headLen+4 {
		return nil, errors.New("长度不对")
	}
	headByte := msg[4 : headLen+4]
	header := &pb.Header{}
	err := proto.Unmarshal(headByte, header)
	return header, err
}

// 从conn读取消息
func (m *Msg) Read(reader *bufio.Reader) ([]byte, error) {
	//读取消息长度
	lengthByte, _ := reader.Peek(4) //读取前4个字节数据
	lengthBuff := bytes.NewBuffer(lengthByte)
	var length uint32
	err := binary.Read(lengthBuff, binary.BigEndian, &length)
	if err != nil {
		return []byte{}, err
	}
	if uint32(reader.Buffered()) < length {
		return []byte{}, errors.New("消息未读取完")
	}
	//读取真正的消息数据
	pack := make([]byte, int(length))
	_, err = reader.Read(pack)
	if err != nil {
		return []byte{}, err
	}
	return pack, nil
}

// 消息头部
func (m *Msg) GenerateHeader(msgtype pb.MessageType, msgid uint64, sendid uint64, fuid uint64, touid uint64, device pb.DeviceType, ctype pb.ContentType, dispatch pb.DispatchType, ptype pb.PongType) *pb.Header {
	return &pb.Header{
		Ver:      1,
		Msgtype:  msgtype, //消息类型
		Msgid:    msgid,
		Sendid:   sendid,
		Fromuid:  fuid,
		Touid:    touid,
		Device:   device,
		Ctype:    ctype,
		Dispatch: dispatch,
		PongType: ptype,
		Ctime:    time.Now().Unix(),
	}
}

// @todo 考虑使用工程方法
func (m *Msg) DecodeBody(ctype pb.ContentType, bodyByte []byte) (body proto.Message, err error) {
	switch ctype {
	case pb.ContentType_CT_SINGNIN:
		body = &pb.SignIn{}
		break
	case pb.ContentType_CT_TEXT:
		body = &pb.Text{}
		break
	case pb.ContentType_CT_CUSTOM:
		body = &pb.Text{}
		break
	default:
		body = nil
		err = errors.New("内容类型不对")
		return
	}
	err = proto.Unmarshal(bodyByte, body)
	return
}

// 文本消息
func (m *Msg) GenerateText(text string) *pb.Text {
	return &pb.Text{
		Text: text,
	}
}

// 文本消息
func (m *Msg) GenerateSingin(token string) *pb.SignIn {
	return &pb.SignIn{
		Token: token,
	}
}

package dao

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"

	"phoenix/pkg/werror"

	"phoenix/internal/im/model"

	"phoenix/pkg/db"
)

type groupDao struct{}

var GroupDao = new(groupDao)

// 通过好友请求
func (*groupDao) Add(group *model.Group) (int64, error) {
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `groups` ( `name`,`identifier`, `avatar`,`creator`, `owner`, `members`,  `comment`, `status`,`join_type`,`is_mute`, `is_private_chat`, `create_time`) values(?,?,?,?,?,?,?,?,?,?,?,?)",
		group.Name, group.Identifier, group.Avatar, group.Creator, group.Owner, group.Members, group.Comment, group.Status, group.JoinType, group.IsMute, group.IsPrivateChat, group.CreateTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "未知错误")
	}

	if affected == 0 {
		return -1, werror.New(300, "群已经存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "创建群失败")
	}

	return id, nil
}

// 通过好友请求
func (*groupDao) GetGroupByidentifier(identifier string) (*model.Group, error) {

	group := model.Group{}
	//查询单个Get

	err := db.DBCli.Get(&group, "select * from groups where identifier = ?",
		identifier)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询群组失败")
	}

	return &group, nil

}

func (*groupDao) GroupByGid(gid int64) (*model.Group, error) {

	group := model.Group{}
	//查询单个Get

	err := db.DBCli.Get(&group, "select * from groups where id = ?",
		gid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询群组失败")
	}

	return &group, nil

}

func (*groupDao) Setting(gid int64, field string, val string) error {

	sql := fmt.Sprintf("UPDATE groups SET `%s` = ?  WHERE id= ? ", field)

	result, err := db.DBCli.Exec(sql, val, gid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*groupDao) UpdateMembers(gid int64, num int) error {

	sql := "UPDATE groups SET `members` = ?  WHERE id= ? "

	result, err := db.DBCli.Exec(sql, num, gid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*groupDao) Assigan(tx *sqlx.Tx, owner int64, gid int64) *sql.Result {
	result := tx.MustExec("UPDATE groups SET owner = ?  WHERE id= ? ", owner, gid)
	return &result
}

func (*groupDao) AddGroupMembers(gid int64, num int) error {

	sql := fmt.Sprintf("UPDATE groups SET `members` = `members`+%d  WHERE id= ? ", num)
	_, err := db.DBCli.Exec(sql, gid)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

func (*groupDao) ReduceGroupMembers(gid int64, num int) error {

	sql := fmt.Sprintf("UPDATE groups SET `members` = `members`-%d  WHERE id= ? ", num)
	_, err := db.DBCli.Exec(sql, gid)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

// 通过好友请求
func (*groupDao) GroupsByGids(gids []int64) (*[]model.Group, error) {

	var gounps []model.Group
	query, args, err := sqlx.In("select * from groups where id in (?)", gids)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	query = db.DBCli.Rebind(query) // Rebind query
	err = db.DBCli.Select(&gounps, query, args...)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &gounps, nil
}

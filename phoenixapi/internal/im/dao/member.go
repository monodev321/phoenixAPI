package dao

import (
	"database/sql"
	"fmt"
	"strings"

	"phoenix/internal/im/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"
)

type memberDao struct{}

var MemberDao = new(memberDao)

// 添加代币
func (*memberDao) Add(valueStrings []string, valueArgs []interface{}) error {
	stmt := fmt.Sprintf("INSERT IGNORE INTO group_members (`gid`, `uid`, `nickname`, `username`,  `avatar`, `referrer`, `role`, `join_type`, `is_address_book`, `join_time`) VALUES %s",
		strings.Join(valueStrings, ","))

	result, err := db.DBCli.Exec(stmt, valueArgs...)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "未知错误")
	}

	return nil

}

// 通过好友请求
func (*memberDao) GetUidsByGid(gid int64) (*[]model.GroupMember, error) {

	var members []model.GroupMember
	err := db.DBCli.Select(&members, "select uid from group_members where gid = ? and status = 0", gid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &members, nil
}

// 通过好友请求
func (*memberDao) UserGroups(uid int64) (*[]model.Gid, error) {

	var gids []model.Gid
	err := db.DBCli.Select(&gids, "select gid from group_members where uid = ? and is_address_book = 1 ", uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &gids, nil
}

// 通过好友请求
func (*memberDao) UserAllGroups(uid int64) (*[]model.GroupMember, error) {

	var gounps []model.GroupMember
	err := db.DBCli.Select(&gounps, "select * from group_members where uid = ? and is_address_book = 1 ", uid)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}

	return &gounps, nil
}

// 通过好友请求
func (*memberDao) MembersByGid(gid int64) (*[]model.GroupMember, error) {

	var members []model.GroupMember
	err := db.DBCli.Select(&members, "select * from group_members where gid = ? and status = 0", gid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &members, nil
}

// 通过好友请求
func (*memberDao) MemberByGidAndUid(gid int64, uid int64) (*model.GroupMember, error) {

	member := model.GroupMember{}
	//查询单个Get

	err := db.DBCli.Get(&member, "select * from group_members where gid = ? and uid = ?",
		gid, uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询群组失败")
	}

	return &member, nil
}

func (*memberDao) Remove(gid int64, uids []int64) error {

	// result, err := db.DBCli.Exec("delete from groups  WHERE id= ? and uid in ?", gid, uids)

	query, args, err := sqlx.In("delete from group_members  WHERE gid= ? and uid in (?)", gid, uids)
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败1")
	}
	query = db.DBCli.Rebind(query) // Rebind query
	result, err := db.DBCli.Exec(query, args...)
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败2")

	}
	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败3")
	}

	return nil

}

func (*memberDao) Assigan(tx *sqlx.Tx, role int, uid int64, gid int64) *sql.Result {
	result := tx.MustExec("UPDATE group_members SET role = ?, is_address_book = 1 WHERE uid= ? and gid = ? ", role, uid, gid)
	return &result
}

func (*memberDao) MuteMember(isMute string, uid int64, gid int64) error {
	result, err := db.DBCli.Exec("UPDATE group_members SET is_mute = ? WHERE uid= ? and gid = ? ", isMute, uid, gid)
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败1")

	}
	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil
}

func (*memberDao) MarknameMember(markname string, uid int64, gid int64) error {
	result, err := db.DBCli.Exec("UPDATE group_members SET markname = ? WHERE uid= ? and gid = ? ", markname, uid, gid)
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败1")

	}
	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil
}

func (*memberDao) ChangeUserInfo(field string, val string, uid int64) error {

	sql := fmt.Sprintf("UPDATE group_members SET `%s` = ?  WHERE uid= ? ", field)

	_, err := db.DBCli.Exec(sql, val, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*memberDao) Setting(gid int64, field string, val string, uid int64) error {

	sql := fmt.Sprintf("UPDATE group_members SET `%s` = ?  WHERE gid= ? and uid = ?", field)

	result, err := db.DBCli.Exec(sql, val, gid, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

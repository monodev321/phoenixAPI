package dao

import (
	"database/sql"
	"time"

	"phoenix/pkg/werror"

	"phoenix/internal/im/model"

	"phoenix/pkg/db"
)

type friendRequestDao struct{}

var FriendRequestDao = new(friendRequestDao)

// 添加好友请求
func (*friendRequestDao) Add(freq *model.FriendRequest) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `friend_request` (`to_uid`, `to_username`, `to_nickname`, `to_avatar`, `from_uid`, `from_username`, `from_nickname`, `from_avatar`, `add_type`, `comment`, `create_time`) values(?,?,?,?,?,?,?,?,?,?,?)",
		freq.ToUid, freq.ToUsername, freq.ToNickname, freq.ToAvatar, freq.FromUid, freq.FromUsername, freq.FromNickname, freq.FromAvatar, freq.AddType, freq.Comment, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "未知错误")
	}

	if affected == 0 {
		return -1, nil
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "获取好友请求id出错")
	}

	return id, nil
}

// Get 获取用户信息
func (*friendRequestDao) GetFriendRequest(id int64) (*model.FriendRequest, error) {

	freq := model.FriendRequest{}
	//查询单个Get

	err := db.DBCli.Get(&freq, "select id,to_uid,to_username,to_nickname,to_avatar,from_uid,from_username,from_nickname,from_avatar,create_time from friend_request where id = ? ",
		id)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, werror.FunWrap(err, 200, "查询请求失败")
	}

	return &freq, nil
}

// Get 获取用户信息
func (*friendRequestDao) GetFriendRequestByUid(toUid int64, fromUid int64) (*model.FriendRequest, error) {

	freq := model.FriendRequest{}
	//查询单个Get
	err := db.DBCli.Get(&freq, "select id from friend_request where to_uid = ? and from_uid = ? ",
		toUid, fromUid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &freq, nil
}

// 获取用户的好友请求列表
func (*friendRequestDao) List(uid int64) (*[]model.FriendRequest, error) {

	var freqs []model.FriendRequest
	err := db.DBCli.Select(&freqs, "select id,from_uid,from_username,from_nickname,from_avatar,comment,add_type,create_time from friend_request where to_uid = ? ",
		uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询好友请求列表失败")
	}
	return &freqs, nil
}

// 通过后，删除好友请求
func (*friendRequestDao) Delete(id int64) error {
	_, err := db.DBCli.Exec("DELETE FROM friend_request where id=?", id)
	if err != nil {
		return werror.FunWrap(err, 200, "删除请求失败")
	}
	return nil
}

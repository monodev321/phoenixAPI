package dao

import (
	"fmt"
	"strings"

	"phoenix/internal/im/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type memberRequestDao struct{}

var MemberRequestDao = new(memberRequestDao)

// 通过好友请求
func (*memberRequestDao) Add(valueStrings []string, valueArgs []interface{}) error {
	stmt := fmt.Sprintf("INSERT IGNORE INTO group_member_apply (`gid`, `uid`, `nickname`, `username`,  `avatar`, `referrer`,  `join_type`, `comment`, `owner`, `create_time`) VALUES %s",
		strings.Join(valueStrings, ","))

	result, err := db.DBCli.Exec(stmt, valueArgs...)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "未知错误")
	}

	return nil
}

// 通过好友请求
func (*memberRequestDao) List(uid int64) (*[]model.GroupMemberApply, error) {
	var applys []model.GroupMemberApply
	//查询单个Get

	err := db.DBCli.Select(&applys, "select * from group_member_apply where  owner = ?",
		uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询群组失败")
	}

	return &applys, nil
}

// 通过好友请求
func (*memberRequestDao) Apply(id int64) (*model.GroupMemberApply, error) {
	var apply model.GroupMemberApply
	//查询单个Get

	err := db.DBCli.Get(&apply, "select * from group_member_apply where  id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询群组失败")
	}
	return &apply, nil
}

// 通过好友请求
func (*memberRequestDao) Delete(id int64) error {
	_, err := db.DBCli.Exec("DELETE FROM group_member_apply where id=?", id)
	if err != nil {
		return werror.FunWrap(err, 200, "删除请求失败")
	}
	return nil
}

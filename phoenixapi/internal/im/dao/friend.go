package dao

import (
	"database/sql"
	"fmt"
	"time"

	"phoenix/pkg/werror"

	"github.com/jmoiron/sqlx"

	"phoenix/internal/im/model"

	"phoenix/pkg/db"
)

type friendDao struct{}

var FriendDao = new(friendDao)

// 通过好友请求
func (*friendDao) Add(friend *model.Friend) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("REPLACE INTO `friends` ( `uid`, `friend_uid`, `friend_username`, `friend_nikcname`, `friend_remarkname`,`friend_avatar`,`status`, `create_time`) values(?,?,?,?,?,?,?,?)",
		friend.Uid, friend.FriendUid, friend.FriendUsername, friend.FriendNickname, friend.FriendRemarkname, friend.FriendAvatar, 0, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "未知错误")
	}

	// if affected == 0 {
	// 	return 0, werror.New(300, "好友已经存在")
	// }

	// id, err := result.LastInsertId()
	// if err != nil {
	// 	return 0, werror.New(300, "添加好友失败")
	// }

	return 0, nil
}

// 获取用户的好友请求列表
func (*friendDao) List(uid int64) (*[]model.Friend, error) {

	var friends []model.Friend
	err := db.DBCli.Select(&friends, "select id,uid,friend_uid,friend_username,IFNULL(friend_nikcname, '') as friend_nikcname,IFNULL(friend_remarkname, '') as friend_remarkname,friend_avatar,IFNULL(tel, '') as tel,IFNULL(comment, '') as comment, status, is_no_notify, is_top, is_allow_watch, is_watch, relation, create_time from friends where uid = ? AND relation = 2 ",
		uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询好友请求列表失败")
	}
	return &friends, nil
}

// 获取用户的好友请求列表
func (*friendDao) FriendByUid(fuid int64, uid int64) (*model.Friend, error) {

	var friend model.Friend
	err := db.DBCli.Get(&friend, "select id,friend_uid,friend_username,IFNULL(friend_nikcname, '') as friend_nikcname,IFNULL(friend_remarkname, '') as friend_remarkname,friend_avatar,IFNULL(tel, '') as tel,IFNULL(comment, '') as comment,status,relation,create_time from friends where uid = ? and friend_uid = ?",
		uid, fuid)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, werror.FunWrap(err, 200, "查询好友请求列表失败")
	}
	return &friend, nil
}

func (*friendDao) Setting(fid int64, uid int64, field string, val string) error {

	sql := fmt.Sprintf("UPDATE friends SET `%s` = ?  WHERE friend_uid= ? and uid = ?", field)

	result, err := db.DBCli.Exec(sql, val, fid, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*friendDao) UpdateMarkname(fid int64, uid int64, avatar string, nickname string, markname string) error {

	sql := "UPDATE friends SET friend_avatar = ?,friend_nikcname = ?,friend_remarkname = ?  WHERE friend_uid= ? and uid = ?"

	result, err := db.DBCli.Exec(sql, avatar, nickname, markname, fid, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*friendDao) ChangeUserInfo(field string, val string, uid int64) error {

	sql := fmt.Sprintf("UPDATE friends SET `%s` = ?  WHERE friend_uid= ? ", field)

	_, err := db.DBCli.Exec(sql, val, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*friendDao) Status(tx *sqlx.Tx, fid int64, uid int64, status int) *sql.Result {
	result := tx.MustExec("UPDATE friends SET status = ?  WHERE friend_uid= ? and uid = ? ", status, fid, uid)
	return &result
}

// 获取用户的好友请求列表
func (*friendDao) GetFriend(uid int64, friendId int64) (*model.Friend, error) {

	var friend model.Friend
	err := db.DBCli.Get(&friend, "select friend_uid,friend_username,IFNULL(friend_nikcname, '') as friend_nikcname,IFNULL(friend_remarkname, '') as friend_remarkname,friend_avatar,relation from friends where uid = ? and friend_uid = ? ",
		uid, friendId)
	if err != nil {

		return nil, werror.FunWrap(err, 200, "查询好友请求列表失败")
	}
	return &friend, nil
}

func (*friendDao) Remove(id int64) error {

	query, args, err := sqlx.In("delete from friends  WHERE id= ? ", id)
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败1")
	}
	query = db.DBCli.Rebind(query) // Rebind query
	result, err := db.DBCli.Exec(query, args...)
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败2")

	}
	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败3")
	}

	return nil

}

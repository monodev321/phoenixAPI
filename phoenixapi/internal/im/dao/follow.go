package dao

import (
	"fmt"
	"time"

	"phoenix/pkg/werror"

	"phoenix/internal/im/model"

	"phoenix/pkg/db"
)

type followDao struct{}

var FollowDao = new(followDao)

// 通过好友请求
func (*followDao) Follow(friend *model.Friend) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("REPLACE INTO `friends` ( `uid`, `friend_uid`, `friend_username`, `friend_nikcname`, `friend_remarkname`,`friend_avatar`,`status`, `relation`,`create_time`) values(?,?,?,?,?,?,?,?,?)",
		friend.Uid, friend.FriendUid, friend.FriendUsername, friend.FriendNickname, friend.FriendRemarkname, friend.FriendAvatar, 0, friend.Relation, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "未知错误")
	}

	return 0, nil
}

func (*followDao) UpdateRelation(id int64, relation int) error {

	sql := fmt.Sprintf("UPDATE friends SET `relation` = ?  WHERE id = ?")

	result, err := db.DBCli.Exec(sql, relation, id)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

func (*followDao) Settings(fid int64, uid int64, sqls string) error {

	sql := fmt.Sprintf("UPDATE friends SET %s  WHERE friend_uid= ? and uid = ?", sqls)

	result, err := db.DBCli.Exec(sql, fid, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "更新失败")
	}

	return nil

}

// func (*followDao) GetFlowRecordByUidAndTokenId(uid int64, offset int64, limit int) (*[]model.Friend, error) {

// 	var friends []model.Friend
// 	var err error
// 	err = db.DBCli.Select(&flowLogs, "select id,order_id,uid,token_id,f_uid,f_username,f_nickname,f_avatar,quantity,balance,log_type,operator,comment,charge,tran_id,create_time from token_flow_logs where uid = ? and token_id = ? and operator = ? and id > ?  ORDER BY id desc LIMIT 0, ?",
// 					uid, tokenId, operator, id, rows)
// 	if err != nil {
// 		return nil, werror.FunWrap(err, 200, "查询列表失败")
// 	}
// 	return &flowLogs, nil
// }

func (*followDao) FollowList(uid uint, limit int, offset int) (*[]model.SnsRelation, error) {
	var followlist []model.SnsRelation
	err := db.DBCli.Select(&followlist, "select friends.id as id, friends.create_time as created_at, friends.uid as user_id, friend_uid as follow_id, relation, friend_remarkname as remark, is_top, is_watch as see_ta, is_allow_watch as see_me  from friends where uid = ? Order by id LIMIT ? OFFSET ?",
		uid, limit, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询关注请求列表失败")
	}
	return &followlist, nil
}

func (*followDao) FollowerList(uid uint, limit int, offset int) (*[]model.SnsRelation, error) {
	var followerlist []model.SnsRelation
	err := db.DBCli.Select(&followerlist, "select friends.id as id, friends.create_time as created_at, friends.uid as user_id, friend_uid as follow_id, relation, friend_remarkname as remark, is_top, is_watch as see_ta, is_allow_watch as see_me  from friends where friend_uid = ? Order by id LIMIT ? OFFSET ?",
		uid, limit, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询关注请求列表失败")
	}
	return &followerlist, nil
}

func (*followDao) GetFollowNewest(uid int64, limit int, offset int) (*[]model.Feed, error) {
	var followenewest []model.Feed
	err := db.DBCli.Select(&followenewest, "select * from feeds where user_id = ? Order by created_at desc LIMIT ? OFFSET ?",
		uid, limit, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询请求失败")
	}
	return &followenewest, nil
}

func (*followDao) SearchUserFollow(uid uint, keywd string, offset int, limit int) (*[]model.SnsRelation, error) {
	var searchuserlist []model.SnsRelation
	err := db.DBCli.Select(&searchuserlist, "select friends.id as id, friends.create_time as created_at, friends.uid as user_id, friend_uid as follow_id, relation, friend_remarkname as remark, is_top, is_watch as see_ta, is_allow_watch as see_me  from friends where uid = ? AND (friend_username = ? OR friend_remarkname = ?) Order by id LIMIT ? OFFSET ?",
		uid, keywd, keywd, limit, offset)
	fmt.Println(limit)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询请求失败")
	}
	fmt.Println(&searchuserlist)
	return &searchuserlist, nil
}

func (*followDao) GetFriendNewest(uid uint, limit int, offset int) (*[]model.Feed, error) {
	var friendnewest []model.Feed
	err := db.DBCli.Select(&friendnewest, "select v.* from friends s left join feeds v on s.friend_uid = v.user_id where s.uid = ? and relation = 2 Order by created_at desc LIMIT ? OFFSET ?",
		uid, limit, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询请求失败")
	}
	return &friendnewest, nil
}

func (*followDao) GetUserLike(uid int) (*model.Feed, error) {
	userlike := model.Feed{}
	err := db.DBCli.Get(&userlike, "select * from feeds where id = ? Order by created_at desc",
		uid)
	fmt.Println(&userlike)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询请求失败")
	}
	return &userlike, nil
}

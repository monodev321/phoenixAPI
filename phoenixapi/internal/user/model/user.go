package model

// User 账户
type User struct {
	ID             int64  `json:"id"`                              // 用户id
	Uid            int64  `json:"uid"`                             // 用户id
	Username       string `json:"username"`                        // 用户名
	Nickname       string `json:"nickname"`                        // 用户昵称
	Password       string `json:"password"`                        // 用户密码
	TranPassword   string `json:"tranPassword" db:"tran_passwd"`   // 用户密码
	AvatarUrl      string `json:"avatar" db:"avatar"`              // 用户头像
	Background     string `json:"background" db:"background"`      // 用户头像
	Referrer       int64  `json:"referrer"`                        // 用户的推荐人
	ReferrerPath   string `json:"referrerPath" db:"referrer_path"` // 用户的推荐人
	Code           string `json:"code"`                            // 用户的推荐人
	Gender         string `json:"gender"`                          // 用户的推荐人
	Signature      string `json:"signature" db:"intro"`            // 用户的推荐人
	Notify         string `json:"notify"`                          // 用户的推荐人
	Active         int    `json:"status" db:"status"`              // 用户的推荐人
	Role           int    `json:"role" db:"role"`                  // 用户的推荐人
	IsTranPassword int    `json:"isTranPassword"`                  // 用户的推荐人
	Tel            string `json:"phone" db:"phone"`                // 电话
	Email          string `json:"email"  db:"email"`               // 电话区号
	CreateTime     int64  `json:"createTime"  db:"created_at"`     // 创建时间
	UpdateTime     int64  `json:"updateTime"  db:"updated_at"`     // 更新时间
}

// User 验证码
type UserCode struct {
	Username       string `json:"username"`                              // 用户名
	Code           string `json:"code"`                                  // 验证码
	CodeType       string `json:"code_type"`                             // 验证类型 'register find_password'
	CreateTime     int64  `json:"create_time"`                           // 创建时间
	ExpirationTime int64  `json:"expiration_time"  db:"expiration_time"` // 更新时间
}

type UserBaseInfo struct {
	UserID   int64  `json:"user_id"`
	UserName string `json:"user_name"`
	Nickname string `json:"nickname"`
	Avatar   string `json:"avatar"`
	Gender   string `json:"gender"` //性别 female, male, none
}

package model

// User 账户
type UserSecret struct {
	Id         string `json:"id"`                          // id
	Uid        int64  `json:"uid"`                         // 用户id
	ToId       int64  `json:"toId" db:"to_id"`             // 用户id
	Secret     string `json:"secret"`                      // 密码
	Type       int    `json:"type"`                        // 1表示用户，2表示群聊
	RsaId      int64  `json:"rsaId" db:"rsa_id"`           // 创建时间
	CreateTime int64  `json:"createTime" db:"create_time"` // 创建时间
}

package model

type UserLogin struct {
	Uid        int64  `db:"uid" json:"uid"`                //  用户id
	Username   string `db:"username" json:"username"`      //  手机品牌
	Num        int32  `db:"num" json:"num"`                //  版本号
	Brand      string `db:"brand" json:"brand"`            //  手机品牌
	Version    string `db:"version" json:"version"`        //  版本
	Token      string `db:"token" json:"token"`            //  版本
	Platform   int32  `db:"platform" json:"platform"`      //  0标示安卓1表示ios
	Push       int32  `db:"push" json:"push"`              //  0没有1是fcm2是apns
	CreateTime int64  `db:"create_time" json:"createTime"` //  第一次登陆时间
}

package model

// User 账户
type UserRsa struct {
	Id         int64  `json:"id"`                          // id
	Uid        int64  `json:"uid"`                         // 用户id
	Prikey1    string `json:"prikey1"`                     // 用户名
	Prikey2    string `json:"prikey2"`                     // 密码
	Prikey3    string `json:"prikey3"`                     // 支付密码
	Prikey4    string `json:"prikey4"`                     // 备用
	Pubkey     string `json:"pubkey"`                      // 备用
	Kind       int    `json:"kind"`                        // 0im,1eth,2ipfs
	Type       int    `json:"type"`                        // 1表示用户，2表示系统
	CreateTime int64  `json:"createTime" db:"create_time"` // 创建时间
}

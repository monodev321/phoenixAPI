package service

import (
	"context"
	"math/rand"
	"strconv"
	"strings"
	"time"

	imDao "phoenix/internal/im/dao"
	"phoenix/internal/user/dao"
	"phoenix/internal/user/model"
	"phoenix/pkg/config"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/mail"
	"phoenix/pkg/werror"
)

type userService struct{}

var UserService = new(userService)

// 用户注册
func (*userService) EmailRegister(ctx context.Context, user *model.User) (int64, error) {

	_now := time.Now().Unix()
	user.Password = coinHash.GetSha256ByConf(user.Password)
	user.CreateTime = _now
	user.UpdateTime = _now
	n := strings.Index(user.Email, "@")
	user.Nickname = "by" + user.Email[0:n]
	user.Username = coinHash.BsonId()
	email := user.Email
	user.Email = coinHash.Md5WithSalt(email, email)
	user.Tel = ""
	user.Active = 0
	//有推荐人的时候 设置推荐人
	user.Referrer = 0
	user.Signature = config.AppCfg.Signature
	avatar := strconv.Itoa(rand.Intn(217) + 1)
	user.AvatarUrl = config.AppCfg.AvatarBase + avatar + ".jpg"
	//生成激活码
	user2, err := dao.UserDao.GetUserByTelOrEmail(user.Email)
	if err != nil {
		return 0, werror.New(100, "查询用户错误")
	}

	if user2 != nil {
		return 0, werror.New(100, "用户名已存在")
	}

	uid, err := dao.UserDao.Add(user)
	if err != nil {
		return 0, err
	}
	//发送邮件
	code := coinHash.GenerateHashCode(user.Username)
	h, _ := time.ParseDuration("0.5h")
	expiration := time.Now().Add(h).Unix()
	usercCodeModel := model.UserCode{
		Username:       user.Email,
		Code:           code,
		CodeType:       "registerEmail",
		CreateTime:     _now,
		ExpirationTime: expiration,
	}
	err = dao.UserCodeDao.Add(usercCodeModel)
	if err != nil {
		return uid, err
	}
	// err = mail.Register(code, user.Username)
	go mail.Register(code, email)
	return uid, nil
}

// 手机号码用户注册
func (*userService) TelRegister(ctx context.Context, user *model.User, code string) (int64, error) {
	//验证验证码是否存在
	codeTel := user.Tel
	codeTel2 := coinHash.Md5WithSalt(user.Tel, user.Tel)

	_now := time.Now().Unix()
	codeModel, err := dao.UserCodeDao.GetByCodeAndUsername(code, codeTel)
	if err != nil {
		return 0, werror.New(200, "验证码不正确")
	}
	if _now >= codeModel.ExpirationTime {
		return 0, werror.New(200, "验证码已过期")
	}

	user.Password = coinHash.GetSha256ByConf(user.Password)
	user.CreateTime = _now
	user.UpdateTime = _now
	user.Nickname = "by" + codeTel2[0:8]
	//有推荐人的时候 设置推荐人
	user.Signature = config.AppCfg.Signature
	avatar := strconv.Itoa(rand.Intn(217) + 1)
	user.AvatarUrl = config.AppCfg.AvatarBase + avatar + ".jpg"
	bg := strconv.Itoa(rand.Intn(21) + 1)
	user.Background = "/bg/" + bg + ".png"
	//生成激活码
	user.Username = codeTel
	user.Tel = codeTel
	user.Email = ""
	user.Active = 1

	user2, err := dao.UserDao.GetUserByTelOrEmail(codeTel)
	if err != nil {
		return 0, werror.New(100, "查询用户错误")
	}

	if user2 != nil {
		return 0, werror.New(100, "用户名已存在")
	}

	//严重邀请码

	// 获取code的绑定值
	refUser, err := dao.UserDao.GetUserByCode(user.Code)
	if err != nil || refUser == nil {
		return 0, werror.New(100, "推荐码不存在")
	}
	uid_s := strconv.FormatInt(refUser.ID, 10)
	user.Referrer = refUser.ID
	user.ReferrerPath = refUser.ReferrerPath + "_" + uid_s
	if refUser.ID == 1 {
		user.ReferrerPath = refUser.ReferrerPath
	}

	user.Gender = "none"
	uid, err := dao.UserDao.Add(user)
	if err != nil {
		return 0, err
	}
	//发送邮件
	dao.UserCodeDao.DelByCode(codeModel.Code, codeTel)
	return uid, nil
}

func (*userService) ReferrerCode(uid int64) (error, string) {
	// 先获取自己是否绑定
	println(uid)
	ref, err := dao.ReferrerDao.GetUserCode(uid)
	if err != nil || ref == nil {
		return werror.New(404, "服务器错误，请重试"), ""
	}
	if ref.Code != "" {
		return nil, ref.Code
	}
	// 先获取自己是否绑定
	codes := [...]string{"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "H", "J", "K", "M", "N", "P", "Q", "R", "S", "T", "W", "S", "Y"}
	i := rand.Intn(25)
	uid_s := strconv.FormatInt(uid, 10)
	code := codes[i] + uid_s
	println(code, uid_s)
	err = dao.ReferrerDao.UpdateCode(code, uid)
	if err != nil {
		return werror.New(404, "获取失败，请刷新重试"), ""
	}
	return nil, code
}

func (*userService) MyReferrer(uid int64) (error, *model.User, string) {
	ref, err := dao.ReferrerDao.GetUserReferrer(uid)
	if err != nil || ref == nil {
		return werror.New(404, "服务器错误，请重试"), nil, ""
	}
	refUser, err := dao.UserDao.GetFriendInfo(ref.Referrer)
	if err != nil {
		return werror.New(404, "获取失败，请刷新重试"), nil, ""
	}
	uid_s := strconv.FormatInt(uid, 10)
	referrer := ref.ReferrerPath + "_" + uid_s
	return nil, refUser, referrer
}

func (*userService) SubReferrers(referrerPath string, role int, offset int64) (*[]model.User, error) {
	subs, err := dao.ReferrerDao.GetSubReferrers(referrerPath, role, offset)
	if err != nil {
		return nil, werror.New(404, "服务器错误，请重试")
	}

	return subs, nil
}

// 找回密码
func (*userService) FindPassword(ctx context.Context, username string, password string, code string) error {
	//验证验证码是否存在
	codeTel := coinHash.Md5WithSalt(username, username)

	_now := time.Now().Unix()
	codeModel, err := dao.UserCodeDao.GetByCodeAndUsername(code, codeTel)
	if err != nil {
		return werror.New(200, "验证码不正确")
	}
	if _now >= codeModel.ExpirationTime {
		return werror.New(200, "验证码已过期")
	}

	userPassword := coinHash.GetSha256ByConf(password)

	err = dao.UserDao.FindPassword(userPassword, codeTel)
	if err != nil {
		return err
	}
	//发送邮件
	dao.UserCodeDao.DelByCode(codeModel.Code, codeTel)
	return nil
}

// 修改注交易密码
func (*userService) ChangeTranPassword(data map[string]string) error {
	uid, err := strconv.ParseInt(data["uid"], 10, 64)
	if err != nil {
		return err
	}
	oldPassword := coinHash.GetSha256ByConf(data["oldPassword"])
	user, err := dao.UserDao.GetTranPassword(uid)
	if err != nil {
		return err
	}
	if user.TranPassword != "" && user.TranPassword != oldPassword {
		return werror.New(100, "原密码不正确")
	}
	password := coinHash.GetSha256ByConf(data["password"])

	err = dao.UserDao.ChangeTranPassword(password, uid)
	if err != nil {
		return err
	}
	return nil
}

// 修改注交易密码
func (*userService) ChangePassword(data map[string]string) error {
	uid, err := strconv.ParseInt(data["uid"], 10, 64)
	if err != nil {
		return err
	}
	oldPassword := coinHash.GetSha256ByConf(data["oldPassword"])
	user, err := dao.UserDao.GetPassword(uid)
	if err != nil {
		return err
	}
	if user.TranPassword != "" && user.TranPassword != oldPassword {
		return werror.New(100, "原密码不正确")
	}
	password := coinHash.GetSha256ByConf(data["password"])

	err = dao.UserDao.ChangePassword(password, uid)
	if err != nil {
		return err
	}
	return nil
}

// 修改注交易密码
func (*userService) EditSignature(data map[string]string) error {
	uid, err := strconv.ParseInt(data["uid"], 10, 64)
	if err != nil {
		return err
	}
	err = dao.UserDao.UpdateSignature(data["signature"], uid)
	if err != nil {
		return err
	}
	return nil
}

// 修改注交易密码
func (*userService) EditNickname(data map[string]string) error {
	uid, err := strconv.ParseInt(data["uid"], 10, 64)
	if err != nil {
		return err
	}

	err = dao.UserDao.UpdateNickname(data["nickname"], uid)
	if err != nil {
		return err
	}
	// 更新群，朋友
	err = imDao.FriendDao.ChangeUserInfo("friend_nikcname", data["nickname"], uid)
	if err != nil {
		return err
	}
	err = imDao.MemberDao.ChangeUserInfo("nickname", data["nickname"], uid)
	if err != nil {
		return err
	}
	return nil
}

// 修改注交易密码
func (*userService) EditAvatar(data map[string]string) error {
	uid, err := strconv.ParseInt(data["uid"], 10, 64)
	if err != nil {
		return err
	}

	err = dao.UserDao.UpdateAvatar(data["avatar"], uid)
	if err != nil {
		return err
	}
	err = imDao.FriendDao.ChangeUserInfo("friend_avatar", data["avatar"], uid)
	if err != nil {
		return err
	}
	err = imDao.MemberDao.ChangeUserInfo("avatar", data["avatar"], uid)
	if err != nil {
		return err
	}
	return nil
}

// 修改注交易密码
func (*userService) EditNotify(data map[string]string) error {
	uid, err := strconv.ParseInt(data["uid"], 10, 64)
	if err != nil {
		return err
	}

	err = dao.UserDao.EditNotify(data["notify"], uid)
	if err != nil {
		return err
	}
	return nil
}

// 用户注册
func (*userService) Verify(ctx context.Context, code string) error {

	_now := time.Now().Unix()
	codeModel, err := dao.UserCodeDao.GetByCode(code)
	if err != nil {
		return err
	}
	// println(_now, codeModel.ExpirationTime)
	if _now >= codeModel.ExpirationTime {
		return werror.New(200, "验证码已过期")
	}

	err = dao.UserDao.SetActiveByEmail(1, codeModel.Username)
	if err != nil {
		return werror.New(200, "用户激活失败")
	}

	err = dao.UserCodeDao.DelByCode(codeModel.Code, codeModel.Username)
	if err != nil {
		return werror.New(200, "删除验证码失败")
	}
	return nil
}

// Get 获取用户信息
func (*userService) GetUserInfo(userId int64) (*model.User, error) {

	user, err := dao.UserDao.GetUserInfo(userId)
	if err != nil {
		return nil, err
	}

	return user, err
}

func (*userService) GetBaseInfo(userId int64) (*model.UserBaseInfo, error) {

	user, err := dao.UserDao.GetBaseInfo(userId)
	if err != nil {
		return nil, err
	}

	return user, err
}

// Get 获取用户列表信息
func (*userService) GetUsersBaseInfo(fids []int64) ([]*model.UserBaseInfo, error) {
	var usermap []*model.UserBaseInfo
	for _, id := range fids {
		users, err := dao.UserDao.GetUserInfo(id)
		if err != nil {
			return nil, err
		}
		req := &model.UserBaseInfo{
			UserID:   users.ID,
			UserName: users.Username,
			Avatar:   users.AvatarUrl,
			Gender:   users.Gender,
		}
		usermap = append(usermap, req)
	}

	return usermap, nil
}

// Get 获取用户信息
func (*userService) GetSessionUser(selfId int64, uid int64, gid int64) (map[string]interface{}, error) {

	user, err := dao.UserDao.GetUserInfo(uid)
	if err != nil {
		return nil, err
	}

	friend, _ := imDao.FriendDao.GetFriend(selfId, uid)
	if friend != nil {
		nickname := friend.FriendNickname
		if friend.FriendRemarkname != "" {
			nickname = friend.FriendRemarkname
		}
		data := map[string]interface{}{
			"uid":       friend.FriendUid,
			"username":  friend.FriendUsername,
			"nickname":  nickname,
			"avatar":    friend.FriendAvatar,
			"type":      1,
			"signature": user.Signature,
		}
		return data, nil
	}
	if gid > 0 {
		member, _ := imDao.MemberDao.MemberByGidAndUid(gid, uid)
		if member != nil {
			nickname := member.Nickname
			if member.Markname != "" {
				nickname = member.Markname
			}
			data := map[string]interface{}{
				"uid":       member.Uid,
				"username":  member.Username,
				"nickname":  nickname,
				"avatar":    member.Avatar,
				"type":      2,
				"signature": user.Signature,
			}
			return data, nil
		}
	}

	data := map[string]interface{}{
		"uid":       user.Uid,
		"username":  user.Username,
		"nickname":  user.Nickname,
		"avatar":    user.AvatarUrl,
		"type":      0,
		"signature": user.Signature,
	}

	return data, err
}

// Get 通过获取用户信息
func (*userService) GetUserByUsername(username string) (*model.User, error) {

	user, err := dao.UserDao.GetUserByUsername(username)
	if err != nil {
		return nil, err
	}

	if user != nil {
		if err != nil {
			return nil, err
		}
	}
	return user, err
}

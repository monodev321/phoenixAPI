package service

import (
	"time"

	"phoenix/internal/user/dao"
	"phoenix/internal/user/model"
)

type loginService struct{}

var LoginService = new(loginService)

func (*loginService) Add(m *model.UserLogin) error {
	m.CreateTime = time.Now().Unix()
	err := dao.LoginDao.Add(m)
	if err != nil {
		return err
	}
	return nil
}

// 用户注册
func (*loginService) GetUserLogin(uid int64) (*model.UserLogin, error) {
	login, err := dao.LoginDao.GetUserLogin(uid)
	if err != nil {
		return nil, err
	}
	return login, nil
}

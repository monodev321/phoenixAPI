package service

import (
	"time"

	"phoenix/internal/user/dao"
	"phoenix/internal/user/model"
)

type secretService struct{}

var SecretService = new(secretService)

// 用户注册
func (*secretService) SaveUserSecret(id string, _type int, rsaId int64, secret string, uid2 int64, rsaId2 int64, secret2 string, uid int64) error {

	mlen := 2
	valueStrings := make([]string, 0, mlen)
	valueArgs := make([]interface{}, 0, mlen*7)
	createTime := time.Now().Unix()

	valueStrings = append(valueStrings, "(?, ?,?, ?,?,?,?)")
	valueArgs = append(valueArgs, id)
	valueArgs = append(valueArgs, uid) //
	valueArgs = append(valueArgs, uid2)
	valueArgs = append(valueArgs, secret)
	valueArgs = append(valueArgs, rsaId)
	valueArgs = append(valueArgs, _type)
	valueArgs = append(valueArgs, createTime)

	valueStrings = append(valueStrings, "(?, ?,?, ?,?,?,?)")
	valueArgs = append(valueArgs, id)
	valueArgs = append(valueArgs, uid2) //
	valueArgs = append(valueArgs, uid)  //
	valueArgs = append(valueArgs, secret2)
	valueArgs = append(valueArgs, rsaId2)
	valueArgs = append(valueArgs, _type)
	valueArgs = append(valueArgs, createTime)

	err := dao.SecretDao.Add(valueStrings, valueArgs)
	if err != nil {
		return err
	}
	return nil
}

func (*secretService) SaveGroupSecret(id string, toid int64, secret string, uid int64) error {

	err := dao.SecretDao.AddGroupSecret(id, toid, secret, uid)
	if err != nil {
		return err
	}
	return nil
}

// 用户注册
func (*secretService) GetUserSecretByToId(toid int64, _type int, uid int64) (*model.UserSecret, error) {
	secret, err := dao.SecretDao.GetUserSecretByToId(toid, _type, uid)
	if err != nil {
		return nil, err
	}
	return secret, nil
}

// 用户注册
func (*secretService) GetUserSecretById(id string, _type int, uid int64) (*model.UserSecret, error) {
	secret, err := dao.SecretDao.GetUserSecretById(id, _type, uid)
	if err != nil {
		return nil, err
	}
	return secret, nil
}

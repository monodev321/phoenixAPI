package service

import (
	"log"
	"net/http"
	"strconv"

	"phoenix/internal/user/dao"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/logger"
	"phoenix/pkg/werror"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-oauth2/oauth2/v4/errors"
	"github.com/go-oauth2/oauth2/v4/generates"
	"github.com/go-oauth2/oauth2/v4/manage"
	"github.com/go-oauth2/oauth2/v4/server"
	"github.com/go-oauth2/oauth2/v4/store"
)

var Oauth2 *server.Server

func init() {
	manager := manage.NewDefaultManager()
	manager.SetAuthorizeCodeTokenCfg(manage.DefaultAuthorizeCodeTokenCfg)

	manager.MapAccessGenerate(generates.NewJWTAccessGenerate(jwt.SigningMethodRS512))

	clientStore, err := store.NewClientStore()
	if err != nil {
		logger.Logger.Info("oauth2 clientStore fail")
	}
	manager.MapClientStorage(clientStore)

	// use mysql token store
	tokenStore := store.NewDefaultStore()

	// defer tokenStore.Close()
	manager.MapTokenStorage(tokenStore)

	Oauth2 = server.NewServer(server.NewConfig(), manager)

	Oauth2.SetPasswordAuthorizationHandler(func(username, password string) (userID string, name string, err error) {
		password = coinHash.GetSha256ByConf(password)
		user, err := dao.UserDao.GetByeUsernameAndPwd(username, password)
		if err != nil {
			return "", "", werror.New(200, "wrong_user_or_password")
		}
		if user.Active == 0 {
			return "", "", werror.New(200, "user_inactive")
		}
		if user.Active == 2 {
			return "", "", werror.New(200, "user_block")
		}
		userID = strconv.FormatInt(user.ID, 10)
		name = user.Username
		return
	})

	Oauth2.SetUserAuthorizationHandler(userAuthorizeHandler)

	Oauth2.SetInternalErrorHandler(func(err error) (re *errors.Response) {
		log.Println("Internal Error:", err.Error())
		return
	})

	Oauth2.SetResponseErrorHandler(func(re *errors.Response) {
		log.Println("Response Error:", re.Error.Error())
	})
}

func userAuthorizeHandler(w http.ResponseWriter, r *http.Request) (userID string, err error) {
	//需要判断用户的 Authorizetoken，也就是需要已经登陆的用户，然后找出用户名和用户id
	return "", nil
}

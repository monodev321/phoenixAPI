package service

import (
	"time"

	"phoenix/internal/user/dao"
	"phoenix/internal/user/model"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/werror"
)

type rsaService struct{}

var RsaService = new(rsaService)

// 用户注册
func (*rsaService) CheckUserInfo(username string, password string, pay string, uid int64) (*model.User, error) {
	_password := coinHash.GetSha256ByConf(password)
	_username := coinHash.Md5WithSalt(username, username)
	_pay := coinHash.GetSha256ByConf(pay)
	_user, _ := dao.UserDao.GetUserModelByUsername(_username)
	if _user == nil || _user.Uid != uid {
		return nil, werror.New(100, "用户名不存在")
	}
	if _user.TranPassword != _pay {
		return nil, werror.New(100, "支付密码不正确")
	}
	if _user.Password != _password {
		return nil, werror.New(100, "登陆密码不正确")
	}
	return _user, nil
}

// 用户注册
func (*rsaService) UpdateImRsa(prikey1 string, prikey2 string, prikey3 string, pubkey string, uid int64) (*model.UserRsa, error) {
	now := time.Now().Unix()
	rsa := &model.UserRsa{
		Uid:        uid,
		Prikey1:    prikey1,
		Prikey2:    prikey2,
		Prikey3:    prikey3,
		Prikey4:    "",
		Pubkey:     pubkey,
		Kind:       0,
		Type:       1,
		CreateTime: now,
	}
	id, err := dao.RsaDao.Add(rsa)
	if err != nil {
		return nil, err
	}
	rsa.Id = id
	return rsa, nil
}

// 用户注册
func (*rsaService) GetUserPub(uid int64) (*model.UserRsa, error) {
	rsa, err := dao.RsaDao.GetUserPub(uid)
	if err != nil {
		return nil, err
	}
	return rsa, nil
}

func (*rsaService) GetUserPri(id int64, uid int64) (*model.UserRsa, error) {
	rsa, err := dao.RsaDao.GetUserPri(id, uid)
	if err != nil {
		return nil, err
	}
	return rsa, nil
}

func (*rsaService) GetUserLastPri(uid int64) (*model.UserRsa, error) {
	rsa, err := dao.RsaDao.GetUserPri(0, uid)
	if err != nil {
		return nil, err
	}
	return rsa, nil
}

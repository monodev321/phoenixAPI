package service

import (
	"time"

	"phoenix/internal/user/dao"
	"phoenix/internal/user/model"
	coinHash "phoenix/pkg/hash"
	"phoenix/pkg/mail"
)

type smsService struct{}

var SmsService = new(smsService)

// 用户注册
func (*smsService) AddCode(tel string, codeType string) (string, error) {

	_now := time.Now().Unix()
	codeTel := tel
	// codeTel := coinHash.Md5WithSalt(tel, tel)
	//验证验证码是否存在

	codeModel, err := dao.UserCodeDao.GetCodeByUsername(codeTel, codeType)
	if err != nil {
		return "", err
	}
	code := ""
	if codeModel == nil || _now >= codeModel.ExpirationTime {
		code = coinHash.GenerateCode6()
	} else {
		return codeModel.Code, nil
	}

	h, _ := time.ParseDuration("0.5h")
	expiration := time.Now().Add(h).Unix()
	usercCodeModel := model.UserCode{
		Username:       codeTel,
		Code:           code,
		CodeType:       codeType,
		CreateTime:     _now,
		ExpirationTime: expiration,
	}
	err = dao.UserCodeDao.Add(usercCodeModel)
	if err != nil {
		return "", err
	}
	return code, nil
}

// 用户注册
func (*smsService) SendEmailActive(email string) error {

	code := coinHash.GenerateHashCode(email)
	_now := time.Now().Unix()
	h, _ := time.ParseDuration("0.5h")
	expiration := time.Now().Add(h).Unix()
	usercCodeModel := model.UserCode{
		Username:       coinHash.Md5WithSalt(email, email),
		Code:           code,
		CodeType:       "registerEmail",
		CreateTime:     _now,
		ExpirationTime: expiration,
	}
	err := dao.UserCodeDao.Add(usercCodeModel)
	if err != nil {
		return err
	}
	go mail.Register(code, email)
	return nil
}

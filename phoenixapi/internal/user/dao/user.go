package dao

import (
	"database/sql"
	"fmt"

	"phoenix/internal/user/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type userDao struct{}

var UserDao = new(userDao)

// Add 插入一条用户信息
func (*userDao) Add(user *model.User) (int64, error) {
	result, err := db.DBCli.Exec("insert IGNORE into users(username,nickname,passwd,intro,avatar,background,referrer,referrer_path,gender,email,phone,status,created_at,updated_at) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
		user.Username, user.Nickname, user.Password, user.Signature, user.AvatarUrl, user.Background, user.Referrer, user.ReferrerPath, user.Gender, user.Tel, user.Tel, user.Active, user.CreateTime, user.UpdateTime)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "未知错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "用户已存在")
	}

	uid, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "获取用户id出错")
	}

	return uid, nil
}

// SetActive 激活用户
func (*userDao) SetActive(active int, uid int64) error {

	_, err := db.DBCli.Exec("UPDATE users SET active=? where uid=?", active, uid)
	if err != nil {
		return werror.FunWrap(err, 200, "更新active状态失败")
	}
	return nil
}

// SetActive 激活用户
func (*userDao) SetActiveByEmail(active int, email string) error {

	_, err := db.DBCli.Exec("UPDATE users SET active=? where email=?", active, email)
	if err != nil {
		return werror.FunWrap(err, 200, "更新active状态失败")
	}
	return nil
}

// Get 获取用户信息
func (*userDao) GetByeUsernameAndPwd(username string, password string) (*model.User, error) {

	user := model.User{}
	//查询单个Get
	err := db.DBCli.Get(&user, "select id,username,status from users where passwd = ? and (username = ? or email = ? or phone = ?)",
		password, username, username, username)
	fmt.Printf("%s %s %+v", username, password, err)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

func (*userDao) ChangeTranPassword(password string, uid int64) error {
	result, err := db.DBCli.Exec("UPDATE users SET  tran_passwd = ?  WHERE id= ? ",
		password, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "新旧密码不能一致")
	}

	return nil
}

func (*userDao) ChangePassword(password string, uid int64) error {
	result, err := db.DBCli.Exec("UPDATE users SET  password = ?  WHERE uid= ? ",
		password, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}

func (*userDao) FindPassword(password string, username string) error {
	_, err := db.DBCli.Exec("UPDATE users SET  password = ?  WHERE email= ? or tel =  ?",
		password, username, username)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

func (*userDao) UpdateNickname(nickname string, uid int64) error {
	result, err := db.DBCli.Exec("UPDATE users SET  nickname = ?  WHERE uid= ? ",
		nickname, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}
func (*userDao) UpdateSignature(signature string, uid int64) error {
	result, err := db.DBCli.Exec("UPDATE users SET  signature = ?  WHERE uid= ? ",
		signature, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}

func (*userDao) UpdateAvatar(avatar string, uid int64) error {
	result, err := db.DBCli.Exec("UPDATE users SET  avatar_url = ?  WHERE uid= ? ",
		avatar, uid)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}
func (*userDao) SetRole(uid int64, role int) error {
	result, err := db.DBCli.Exec("UPDATE users SET  role = ?  WHERE id= ? ",
		role, uid)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	return nil
}

func (*userDao) EditNotify(notify string, uid int64) error {
	result, err := db.DBCli.Exec("UPDATE users SET  notify = ?  WHERE uid= ? ",
		notify, uid)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}

// Get 获取用户信息
func (*userDao) GetTranPassword(id int64) (*model.User, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select IFNULL(tran_passwd, '') as tran_passwd,status from users where id = ? ",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

// Get 获取用户信息
func (*userDao) GetPassword(id int64) (*model.User, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select password from users where uid = ? ",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

// Get 获取用户信息
func (*userDao) GetUserInfo(id int64) (*model.User, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select id,username,status,nickname,avatar,gender,referrer,referrer_path,IFNULL(phone, '') as phone,IFNULL(intro, '') as intro,email,IFNULL(tran_passwd, '') as tran_passwd,created_at from users where id = ? ",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

func (*userDao) GetBaseInfo(id int64) (*model.UserBaseInfo, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select id,username,avatar,gender, nickname from users where id = ? ",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}
	userInfo := model.UserBaseInfo{
		UserID:   user.ID,
		UserName: user.Username,
		Nickname: user.Nickname,
		Avatar:   user.AvatarUrl,
		Gender:   user.Gender,
	}
	return &userInfo, nil
}

// Get 获取用户信息
func (*userDao) GetFriendInfo(id int64) (*model.User, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select id,username,nickname,avatar,IFNULL(intro, '') as intro,role,created_at from users where id = ? ",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

// Get 获取用户信息
func (*userDao) GetUserModelByUsername(username string) (*model.User, error) {
	username2 := username

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select uid,username,password,IFNULL(tran_password, '') as tran_password,create_time from users where email = ? or tel = ?",
		username2, username2)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

// Get 获取用户信息
func (*userDao) GetUserByUsername(username string) (*model.User, error) {
	username2 := username
	// if !bson.IsObjectIdHex(username) {
	// 	username2 = coinHash.Md5WithSalt(username, username)
	// }

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select id,username,status,nickname,avatar,referrer,IFNULL(phone, '') as phone,IFNULL(intro, '') as intro,email,IFNULL(tran_passwd, '') as tran_passwd,background,role,created_at   from users where username = ? or email = ? or phone = ?",
		username2, username2, username2)
	if err != nil {
		fmt.Printf("%+v", err)
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}
	return &user, nil
}

// Get 获取用户信息通过tel和email
func (*userDao) GetUserByTelOrEmail(name string) (*map[string]interface{}, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select id,username,nickname,avatar,IFNULL(intro, '') as intro from users where phone = ? or email = ?",
		name, name)
	fmt.Printf("%+v", err)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}
	res := &map[string]interface{}{
		"id":        user.Uid,
		"username":  user.Username,
		"nickname":  user.Nickname,
		"avatar":    user.AvatarUrl,
		"signature": user.Signature,
	}
	return res, nil
}

// Get 获取用户信息通过tel和email
func (*userDao) GetUserByCode(code string) (*model.User, error) {

	user := model.User{}
	//查询单个Get

	err := db.DBCli.Get(&user, "select id,referrer,referrer_path from users where code = ?",
		code)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}
	return &user, nil
}

func (*userDao) UserList(status int, username string, offset int64, limit int) (*[]model.User, error) {
	sql := "select id,username,nickname, IFNULL(tran_passwd, '') as tran_passwd,avatar,IFNULL(phone, '') as phone,email,role,created_at from users where id > 0 "
	// sql := "select * from users where id > 0 "

	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if username != "" {
		sql = sql + "and username = '%s' "
		sql = fmt.Sprintf(sql, username)
	}

	sql = sql + "order by created_at limit %d, %d"
	sql = fmt.Sprintf(sql, offset, limit)

	var orders []model.User
	err := db.DBCli.Select(&orders, sql)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

func (*userDao) UserListCount(status int, username string) (int64, error) {
	sql := "select count(1) from users where id > 0 "
	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if username != "" {
		sql = sql + "and username = '%s' "
		sql = fmt.Sprintf(sql, username)
	}

	var num int64
	err := db.DBCli.Get(&num, sql)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	return num, nil
}

func (*userDao) ChangeUserField(uid int64, field string, value int) error {
	sql := fmt.Sprintf("UPDATE users SET %s = ?  WHERE uid= ?", field)
	_, err := db.DBCli.Exec(sql, value, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

func (*userDao) UpdateColumn(uid int64, col string, change int) error {
	if change == 0 {
		return nil
	}
	val := ""
	if change < 0 {
		val = fmt.Sprintf("%s %d", col, change)
	} else {
		val = fmt.Sprintf("%s + %d", col, change)
	}
	sql := fmt.Sprintf("UPDATE users SET %s = %s  WHERE id= ?", col, val)
	println(sql)
	_, err := db.DBCli.Exec(sql, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

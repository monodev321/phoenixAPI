package dao

import (
	"fmt"
	"strings"
	"time"

	"phoenix/internal/user/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type secretDao struct{}

var SecretDao = new(secretDao)

func (*secretDao) Add(valueStrings []string, valueArgs []interface{}) error {

	stmt := fmt.Sprintf("INSERT IGNORE INTO user_secret (`id`, `uid`,`to_id`, `secret`, `rsa_id`,  `type`, `create_time`) VALUES %s",
		strings.Join(valueStrings, ","))

	result, err := db.DBCli.Exec(stmt, valueArgs...)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	_, err = result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "未知错误")
	}

	return nil

}

func (*secretDao) AddGroupSecret(id string, toid int64, secret string, uid int64) error {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO user_secret (`id`, `uid`,`to_id`, `secret`, `rsa_id`,  `type`, `create_time`)  values(?,?,?,?,?,?,?)",
		id, uid, toid, secret, 0, 2, createTime)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "未知错误")
	}

	if affected == 0 {
		return werror.New(300, "用户已存在")
	}
	return nil
}

func (*secretDao) GetUserSecretByToId(toid int64, _type int, uid int64) (*model.UserSecret, error) {
	pub := model.UserSecret{}
	var err error
	createTime := time.Now().Unix() - 10*24*3600
	if _type == 1 {
		err = db.DBCli.Get(&pub, "select * from user_secret where uid=? and to_id = ? and type = ? and create_time > ? order by create_time desc ",
			uid, toid, _type, createTime)
	} else {
		err = db.DBCli.Get(&pub, "select * from user_secret where to_id = ? and type = ? and create_time > ? order by create_time desc ",
			toid, _type, createTime)
	}

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询私钥失败")
	}

	return &pub, nil
}

func (*secretDao) GetUserSecretById(id string, _type int, uid int64) (*model.UserSecret, error) {
	pub := model.UserSecret{}
	var err error
	if _type == 1 {
		err = db.DBCli.Get(&pub, "select * from user_secret where id = ? and uid = ?",
			id, uid)
	} else {
		err = db.DBCli.Get(&pub, "select * from user_secret where id = ? ",
			id)
	}
	//查询单个Get

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询私钥失败")
	}

	return &pub, nil
}

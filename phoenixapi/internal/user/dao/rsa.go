package dao

import (
	"phoenix/internal/user/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type rsaDao struct{}

var RsaDao = new(rsaDao)

// Add 插入一条用户信息
func (*rsaDao) Add(rsa *model.UserRsa) (int64, error) {
	result, err := db.DBCli.Exec("insert IGNORE into `user_rsa`(`uid`, `prikey1`, `prikey2`, `prikey3`, `prikey4`, `pubkey`, `kind`, `type`, `create_time`) values(?,?,?,?,?,?,?,?,?)",
		rsa.Uid, rsa.Prikey1, rsa.Prikey2, rsa.Prikey3, rsa.Prikey4, rsa.Pubkey, rsa.Kind, rsa.Type, rsa.CreateTime)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "未知错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "用户已存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "获取用户id出错")
	}

	return id, nil
}

func (*rsaDao) GetUserPub(uid int64) (*model.UserRsa, error) {
	pub := model.UserRsa{}
	//查询单个Get
	err := db.DBCli.Get(&pub, "select id,uid,pubkey,kind,type,create_time from user_rsa where uid = ? order by id desc limit 1 ",
		uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询私钥失败")
	}

	return &pub, nil
}

func (*rsaDao) GetUserPri(id int64, uid int64) (*model.UserRsa, error) {
	pub := model.UserRsa{}
	//查询单个Get
	var err error
	if id == 0 {
		err = db.DBCli.Get(&pub, "select id,uid,prikey1,kind,type,create_time from user_rsa where uid = ? order by id desc limit 1 ",
			uid)
	} else {
		err = db.DBCli.Get(&pub, "select id,uid,prikey1,kind,type,create_time from user_rsa where id = ? and uid =? order by id desc limit 1 ",
			id, uid)
	}

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询私钥失败")
	}

	return &pub, nil
}

package dao

import (
	// "fmt"
	// "strings"

	"phoenix/internal/user/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type loginDao struct{}

var LoginDao = new(loginDao)

func (*loginDao) Add(m *model.UserLogin) error {

	_, err := db.DBCli.Exec("REPLACE INTO user_login(`uid`,`username`, `num`, `brand`, `version`, `platform`, `push`,`token`, `create_time`) values(?,?,?,?,?,?,?,?,?)",
		m.Uid, m.Username, m.Num, m.Brand, m.Version, m.Platform, m.Push, m.Token, m.CreateTime)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	return nil
}

func (*loginDao) GetUserLogin(uid int64) (*model.UserLogin, error) {
	m := model.UserLogin{}
	var err error
	err = db.DBCli.Get(&m, "select * from user_login where uid = ? ",
		uid)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询私钥失败")
	}
	return &m, nil
}

package dao

import (
	"database/sql"

	"phoenix/pkg/werror"

	"phoenix/internal/user/model"

	"phoenix/pkg/db"
)

type userCodeDao struct{}

var UserCodeDao = new(userCodeDao)

// Add 插入一条code信息
func (*userCodeDao) Add(code model.UserCode) error {
	_, err := db.DBCli.Exec("replace into users_code(username,code,code_type,create_time,expiration_time) values(?,?,?,?,?)",
		code.Username, code.Code, code.CodeType, code.CreateTime, code.ExpirationTime)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

// Add 插入一条code信息
func (*userCodeDao) GetByCode(code string) (model.UserCode, error) {
	userCode := model.UserCode{}
	//查询单个Get

	err := db.DBCli.Get(&userCode, "select username,expiration_time from users_code where code=?", code)
	if err != nil {
		return model.UserCode{}, werror.FunWrap(err, 200, "查询code失败")
	}
	return userCode, nil

}
func (*userCodeDao) GetByCodeAndUsername(code string, username string) (*model.UserCode, error) {
	userCode := model.UserCode{}
	//查询单个Get

	err := db.DBCli.Get(&userCode, "select username,code,expiration_time from users_code where code=? and username = ?", code, username)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询code失败")
	}
	return &userCode, nil
}
func (*userCodeDao) GetCodeByUsername(username string, codeType string) (*model.UserCode, error) {
	userCode := model.UserCode{}
	//查询单个Get

	err := db.DBCli.Get(&userCode, "select username,code,expiration_time from users_code where code_type=? and username = ?", codeType, username)

	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询code失败")
	}
	return &userCode, nil
}

func (*userCodeDao) DelByCode(code string, username string) error {

	_, err := db.DBCli.Exec("DELETE FROM users_code where code=? and username = ?", code, username)
	if err != nil {
		return werror.FunWrap(err, 200, "删除验证码失败")
	}
	return nil
}

package dao

import (
	"fmt"
	"phoenix/internal/user/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type referrerDao struct{}

var ReferrerDao = new(referrerDao)

// SetActive 激活用户
func (*referrerDao) UpdateCode(code string, uid int64) error {

	_, err := db.DBCli.Exec("UPDATE users SET code=? where id=?", code, uid)
	if err != nil {
		return werror.FunWrap(err, 200, "更新邀请码失败")
	}
	return nil
}

// Get 获取用户信息
func (*referrerDao) GetUserCode(uid int64) (*model.User, error) {

	user := model.User{}
	//查询单个Get
	err := db.DBCli.Get(&user, "select IFNULL(code, '') as code from users where id = ?",
		uid)
	fmt.Printf("%+v", err)
	if err != nil {
		// if err == sql.ErrNoRows {
		// 	return nil, nil
		// }
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

// Get 获取用户信息
func (*referrerDao) GetUserReferrer(uid int64) (*model.User, error) {

	user := model.User{}
	//查询单个Get
	err := db.DBCli.Get(&user, "select id,referrer,referrer_path from users where id = ?",
		uid)
	fmt.Printf("%+v", err)
	if err != nil {
		// if err == sql.ErrNoRows {
		// 	return nil, nil
		// }
		return nil, werror.FunWrap(err, 200, "查询用户失败")
	}

	return &user, nil
}

// Get 获取用户信息
func (*referrerDao) GetSubReferrers(referrerPath string, role int, offset int64) (*[]model.User, error) {

	var orders []model.User
	err := db.DBCli.Select(&orders, "select id,username,nickname,avatar,intro,created_at from users where referrer_path = ? and role = ? order by id limit ?,?", referrerPath, role, offset, 1)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

package dao

import (
	"fmt"
	"time"

	"phoenix/internal/admin/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type administratorDao struct{}

var AdministratorDao = new(administratorDao)

func (*administratorDao) Add(user *model.Administrator) error {
	createTime := time.Now().Unix()
	_, err := db.DBCli.Exec("INSERT IGNORE `administrators` (`uid`,  `username`,`nickname`,`avatar`,`role`, `create_time`) VALUES (?,?,?,?,?,?) ",
		user.Uid, user.Username, user.Nickname, user.Avatar, user.Role, createTime)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

func (*administratorDao) UpdateToken(user *model.Administrator) error {
	result, err := db.DBCli.Exec("UPDATE administrators SET scope =?, token_type = ?, access = ?, refresh = ?, token_create_at = ?, token_expired_at = ?  WHERE uid= ? ",
		user.Scope, user.TokenType, user.Access, user.Refresh, user.TokenCreateAt, user.TokenExpiredAt, user.Uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}
	return nil
}

func (*administratorDao) UpdateUserToken(user *model.Administrator) error {
	result, err := db.DBCli.Exec("UPDATE administrators SET scope =?, token_type = ?, access = ?, refresh = ?, token_create_at = ?, token_expired_at = ?  WHERE uid= ? ",
		user.Scope, user.TokenType, user.Access, user.Refresh, user.TokenCreateAt, user.TokenExpiredAt, user.Uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}
	return nil
}

func (*administratorDao) GetUser(uid int64) (*model.Administrator, error) {
	var user model.Administrator
	err := db.DBCli.Get(&user, "select uid,role,status, username, nickname, avatar, scope, token_type, access, refresh,token_expired_at,token_create_at,create_time from administrators where uid = ?",
		uid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	return &user, nil
}

func (*administratorDao) GetRole(uid int64) (int, error) {
	var user model.Administrator
	err := db.DBCli.Get(&user, "select role from administrators where status = 1 and uid = ?",
		uid)
	if err != nil {
		return 0, err
	}

	role := 0
	if user.Role == 1 {
		role = 1
	}
	return role, nil
}

func (*administratorDao) AdministratorList(status int, username string, offset int64, limit int) (*[]model.Administrator, error) {
	sql := "select uid,role,status,username,nickname,avatar,create_time from administrators where uid > 0 "

	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if username != "" {
		sql = sql + "and username = '%s' "
		sql = fmt.Sprintf(sql, username)
	}

	sql = sql + "order by create_time limit %d, %d"
	sql = fmt.Sprintf(sql, offset, limit)

	var orders []model.Administrator
	err := db.DBCli.Select(&orders, sql)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询列表失败")
	}
	return &orders, nil
}

func (*administratorDao) AdministratorListCount(status int, username string) (int64, error) {
	sql := "select count(1) from administrators where uid > 0 "
	if status > -1 {
		sql = sql + "and status = %d "
		sql = fmt.Sprintf(sql, status)
	}

	if username != "" {
		sql = sql + "and username = '%s' "
		sql = fmt.Sprintf(sql, username)
	}
	var num int64
	err := db.DBCli.Get(&num, sql)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	return num, nil
}

func (*administratorDao) ChangeAdminField(uid int64, field string, value int) error {
	sql := fmt.Sprintf("UPDATE administrators SET %s = ?  WHERE uid= ?", field)
	_, err := db.DBCli.Exec(sql, value, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

func (*administratorDao) Delete(uid int64) error {
	sql := "Delete from administrators  WHERE uid= ?"
	_, err := db.DBCli.Exec(sql, uid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

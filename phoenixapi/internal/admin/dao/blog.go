package dao

import (
	"fmt"
	"phoenix/internal/admin/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
	"time"
)

type blogDao struct{}

var BlogDao = new(blogDao)

func (*blogDao) Add(blog *model.Blog) (*model.Blog, error) {
	blog.CreatedAt = time.Now().Unix()
	blog.UpdatedAt = time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT INTO `blogs` (`title`, `content`, `cover_url`, `user_id`, `nickname`,`category`,`language`, `is_top`,`is_recommend`,`sort`,`views_num`, `is_status`, `created_at`, `updated_at`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)", blog.Title, blog.Content, blog.CoverUrl, blog.UserID, blog.Nickname, blog.Category, blog.Language, blog.IsTop, blog.IsRecommend, blog.Sort, blog.ViewsNum, blog.IsStatus, blog.CreatedAt, blog.UpdatedAt)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "Blog Insert Failed")
	}
	id, _ := result.LastInsertId()
	blog.ID = id
	return blog, nil
}

func (*blogDao) BlogList(category string, offset int64, limit int) (*[]model.Blog, error) {
	var blogs []model.Blog
	sql := "select * from `blogs` Order by created_at desc LIMIT %d OFFSET %d"
	if category == "" {
		sql = fmt.Sprintf(sql, limit, offset)
		err := db.DBCli.Select(&blogs, sql)
		if err != nil {
			return nil, werror.FunWrap(err, 200, "something went wrong,unable to fetch blogs")
		}
		return &blogs, nil
	} else {
		sql = "select * from `blogs` where category = ? Order by created_at desc LIMIT %d OFFSET %d"
		sql = fmt.Sprintf(sql, limit, offset)
	}
	err := db.DBCli.Select(&blogs, sql, category)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "something went wrong,unable to fetch blogs")
	}
	return &blogs, nil
}
func (*blogDao) BlogListCount(category string) (int64, int64, error) {
	sql := "select count(1) from blogs where id > 0 "

	var total_num int64
	err := db.DBCli.Get(&total_num, sql)
	if err != nil {
		return 0, 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	var num int64
	err1 := db.DBCli.Get(&num, "select count(1) from blogs where id > 0 and category =?", category)
	if err1 != nil {
		return 0, 0, werror.FunWrap(err1, 200, "查询列表失败")
	}
	if category == "" {
		num = total_num
	}
	return total_num, num, nil
}
func (*blogDao) GetBlogInfo(id int64) (*model.Blog, error) {
	blog := model.Blog{}
	err := db.DBCli.Get(&blog, "select id,title, content,cover_url, user_id, nickname, category, language, is_top, is_recommend, sort, views_num,is_status,created_at,updated_at from blogs where id = ?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &blog, nil
}

package dao

import (
	// "phoenix/app/types"
	"fmt"
	"phoenix/internal/admin/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
	"time"
)

type feedDao struct{}

var FeedDao = new(feedDao)

// func (*feedDao) GetSecret(uid uint, offset int, limit int) (*[]model.Feed, error) {
// 	var feed []model.Feed
// 	err := db.DBCli.Select(&feed, "select id,feed_url,cover_url,bgm_url, intro, location, cate, tag, like_num, star_num, comment_num,share_num,watch_num,created_at, updated_at from feeds where user_id = ? and visible = ?",
// 		uid, types.VisibleEnumSelf)
// 	if err != nil {
// 		return nil, werror.FunWrap(err, 200, "查询失败")
// 	}
// 	return &feed, nil
// }
// func (*feedDao) GetAll(uid uint, offset int, limit int) (*[]model.Feed, error) {
// 	var feed []model.Feed
// 	err := db.DBCli.Select(&feed, "select id,feed_url,cover_url,bgm_url, intro, location, cate, tag, like_num, star_num, comment_num,share_num,watch_num,created_at,updated_at from feeds where user_id = ? and visible < ?",
// 		uid, types.VisibleEnumSelf)
// 	if err != nil {
// 		return nil, werror.FunWrap(err, 200, "查询失败")
// 	}
// 	return &feed, nil
// }

func (*feedDao) GetList(offset int, limit int) (*[]model.Feed, error) {
	var feed []model.Feed
	err := db.DBCli.Select(&feed, "select feeds.id,feed_url,cover_url, user_id, feeds.intro, location, cate, tag, like_num, star_num, comment_num,share_num,watch_num,feeds.created_at,feeds.updated_at, users.nickname as nickname from feeds left join users on feeds.user_id = users.id Order by feeds.id LIMIT ? OFFSET ?",
		limit, offset)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &feed, nil
}

func (*feedDao) GetFeedInfo(id int64) (*model.Feed, error) {
	fmt.Println(id)
	feed := model.Feed{}
	err := db.DBCli.Get(&feed, "select feeds.id,feed_url,cover_url, user_id, feeds.intro, location, cate, tag, like_num, star_num, comment_num,share_num,watch_num,feeds.created_at,feeds.updated_at, users.nickname as nickname from feeds left join users on feeds.user_id = users.id where feeds.id=?",
		id)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}
	return &feed, nil
}

func (*feedDao) Create(newFeed model.Feed) error {
	createTime := time.Now().Unix()
	updateTime := time.Now().Unix()
	_, err := db.DBCli.Exec("INSERT IGNORE `feeds` (`user_id`,  `visible`,`cate`,`tag`,`location`,`intro`,`feed_url`, `cover_url`, `created_at`, `updated_at`,`like_num`,`star_num`,`comment_num`,`share_num`,`watch_num`,`deleted_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ",
		newFeed.UserID, newFeed.Visible, newFeed.Cate, newFeed.Tag, newFeed.Location, newFeed.Intro, newFeed.FeedUrl, newFeed.CoverUrl, createTime, updateTime, newFeed.LikeNum, newFeed.StarNum, newFeed.CommentNum, newFeed.ShareNum, newFeed.WatchNum, newFeed.DeletedAt)
	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}
	return nil
}

func (*feedDao) FeedListCount() (int64, error) {
	sql := "select count(1) from feeds where id > 0 "

	var total_num int64
	err := db.DBCli.Get(&total_num, sql)
	if err != nil {
		return 0, werror.FunWrap(err, 200, "查询列表失败")
	}
	return total_num, nil
}

func (*feedDao) DeleteFeed(id int64) error {
	_, err := db.DBCli.Exec("DELETE FROM `feeds` WHERE id=? ", id)
	return err
}

func (*feedDao) EditFeed(feed *model.Feed) (*model.Feed, error) {
	// title := slugify.Make(blog.Title)
	result, err := db.DBCli.Exec("UPDATE `feeds` SET visible = ?, cate = ?, tag =?, location = ?, intro = ?, feed_url = ?, cover_url = ?, updated_at = ?  WHERE id= ? ", feed.Visible, feed.Cate, feed.Tag, feed.Location, feed.Intro, feed.FeedUrl, feed.CoverUrl, feed.UpdatedAt, feed.ID)

	if err != nil {
		return nil, werror.FunWrap(err, 200, "we are unable to update this feed")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return nil, werror.FunWrap(err, 200, "Feed not found")
	}

	if affected == 0 {
		return nil, werror.New(300, "Feed not updated")
	}

	updatedFeed, err := FeedDao.GetFeedInfo(feed.ID)

	if err != nil {
		return nil, werror.New(300, "Feed not found")
	}

	return updatedFeed, nil
}

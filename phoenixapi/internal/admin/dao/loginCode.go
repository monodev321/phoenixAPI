package dao

import (
	"time"

	"phoenix/internal/admin/model"
	"phoenix/pkg/db"
	"phoenix/pkg/werror"
)

type loginCodeDao struct{}

var LoginCodeDao = new(loginCodeDao)

func (*loginCodeDao) Add(login *model.LoginCode) error {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT IGNORE INTO `admin_login_codes` (`uuid`, `create_time`) values(?,?)",
		login.Uuid, createTime)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "获取登陆信息失败")
	}
	return nil
}

// 添加代币
func (*loginCodeDao) GetCodeByUuid(uuid string) (*model.LoginCode, error) {

	var loginCode model.LoginCode
	err := db.DBCli.Get(&loginCode, "select uuid,uid from `admin_login_codes` where uuid = ?",
		uuid)
	if err != nil {
		return nil, werror.FunWrap(err, 200, "查询失败")
	}

	if loginCode.Uid == 0 {
		return nil, werror.New(200, "还未授权登陆")
	}
	return &loginCode, nil
}

func (*loginCodeDao) Login(uid int64, uuid string) error {

	result, err := db.DBCli.Exec("UPDATE admin_login_codes SET uid = ?  WHERE uuid= ? ",
		uid, uuid)

	if err != nil {
		return werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return werror.New(300, "更新数据库失败")
	}

	return nil
}

func (s *loginCodeDao) Clean() error {
	now := time.Now().Unix() - 600
	_, err := db.DBCli.Exec("DELETE FROM `admin_login_codes` WHERE create_time<=? ", now)
	return err

}

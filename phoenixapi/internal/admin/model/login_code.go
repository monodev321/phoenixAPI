package model

type LoginCode struct {
	Uuid       string `json:"uuid" db:"uuid"`              // 登陆识别码
	Uid        int64  `json:"uid"`                         // 用户id
	CreateTime int64  `json:"createTime" db:"create_time"` // 第一次登陆时间
}

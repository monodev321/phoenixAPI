package model

import (
	usermodel "phoenix/internal/user/model"
)

type Blog struct {
	ID          int64  `json:"id"`
	Title       string `json:"title" db:"title"`
	Content     string `json:"content" db:"content"`
	CoverUrl    string `json:"coverUrl" db:"cover_url"`
	UserID      int64  `json:"userId" db:"user_id"`
	Nickname    string `json:"nickname" db:"nickname"`
	Category    string `json:"category" db:"category"`
	Language    string `json:"language" db:"language"`
	IsTop       uint   `json:"istop" db:"is_top"`
	IsRecommend uint   `json:"isrecommend" db:"is_recommend"`
	Sort        uint   `json:"sort" db:"sort"`
	ViewsNum    uint   `json:"viewsNum" db:"views_num"`
	IsStatus    uint   `json:"isStatus" db:"is_status"`
	CreatedAt   int64  `json:"createdAt" db:"created_at"`
	UpdatedAt   int64  `json:"updatedAt" db:"updated_at"`
}

type BlogInfo struct {
	*Blog
	UserBaseInfo *usermodel.UserBaseInfo
}

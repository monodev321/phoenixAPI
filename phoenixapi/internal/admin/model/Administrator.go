package model

type Administrator struct {
	Uid            int64  `json:"uid"`                                  // 用户id
	Role           int    `json:"role"`                                 // 0表示普通用户，1表示超级管理员，2表示普通管理员
	Status         int    `json:"status"`                               // 0表示冻结，1表示正常
	Username       string `json:"username"`                             // 用户名，为邮箱
	Nickname       string `json:"nickname"`                             // 用户昵称
	Avatar         string `json:"avatar"`                               // 用户头像
	Scope          string `json:"scope"`                                // 授权范围
	TokenType      string `json:"tokenType" db:"token_type"`            // token的类型
	Access         string `json:"access"`                               // jwt的token
	Refresh        string `json:"refresh"`                              // 刷新的token
	TokenExpiredAt int64  `json:"tokenExpiredAt" db:"token_expired_at"` // token过期时间
	TokenCreateAt  int64  `json:"tokenCreateAt" db:"token_create_at"`   // 创建时间
	CreateTime     int64  `json:"createTime" db:"create_time"`          // 第一次登陆时间
}
type AdministratorAccessTokens struct {
	Scope      string `json:"scope"`                           // 授权范围
	TokenType  string `json:"tokenType" db:"token_type"`       // token的类型
	Access     string `json:"access"`                          // jwt的token
	Refresh    string `json:"refresh"`                         // 刷新的token
	ExpiredAt  int64  `json:"expiredAt" db:"token_expired_at"` // token过期时间
	CreateTime int64  `json:"createTime" db:"create_time"`     // 第一次登陆时间
}

type AdministratorInfo struct {
	Uid        int64  `json:"uid"`                         // 用户id
	Role       int    `json:"role"`                        // 0表示普通用户，1表示超级管理员，2表示普通管理员
	Status     int    `json:"status"`                      // 0表示冻结，1表示正常
	Username   string `json:"username"`                    // 用户名，为邮箱
	Nickname   string `json:"nickname"`                    // 用户昵称
	Avatar     string `json:"avatar"`                      // 用户头像
	CreateTime int64  `json:"createTime" db:"create_time"` // 第一次登陆时间
}

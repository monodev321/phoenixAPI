package model

import (
	usermodel "phoenix/internal/user/model"
)

type Feed struct {
	ID         int64  `json:"id" db:"id" `
	CreatedAt  int64  `json:"created_at" db:"created_at"`
	UpdatedAt  int64  `json:"updated_at" db:"updated_at"`
	DeletedAt  int64  `json:"-"`
	UserID     int64  `json:"user_id" db:"user_id"`
	LikeNum    uint   `json:"like_num" db:"like_num"`
	Nickname   string `json:"nickname"`
	StarNum    uint   `json:"star_num" db:"star_num"`
	CommentNum uint   `json:"comment_num" db:"comment_num"`
	ShareNum   uint   `json:"share_num" db:"share_num"`
	WatchNum   uint   `json:"watch_num" db:"watch_num"`
	Visible    uint8  `json:"visible"` //public = 0, friend = 1, self = 2
	Cate       string `json:"cate" db:"cate"`
	Tag        string `json:"tag" db:"tag"`
	Location   string `json:"location" db:"location"`
	Intro      string `json:"intro" db:"intro"`
	FeedUrl    string `json:"feed_url" db:"feed_url"`
	CoverUrl   string `json:"cover_url" db:"cover_url"`
}

type FeedInfo struct {
	*Feed
	UserBaseInfo *usermodel.UserBaseInfo
}

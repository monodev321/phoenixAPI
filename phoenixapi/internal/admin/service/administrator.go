package service

import (
	"phoenix/internal/admin/dao"
	"phoenix/internal/admin/model"
)

type administratorService struct{}

var AdministratorService = new(administratorService)

func (*administratorService) GetUser(uid int64) (*model.Administrator, error) {
	user, err := dao.AdministratorDao.GetUser(uid)
	if err != nil {
		return nil, err
	}
	return user, err
}

func (*administratorService) UpdateUserToken(user *model.Administrator) error {
	err := dao.AdministratorDao.UpdateUserToken(user)
	return err
}

func (*administratorService) AdministratorList(status int, username string, offset int64, limit int) (*[]model.Administrator, int64, error) {
	user, err := dao.AdministratorDao.AdministratorList(status, username, offset, limit)
	if err != nil {
		return nil, 0, err
	}
	count := int64(0)
	if offset == 0 {
		count, err = dao.AdministratorDao.AdministratorListCount(status, username)
		if err != nil {
			return nil, 0, err
		}
	}
	return user, count, err
}

func (*administratorService) ChangeAdminField(uid int64, field string, value int) error {
	err := dao.AdministratorDao.ChangeAdminField(uid, field, value)
	return err
}

func (*administratorService) AdministratorDel(uid int64) error {
	err := dao.AdministratorDao.Delete(uid)
	return err
}

package service

import (
	dappDao "phoenix/internal/dapp/dao"
	dappModel "phoenix/internal/dapp/model"
)

type dappService struct{}

var DappService = new(dappService)

func (*dappService) DappList(status int, isRecommend, isPublishint int, symbol string, offset int64, limit int) (*[]dappModel.Dapp, int64, error) {
	user, err := dappDao.DappDao.DappList(status, isRecommend, isPublishint, symbol, offset, limit)
	if err != nil {
		return nil, 0, err
	}
	count := int64(0)
	if offset == 0 {
		count, err = dappDao.DappDao.DappListCount(status, isRecommend, isPublishint, symbol)
		if err != nil {
			return nil, 0, err
		}
	}
	return user, count, err
}

func (*dappService) ChangeDappField(id int64, field string, value int) error {
	err := dappDao.DappDao.ChangeField(id, field, value)
	return err
}

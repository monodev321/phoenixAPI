package service

import (
	dappDao "phoenix/internal/dapp/dao"
	dappModel "phoenix/internal/dapp/model"
)

type developerService struct{}

var DeveloperService = new(developerService)

func (*developerService) DeveloperList(status int, username string, offset int64, limit int) (*[]dappModel.Developer, int64, error) {
	user, err := dappDao.DevelopDao.DeveloperList(status, username, offset, limit)
	if err != nil {
		return nil, 0, err
	}
	count := int64(0)
	if offset == 0 {
		count, err = dappDao.DevelopDao.DeveloperListCount(status, username)
		if err != nil {
			return nil, 0, err
		}
	}
	return user, count, err
}

func (*developerService) ChangeDeveloperField(id int64, field string, value int) error {
	err := dappDao.DevelopDao.ChangeDeveloperField(id, field, value)
	return err
}

package service

import (
	"phoenix/internal/admin/dao"
	"phoenix/internal/admin/model"
)

type blogService struct{}

var BlogService = new(blogService)

func (*blogService) Add(blog *model.Blog) (*model.Blog, error) {
	result, err := dao.BlogDao.Add(blog)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (*blogService) BlogList(category string, offset int64, limit int) (*[]model.Blog, int64, int64, error) {
	count := int64(0)
	total := int64(0)
	total, count, err := dao.BlogDao.BlogListCount(category)
	if err != nil {
		return nil, 0, 0, err
	}

	blogs, err := dao.BlogDao.BlogList(category, offset, limit)
	if err != nil {
		return nil, 0, 0, err
	}
	return blogs, total, count, nil
}

func (*blogService) GetBlogInfo(id int64) (*model.Blog, error) {
	blog, err := dao.BlogDao.GetBlogInfo(id)
	if err != nil {
		return nil, err
	}
	return blog, err
}

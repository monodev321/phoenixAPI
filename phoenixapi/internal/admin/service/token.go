package service

import (
	"phoenix/internal/token/dao"
	"phoenix/internal/token/model"
)

type tokenService struct{}

var TokenService = new(tokenService)

func (*tokenService) TokenList(status int, symbol string, offset int64, limit int) (*[]model.CoinToken, int64, error) {
	user, err := dao.TokenDao.TokenList(status, symbol, offset, limit)
	if err != nil {
		return nil, 0, err
	}
	count := int64(0)
	if offset == 0 {
		count, err = dao.TokenDao.TokenListCount(status, symbol)
		if err != nil {
			return nil, 0, err
		}
	}
	return user, count, err
}

func (*tokenService) ChangeTokenField(id int64, field string, value int) error {
	err := dao.TokenDao.ChangeField(id, field, value)
	return err
}

package service

import (
	"strconv"
	"time"

	"phoenix/internal/admin/dao"
	"phoenix/internal/admin/model"

	"phoenix/pkg/config"
	"phoenix/pkg/util"
)

type authService struct{}

var AuthService = new(authService)

func (*authService) Login(uuid string, code string, uid int64, username string) error {

	//获取token
	data := map[string]string{
		"code":          code,
		"grant_type":    "authorization_code",
		"redirect_uri":  config.AppCfg.AdminOauth2.RedirectUri,
		"client_id":     strconv.FormatInt(config.AppCfg.AdminOauth2.ID, 10),
		"client_secret": config.AppCfg.AdminOauth2.Secret,
	}
	res, err := util.Post("/v1/oauth2/token", data, "user", true)
	if err != nil {
		return err
	}
	// 写入数据库
	now := time.Now().Unix()
	expire := strconv.FormatFloat(res["expires_in"].(float64), 'g', -1, 64)
	expire2, err := strconv.ParseInt(expire, 10, 64)
	if err != nil {
		return err
	}

	//@写入用户信息
	user, err := dao.AdministratorDao.GetUser(uid)
	if err != nil {
		return err
	}
	userModel := &model.Administrator{
		Uid:            uid,
		Username:       username,
		Scope:          res["scope"].(string),
		TokenType:      res["token_type"].(string),
		Access:         res["access_token"].(string),
		Refresh:        res["refresh_token"].(string),
		TokenExpiredAt: now + expire2,
		TokenCreateAt:  now,
	}

	if user != nil {
		err = dao.AdministratorDao.UpdateToken(userModel)
		if err != nil {
			return err
		}
	}

	err = dao.LoginCodeDao.Login(uid, uuid)
	return err
}

// // 增加sql功能
func (*authService) RefreshToken(refresh string, uid int64) (*model.Administrator, error) {
	data := map[string]string{
		"grant_type":    "refresh_token",
		"refresh_token": refresh,
		"client_id":     strconv.FormatInt(config.AppCfg.AdminOauth2.ID, 10),
		"client_secret": config.AppCfg.AdminOauth2.Secret,
	}
	res, err := util.Post("/v1/oauth2/token", data, "user", true)
	if err != nil {
		return nil, err
	}
	// 写入数据库
	now := time.Now().Unix()
	expire := strconv.FormatFloat(res["expires_in"].(float64), 'g', -1, 64)
	expire2, err := strconv.ParseInt(expire, 10, 64)
	if err != nil {
		return nil, err
	}
	user := &model.Administrator{
		Uid:            uid,
		Scope:          res["scope"].(string),
		TokenType:      res["token_type"].(string),
		Access:         res["access_token"].(string),
		Refresh:        res["refresh_token"].(string),
		TokenExpiredAt: now + expire2,
		TokenCreateAt:  now,
	}
	dao.AdministratorDao.UpdateToken(user)

	return user, nil
}

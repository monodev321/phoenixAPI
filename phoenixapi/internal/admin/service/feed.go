package service

import (
	"phoenix/internal/admin/dao"
	"phoenix/internal/admin/model"
)

type feedService struct{}

var FeedService = new(feedService)

func (*feedService) GetList(offset int, limit int) (*[]model.Feed, int64, error) {
	total := int64(0)
	total, err := dao.FeedDao.FeedListCount()
	if err != nil {
		return nil, 0, err
	}
	feed, err := dao.FeedDao.GetList(offset, limit)
	if err != nil {
		return nil, 0, err
	}
	return feed, total, err
}

func (*feedService) GetFeedInfo(id int64) (*model.Feed, error) {
	feed, err := dao.FeedDao.GetFeedInfo(id)
	if err != nil {
		return nil, err
	}
	return feed, err
}

func (*feedService) Create(newFeed model.Feed) error {
	err := dao.FeedDao.Create(newFeed)
	if err != nil {
		return err
	}
	return nil
}

func (*feedService) DeleteFeedById(id int64) error {
	err := dao.FeedDao.DeleteFeed(id)
	if err != nil {
		return err
	}
	return nil
}

func (*feedService) EditFeedById(feed *model.Feed) (*model.Feed, error) {
	editFeed, err := dao.FeedDao.EditFeed(feed)
	if err != nil {
		return nil, err
	}

	return editFeed, nil
}

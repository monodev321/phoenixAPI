package service

import (
	"time"

	"phoenix/internal/admin/dao"
	"phoenix/internal/admin/model"
	"phoenix/pkg/hash"
)

type loginCodeService struct{}

var LoginCodeService = new(loginCodeService)

func (*loginCodeService) GenerateLogin() (*model.LoginCode, error) {
	createTime := time.Now().Unix()
	lUuid := hash.GetUuid()
	login := &model.LoginCode{
		CreateTime: createTime,
		Uuid:       lUuid,
	}
	err := dao.LoginCodeDao.Add(login)
	if err != nil {
		return login, err
	}
	// 推送好友消息到好友申请服务器

	return login, nil
}

func (*loginCodeService) LoopLogin(uuid string) (*model.LoginCode, error) {

	code, err := dao.LoginCodeDao.GetCodeByUuid(uuid)
	if err != nil {
		return nil, err
	}
	return code, nil
}

func (*loginCodeService) CleanLoginCode() error {
	err := dao.LoginCodeDao.Clean()
	return err
}

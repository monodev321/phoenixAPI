package service

import (
	adminDao "phoenix/internal/admin/dao"
	adminModel "phoenix/internal/admin/model"
	"phoenix/internal/user/dao"
	"phoenix/internal/user/model"
)

type userService struct{}

var UserService = new(userService)

func (*userService) UserList(status int, username string, offset int64, limit int) (*[]model.User, int64, error) {
	user, err := dao.UserDao.UserList(status, username, offset, limit)
	if err != nil {
		return nil, 0, err
	}
	count := int64(0)
	if offset == 0 {
		count, err = dao.UserDao.UserListCount(status, username)
		if err != nil {
			return nil, 0, err
		}
	}
	return user, count, err
}

func (*userService) ChangeUserField(uid int64, field string, value int) error {
	err := dao.UserDao.ChangeUserField(uid, field, value)
	return err
}

func (*userService) AdministratorAdd(uid int64) error {
	user, err := dao.UserDao.GetUserInfo(uid)
	if err != nil {
		return err
	}
	admin := &adminModel.Administrator{
		Uid:      user.ID,
		Username: user.Username,
		Nickname: user.Nickname,
		Avatar:   user.AvatarUrl,
		Role:     1,
	}
	err = adminDao.AdministratorDao.Add(admin)
	return err
}

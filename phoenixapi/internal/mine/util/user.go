package util

import (
	"phoenix/internal/mine/dao"
	"phoenix/internal/mine/model"
	userDao "phoenix/internal/user/dao"
	userModel "phoenix/internal/user/model"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"
	"strconv"
	"strings"
)

type userUtil struct{}

var UserUtil = new(userUtil)

func (*userUtil) GetReferrerArr(referrer string) []string {

	str_arr := strings.Split(referrer, `_`)
	return str_arr
}

func (*userUtil) AddVipEarning(referrer string, level int, amount int, uid int64, tokenId int64, symbol string) error {
	ym, _ := strconv.Atoi(util.GetNowYM())
	ymd, _ := strconv.Atoi(util.GetNowYMD())
	money := 0
	if level == 1 {
		money = amount * 15 / 100
	}
	if level == 2 {
		money = amount * 6 / 100
	}

	var user *userModel.User
	var err error
	referrerArr := UserUtil.GetReferrerArr(referrer)
	if len(referrerArr) >= level {
		index := len(referrerArr) - level
		refUid, _ := strconv.ParseInt(referrerArr[index], 10, 64)
		user, err = userDao.UserDao.GetUserInfo(refUid)
		if err != nil {
			return werror.New(200, "用户不存在")
		}
	} else {
		return werror.New(200, "用户不存在")
	}
	log := model.VipEarning{
		Uid:         user.ID,
		Nickname:    user.Nickname,
		Username:    user.Username,
		Avatar:      user.AvatarUrl,
		Ym:          ym,
		Ymd:         ymd,
		Level:       level,
		TokenId:     tokenId,
		TokenSymbol: symbol,
		Amount:      money,
		Vuid:        uid,
		Status:      0,
	}
	dao.VipDao.Add(&log)
	return nil
}

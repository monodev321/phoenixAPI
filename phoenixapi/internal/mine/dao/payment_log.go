package dao

import (
	"time"

	"phoenix/internal/mine/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type paymentLogDao struct{}

var PaymentLogDao = new(paymentLogDao)

// 添加代币
func (*paymentLogDao) Add(m *model.PaymentLog) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT INTO `payment_logs` (`order_id`, `uid`, `nickname`, `username`, `avatar`, `kind`, `token_id`, `token_symbol`, `amount`, `status`, `create_time`) values(?,?,?,?,?,?,?,?,?,?,?)",
		m.OrderId, m.Uid, m.Nickname, m.Username, m.Avatar, m.Kind, m.TokenId, m.TokenSymbol, m.Amount, m.Status, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "代币已经存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加代币失败")
	}

	return id, nil
}

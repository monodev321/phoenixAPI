package dao

import (
	"time"

	"phoenix/internal/mine/model"
	"phoenix/pkg/werror"

	"phoenix/pkg/db"
)

type vipDao struct{}

var VipDao = new(vipDao)

// 添加代币
func (*vipDao) Add(m *model.VipEarning) (int64, error) {
	createTime := time.Now().Unix()
	result, err := db.DBCli.Exec("INSERT INTO `vip_earning` (`uid`, `nickname`, `username`, `avatar`, `ym`, `ymd`, `level`, `token_id`, `token_symbol`, `amount`, `vuid`, `status`, `create_time`) values(?,?,?,?,?,?,?,?,?,?,?,?,?)",
		m.Uid, m.Nickname, m.Username, m.Avatar, m.Ym, m.Ymd, m.Level, m.TokenId, m.TokenSymbol, m.Amount, m.Vuid, m.Status, createTime)

	if err != nil {
		return 0, werror.FunWrap(err, 200, "插入数据库错误")
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return 0, werror.FunWrap(err, 200, "获取数据库结果错误")
	}

	if affected == 0 {
		return 0, werror.New(300, "代币已经存在")
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, werror.New(300, "添加代币失败")
	}

	return id, nil
}

package service

import (
	"phoenix/internal/mine/dao"
	"phoenix/internal/mine/model"
	"phoenix/internal/mine/util"
	tokenDao "phoenix/internal/token/dao"
	userDao "phoenix/internal/user/dao"
	"phoenix/pkg/werror"
)

type vipService struct{}

var VipService = new(vipService)

// @todo 这三个需要增加权限判断，是否有操作权限，uid是否等于开发者id
func (*vipService) Add(OrderId string, uid int64, tokenId int64, kind string, amount int) error {
	user, err := userDao.UserDao.GetUserInfo(uid)
	if err != nil {
		return werror.New(200, "收款用户不存在")
	}
	token, err := tokenDao.TokenDao.GetTokenById(tokenId)
	if err != nil {
		return werror.New(200, "收款代币不存在")
	}
	// - [ ] 增加购买记录
	log := model.PaymentLog{
		Uid:         user.ID,
		Nickname:    user.Nickname,
		Username:    user.Username,
		Avatar:      user.AvatarUrl,
		Kind:        kind,
		TokenId:     tokenId,
		TokenSymbol: token.Symbol,
		Amount:      amount, //  支付金额
		Status:      1,      //  1表示关注，0表示取消关注
	}
	_, err = dao.PaymentLogDao.Add(&log)
	if err != nil {
		return err
	}
	// - [ ] 增加vip身份
	if user.Role == 0 {
		userDao.UserDao.SetRole(uid, 1)
	}

	// - [ ] 增加用户分润
	err = util.UserUtil.AddVipEarning(user.ReferrerPath, 1, amount, user.ID, tokenId, token.Symbol)
	if err != nil {
		println(err, 1)
	}
	err = util.UserUtil.AddVipEarning(user.ReferrerPath, 2, amount, user.ID, tokenId, token.Symbol)
	if err != nil {
		println(err, 2)
	}
	return nil
}

package service

import (
	"phoenix/internal/mine/dao"
	"phoenix/internal/mine/model"
	tokenDao "phoenix/internal/token/dao"
	userDao "phoenix/internal/user/dao"
	"phoenix/pkg/werror"
)

type paymentService struct{}

var PaymentService = new(paymentService)

// @todo 这三个需要增加权限判断，是否有操作权限，uid是否等于开发者id
func (*paymentService) Add(symbol string, username string, amount int, kind string) (int64, error) {
	_, err := dao.PaymentDao.GetPaymentByKind(kind)
	if err != nil {
		return 0, err
	}

	user, err := userDao.UserDao.GetUserByUsername(username)
	if err != nil {
		return 0, werror.New(200, "收款用户不存在")
	}
	token, err := tokenDao.TokenDao.GetTokenBySymbol(symbol)
	if err != nil {
		return 0, werror.New(200, "收款代币不存在")
	}

	payment := &model.SystemPayment{
		Uid:         (*user).ID,
		Username:    (*user).Username,
		Nickname:    (*user).Nickname,
		Avatar:      (*user).AvatarUrl,
		TokenId:     token.Id,
		TokenSymbol: token.Symbol,
		Kind:        kind,
		Amount:      amount,
		Status:      1,
	}
	id, err := dao.PaymentDao.Add(payment)
	if err != nil {
		return 0, err
	}
	// 推送好友消息到好友申请服务器

	return id, err
}

func (*paymentService) GetPaymentByKind(kind string) (*model.SystemPayment, error) {
	payment, err := dao.PaymentDao.GetPaymentByKind(kind)
	if err != nil {
		return nil, err
	}
	// 推送好友消息到好友申请服务器

	return payment, err
}

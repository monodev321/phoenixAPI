package model

type BloggerEarning struct {
	ID          int64  `db:"id" json:"id"`                    //  关系id
	Uid         int64  `db:"uid" json:"uid"`                  //  收款uid
	Nickname    string `db:"nickname" json:"nickname"`        //  收款用户名
	Username    string `db:"username" json:"username"`        //  收款用户名
	Avatar      string `db:"avatar" json:"avatar"`            //  头像
	Vid         int64  `db:"vid" json:"vid"`                  //  作品id
	Ym          int    `db:"ym" json:"ym"`                    //  年月202301
	Ymd         int    `db:"ymd" json:"ymd"`                  //  年月2023013
	Level       int    `db:"level" json:"level"`              //  分润等级
	TokenId     int64  `db:"token_id" json:"tokenId"`         //  代币id
	TokenSymbol string `db:"token_symbol" json:"tokenSymbol"` //  代币代号
	Amount      int64  `db:"amount" json:"amount"`            //  支付金额
	Views       int    `db:"views" json:"views"`              //  用户观看数量
	Times       int    `db:"times" json:"times"`              //  挖矿时间
	Status      int    `db:"status" json:"status"`            //  1表示已经确定，0表示待确定，2表示已经发放,3表示取消
	CreateTime  int64  `db:"create_time" json:"createTime"`   //  创建时间
}

type VideoLogs struct {
	ID        int64  `db:"id" json:"id"`                //  关系id
	Uid       int64  `db:"uid" json:"uid"`              //  收款uid
	Nickname  string `db:"nickname" json:"nickname"`    //  收款用户名
	Username  string `db:"username" json:"username"`    //  收款用户名
	Avatar    string `db:"avatar" json:"avatar"`        //  头像
	Vid       int64  `db:"vid" json:"vid"`              //  作品id
	Ym        int    `db:"ym" json:"ym"`                //  年月202301
	Ymd       int    `db:"ymd" json:"ymd"`              //  年月2023013
	Puid      int64  `db:"puid" json:"puid"`            //  收款uid
	Pnickname string `db:"pnickname" json:"pnickname"`  //  收款用户名
	Pusername string `db:"pusername" json:"pusername"`  //  收款用户名
	Pavatar   string `db:"pavatar" json:"pavatar"`      //  头像
	Status    int    `db:"status" json:"status"`        //  1表示已经确定，0表示待确定，2表示取消
	StartTime int64  `db:"start_time" json:"startTime"` //  创建时间
	EndTime   int64  `db:"end_time" json:"endTime"`     //  结束时间
}

package model

type SystemPayment struct {
	ID          int64  `db:"id" json:"id"`                    //  关系id
	Uid         int64  `db:"uid" json:"uid"`                  //  收款uid
	Nickname    string `db:"nickname" json:"nickname"`        //  收款用户名
	Username    string `db:"username" json:"username"`        //  收款用户名
	Avatar      string `db:"avatar" json:"avatar"`            //  头像
	Kind        string `db:"kind" json:"kind"`                //  类型：vip/矿机/主播/博主
	TokenId     int64  `db:"token_id" json:"tokenId"`         //  代币id
	TokenSymbol string `db:"token_symbol" json:"tokenSymbol"` //  代币代号
	Amount      int    `db:"amount" json:"amount"`            //  支付金额
	Status      int    `db:"status" json:"status"`            //  1表示关注，0表示取消关注
	CreateTime  int64  `db:"create_time" json:"createTime"`   //  创建时间
}

type PaymentLog struct {
	ID          int64  `db:"id" json:"id"`                    //  关系id
	OrderId     string `db:"order_id" json:"orderId"`         //  类型：vip/矿机/主播/博主
	Uid         int64  `db:"uid" json:"uid"`                  //  收款uid
	Nickname    string `db:"nickname" json:"nickname"`        //  收款用户名
	Username    string `db:"username" json:"username"`        //  收款用户名
	Avatar      string `db:"avatar" json:"avatar"`            //  头像
	Kind        string `db:"kind" json:"kind"`                //  类型：vip/矿机/主播/博主
	TokenId     int64  `db:"token_id" json:"token_id"`        //  代币id
	TokenSymbol string `db:"token_symbol" json:"tokenSymbol"` //  代币代号
	Amount      int    `db:"amount" json:"amount"`            //  支付金额
	Status      int    `db:"status" json:"status"`            //  1表示关注，0表示取消关注
	CreateTime  int64  `db:"create_time" json:"createTime"`   //  创建时间
}

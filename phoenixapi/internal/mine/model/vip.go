package model

type VipEarning struct {
	ID          int64  `db:"id" json:"id"`                    //  关系id
	Uid         int64  `db:"uid" json:"uid"`                  //  收款uid
	Nickname    string `db:"nickname" json:"nickname"`        //  收款用户名
	Username    string `db:"username" json:"username"`        //  收款用户名
	Avatar      string `db:"avatar" json:"avatar"`            //  头像
	Ym          int    `db:"ym" json:"ym"`                    //  年月202301
	Ymd         int    `db:"ymd" json:"ymd"`                  //  年月2023013
	Level       int    `db:"level" json:"level"`              //  分润等级
	TokenId     int64  `db:"token_id" json:"tokenId"`         //  代币id
	TokenSymbol string `db:"token_symbol" json:"tokenSymbol"` //  代币代号
	Amount      int    `db:"amount" json:"amount"`            //  支付金额
	Vuid        int64  `db:"vuid" json:"vuid"`                //  被推荐人uid
	Status      int    `db:"status" json:"status"`            //  1表示已经确定，0表示待确定，2表示已经发放,3表示取消
	CreateTime  int64  `db:"create_time" json:"createTime"`   //  创建时间
}

package model

type MineNodes struct {
	ID         int64  `db:"id" json:"id"`                  //  关系id
	Serial     string `db:"serial" json:"serial"`          //  类型：节点编号，年月日+id
	Ym         int    `db:"ym" json:"ym"`                  //  年月202301
	Ymd        int    `db:"ymd" json:"ymd"`                //  年月2023013
	Level      int    `db:"level" json:"level"`            //  分润等级
	Link       string `db:"link" json:"link"`              //  代币代号
	Gross      int    `db:"gross" json:"gross"`            //  总额
	Share      int    `db:"share" json:"share"`            //  支付金额
	Favorer    int    `db:"favorer" json:"favorer"`        //  年月2023013
	Status     int    `db:"status" json:"status"`          //  1表示已经确定，0表示待确定，2表示已经发放,3表示取消
	StartTime  int64  `db:"start_time" json:"startTime"`   //  创建时间
	EndTime    int64  `db:"end_time" json:"endTime"`       //  创建时间
	CreateTime int64  `db:"create_time" json:"createTime"` //  创建时间
}

type UserNodes struct {
	ID         int64  `db:"id" json:"id"`                  //  关系id
	Uid        int64  `db:"uid" json:"uid"`                //  收款uid
	Nickname   string `db:"nickname" json:"nickname"`      //  收款用户名
	Username   string `db:"username" json:"username"`      //  收款用户名
	Avatar     string `db:"avatar" json:"avatar"`          //  头像
	Nid        int64  `db:"nid" json:"nid"`                //  节点id
	Serial     string `db:"serial" json:"serial"`          //  类型：节点编号，年月日+id
	Ym         int    `db:"ym" json:"ym"`                  //  年月202301
	Ymd        int    `db:"ymd" json:"ymd"`                //  年月2023013
	Level      int    `db:"level" json:"level"`            //  分润等级
	Link       string `db:"link" json:"link"`              //  代币代号
	Share      int    `db:"share" json:"share"`            //  支付金额
	Status     int    `db:"status" json:"status"`          //  1表示已经确定，0表示待确定，2表示已经发放,3表示取消
	CreateTime int64  `db:"create_time" json:"createTime"` //  创建时间
}

type NodeEarning struct {
	ID          int64  `db:"id" json:"id"`                    //  关系id
	Uid         int64  `db:"uid" json:"uid"`                  //  收款uid
	Nickname    string `db:"nickname" json:"nickname"`        //  收款用户名
	Username    string `db:"username" json:"username"`        //  收款用户名
	Avatar      string `db:"avatar" json:"avatar"`            //  头像
	Nid         int64  `db:"nid" json:"nid"`                  //  节点id
	Serial      string `db:"serial" json:"serial"`            //  类型：节点编号，年月日+id
	Ym          int    `db:"ym" json:"ym"`                    //  年月202301
	Ymd         int    `db:"ymd" json:"ymd"`                  //  年月2023013
	Level       int    `db:"level" json:"level"`              //  分润等级
	TokenId     int64  `db:"token_id" json:"token_id"`        //  代币id
	TokenSymbol string `db:"token_symbol" json:"tokenSymbol"` //  代币代号
	Amount      int    `db:"amount" json:"amount"`            //  支付金额
	Share       int    `db:"share" json:"share"`              //  支付金额
	Status      int    `db:"status" json:"status"`            //  1表示已经确定，0表示待确定，2表示已经发放,3表示取消
	CreateTime  int64  `db:"create_time" json:"createTime"`   //  创建时间
}

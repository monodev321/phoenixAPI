package auth

import (
	"context"
	"errors"

	"google.golang.org/grpc/metadata"

	// "google.golang.org/grpc/status"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
)

// AuthToekn 自定义认证
type AuthToekn struct {
	Token string
}

func (c AuthToekn) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"authorization": c.Token,
	}, nil
}

func (c AuthToekn) RequireTransportSecurity() bool {
	return false
}

func getTokenFromContext(ctx context.Context) (string, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return "", errors.New("获取metadata错误")
	}
	// md 的类型是 type MD map[string][]string
	token, ok := md["authorization"]
	if !ok || len(token) == 0 {
		return "", errors.New("获取token失败")
	}
	// 因此，token 是一个字符串数组，我们只用了 token[0]
	return token[0], nil
}

func CheckAuth(ctx context.Context) (context.Context, error) {
	// accessToken, err := getTokenFromContext(ctx)
	// if err != nil {
	// 	return ctx, status.Error(401, "获取token失误")
	// }

	// uid, username, err := util.ParseJwtToken(accessToken, config.AppConf.Oauth2.PublickKey)
	// if err != nil {
	// 	return ctx, status.Error(404, "token错误")
	// }

	// md, ok := metadata.FromIncomingContext(ctx)
	// if !ok {
	// 	return ctx, status.Error(404, "存储用户错误")
	// }
	// // md.Set("uid", "2")
	// // md.Set("username", accessToken)
	// md.Set("uid", uid.(string))
	// md.Set("username", username.(string))
	// fmt.Printf("%+v ===", md)
	// ctx = metadata.NewOutgoingContext(ctx, md)
	return ctx, nil
}

func GetUser(ctx context.Context) (string, string, error) {

	accessToken, err := getTokenFromContext(ctx)
	if err != nil {
		return "", "", errors.New("获取token失误")
	}

	uid, username, err := util.ParseJwtToken(accessToken, config.AppCfg.Oauth2.PublickKey)
	if err != nil {
		return "", "", errors.New("token错误")
	}

	return uid.(string), username.(string), nil
}

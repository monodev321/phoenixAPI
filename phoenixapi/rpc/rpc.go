package rpc

import (
	"log"
	"net"

	"phoenix/pkg/config"
	"phoenix/rpc/auth"
	pb "phoenix/rpc/proto"
	"phoenix/rpc/service"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"google.golang.org/grpc"
)

func RunRpcService() error {
	server := grpc.NewServer(grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
		grpc_auth.UnaryServerInterceptor(auth.CheckAuth),
	)))
	pb.RegisterUserServiceServer(server, &service.UserServer{})
	lis, err := net.Listen("tcp", config.AppCfg.GrpcAddr)
	if err != nil {
		log.Fatalf("net.Listen err: %v", err)
		return err
	}
	err = server.Serve(lis)
	if err != nil {
		log.Fatalf("net.Listen err: %v", err)
	}
	return err
}

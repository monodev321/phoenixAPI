package service

import (
	"errors"
	"fmt"
	"strconv"

	"phoenix/rpc/auth"
	pb "phoenix/rpc/proto"

	"golang.org/x/net/context"

	imService "phoenix/internal/im/service"
	"phoenix/internal/user/model"
	"phoenix/internal/user/service"
)

type UserServer struct {
}

func (d *UserServer) Login(ctx context.Context, in *pb.LoginRequest) (*pb.LoginResponse, error) {

	uid, username, err := auth.GetUser(ctx)
	if err != nil {
		return &pb.LoginResponse{
			Code: 500,
			Msg:  fmt.Sprintf("%s", err.Error()),
		}, err
	}
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		return &pb.LoginResponse{
			Code: 500,
			Msg:  fmt.Sprintf("%s", err.Error()),
		}, err
	}
	m := &model.UserLogin{
		Num:      in.Num,
		Brand:    in.Brand,
		Platform: in.Platform,
		Version:  in.Version,
		Push:     in.Push,
		Token:    in.Token,
		Uid:      uid_i,
		Username: username,
	}
	err = service.LoginService.Add(m)
	if err != nil {
		return &pb.LoginResponse{
			Code: 500,
			Msg:  fmt.Sprintf("%s", err.Error()),
		}, err
	}
	return &pb.LoginResponse{
		Code: 0,
		Msg:  "success",
	}, nil
}

func (d *UserServer) Pull(ctx context.Context, in *pb.PullRequest) (*pb.PullResponse, error) {
	uid, _, err := auth.GetUser(ctx)
	if err != nil {
		return &pb.PullResponse{
			Code:     500,
			Messages: nil,
		}, err
	}
	msgs, err := imService.MessageService.Pull(uid)
	// fmt.Printf("%+v pull msg", msgs)
	if err != nil {
		return &pb.PullResponse{
			Code:     500,
			Messages: nil,
		}, err
	}
	return &pb.PullResponse{
		Code:     0,
		Messages: *msgs,
	}, nil
}

func (d *UserServer) Ack(ctx context.Context, in *pb.AckRequest) (*pb.AckResponse, error) {
	// fmt.Printf("%+v msg ask", in.Messages)
	uid, _, err := auth.GetUser(ctx)
	if err != nil {
		return &pb.AckResponse{
			Code: 500,
		}, err
	}
	if len(in.Messages) == 0 {
		return &pb.AckResponse{
			Code: 500,
		}, errors.New("msg id 不能为空")
	}
	err = imService.MessageService.Ack(in.Messages, uid)
	if err == nil {
		return &pb.AckResponse{
			Code: 0,
		}, nil
	}
	return &pb.AckResponse{
		Code: 500,
	}, err
}

func (d *UserServer) Push(ctx context.Context, in *pb.PushRequest) (*pb.PushResponse, error) {
	// fmt.Printf("%+v msg Push", in.Messages)
	uid, _, err := auth.GetUser(ctx)
	if err != nil {
		return &pb.PushResponse{
			Code: 500,
			Uids: nil,
		}, err
	}
	uid_i, err := strconv.ParseUint(uid, 10, 64)
	if err != nil {
		return &pb.PushResponse{
			Code: 500,
			Uids: nil,
		}, err
	}
	data, err := imService.MessageService.Push(in.Messages, uid_i)
	if err != nil {
		return &pb.PushResponse{
			Code: 500,
			Uids: nil,
		}, err
	}
	return &pb.PushResponse{
		Code: 0,
		Uids: data,
	}, nil
}

1. 安装命令行

go install -tags 'mysql' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
1. 测试如何使用
    ```dotnetcli
    #-ext 表示文件扩展
    migrate create -ext sql -dir sql create_wallet_database

    ## -digits 表示seq的位数
    migrate create -ext sql -dir database -seq -digits 3 create_wallet_database

    ## 其他参数 -seq 默认是6位数
    migrate create -ext sql -dir database -seq create_wallet_database
     
    -format格式化文件名，可以不管
    migrate create -ext txt -dir database -format 1_create_wallet_database3.txt

    ```
1. 执行升级数据库
    ```dotnetcli
    migrate -verbose -source file://sql -path /Users/villian/app/wallet/server/tools/migrations/ -database mysql://root:123456@tcp(127.0.0.1:3306)/wallet?multiStatements=true up

    migrate -verbose -source file://sql -path /Users/villian/app/wallet/server/tools/migrations/ -database mysql://root:123456@tcp(127.0.0.1:3306)/wallet?multiStatements=true force 20200724102142

    ```
1. 执行回滚
    ```dotnetcli
    migrate -verbose -source file://sql -path /Users/villian/app/wallet/server/tools/migrations/ -database mysql://root:123456@tcp(127.0.0.1:3306)/wallet?multiStatements=true down
    ```
1. 创建数据库
    ```dotnetcli
    migrate create -ext sql -dir database create_wallet_database

    CREATE DATABASE `wallet` CHARACTER SET utf8 COLLATE utf8_general_ci;

    drop database `wallet`
    ```

2. 创建用户表
 ```bash
 migrate create -ext sql -dir sql create_table_user
 ```

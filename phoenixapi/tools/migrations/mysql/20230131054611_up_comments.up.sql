ALTER TABLE `comments` ADD  `film_id` BIGINT(20) AFTER `feed_id`;
CREATE INDEX `idx_comment_film_id` ON `comments` (`film_id`);
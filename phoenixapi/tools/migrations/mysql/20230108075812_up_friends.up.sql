ALTER TABLE `friends` ADD  `relation` tinyint(2) NOT NULL DEFAULT '1' COMMENT '关系0:拉黑，1:关注，2是好友' AFTER `is_top`;
ALTER TABLE `friends` ADD  `is_watch` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否关注作品，默认1' AFTER `is_top`;
ALTER TABLE `friends` ADD  `is_allow_watch` tinyint(2) NOT NULL DEFAULT '1' COMMENT '是否允许被关注' AFTER `is_top`;
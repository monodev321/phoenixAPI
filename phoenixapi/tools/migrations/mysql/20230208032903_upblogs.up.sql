ALTER TABLE `blogs` ADD  `is_top` tinyint(1) NOT NULL DEFAULT '0'  AFTER `is_status`;
ALTER TABLE `blogs` ADD  `is_recommend` tinyint(4) NOT NULL DEFAULT '0' AFTER `is_status`;
ALTER TABLE `blogs` ADD  `sort` tinyint(4) NOT NULL DEFAULT '0' AFTER `is_status`;
ALTER TABLE `blogs` ADD  `language` VARCHAR(255) NOT NULL AFTER `category`;
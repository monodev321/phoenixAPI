CREATE TABLE IF NOT EXISTS blogs (
    `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(2000) NOT NULL,
    `content` TEXT NOT NULL,
    `cover_url` VARCHAR(255) NOT NULL,
    `user_id` int UNSIGNED NOT NULL,
    `nickname` VARCHAR(255) NOT NULL,
    `category` VARCHAR(255) NOT NULL,
    `language` VARCHAR(255) NOT NULL,
    `views_num` int UNSIGNED NOT NULL,
    `is_status` int UNSIGNED NOT NULL,
    `created_at` int(11) NOT NULL DEFAULT 0,
    `updated_at` int(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
CREATE TABLE `user_login` (
  `uid` INT(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `username` VARCHAR(150) DEFAULT NULL DEFAULT "" COMMENT '加密的用户名',
  `num` INT(5) NOT NULL DEFAULT 0 COMMENT '版本号',
  `brand` VARCHAR(200) NOT NULL DEFAULT "" COMMENT '手机品牌',
  `token` VARCHAR(1000) NOT NULL DEFAULT "" COMMENT 'pushtoken',
  `version` VARCHAR(80) NOT NULL DEFAULT "" COMMENT '版本',
  `platform` tinyint(1)  NOT NULL DEFAULT 0 COMMENT '0标示安卓1表示ios',
  `push`  tinyint(1)  NOT NULL DEFAULT 1 COMMENT '0没有1是fcm2是apns',
  `create_time` int(11) NOT NULL DEFAULT 0 COMMENT '第一次登陆时间',
   UNIQUE KEY uid( `uid` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

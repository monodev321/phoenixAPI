
CREATE TABLE `system_payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `kind` varchar(20) DEFAULT NULL COMMENT '类型：vip/矿机/主播/博主',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1表示关注，0表示取消关注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



CREATE TABLE `payment_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `order_id` varchar(20) DEFAULT NULL COMMENT '类型：vip/矿机/主播/博主',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `kind` varchar(20) DEFAULT NULL COMMENT '类型：vip/矿机/主播/博主',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1表示关注，0表示取消关注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `mine_nodes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `serial` varchar(20) DEFAULT NULL COMMENT '类型：节点编号，年月日+id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `link` varchar(150) NOT NULL COMMENT '代币代号',
  `gross` int(4) NOT NULL DEFAULT '0' COMMENT '总额',
  `share` int(4) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `favorer` int(11) NOT NULL DEFAULT '0' COMMENT '年月2023013',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `start_time` int(11) NOT NULL COMMENT '创建时间',
  `end_time` int(11) NOT NULL COMMENT '创建时间',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `user_nodes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `nid` int(11) NOT NULL  DEFAULT '0' COMMENT '节点id',
  `serial` varchar(20) DEFAULT NULL COMMENT '类型：节点编号，年月日+id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `link` varchar(150) NOT NULL COMMENT '代币代号',
  `share` int(4) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `node_earning` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `nid` int(11) NOT NULL  DEFAULT '0' COMMENT '节点id',
  `serial` varchar(20) DEFAULT NULL COMMENT '类型：节点编号，年月日+id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `share` int(4) NOT NULL DEFAULT '0' COMMENT '支付金额',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `anchor_earning` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `pid` int(11) NOT NULL  DEFAULT '0' COMMENT '主播场次id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `views` int(5) NOT NULL DEFAULT '0' COMMENT '用户观看数量',
  `times` int(5) NOT NULL DEFAULT '0' COMMENT '挖矿时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `anchor_spectator` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `pid` int(11) NOT NULL  DEFAULT '0' COMMENT '主播场次id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `puid` int(11) NOT NULL COMMENT '收款uid',
  `pnickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `pusername` varchar(150) NOT NULL COMMENT '收款用户名',
  `pavatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1表示已经确定，0表示待确定，2表示取消',
  `start_time` int(11) NOT NULL COMMENT '创建时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `blogger_earning` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `vid` int(11) NOT NULL  DEFAULT '0' COMMENT '作品id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `views` int(5) NOT NULL DEFAULT '0' COMMENT '用户观看数量',
  `times` int(5) NOT NULL DEFAULT '0' COMMENT '挖矿时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `video_logs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `vid` int(11) NOT NULL  DEFAULT '0' COMMENT '作品id',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `puid` int(11) NOT NULL COMMENT '收款uid',
  `pnickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `pusername` varchar(150) NOT NULL COMMENT '收款用户名',
  `pavatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1表示已经确定，0表示待确定，2表示取消',
  `start_time` int(11) NOT NULL COMMENT '创建时间',
  `end_time` int(11) NOT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `vip_earning` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `vuid` int(11) NOT NULL  DEFAULT '0' COMMENT '被推荐人uid',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



CREATE TABLE `task_earning` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '关系id',
  `uid` int(11) NOT NULL COMMENT '收款uid',
  `nickname` varchar(150) NOT NULL COMMENT '收款用户名',
  `username` varchar(150) NOT NULL COMMENT '收款用户名',
  `avatar` varchar(150) NOT NULL DEFAULT '' COMMENT '头像',
  `ym` int(11) NOT NULL COMMENT '年月202301',
  `ymd` int(11) NOT NULL COMMENT '年月2023013',
  `level` int(5) NOT NULL COMMENT '分润等级',
  `token_id` int(11) NOT NULL COMMENT '代币id',
  `token_symbol` varchar(150) NOT NULL COMMENT '代币代号',
  `amount` bigint(20) NOT NULL COMMENT '支付金额',
  `views` int(5) NOT NULL DEFAULT '0' COMMENT '用户观看数量',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1表示已经确定，0表示待确定，2表示已经发放,3表示取消',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

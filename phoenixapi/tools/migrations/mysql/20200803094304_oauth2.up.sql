CREATE TABLE IF NOT EXISTS `oauth2_clients`(
   `id` INT(11) UNSIGNED AUTO_INCREMENT COMMENT 'client id',
   `uid` INT(11) NOT NULL COMMENT '用户id',
   `username` VARCHAR(150) NOT NULL COMMENT '用户名，为邮箱',
   `secret` VARCHAR(150) NOT NULL COMMENT 'client密钥',
   `domain` VARCHAR(150) NOT NULL COMMENT '域名',
   `scope` VARCHAR(500) DEFAULT NULL COMMENT '范围',
   `redirect_url` VARCHAR(150) NOT NULL COMMENT '跳转url',
   `private_key`  TEXT NOT NULL COMMENT '私钥，用于jwt签名',
   `publick_key`  TEXT NOT NULL COMMENT '公钥：用于jwt验证',
   `type` VARCHAR(40) COMMENT 'admin表示管理模块,user表示app用户模块,developer开发者模块,dapp用户授权',
   `create_time` int(11) NOT NULL COMMENT '创建时间',
   `update_time` int(11) NOT NULL COMMENT '更新时间',
   PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `oauth2_tokens` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'token di',
  `expired_at` bigint(20) DEFAULT NULL COMMENT 'token过期时间',
  `username` VARCHAR(150) NOT NULL COMMENT '用户名，为邮箱',
  `client_id` INT(11) NOT NULL COMMENT '判断客户端',
  `code` varchar(255) DEFAULT NULL COMMENT 'oauth模式时候的code',
  `access` varchar(2500) DEFAULT NULL COMMENT 'jwt的token',
  `refresh` varchar(255) DEFAULT NULL COMMENT '刷新的token',
  `data`  TEXT NOT NULL COMMENT '序列化的token内容',
  PRIMARY KEY (`id`),
  KEY `idx_code` (`code`) USING BTREE,
  KEY `idx_refresh` (`refresh`) USING BTREE,
  KEY `idx_username` (`username`) USING BTREE,
  KEY `idx_expired_at` (`expired_at`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `oauth2_clients` (`uid`, `username`, `secret`,`scope`, `domain`, `redirect_url`, `private_key`, `publick_key`, `type`, `create_time`, `update_time`)
VALUES
	( 1, 'mybestlove@coinuse.io', '5f486f43b5dc9054fe90034ccc1a5ef9','user', 'http://127.0.0.1:9000', 'http://127.0.0.1:9000', '-----BEGIN rsa private key-----\nMIIEpAIBAAKCAQEAmIOTpXEncFIsfQV//IYI9Jko+/3p7Q0KcTF8+4baDIhmIYP7\nD2BVfwPUxCTPOL/A6BejksOyjtma4lddhgW1Iqt7//lFNoDHVnqCMbQrFKqZKjiC\n3k5oS7mjmux6bfvK47GTiOzFpDoG25JXJg1D2s/Hi9j1cCNWpdF023UM7B5DNGYN\nZfHLKu0jQxwnCycxYh5VLY4RbuoJ9nHI+gnJ/O98/bruQSY1LsE2pXOfQsYT7G8Z\nFBMb9MRc1w5VzqhLw3JdNGzRYo+E8MACWYUt+CG3r7Y8As9LaO8KPMqAnGjyPVyC\nVM0VzluWCD5lt3YiwRniRnEIu50WVkV5juvXiQIDAQABAoIBAQCIjVZGzNdY89ot\nXh1HxeKCzhcp+B/YRNLSolKbChR1IDMBUyAYllI2VQHNVH/M+bMImxGzuFBJIoBQ\no5RzCO0zS4LMnyzfLdJMlYe4ZO4lpNFaQhq6XE15b48SiSTdEJKJ51aHRCd5cRLu\nyUoKIdpdMagiiHDIpHtSIAWW8GhPhy8dH4Tox7eSIdAF8VmKbX9mkCktuwMNurjD\navLY5LFnFN8iDjZQThSgA6wh3FHa0SwL/bg2k+6z3UmKoM0u3u1qI0SnL6MfDNEH\npChwOwdymbRCBJAqO9ohuKAPm0Jwp+RrX2PYxd7mQ8Wdw+H5YanaC24zhFpC2VxJ\n8Kk5GgpZAoGBAMfnMSDuooSNqghW+RVOLI0+VRCD24BqkXL0Tsvdgixi5Hqvw+9B\nIkI3FLFzRktcKXReJVdD1onSkaWGljbTZWM0mo63qjn3E0lXV0i7BuLMr3FF7xEd\n1CkrHyeHr21Gbkz+G0dlgx4Df/q9D2aupWK8ICejr+3BDVnNER5+UCH7AoGBAMNQ\nAUApG7TCD4TRNC6zC3k8XjuJUOgCAjIYqJ6zhy2VaJTNmMsE/L8VsEoDK5wrfu9f\nVjUlnaeknpfVMVBPBI3cDYAaeU3OzNkhBpROiwjN9JaD6pED54aYpp1P1LHanYM6\nrvU3cSoqJaTMQTCx6QB3x/vALLFeI7erjZHc0zlLAoGAdR0aKDwGVHgWZHpnxTae\nXUVmWp2VbF+CxQEiZMdqKwzo5IOy26miI37XjbqGNUJSOw2o/L4Oo4IMuBfoS0h/\n/qXj4rbxcLIyx2X3CfIgb6ERK+CX1cRO9qt/vg39FZ0Mo+i/HI35Sg0s3j4DFto8\npGKox/Ba8oSGiNTeMao40h8CgYBUBsBy/PyJeI+6oVJk4OyHvPwBeyMOnYaaTNeG\nZ1A5wTScCBO7DCs8fjU4skJ5zpCupQJunP9Fl2IPik3L8iLQcSJxwXH8koK4yyeM\neHoCULDwbFJnDxn1YwkTVrv3xEjNo1qP7slFzZoiWymUrIXEiMPFwDi6Gfkd1LV0\nUl+hHwKBgQCflA/MxTcmiHbBSbqZ5g7s+t4YuaYhJx7c4Y3BoqupAzks2t4k8GrQ\nM9yssXtnXVaAtDshpPxCjWsHW4bNr4I+YSG+MyjDAzmCj0+VCZrt6ZxYDJTYwTAu\n3eO+SlWxpOoX8h8uUvJdeCg8CrlegKbxmD2/LkF+/4CEmO6f0LilYg==\n-----END rsa private key-----\n', '-----BEGIN rsa public key-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmIOTpXEncFIsfQV//IYI\n9Jko+/3p7Q0KcTF8+4baDIhmIYP7D2BVfwPUxCTPOL/A6BejksOyjtma4lddhgW1\nIqt7//lFNoDHVnqCMbQrFKqZKjiC3k5oS7mjmux6bfvK47GTiOzFpDoG25JXJg1D\n2s/Hi9j1cCNWpdF023UM7B5DNGYNZfHLKu0jQxwnCycxYh5VLY4RbuoJ9nHI+gnJ\n/O98/bruQSY1LsE2pXOfQsYT7G8ZFBMb9MRc1w5VzqhLw3JdNGzRYo+E8MACWYUt\n+CG3r7Y8As9LaO8KPMqAnGjyPVyCVM0VzluWCD5lt3YiwRniRnEIu50WVkV5juvX\niQIDAQAB\n-----END rsa public key-----\n', 'user', 1596429643, 1596429643);

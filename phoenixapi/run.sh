#!/bin/bash

cd /phoenix 
go mod tidy 
go run main.go -c app-prod.yaml -m prod 
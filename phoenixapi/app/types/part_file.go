package types

const (
	FileMaxPartNum  = 256
	FileMinPartSize = 1 << 20  // 1M
	FileMaxPartSize = 16 << 20 // 16M
)

type PartFile struct {
	Base
	UserID    uint        `json:"user_id"`                                               //user id
	Hash      string      `json:"hash" gorm:"type:char(64)"`                             //md5
	FileName  string      `json:"file_name" gorm:"size:256"`                             //file name
	Uri       string      `json:"uri" gorm:"size:256"`                                   //uri
	UUID      string      `json:"uuid" gorm:"type:char(64);index:part_file_uuid,unique"` //upload uuid, uniqueID
	TotalSize uint        `json:"total_size"`                                            //total size
	PartNum   uint        `json:"part_num"`                                              //part num
	PartSize  uint        `json:"part_size"`                                             //part size
	PartMetas []*PartMeta `json:"part_metas" gorm:"foreignkey:UUID;references:UUID"`
	Status    string      `json:"status" gorm:"type:enum('wait','merge','finish');default 'wait'"` //waiting: 等待分片上传; merge:合并分片; finish: 完成
}

type PartMeta struct {
	UUID   string `json:"-" gorm:"type:char(64); index:part_meta_uuid"` // ref to PartFile.UUID
	Hash   string `json:"hash" gorm:"type:char(64)"`                    // part hash
	Num    uint   `json:"num"`                                          // the number of part
	Size   uint   `json:"size"`                                         // size
	Finish bool   `json:"finish"`                                       // finish upload
}

func (p *PartFile) GetPart(partNum uint) *PartMeta {
	for i, v := range p.PartMetas {
		if v.Num == partNum {
			return p.PartMetas[i]
		}
	}
	return nil
}

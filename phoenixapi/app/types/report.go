package types

type Report struct {
	Base
	FromUser   uint   `json:"from_user,omitempty" gorm:"index:idx_report_from_uid"` //举报人
	ReportUser uint   `json:"report_user" gorm:"index:idx_report_uid"`              //被举报人
	FeedID     uint   `json:"feed_id"`                                              //被举报视频
	Cate       string `json:"cate" gorm:"size:20"`                                  //分类
	Tag        string `json:"tag" gorm:"size:20"`                                   //标签, 子分类
	Images     string `json:"images,omitempty" gorm:"size:500"`                     //上传的图片, 换行分开
	Reason     string `json:"reason,omitempty" gorm:"type:text"`                    //原因

	//侵权投诉信息
	Holder   string `json:"holder,omitempty" gorm:"size:20"`    //权利人
	Subject  string `json:"subject,omitempty" gorm:"size:20"`   //权利主体
	Evidence string `json:"evidence,omitempty" gorm:"size:500"` //证明材料, 换行分开
	Links    string `json:"links,omitempty" gorm:"size:500"`    //原创作品链接, 换行分开
}

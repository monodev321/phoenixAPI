package types

const (
	LiveStatusLiving = "living"
	LiveStatusClosed = "closed"
)

type Live struct {
	Base
	UserID   uint   `json:"user_id"`
	LikeNum  uint   `json:"like_num"`
	Status   string `json:"status" gorm:"type:enum('living','closed');index"`
	Cate     string `json:"cate" gorm:"size:20"`
	Title    string `json:"intro" gorm:"size:100"`
	FeedUrl  string `json:"feed_url" gorm:"size:100"`
	CoverUrl string `json:"cover_url" gorm:"size:100"`
	AdUrl    string `json:"ad_url" gorm:"size:100"`
}

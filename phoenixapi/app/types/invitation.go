package types

type Invitation struct {
	Base
	UserID   uint `json:"user_id" gorm:"index:idx_invite_uid,unique"`
	InviteID uint `json:"invite_id" gorm:"index:idx_invite_uid,unique"`
	Reward   uint `json:"reward"`
}

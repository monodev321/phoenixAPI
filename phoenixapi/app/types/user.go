package types

import "gorm.io/gorm"

type User struct {
	Base
	Username     string `json:"username" gorm:"size:150"`
	Nickname     string `json:"nickname" gorm:"size:150"`
	Phone        string `json:"-" gorm:"type:char(30);index:idx_phone_id,unique"` //手机号,唯一索引
	Email        string `json:"email" gorm:"type:char(30);unique"`                //手机号,唯一索引
	Passwd       string `json:"-" gorm:"size:100"`
	TranPasswd   string `json:"tranPasswd" gorm:"size:100"`
	Avatar       string `json:"avatar" gorm:"size:100"`
	Intro        string `json:"intro" gorm:"size:200"`
	Gender       string `json:"gender" gorm:"type:enum('female','male','none');default 'none'"`
	BirthDay     string `json:"birth_day" gorm:"size:10"`
	Local        string `json:"local" gorm:"size:20"`
	School       string `json:"school" gorm:"size:50"`
	Background   string `json:"background" gorm:"size:100"`
	Referrer     uint   `json:"referrer"`
	ReferrerPath string `json:"referrerPath" gorm:"size:2000"  db:"referrer_path"`
	Code         string `json:"code" gorm:"size:20"`
	Role         int    `gorm:"column:role;type:tinyint;default:0;comment:'角色权限0普通1vip2博主3主播'" db:"role" json:"role"`           //0未审核，1上线，2 禁用
	Status       int    `gorm:"column:status;type:tinyint;default:0;comment:'用户状态0未验证1正常2禁用支付3禁用登录'" db:"status" json:"status"` //0未审核，1上线，2 禁用
	RealName     string `json:"-"`
	IdNumber     string `json:"-"`

	BeLikeNum   uint `json:"be_like_num"`  //被赞次数
	FollowNum   uint `json:"follow_num"`   // 关注人数
	FollowerNum uint `json:"follower_num"` // 粉丝人数

	IdVerify bool `json:"id_verify" gorm:"-"`
}

func (u *User) AfterFind(db *gorm.DB) error {
	if u.RealName != "" && u.IdNumber != "" {
		u.IdVerify = true
	}
	return nil
}

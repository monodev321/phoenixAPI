package types

var (
	MigrateTables = []any{
		&User{}, &SnsRelation{}, &Feed{}, &Comment{}, &Live{},
		&Uninterested{}, &BlockUser{}, &Feedback{}, &Report{},
		&Invitation{},
		&User{}, &SnsRelation{}, &Feed{}, &Comment{}, &Live{}, &Film{}, &TVSeries{},
		&PartFile{}, &PartMeta{},
	}
)

type Base struct {
	ID        uint  `json:"id" gorm:"primarykey"`
	CreatedAt int64 `json:"created_at"`
	UpdatedAt int64 `json:"-"`
	DeletedAt int64 `json:"-" gorm:"index"`
}

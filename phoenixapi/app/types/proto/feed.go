package proto

type CreateFeed struct {
	Visible  uint8  `json:"visible"` //public = 0, friend = 1, self = 2
	Cate     string `json:"cate" binding:"required"`
	Tag      string `json:"tag,omitempty"`
	Location string `json:"location,omitempty"`
	Intro    string `json:"intro,omitempty"`
	FeedUrl  string `json:"feed_url" binding:"required"`
	CoverUrl string `json:"cover_url" binding:"required"`
	BgmUrl   string `json:"bgm_url" binding:"required"`
}

type UninterestedReq struct {
	FeedID uint   `json:"feed_id" binding:"required"`                             //feed_id
	Reason string `json:"reason" binding:"required,oneof=none author music cate"` //one of: 'none','author','music','cate'
}

type FeedListReq struct {
	Paginate
	Visible uint8 `json:"visible"` // public = 0; self = 2;
}

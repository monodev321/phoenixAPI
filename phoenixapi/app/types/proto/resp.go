package proto

type ErrorMsg struct {
	Error string `json:"error"`
}

type SuccessMsg struct {
	Message  string   `json:"message"`
	Data     any      `json:"data"`
	Paginate Paginate `json:"paginate,omitempty"`
}

package proto

import "phoenix/app/types"

type IdCardInfo struct {
	Name     string `json:"name" binding:"required"`      //名字
	IdNumber string `json:"id_number" binding:"required"` //身份证号
}

type CreateLive struct {
	Cate     string `json:"cate" binding:"required"`      //分类
	Title    string `json:"title" binding:"required"`     //标题
	FeedUrl  string `json:"feed_url" binding:"required"`  //视频地址
	CoverUrl string `json:"cover_url" binding:"required"` //封面
	AdUrl    string `json:"ad_url,omitempty"`             //广告
}

type UserLive struct {
	*UserBaseInfo
	*types.Live
}

package proto

import (
	"phoenix/app/types"
	"phoenix/internal/im/model"
	usermodel "phoenix/internal/user/model"
)

type FeedListResp struct {
	Message  string     `json:"message"`
	Data     []UserFeed `json:"data"`
	Paginate Paginate
}

type UserFeed struct {
	*types.Feed
	UserBaseInfo *UserBaseInfo `json:"user_info"`
	Liked        bool          `json:"liked"`  //已经点赞
	Stared       bool          `json:"stared"` //已经收藏
}
type UserFeed1 struct {
	model.Feed
	UserBaseInfo usermodel.UserBaseInfo `json:"user_info"`
	Liked        bool                   `json:"liked"`  //已经点赞
	Stared       bool                   `json:"stared"` //已经收藏
}

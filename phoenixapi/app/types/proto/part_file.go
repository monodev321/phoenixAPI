package proto

type PartFileCreate struct {
	FileName  string `json:"file_name" binding:"required"`  //file name
	Hash      string `json:"hash" binding:"required"`       //md5
	TotalSize uint   `json:"total_size" binding:"required"` //total size
	PartNum   uint   `json:"part_num" binding:"required"`   //part num
	PartSize  uint   `json:"part_size" binding:"required"`  //part size, 1M-10M
}

type PartFileResp struct {
	FileName  string `json:"file_name" binding:"required"`  //file name
	Hash      string `json:"hash" binding:"required"`       //md5
	TotalSize uint   `json:"total_size" binding:"required"` //total size
	PartNum   uint   `json:"part_num" binding:"required"`   //part num
	PartSize  uint   `json:"part_size" binding:"required"`  //part size, 1M-10M
}

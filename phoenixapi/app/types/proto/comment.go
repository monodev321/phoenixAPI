package proto

import "phoenix/app/types"

type CreateComment struct {
	FeedID  uint   `json:"feed_id"`
	FilmID  uint   `json:"film_id"`
	ReplyID uint   `json:"reply_id,omitempty"`
	Content string `json:"content" binding:"required"`
}

type UserComment struct {
	*UserBaseInfo
	*types.Comment
	Replies []*UserComment `json:"replies,omitempty"`
}

package proto

const (
	defaultPageSize = 20
)

type AnyID64 struct {
	ID   int64 `json:"id" binding:"required"`
	Undo bool  `json:"undo,omitempty"`
}
type AnyID struct {
	ID   uint `json:"id" binding:"required"`
	Undo bool `json:"undo,omitempty"`
}

type AnyVal struct {
	ID  uint   `json:"id" binding:"required"`
	Val string `json:"val,omitempty"`
}

type Paginate struct {
	ID      uint `json:"id" binding:"required"` //ID
	PageNum int  `json:"page_num"`              //页数
	Limit   int  `json:"limit"`                 //每页条数
}

func (p *Paginate) Fixed() {
	if p.PageNum <= 0 {
		p.PageNum = 1
	}
	if p.Limit <= 0 || p.Limit >= 20 {
		p.Limit = defaultPageSize
	}
}

func (p *Paginate) Offset() int {
	return (p.PageNum - 1) * p.Limit
}

package proto

import (
	"phoenix/app/types"
	"phoenix/internal/im/model"
	usermodel "phoenix/internal/user/model"
)

// SnsSetting 设置好友权限等操作,可设置的值有:friend, close_friend, is_top, see_ta, see_me
type SnsSetting struct {
	ID   uint            `json:"id" binding:"required"` //用户ID
	Data map[string]bool `json:"data"`                  //map[string]bool
}

type SnsUser2 struct {
	Relation *model.Friend `json:"relation"`
	User     *UserBaseInfo `json:"user"`
}

type SnsUser struct {
	Relation *types.SnsRelation `json:"relation"`
	User     *UserBaseInfo      `json:"user"`
}
type SnsUser1 struct {
	Relation model.SnsRelation      `json:"relation"`
	User     usermodel.UserBaseInfo `json:"user"`
}

type SnsUserListResp struct {
	Message string    `json:"message"`
	Data    []SnsUser `json:"data"`
}

package proto

type SearchReq struct {
	Keyword  string `json:"keyword" binding:"required"`
	Paginate `json:"paginate,omitempty"`
}

type UserSearchResp struct {
	Keyword  string     `json:"keyword,omitempty"`
	Data     []*SnsUser `json:"data,omitempty"`
	Paginate `json:"paginate"`
}

type FeedSearchResp struct {
	Keyword  string      `json:"keyword,omitempty"`
	Data     []*UserFeed `json:"data,omitempty"`
	Paginate `json:"paginate"`
}

type QuickFilterReq struct {
	KindId     int    `json:"kind_id,omitempty"`
	PlotId     string `json:"plot_id,omitempty"`
	RegionId   int    `json:"region_id,omitempty"`
	LanguageId int    `json:"language_id,omitempty"`
	TimeId     int    `json:"time_id,omitempty"`
	Name       string `json:"name,omitempty"`
	Paginate
}

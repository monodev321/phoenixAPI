package proto

import (
	"phoenix/app/types"
)

type LoginReq struct {
	PhoneID    string `json:"phone_id" binding:"required"` //手机号, 如: +8613567891000
	VerifyCode string `json:"verify_code"`                 //'/user/verifyCode' 获取到的验证码
	Passwd     string `json:"passwd"`                      //用户设置的密码
}

type VerifyReq struct {
	PhoneID string `json:"phone_id" binding:"required"` //手机号, 如: +8613567891000
	Cate    uint   `json:"cate" binding:"required"`     //验证码类型: 1: register; 2: login; 3: retrieve password
}

type UpdateProfile struct {
	Keys   []string `json:"keys" binding:"required"`   //需要更新的字段名, (Keys,Value)数量必须一致
	Values []string `json:"values" binding:"required"` //需要更新的字段值
}

type UserBaseInfo struct {
	UserID   uint   `json:"user_id"`
	UserName string `json:"user_name"`
	Avatar   string `json:"avatar"`
	Gender   string `json:"gender"`   //性别 female, male, none
	Relation uint8  `json:"relation"` //关系 0:None, 1:关注, 2: 粉丝, 3:相互关注
}

type UserProfile struct {
	User       *types.User        `json:"user"`                  //基础信息
	Relation   *types.SnsRelation `json:"relation"`              //关系
	SelfFeed   []*types.Feed      `json:"self_feed"`             //自己的作品
	LikeFeed   []*UserFeed        `json:"like_feed"`             //喜欢的作品
	SecretFeed []*types.Feed      `json:"secret_feed,omitempty"` //秘密作品,只有查看自己的Profile时, 才会返回
}

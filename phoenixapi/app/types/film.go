package types

type Film struct {
	Id         int     `gorm:"column:id" db:"id" json:"id"`                                                                 //订单id
	Title      string  `gorm:"column:title;default:'';size:280;comment:'视频标题'" db:"title" json:"title"`                     //视频标题
	Filename   string  `gorm:"column:filename;default:'';size:280;comment:'文件名称'" db:"filename" json:"filename"`            //原来视频名
	Md5        string  `gorm:"column:md5;default:'';size:64;comment:'md5'" db:"md5" json:"md5"`                             //文件md5
	Url        string  `gorm:"column:url;default:'';size:1280;comment:'图片'" db:"url" json:"url"`                            //视频链接
	Cover      string  `gorm:"column:cover;default:'';size:280;comment:'封面'" db:"cover" json:"cover"`                       //用户头像
	Cover2     string  `gorm:"column:cover2;default:'';size:280;comment:'封面2'" db:"cover2" json:"cover2"`                   //用户头像
	Director   string  `gorm:"column:director;default:'';size:280;comment:'导演'" db:"director" json:"director"`              //导演
	Actor      string  `gorm:"column:actor;default:'';size:1000;comment:'演员'" db:"actor" json:"actor"`                      //演员
	Desc       string  `gorm:"column:desc;default:'';size:2000;comment:'描述'" db:"desc" json:"desc"`                         //描述
	Age        int     `gorm:"column:age;type:tinyint;default:0;comment:'年龄限制'" db:"age" json:"age"`                        //年龄限制
	Duration   int     `gorm:"column:duration;default:0;comment:'时长'" db:"duration" json:"duration"`                        //时长
	Grade      float32 `gorm:"column:grade;type:float;default:0;comment:'评分'" db:"grade" json:"grade"`                      //时长
	KindId     int     `gorm:"column:kind_id;type:tinyint;default:0;comment:'类别id'" db:"kind_id" json:"kindId"`             //类别id
	Kind       string  `gorm:"column:kind;default:'';size:100;comment:'类别'" db:"kind" json:"kind"`                          //类别
	PlotId     string  `gorm:"column:plot_id;size:150;default:0;comment:'剧情id'" db:"plot_id" json:"plotId"`                 //剧情id
	Plot       string  `gorm:"column:plot;default:'';size:100;comment:'剧情'" db:"plot" json:"plot"`                          //剧情
	RegionId   int     `gorm:"column:region_id;type:tinyint;default:0;comment:'区域id'" db:"region_id" json:"regionId"`       //地区id
	Region     string  `gorm:"column:region;default:'';size:100;comment:'区域'" db:"region" json:"region"`                    //地区
	LanguageId int     `gorm:"column:language_id;type:tinyint;default:0;comment:'语言id'" db:"language_id" json:"languageId"` //语言id
	Language   string  `gorm:"column:language;default:'';size:100;comment:'语言'" db:"language" json:"language"`              //语言
	TimeId     int     `gorm:"column:time_id;type:tinyint;default:0;comment:'年代id'" db:"time_id" json:"timeId"`             //年代id
	Times      string  `gorm:"column:times;default:'';size:100;comment:'年代'" db:"times" json:"times"`                       //年代
	Sort       int     `gorm:"column:sort;type:tinyint;default:0;comment:'排序'" db:"sort" json:"sort"`                       //排序等级
	IsTop      int     `gorm:"column:is_top;type:tinyint;default:0;comment:'是否置顶'" db:"is_top" json:"isTop"`                //0未置顶1置顶
	IsHot      int     `gorm:"column:is_hot;type:tinyint;default:0;comment:'是否推荐'" db:"is_hot" json:"isHot"`                //0未置顶1置顶
	Status     int     `gorm:"column:status;type:tinyint;default:0;comment:'审核状态'" db:"status" json:"status"`               //0未审核，1上线，2 禁用
	IsIpfs     int     `gorm:"column:is_ipfs;type:tinyint;default:0;comment:'是否上传到ipfs'" db:"is_ipfs" json:"isIpfs"`        //0未转化，1已转化
	Views      int     `gorm:"column:views;default:0;comment:'浏览量'" db:"views" json:"views"`                                //浏览量
	Comments   int     `gorm:"column:comments;default:0;comment:'评论数'" db:"comments" json:"comments"`                       //评论数
	Likes      int     `gorm:"column:likes;default:0;comment:'点赞数'" db:"likes" json:"likes"`                                //点赞数
	CreateTime int64   `gorm:"column:create_time;default:0;comment:'创建时间'" db:"create_time" json:"createTime"`              //创建时间
	UpdateTime int64   `gorm:"column:update_time;default:0;comment:'最后更新时间'" db:"update_time" json:"updateTime"`            //最后更新时间
}

type TVSeries struct {
	Id         int    `gorm:"column:id" db:"id" json:"id"`                                                          //订单id
	Fid        int    `gorm:"column:fid" db:"fid" json:"fid"`                                                       //订单id
	Title      string `gorm:"column:title;default:'';size:280;comment:'视频标题'" db:"title" json:"title"`              //视频标题
	Filename   string `gorm:"column:filename;default:'';size:280;comment:'文件名称'" db:"filename" json:"filename"`     //原来视频名
	Md5        string `gorm:"column:md5;default:'';size:64;comment:'md5'" db:"md5" json:"md5"`                      //文件md5
	Url        string `gorm:"column:url;default:'';size:280;comment:'图片'" db:"url" json:"url"`                      //视频链接
	Cover      string `gorm:"column:cover;default:'';size:280;comment:'封面'" db:"cover" json:"cover"`                //用户头像
	Cover2     string `gorm:"column:cover2;default:'';size:280;comment:'封面2'" db:"cover2" json:"cover2"`            //用户头像
	Desc       string `gorm:"column:desc;default:'';size:1000;comment:'描述'" db:"desc" json:"desc"`                  //描述
	Duration   int    `gorm:"column:duration;type:int;default:0;comment:'时长'" db:"duration" json:"duration"`        //时长
	Num        int    `gorm:"column:num;type:tinyint;default:1;comment:'集数'" db:"num" json:"num"`                   //集数
	Status     int    `gorm:"column:status;type:tinyint;default:0;comment:'审核状态'" db:"status" json:"status"`        //0未审核，1上线，2 禁用
	IsIpfs     int    `gorm:"column:is_ipfs;type:tinyint;default:0;comment:'是否上传到ipfs'" db:"is_ipfs" json:"isIpfs"` //0未转化，1已转化
	Views      int    `gorm:"column:views;default:0;comment:'浏览量'" db:"views" json:"views"`                         //浏览量
	Comments   int    `gorm:"column:comments;default:0;comment:'评论数'" db:"comments" json:"comments"`                //评论数
	Likes      int    `gorm:"column:likes;default:0;comment:'点赞数'" db:"likes" json:"likes"`                         //点赞数
	CreateTime int64  `gorm:"column:create_time;default:0;comment:'创建时间'" db:"create_time" json:"createTime"`       //创建时间
	UpdateTime int64  `gorm:"column:update_time;default:0;comment:'最后更新时间'" db:"update_time" json:"updateTime"`     //最后更新时间
}

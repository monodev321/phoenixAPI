package types

type Uninterested struct {
	Base
	UserID uint   `json:"user_id" gorm:"index:idx_uninterested_uid"`                              // user_id
	Reason string `json:"reason" gorm:"type:enum('none','author','music','cate');default 'none'"` // uninterested reason: one of: 'none','author','music','cate'
	FeedID uint   `json:"feed_id"`                                                                // feed_id
}

package types

type Comment struct {
	Base
	UserID  uint   `json:"user_id"`
	FeedID  uint   `json:"feed_id" gorm:"index:idx_comment_feed_id"`
	FilmID  uint   `json:"film_id" gorm:"index:idx_comment_film_id"`
	ReplyID uint   `json:"reply_id" gorm:"index:idx_comment_reply_id"` //回复的ID, 如果不是回复,则为0;回复的哪一条消息
	LikeNum uint   `json:"like_num"`                                   //赞成的次数
	Content string `json:"content" gorm:"type:text"`
	Liked   bool   `json:"liked" gorm:"-"` //是否已经点赞过,这个需要从缓存中查询
}

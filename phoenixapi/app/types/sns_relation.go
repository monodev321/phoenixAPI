package types

import "time"

type RelationType uint8

const (
	RelationTypeNone     RelationType = 0b00
	RelationTypeFollow   RelationType = 0b01
	RelationTypeFollower RelationType = 0b10
	RelationTypeFriend   RelationType = 0b11
)

type SnsRelation struct {
	ID         uint      `json:"id" gorm:"primarykey"`
	CreatedAt  time.Time `json:"created_at"`
	UserID     uint      `json:"user_id" gorm:"index:idx_rel_user_follow,unique"`   //用户ID
	FollowID   uint      `json:"follow_id" gorm:"index:idx_rel_user_follow,unique"` //关注的人
	Group      string    `json:"group" gorm:"size:20"`                              //分组
	Remark     string    `json:"remark" gorm:"size:20"`                             //备注名
	FriendFlag uint8     `json:"friend_flag" gorm:"type:tinyint;default:0"`         //是否是朋友: 0:不是朋友, 1:是朋友, 2:密友
	IsTop      bool      `json:"is_top" gorm:"type:tinyint;default:0"`              //置顶
	SeeTa      bool      `json:"see_ta" gorm:"type:tinyint;default:1"`              //看他
	SeeMe      bool      `json:"see_me" gorm:"type:tinyint;default:1"`              //让他看
}

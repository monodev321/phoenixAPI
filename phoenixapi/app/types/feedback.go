package types

type Feedback struct {
	Base
	UserID      uint   `json:"user_id,omitempty" gorm:"index:idx_feedback_uid"`
	Cate        string `json:"cate" gorm:"size:20"`
	Content     string `json:"content,omitempty" gorm:"type:text"`
	PhoneNumber string `json:"phone_number,omitempty" gorm:"size:20"`
	Picture     string `json:"picture,omitempty" gorm:"size:200"`
}

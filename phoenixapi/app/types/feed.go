package types

type VisibleEnum uint8

const (
	VisibleEnumPublic VisibleEnum = 0b0000
	VisibleEnumFriend VisibleEnum = 0b0001
	VisibleEnumSelf   VisibleEnum = 0b0010
)

type Feed struct {
	Base
	UserID     uint   `json:"user_id"`
	LikeNum    uint   `json:"like_num"`
	StarNum    uint   `json:"star_num"`
	CommentNum uint   `json:"comment_num"`
	ShareNum   uint   `json:"share_num"`
	WatchNum   uint   `json:"watch_num"`
	Visible    uint8  `json:"visible"` //public = 0, friend = 1, self = 2
	Cate       string `json:"cate" gorm:"size:20"`
	Tag        string `json:"tag" gorm:"size:20"`
	Location   string `json:"location" gorm:"size:20"`
	Intro      string `json:"intro" gorm:"size:100"`
	FeedUrl    string `json:"feed_url" gorm:"size:100"`
	CoverUrl   string `json:"cover_url" gorm:"size:100"`
	BgmUrl     string `json:"bgm_url" gorm:"size:100"`
}

func ListUserIds(list []*Feed) []uint {
	if len(list) == 0 {
		return nil
	}
	idMap := make(map[uint]struct{})
	for _, v := range list {
		idMap[v.UserID] = struct{}{}
	}
	ids := make([]uint, 0, len(idMap))
	for k := range idMap {
		ids = append(ids, k)
	}
	return ids
}

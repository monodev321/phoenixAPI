package app

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"phoenix/pkg/config"
	"phoenix/pkg/db"
	"phoenix/pkg/util"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/urfave/cli/v2"
)

const (
	version = "v0.1"
	gitHash = "-"
)

func Run() {
	app := cli.NewApp()
	app.Name = "phoenixServer"
	app.Version = fmt.Sprintf("version: %s-%s", version, gitHash)
	app.Description = "phoenix community api server"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:     "config",
			Aliases:  []string{"c"},
			Usage:    "the config file of app",
			Required: true,
			Value:    "app.yaml",
		},
		&cli.StringFlag{
			Name:        "mode",
			Aliases:     []string{"m"},
			Usage:       "the mode of app running, one of debug,dev,prod",
			Value:       "dev",
			Destination: &config.AppMode,
		},
	}
	app.Action = runAction

	if err := app.Run(os.Args); err != nil {
		log.Println(err)
	}
}

func runAction(c *cli.Context) error {
	cfgFile := c.String("config")
	err := config.Init(cfgFile)
	if err != nil {
		return err
	}
	db.Init()
	db.ApiLevelDB.Init2()
	defer db.LevelDBApi.Close()
	e := gin.Default()
	e.MaxMultipartMemory = 200 << 20
	e.Use(CorsMiddleware())
	//e.MaxMultipartMemory = 64 << 20

	e = register(e)
	if config.AppMode == config.AppModeProd {
		gin.SetMode(gin.ReleaseMode)
	}
	srv := &http.Server{
		Addr:    config.AppCfg.App.Listen,
		Handler: e,
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal("listen: ", err)
		}
	}()
	// 等待中断信号以优雅地关闭服务器（设置 5 秒的超时时间）
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	fmt.Printf("%s Shutdown Server ... \r\n", time.Now().UTC().String())

	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
	return nil
}

// func setupRouter(e *gin.Engine) {
// 	panic("unimplemented")
// }

func register(r *gin.Engine) *gin.Engine {

	setupRouter(r)
	BindValidator()
	r.NoRoute(func(c *gin.Context) {
		if c.Request.Method != "OPTIONS" {
			util.FailWithMsg(c, "please check request url !")
		}
	})
	return r
}

// 浏览器跨域请求
func CorsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		var openCorsFlag = true
		if openCorsFlag {
			c.Header("Access-Control-Allow-Origin", "*")
			c.Header("Access-Control-Allow-Headers", "Origin, X-Requested-With,X-CSRF-Token, Content-Type, Accept,Authorization,Referer,User-Agent")
			c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
			c.Header("Access-Control-Allow-Credentials", "true")
			c.Header("Access-Control-Allow-Methods", "GET, OPTIONS, POST, PUT, DELETE")
			c.Set("content-type", "application/json")
		}
		if method == "OPTIONS" {
			c.JSON(http.StatusOK, nil)
			return
		}
		c.Next()
	}
}

func BindValidator() {
	// 绑定验证器
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("passwordValid", util.Password)
	}
}

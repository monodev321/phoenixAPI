package service

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"phoenix/app/model"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/pkg/config"
	"strconv"
	"strings"

	"github.com/google/uuid"
)

type PartFileService struct {
	part model.PartFileModel
}

func (s PartFileService) FindUpload(uuid string) (f *types.PartFile, err error) {
	return s.part.MustByUUID(uuid)
}

func (s PartFileService) MetaUpdate(f *types.PartFile, meta *types.PartMeta) error {
	if meta.Finish == true {
		return nil
	}

	name := fmt.Sprintf("partFile/%d/%s.part_%03d", f.UserID, f.Hash, meta.Num)
	fn, _ := filepath.Abs(filepath.Join(config.AppCfg.App.ResourceDir, name))
	file, e := os.OpenFile(fn, os.O_RDONLY, os.ModePerm)
	if e != nil {
		return e
	}
	hash := md5.New()
	n, _ := io.Copy(hash, file)
	if n != int64(meta.Size) {
		return fmt.Errorf("size error %d / %d", hash.Size(), meta.Size)
	}
	h := fmt.Sprintf("%x", hash.Sum(nil))
	if h != strings.ToLower(meta.Hash) {
		return fmt.Errorf("error hash verify %s ?=? %s", h, meta.Hash)
	}
	meta.Finish = true
	_, err := s.part.UpdateMeta(f.UUID, meta.Num, map[string]any{
		"hash":   meta.Hash,
		"finish": meta.Finish,
	})
	return err
}

func (s PartFileService) MergeCheck(f *types.PartFile) (err error) {
	if f.Status == "finish" {
		return
	}
	finishNum := 0
	for _, v := range f.PartMetas {
		if v.Finish {
			finishNum++
		}
	}
	if finishNum == int(f.PartNum) {
		err = s.doMergeFile(f)
	}
	return
}

func (s PartFileService) doMergeFile(f *types.PartFile) error {
	if f.Status != "wait" {
		return errors.New("status not allow merge")
	}
	canMerge := s.part.UpdateStatus(f.UUID, "merge")
	if !canMerge {
		return errors.New("status not allow merge")
	}
	ext := filepath.Ext(f.FileName)
	filePath := filepath.Join(config.AppCfg.App.ResourceDir, "feeds", strconv.Itoa(int(f.UserID)), f.Hash+ext)
	os.MkdirAll(filepath.Dir(filePath), 0750)
	mergeFile, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY, 0755)
	if err != nil {
		return err
	}
	hash := md5.New()
	prefix := filepath.Join(config.AppCfg.App.ResourceDir, "partFile", strconv.Itoa(int(f.UserID)))
	for i := uint(1); i <= f.PartNum; i++ {
		name := fmt.Sprintf("%s.part_%03d", f.Hash, i)
		fn, _ := filepath.Abs(filepath.Join(prefix, name))
		file, e := os.OpenFile(fn, os.O_RDONLY, os.ModePerm)
		if e != nil {
			return e
		}
		_, e = mergeFile.ReadFrom(file)
		if err != nil {
			return e
		}
		file.Seek(0, 0)
		io.Copy(hash, file)
	}
	h := fmt.Sprintf("%x", hash.Sum(nil))
	if h != strings.ToLower(f.Hash) {
		return errors.New(fmt.Sprintf("error hash verify %s = %s", h, f.Hash))
	}
	f.Status = "finish"
	finish := s.part.UpdateStatus(f.UUID, f.Status)
	if !finish {
		return errors.New("error update status: finish")
	}

	f.Uri = filepath.Join("/uploads/feeds", strconv.Itoa(int(f.UserID)), f.Hash+ext)
	s.part.UpdateUri(f.UUID, f.Uri)
	return nil
}

func (s PartFileService) CreateUpload(userId uint, req *proto.PartFileCreate) (f *types.PartFile, err error) {
	if req.PartNum <= 0 || req.PartNum > types.FileMaxPartNum {
		return nil, errors.New(fmt.Sprintf("PartNum out of range: %d(should be 1 ~ %d)",
			req.PartNum, types.FileMaxPartNum))
	}
	if req.PartSize < types.FileMinPartSize || req.PartSize > types.FileMaxPartSize {
		return nil, errors.New(fmt.Sprintf("PartSize out of range: %d(should be %d ~ %d)",
			req.PartSize, types.FileMinPartSize, types.FileMaxPartSize))
	}
	uuid := getUUID()
	metas := make([]*types.PartMeta, 0, req.PartNum)
	for i := uint(1); i <= req.PartNum; i++ {
		size := req.PartSize
		if req.TotalSize < req.PartSize*i {
			size = req.TotalSize - (req.PartSize * (i - 1))
		}
		meta := &types.PartMeta{
			Num:  i,
			Size: size,
		}
		metas = append(metas, meta)
	}
	f = &types.PartFile{
		UserID:    userId,
		FileName:  req.FileName,
		Hash:      req.Hash,
		UUID:      uuid,
		TotalSize: req.TotalSize,
		PartNum:   req.PartNum,
		PartSize:  req.PartSize,
		Status:    "wait",
		PartMetas: metas,
	}
	err = s.part.Create(f)
	return
}

//getUUID 同一文件不能重复上传
//func getUUID(userId uint, hash string) string {
//	h := md5.New()
//	h.Write([]byte(fmt.Sprintf("%d-%s", userId, hash)))
//	if true {
//		h.Write([]byte(time.Now().String()))
//	}
//	id := fmt.Sprintf("%x", h.Sum(nil))
//	return id
//}

// getUUID 同一文件可以多次上传, 每次都重新上传
func getUUID() string {
	return uuid.New().String()
}

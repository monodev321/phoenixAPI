package service

import (
	"log"
	"phoenix/app/model"
	"phoenix/app/types"
	"phoenix/app/types/proto"
)

type SnsService struct {
	user model.UserModel
	model.SnsModel
}

//FollowUser 关注用户
//UnfollowUser 取消关注
func (s SnsService) FollowUser(uid, fid uint, undo bool) error {
	//关注数据, 修改数据库关注总数,修改对方粉丝数
	var err error
	err = s.Follow(uid, fid, undo)
	if err != nil {
		return err
	}
	var change int = 1
	if undo {
		change = -1
	}
	s.user.UpdateColumn(uid, "follow_num", change)
	s.user.UpdateColumn(fid, "follower_num", change)

	return nil
}

func (s SnsService) UserRelation(uid, fid uint) *types.SnsRelation {
	if uid == fid || uid == 0 {
		return nil
	}
	r, err := s.Relation(uid, fid)
	if err != nil {
		log.Println(err)
	}
	return r
}

func (s SnsService) GetFollowList(uid uint, offset, limit int) (data []*proto.SnsUser, err error) {
	list, err := s.GetUserFollow(uid, offset, limit)
	if err != nil {
		return nil, err
	}
	if len(list) == 0 {
		return
	}
	uids := make([]uint, 0, len(list))
	for _, v := range list {
		uids = append(uids, v.FollowID)
	}
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	for i, v := range list {
		data = append(data, &proto.SnsUser{
			Relation: list[i],
			User:     userMap[v.FollowID],
		})
	}
	return
}

func (s SnsService) GetFollowerList(uid uint, offset, limit int) (data []*proto.SnsUser, err error) {
	list, err := s.GetUserFollower(uid, offset, limit)
	if err != nil {
		return nil, err
	}
	if len(list) == 0 {
		return
	}
	uids := make([]uint, 0, len(list))
	for _, v := range list {
		uids = append(uids, v.FollowID)
	}
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	for i, v := range list {
		data = append(data, &proto.SnsUser{
			Relation: list[i],
			User:     userMap[v.FollowID],
		})
	}
	return
}

func (s SnsService) GetFriendList(uid uint, offset, limit int) (data []*proto.SnsUser, err error) {
	list, err := s.GetUserFriend(uid, offset, limit)
	if err != nil {
		return nil, err
	}
	if len(list) == 0 {
		return
	}
	uids := make([]uint, 0, len(list))
	for _, v := range list {
		uids = append(uids, v.FollowID)
	}
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	for i, v := range list {
		data = append(data, &proto.SnsUser{
			Relation: list[i],
			User:     userMap[v.FollowID],
		})
	}
	return
}

func (s SnsService) SearchUserFollow(uid uint, q string, offset, limit int) ([]*proto.SnsUser, error) {
	q = "%" + q + "%"
	list, err := s.SnsModel.SearchUserFollow(uid, q, offset, limit)
	if err != nil {
		return nil, err
	}
	var resp []*proto.SnsUser
	if len(list) > 0 {
		resp = make([]*proto.SnsUser, 0, len(list))
		fids := make([]uint, 0, len(list))
		for k := range list {
			fids = append(fids, list[k].FollowID)
		}
		log.Printf("%#v", list[0])
		userMap, err := s.user.GetUsersBaseInfo(fids)
		if err != nil {
			return nil, err
		}
		for k, v := range list {
			su := &proto.SnsUser{
				Relation: list[k],
				User:     userMap[v.FollowID],
			}
			resp = append(resp, su)
		}
	}
	return resp, nil
}

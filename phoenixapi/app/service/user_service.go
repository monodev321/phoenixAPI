package service

import (
	"crypto/md5"
	"fmt"
	"log"
	"phoenix/app/model"
	"phoenix/app/types"
	"phoenix/pkg/config"
	"strconv"
)

type UserService struct {
	model.UserModel
	block  model.BlockUserModel
	invite model.InvitationModel
}

func (s UserService) GenPasswdHash(phoneId, passwd string) string {
	println(config.AppCfg.Salt)
	hash := md5.New()
	hash.Write([]byte(phoneId))
	hash.Write([]byte(config.AppCfg.Salt))
	hash.Write([]byte(passwd))
	p := fmt.Sprintf("%x", hash.Sum(nil))
	return p
}

func (s UserService) VerifyPasswd(u *types.User, passwd string) error {
	hash := s.GenPasswdHash(u.Phone, passwd)
	if u.Passwd != hash {
		return fmt.Errorf("passwd[%s] hash %s != %s", passwd, u.Passwd, hash)
	}
	return nil
}

func (s UserService) UpdateLikeNum(userId uint, undo bool) error {
	var change = 1
	if undo {
		change = -1
	}
	return s.UpdateColumn(userId, "be_like_num", change)
}

func (s UserService) CreateUser(phoneId string, passwd string, name string, inviteId string) (user *types.User, err error) {
	if name == "" {
		name = "user-" + phoneId[len(phoneId)-4:]
	}
	passHash := ""
	if passwd != "" {
		passHash = s.GenPasswdHash(phoneId, passwd)
	}
	user = &types.User{
		Nickname: name,
		Phone:    phoneId,
		Passwd:   passHash,
		Gender:   "none",
	}
	if err = s.Save(user); err != nil {
		return nil, err
	}
	iid, _ := strconv.Atoi(inviteId)
	if iid > 0 {
		err = s.invite.Save(&types.Invitation{
			UserID:   uint(iid),
			InviteID: user.ID,
		})
		if err != nil {
			log.Println("error inviteId ", inviteId, user)
		}
	}
	return
}

func (s UserService) SearchUser(q string, offset, limit int) (list []*types.User, err error) {
	q = "%" + q + "%"
	list, err = s.UserModel.SearchName(q, offset, limit)
	return
}

func (s UserService) Block(uId, fId uint) error {
	b := types.BlockUser{UserID: uId, BlockID: fId}
	err := s.block.Create(&b)
	return err
}

func (s UserService) UnBlock(uId, fId uint) error {
	err := s.block.Delete(uId, fId)
	return err
}

func (s UserService) GetBlockIds(uId uint) []uint {
	data, err := s.block.GetBlockIds(uId)
	if err != nil {
		log.Println(err)
	}
	return data
}

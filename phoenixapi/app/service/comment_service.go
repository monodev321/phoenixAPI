package service

import (
	"phoenix/app/model"
	"phoenix/app/types/proto"
)

type CommentService struct {
	model.CommentModel
	user model.UserModel
}

func (s CommentService) GetList(vid, uid uint, offset, limit int) (list []*proto.UserComment, err error) {
	comments, err := s.GetListPage(uid, vid, offset, limit)
	if err != nil {
		return nil, err
	}
	uids := make([]uint, 0, len(comments))
	for _, v := range comments {
		uids = append(uids, v.UserID)
	}
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserComment, 0, len(comments))
	for k, v := range comments {
		list = append(list, &proto.UserComment{
			UserBaseInfo: userMap[v.UserID],
			Comment:      comments[k],
		})
	}
	err = s.SetReplies(uid, list)
	if err != nil {
		return nil, err
	}
	return
}

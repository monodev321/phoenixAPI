package service

import (
	"phoenix/app/model"
	"phoenix/app/types"
	"phoenix/app/types/proto"
)

type FeedService struct {
	model.FeedModel
	user model.UserModel
}

//GetRecommend 所有关注的人最新发表的
func (s FeedService) GetRecommend(uid uint, offset, limit int) (list []*proto.UserFeed, err error) {
	if uid == 0 {
		//用户没有登录, 找几个最新发布的
		list, err = s.GetLatest(offset, limit)

	} else {
		list, err = s.GetFollowNewest(uid, offset, limit)
	}
	return
}

func (s FeedService) GetUserAll(uid uint, offset, limit int) (list []*proto.UserFeed, err error) {
	data, err := s.GetAll(uid, offset, limit)
	if err != nil {
		return
	}
	info, err := s.user.GetBaseInfo(uid)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserFeed, 0, len(list))
	for k := range data {
		uf := &proto.UserFeed{
			Feed:         data[k],
			UserBaseInfo: info,
		}
		list = append(list, uf)
	}
	return list, err
}

func (s FeedService) GetUserLike(uid uint, offset, limit int) (list []*proto.UserFeed, err error) {
	start := int64(offset)
	stop := int64(offset + limit)
	data, err := s.FeedModel.GetUserLike(uid, start, stop)
	if err != nil {
		return nil, err
	}
	uids := types.ListUserIds(data)
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserFeed, 0, len(data))
	for k := range data {
		uf := &proto.UserFeed{
			Feed:         data[k],
			UserBaseInfo: userMap[data[k].UserID],
			Liked:        true,
		}
		list = append(list, uf)
	}
	return
}

func (s FeedService) SearchFeed(q string, offset, limit int) (list []*proto.UserFeed, err error) {
	q = "%" + q + "%"
	data, err := s.FeedModel.SearchIntro(q, offset, limit)
	if err != nil {
		return nil, err
	}
	uids := types.ListUserIds(data)
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserFeed, 0, len(data))
	for k := range data {
		uf := &proto.UserFeed{
			Feed:         data[k],
			UserBaseInfo: userMap[data[k].UserID],
		}
		list = append(list, uf)
	}
	return
}

func (s FeedService) UninterestedFeed(uid uint, feedId uint, reason string) error {
	data := &types.Uninterested{
		UserID: uid,
		Reason: reason,
		FeedID: feedId,
	}
	m := model.UninterestedModel{}
	err := m.Save(data)
	return err
}

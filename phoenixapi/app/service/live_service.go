package service

import (
	"phoenix/app/model"
	"phoenix/app/types"
	"phoenix/app/types/proto"
)

type LiveService struct {
	model.LiveModel
	user model.UserModel
}

func (s LiveService) GetUser(uid uint) (*types.User, error) {
	return s.user.Get(uid)
}

func (s LiveService) SaveIdNumber(uid uint, name, idNumber string) error {
	err := s.user.Update(uid, &types.User{RealName: name, IdNumber: idNumber})
	return err
}

func (s LiveService) GetLivingList(offset, limit int) (list []*proto.UserLive, err error) {
	livings, err := s.LivingList(offset, limit)
	if err != nil {
		return nil, err
	}

	uids := make([]uint, 0, len(livings))
	for _, v := range livings {
		uids = append(uids, v.UserID)
	}
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserLive, 0, len(livings))
	for i, v := range livings {
		ul := &proto.UserLive{
			UserBaseInfo: userMap[v.UserID],
			Live:         livings[i],
		}
		list = append(list, ul)
	}
	return
}

func (s LiveService) LivingUserList(offset, limit int) (list []*proto.UserLive, err error) {
	livings, err := s.LivingList(offset, limit)
	if err != nil {
		return nil, err
	}

	uids := make([]uint, 0, len(livings))
	for _, v := range livings {
		uids = append(uids, v.UserID)
	}
	userMap, err := s.user.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserLive, 0, len(livings))
	for i, v := range livings {
		ul := &proto.UserLive{
			UserBaseInfo: userMap[v.UserID],
			Live:         livings[i],
		}
		list = append(list, ul)
	}
	return
}

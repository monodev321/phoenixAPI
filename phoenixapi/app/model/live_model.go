package model

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"phoenix/app/types"
	"phoenix/pkg/db"
	"strconv"
	"time"
)

type LiveModel struct {
	table types.Live
}

func (m LiveModel) Get(lid uint) (r *types.Live, err error) {
	r = &types.Live{}
	err = db.Orm.Model(&m.table).Where("id = ?", lid).Take(r).Error
	if err != nil {
		return nil, err
	}
	return
}

func (m LiveModel) Save(r *types.Live) error {
	return db.Orm.Model(&m.table).Save(r).Error
}

func (m LiveModel) CloseLiving(lid uint) error {
	return m.update(lid, "status", types.LiveStatusClosed)
}

func (m LiveModel) Like(uid, lid uint, undo bool) error {
	ctx := context.Background()
	key := fmt.Sprintf(KeyLiveLike, uid)
	var rst *redis.IntCmd
	var exp clause.Expr
	ts := db.Redis.ZScore(ctx, key, strconv.Itoa(int(lid))).Val()
	if undo {
		//not like
		if ts == 0 {
			return nil
		}
		exp = gorm.Expr("like_num - 1")
		rst = db.Redis.ZRem(ctx, key, lid)
	} else {
		//had liked
		if ts > 0 {
			return nil
		}
		exp = gorm.Expr("like_num + 1")
		rst = db.Redis.ZAdd(ctx, key, &redis.Z{Member: lid, Score: float64(time.Now().Unix())})
	}
	if rst.Err() != nil {
		return rst.Err()
	}
	return m.update(lid, "like_num", exp)
}
func (m LiveModel) update(lid uint, f string, v any) error {
	err := db.Orm.Model(&m.table).
		Where("id = ?", lid).
		Update(f, v).Error
	return err
}

func (m LiveModel) LivingList(offset, limit int) (data []*types.Live, err error) {
	err = db.Orm.Model(&m.table).
		Where("status = ?", types.LiveStatusLiving).
		Offset(offset).Limit(limit).
		Order("created_at desc").
		Find(&data).Error
	return
}

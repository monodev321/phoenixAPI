package model

import (
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type FeedbackModel struct {
	table types.Feedback
}

func (m FeedbackModel) Save(r *types.Feedback) (err error) {
	rst := db.Orm.Model(&m.table).Save(r)
	return rst.Error
}

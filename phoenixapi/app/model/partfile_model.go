package model

import (
	"errors"
	"gorm.io/gorm/clause"
	"log"
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type PartFileModel struct {
	table types.PartFile
}

func (m PartFileModel) Create(pf *types.PartFile) (err error) {
	err = db.Orm.Save(pf).Error
	return err
}

func (m PartFileModel) UpdateMeta(uuid string, partNum uint, updates map[string]any) (meta *types.PartMeta, err error) {
	meta = &types.PartMeta{}
	err = db.Orm.Model(meta).
		Clauses(clause.Returning{}).
		Where("uuid = ? AND num = ?", uuid, partNum).UpdateColumns(updates).Error
	return meta, err
}

func (m PartFileModel) UpdateStatus(uuid string, st string) bool {
	rst := db.Orm.Model(&m.table).Where("uuid = ?", uuid).Update("status", st)
	if rst.Error != nil {
		log.Print("error:", rst.Error)
	}
	return rst.Error == nil && rst.RowsAffected == 1
}

func (m PartFileModel) UpdateUri(uuid string, uri string) bool {
	rst := db.Orm.Model(&m.table).Where("uuid = ?", uuid).Update("uri", uri)
	if rst.Error != nil {
		log.Print("error:", rst.Error)
	}
	return rst.Error == nil && rst.RowsAffected == 1
}

func (m PartFileModel) MustByUUID(uuid string) (f *types.PartFile, err error) {
	if uuid == "" {
		return nil, errors.New("empty")
	}
	f = &types.PartFile{}
	err = db.Orm.Model(&m.table).Take(&f, "uuid = ?", uuid).Error
	if err != nil {
		return nil, err
	}
	err = db.Orm.Model(&types.PartMeta{}).Order("num asc").Find(&f.PartMetas, "uuid = ?", uuid).Error
	if err != nil {
		return nil, err
	}
	return f, err
}

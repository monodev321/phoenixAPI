package model

import (
	"errors"
	"fmt"
	"gorm.io/gorm"
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type SnsModel struct {
	table types.SnsRelation
}

func (m SnsModel) getBy(r *types.SnsRelation) (exist bool, err error) {
	rst := db.Orm.Where("user_id = ? AND follow_id = ?", r.UserID, r.FollowID).Take(r)
	if rst.Error != nil {
		if errors.Is(rst.Error, gorm.ErrRecordNotFound) {
			return false, nil
		}
		return false, rst.Error
	}
	return true, nil
}

func (m SnsModel) Get(rid uint) (r *types.SnsRelation, err error) {
	r = &types.SnsRelation{}
	err = db.Orm.Model(&m.table).Take(r, "id = ?", rid).Error
	if err != nil {
		return nil, err
	}
	return
}

func (m SnsModel) UpdateColumns(r *types.SnsRelation) (err error) {
	err = db.Orm.Model(&m.table).
		Where("id = ?", r.ID).
		Select([]string{"friend_flag", "is_top", "see_ta", "see_me"}).
		UpdateColumns(r).Error
	return
}

func (m SnsModel) Update(rid uint, k string, v any) (err error) {
	err = db.Orm.Model(&m.table).Where("id = ?", rid).Update(k, v).Error
	return
}

func (m SnsModel) Relation(uid, fid uint) (r *types.SnsRelation, err error) {
	var rel = &types.SnsRelation{}
	rst := db.Orm.Model(&m.table).
		Where("user_id = ? AND follow_id = ?", uid, fid).
		//Or("user_id = ? AND follow_id = ?", fid, uid).
		Find(&rel)
	if rst.Error != nil {
		err = rst.Error
		return
	}
	if rst.RowsAffected == 0 {
		return
	}
	return rel, nil
	//for _, v := range rel {
	//	if v.UserID == uid {
	//		r |= types.RelationTypeFollow
	//	}
	//	if v.FollowID == uid {
	//		r |= types.RelationTypeFollower
	//	}
	//}
	//return
}

//GetReverseFollow 查询fid -> uid是否关注
func (m SnsModel) GetReverseFollow(uid uint, fids []uint) (r map[uint]*types.SnsRelation, err error) {
	var rel = make([]*types.SnsRelation, 0, 2)
	rst := db.Orm.Model(rel).
		Where("user_id IN ? AND follow_id = ?", fids, uid).
		Find(rel)
	if rst.Error != nil || rst.RowsAffected == 0 {
		err = rst.Error
		return
	}
	r = make(map[uint]*types.SnsRelation)
	for i, v := range rel {
		r[v.UserID] = rel[i]
	}
	return
}

//queryByCond query by condition
//1. follow list
//2. follower list
//3. friend list
func (m SnsModel) queryByCond(cond map[string]any, offset, limit int) (list []*types.SnsRelation, err error) {
	if len(cond) == 0 ||
		(cond["user_id"] == nil && cond["follow_id"] == nil) {
		return nil, errors.New("condition not validated")
	}
	tx := db.Orm.Model(&m.table)
	for k, v := range cond {
		tx = tx.Where(fmt.Sprintf(`%s = ?`, k), v)
	}
	err = tx.Offset(offset).Limit(limit).Order("is_top desc, created_at desc").Find(&list).Error
	return
}

//GetUserFollow 关注的人
func (m SnsModel) GetUserFollow(uid uint, offset, limit int) (list []*types.SnsRelation, err error) {
	cond := map[string]any{
		"user_id": uid,
	}
	list, err = m.queryByCond(cond, offset, limit)
	return
}

func (m SnsModel) SearchUserFollow(uid uint, q string, offset, limit int) (list []*types.SnsRelation, err error) {
	tx := db.Orm.Model(&m.table).
		Joins("LEFT JOIN users ON users.id = sns_relations.follow_id").
		Where("user_id = ?", uid).
		Where("users.name LIKE ? OR sns_relations.remark LIKE ?", q, q).
		Order("sns_relations.created_at desc").
		Offset(offset).Limit(limit).Find(&list)
	err = tx.Error
	return
}

func (m SnsModel) GetUserFriend(uid uint, offset, limit int) (list []*types.SnsRelation, err error) {
	cond := map[string]any{
		"user_id":     uid,
		"friend_flag": gorm.Expr("friend_flag > ?", 0),
	}
	list, err = m.queryByCond(cond, offset, limit)
	return
}

func (m SnsModel) GetUserFollower(uid uint, offset, limit int) (list []*types.SnsRelation, err error) {
	cond := map[string]any{
		"follow_id": uid,
	}
	list, err = m.queryByCond(cond, offset, limit)
	return
}

func (m SnsModel) Follow(uid, fid uint, undo bool) (err error) {
	var rel = &types.SnsRelation{UserID: uid, FollowID: fid}
	isFollow, err := m.getBy(rel)
	if err != nil {
		return err
	}
	//取消关注
	if undo {
		if !isFollow {
			return errors.New("not followed")
		}
		if rel.ID > 0 {
			err = db.Orm.Delete(rel).Error
		}
	} else {
		if isFollow {
			return errors.New("had followed yet")
		}
		fol := &types.SnsRelation{UserID: fid, FollowID: uid}
		isFollower, err := m.getBy(fol)
		if err != nil {
			return err
		}
		var friend uint8 = 0
		if isFollower {
			friend = 1
		}
		rel = &types.SnsRelation{
			UserID:     uid,
			FollowID:   fid,
			Group:      "follow",
			FriendFlag: friend,
			IsTop:      false,
			SeeTa:      true,
			SeeMe:      true,
		}
		err = db.Orm.Model(rel).Save(rel).Error
		if err != nil {
			return err
		}
		if isFollower && fol.FriendFlag == 0 {
			err = db.Orm.Model(fol).UpdateColumn("friend_flag", 1).Error
			if err != nil {
				return err
			}
		}

	}
	return
}

//func (m SnsModel) RedisFollow(uid, fid uint, undo bool) error {
//	ctx := context.Background()
//	k1 := fmt.Sprintf(KeyUserFollow, uid)
//	k2 := fmt.Sprintf(KeyUserFollower, fid)
//	var fn func(pipe redis.Pipeliner) error
//	if undo {
//		fn = func(pipe redis.Pipeliner) error {
//			pipe.ZRem(ctx, k1, fid)
//			pipe.ZRem(ctx, k2, uid)
//			return nil
//		}
//	} else {
//		score := float64(time.Now().Unix())
//		fn = func(pipe redis.Pipeliner) error {
//			pipe.ZAdd(ctx, k1, &redis.Z{Member: fid, Score: score})
//			pipe.ZAdd(ctx, k2, &redis.Z{Member: uid, Score: score})
//			return nil
//		}
//	}
//	_, err := db.Redis.Pipelined(ctx, fn)
//	return err
//}
//
//func (m SnsModel) RedisRelation(uid, fid uint) (r types.RelationType, err error) {
//	ctx := context.Background()
//	k1 := fmt.Sprintf(KeyUserFollow, uid)
//	k2 := fmt.Sprintf(KeyUserFollow, fid)
//	var fn = func(pipe redis.Pipeliner) error {
//		f1 := pipe.ZScore(ctx, k1, strconv.Itoa(int(fid))).Val()
//		f2 := pipe.ZScore(ctx, k2, strconv.Itoa(int(uid))).Val()
//		if f1 > 0 {
//			r |= types.RelationTypeFollow
//		}
//		if f2 > 0 {
//			r |= types.RelationTypeFollower
//		}
//		return nil
//	}
//	_, err = db.Redis.Pipelined(ctx, fn)
//	return
//}

package model

import (
	"errors"
	"github.com/golang-jwt/jwt/v4"
	"phoenix/pkg/config"
	"time"
)

type AuthClaims struct {
	UserID   uint   `json:"user_id"`
	UserName string `json:"user_name"`
	*jwt.RegisteredClaims
}

func (c *AuthClaims) Valid() error {
	if c.UserID <= 0 {
		return errors.New("userid invalid")
	}
	return c.RegisteredClaims.Valid()
}

func (c *AuthClaims) CreateToken() (string, error) {
	if c.RegisteredClaims == nil {
		now := time.Now()
		c.RegisteredClaims = &jwt.RegisteredClaims{
			Issuer:    "phoenixApiServer",
			NotBefore: jwt.NewNumericDate(now.Add(-time.Minute)),
			ExpiresAt: jwt.NewNumericDate(now.AddDate(0, 0, 30)),
		}
	}
	//signed token
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, c).SignedString(config.TokenSecret)
	return token, err
}

package film

import (
	"fmt"
	"phoenix/app/types"
	"phoenix/pkg/db"
	"time"
)

type Film struct {
}

func (f Film) Films(offset int) (films *[]types.Film, count int64, err error) {
	var film types.Film
	db.Orm.Model(film).Count(&count)
	err = db.Orm.Model(film).Offset(offset).Limit(10).Find(&films).Error
	fmt.Printf("%+v \n", err)
	if err != nil {
		films = nil
	}
	return
}

func (f Film) FilmById(id int) (film *types.Film, err error) {
	film = &types.Film{}
	err = db.Orm.Model(film).Take(film, id).Error
	if err != nil {
		film = nil
	}
	return
}

func (f Film) DeleteFilm(id int) (err error) {
	film := &types.Film{}
	film.Id = id
	err = db.Orm.Where("id = ?", id).Delete(film).Error
	if err != nil {
		film = nil
	}
	return
}

func (f Film) FilmEdit(json map[string]interface{}) (err error) {
	film := &types.Film{}
	film.Id = int(json["id"].(float64))
	err = db.Orm.Model(film).Take(film, film.Id).Error

	film.Title = json["title"].(string)
	film.Cover = json["cover"].(string)
	film.Cover2 = json["cover2"].(string)
	film.KindId = int(json["kindId"].(float64))
	film.Kind = json["kind"].(string)

	film.LanguageId = int(json["languageId"].(float64))
	film.Language = json["language"].(string)
	film.RegionId = int(json["regionId"].(float64))
	film.Region = json["region"].(string)
	film.TimeId = int(json["timeId"].(float64))
	film.Times = json["times"].(string)
	film.PlotId = json["plotId"].(string)
	film.Plot = json["plot"].(string)

	film.Age = int(json["age"].(float64))
	film.Desc = json["desc"].(string)
	film.Director = json["director"].(string)
	film.Actor = json["actor"].(string)
	film.Duration = int(json["duration"].(float64))
	film.Grade = float32(json["grade"].(float64))
	now := time.Now().Unix()
	if film.CreateTime == 0 {
		film.CreateTime = now
	}
	film.UpdateTime = now
	err = db.Orm.Model(film).Updates(film).Error
	if err != nil {
		film = nil
	}
	return
}

func (f Film) FilmAdd(json map[string]interface{}) (err error) {
	film := &types.Film{}

	film.Title = json["title"].(string)
	film.Url = json["url"].(string)
	film.Md5 = json["md5"].(string)
	film.Cover = json["cover"].(string)
	film.Cover2 = json["cover2"].(string)
	film.KindId = int(json["kindId"].(float64))
	film.Kind = json["kind"].(string)

	film.LanguageId = int(json["languageId"].(float64))
	film.Language = json["language"].(string)
	film.RegionId = int(json["regionId"].(float64))
	film.Region = json["region"].(string)
	film.TimeId = int(json["timeId"].(float64))
	film.Times = json["times"].(string)
	film.PlotId = json["plotId"].(string)
	film.Plot = json["plot"].(string)

	film.Age = int(json["age"].(float64))
	film.Desc = json["desc"].(string)
	film.Director = json["director"].(string)
	film.Actor = json["actor"].(string)
	film.Duration = int(json["duration"].(float64))
	film.Grade = float32(json["grade"].(float64))
	now := time.Now().Unix()
	film.CreateTime = now
	film.UpdateTime = now
	err = db.Orm.Model(film).Create(film).Error
	if err != nil {
		film = nil
	}
	return
}

func (f Film) FilmChangeField(json map[string]interface{}) (err error) {
	film := &types.Film{}
	film.Id = int(json["id"].(float64))
	field := json["field"].(string)
	err = db.Orm.Model(film).Take(film, film.Id).Error
	val := int(json["value"].(float64))
	switch field {
	case "status":
		film.Status = val
	case "sort":
		film.Sort = val
	case "isTop":
		film.IsTop = val
	}
	now := time.Now().Unix()
	if film.CreateTime == 0 {
		film.CreateTime = now
	}
	film.UpdateTime = now
	err = db.Orm.Model(film).Updates(film).Error
	if err != nil {
		film = nil
	}
	return
}

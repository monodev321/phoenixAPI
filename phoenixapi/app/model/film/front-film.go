package film

import (
	"fmt"
	"phoenix/app/types"
	"phoenix/pkg/db"
)

func (f Film) FrontFilms(offset int, kindId int, plotId string, regionId int, languageId int, timeId int) (films *[]types.Film, count int64, err error) {
	var film types.Film
	if kindId > 0 {
		film.KindId = kindId
	}
	if plotId != "0" {
		film.PlotId = plotId + "_"
	}
	if regionId > 0 {
		film.RegionId = regionId
	}
	if languageId > 0 {
		film.KindId = languageId
	}
	if timeId > 0 {
		film.TimeId = timeId
	}
	db.Orm.Model(film).Count(&count)
	err = db.Orm.Model(film).Offset(offset).Limit(18).Find(&films).Error
	fmt.Printf("%+v \n", err)
	if err != nil {
		films = nil
	}
	return
}

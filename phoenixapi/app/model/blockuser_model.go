package model

import (
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type BlockUserModel struct {
	table types.BlockUser
}

func (m BlockUserModel) GetBlockIds(uId uint) ([]uint, error) {
	data := make([]uint, 0, 2)
	err := db.Orm.Select("block_id").Find(&data, "user_id = ?", uId).Error
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (m BlockUserModel) Create(data *types.BlockUser) error {
	err := db.Orm.Model(&m.table).Create(data).Error
	return err
}

func (m BlockUserModel) Delete(uid, bid uint) error {
	err := db.Orm.Model(&m.table).Delete("user_id = ? AND block_id = ?", uid, bid).Error
	return err
}

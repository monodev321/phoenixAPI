package model

const (
	KeyFeedLike     = "feed_like:%d"      //key:userId, member:feedId, score:timestamp
	KeyFeedStar     = "feed_star:%d"      //key:userId, member:feedId, score:timestamp
	KeyCommentLike  = "comment_like:%d"   //key:userId, member:commentId, score:timestamp
	KeyUserFollow   = "user_follow:%d"    //key:userId, member:userId, score:timestamp
	KeyUserFollower = "user_follower:%d"  //key:userId, member:userId, score:timestamp
	KeyShareLimit   = "share_limit:%d:%d" //key:userId,feedId, score:share_num
	keyWatchFeed    = "feed_watch:%d"     //key:feedId

	KeyVerifyCode = "verify_code:%d:%s" //key:codeCate:phoneId, val: code
	KeyLiveLike   = "live_like:%d"      //key:userId, member:liveId, score:timestamp
)

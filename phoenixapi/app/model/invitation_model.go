package model

import (
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type InvitationModel struct {
	table types.Invitation
}

func (m InvitationModel) Save(data *types.Invitation) error {
	in := &types.Invitation{}
	rst := db.Orm.Model(&m.table).Find(&in, "user_id =? AND invite_id = ?", data.UserID, data.InviteID)
	if rst.Error != nil {
		return rst.Error
	}
	if rst.RowsAffected == 0 {
		return db.Orm.Model(&m.table).Create(data).Error
	}
	return nil
}

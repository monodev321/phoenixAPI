package model

import (
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type ReportModel struct {
	table types.Report
}

func (m ReportModel) Save(r *types.Report) (err error) {
	rst := db.Orm.Model(&m.table).Save(r)
	return rst.Error
}

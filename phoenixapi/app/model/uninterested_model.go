package model

import (
	"phoenix/app/types"
	"phoenix/pkg/db"
)

type UninterestedModel struct {
	table types.Uninterested
}

func (m UninterestedModel) FindByUserID(uid uint) (data []*types.Uninterested, err error) {

	return nil, nil
}

func (m UninterestedModel) Save(data *types.Uninterested) error {
	err := db.Orm.Model(&m.table).Save(data).Error
	return err
}

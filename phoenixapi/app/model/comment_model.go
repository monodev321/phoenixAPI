package model

import (
	"context"
	"fmt"
	"math"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/pkg/db"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type CommentModel struct {
	table types.Comment
}

func (m CommentModel) GetOne(id uint) (c *types.Comment, err error) {
	c = &types.Comment{}
	err = db.Orm.Model(&m.table).Take(c, "id = ?", id).Error
	return
}

func (m CommentModel) GetListPage(uid, vid uint, offset, limit int) (list []*types.Comment, err error) {
	err = db.Orm.Model(&m.table).
		Where("feed_id = ? AND reply_id = 0", vid).
		Offset(offset).Limit(limit).
		Find(&list).
		Order("id desc").Error
	if err != nil {
		return nil, err
	}
	err = m.fillCommentLiked(uid, list)
	return
}

func (m CommentModel) GetCommentsByFilm(uid uint, film_id, offset, limit int) (list []*types.Comment, count int, err error) {
	err = db.Orm.Model(&m.table).
		Where("film_id = ? AND reply_id = 0", film_id).
		Offset(offset).Limit(limit).
		Find(&list).
		Order("id desc").Error
	if err != nil {
		return nil, 0, err
	}
	err = m.fillCommentLiked(uid, list)
	db.DBCli.Get(&count, "select count(1) from comments where film_id = ? and reply_id = 0", film_id)
	return
}

func (m CommentModel) Save(c *types.Comment) (err error) {
	err = db.Orm.Model(&m.table).Save(c).Error
	return
}

func (m CommentModel) Update(id uint, key string, val any) (err error) {
	err = db.Orm.Model(&m.table).Where("id = ?", id).UpdateColumn(key, val).Error
	return
}

func (m CommentModel) fillCommentLiked(uid uint, list []*types.Comment) (err error) {
	if uid == 0 {
		return nil
	}
	ctx := context.Background()
	key := fmt.Sprintf(KeyCommentLike, uid)
	members := make([]string, 0, len(list))
	for _, v := range list {
		members = append(members, fmt.Sprint(v.ID))
	}
	cmd := db.Redis.ZMScore(ctx, key, members...)
	if cmd.Err() != nil {
		return
	}
	for k, v := range cmd.Val() {
		if math.IsNaN(v) || v <= 0 {
			list[k].Liked = false
		} else {
			list[k].Liked = true
		}
	}
	return
}

func (m CommentModel) Like(uid, cid uint, undo bool) (err error) {
	ctx := context.Background()
	key := fmt.Sprintf(KeyCommentLike, uid)
	var exp clause.Expr
	ts := db.Redis.ZScore(ctx, key, strconv.Itoa(int(cid))).Val()
	if undo {
		if ts == 0 {
			return nil //还没有赞过
		}
		db.Redis.ZRem(ctx, key, cid)
		exp = gorm.Expr("like_num - 1")
	} else {
		if ts > 0 {
			return nil //已经赞过
		}
		db.Redis.ZAdd(ctx, key, &redis.Z{Member: cid, Score: float64(time.Now().Unix())})
		exp = gorm.Expr("like_num + 1")
	}
	err = db.Orm.Model(&m.table).
		Where("id = ?", cid).
		UpdateColumn("like_num", exp).Error
	return
}

func (m CommentModel) SetReplies(uid uint, list []*proto.UserComment) (err error) {
	if len(list) == 0 {
		return
	}
	nodeIds := make([]uint, 0, len(list))
	for _, v := range list {
		nodeIds = append(nodeIds, v.ID)
	}
	replies := make([]*types.Comment, 0, 10)
	err = db.Orm.Model(&m.table).
		Where("reply_id IN ?", nodeIds).
		Order("reply_id desc,id asc").
		Limit(200).
		Find(&replies).Error
	if err != nil {
		return err
	}
	if err = m.fillCommentLiked(uid, replies); err != nil {
		return err
	}
	repliesMap := make(map[uint][]*proto.UserComment)
	uids := make([]uint, 0, len(replies))
	for _, v := range replies {
		uids = append(uids, v.UserID)
	}
	userMap, err := UserModel{}.GetUsersBaseInfo(uids)
	if err != nil {
		return err
	}
	for i, v := range replies {
		repliesMap[v.ReplyID] = append(repliesMap[v.ReplyID], &proto.UserComment{
			UserBaseInfo: userMap[v.UserID],
			Comment:      replies[i],
		})
	}
	for k, v := range list {
		list[k].Replies = repliesMap[v.ID]
	}
	return
}

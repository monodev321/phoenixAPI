package model

import (
	"context"
	"errors"
	"fmt"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/pkg/db"
	"strconv"
	"time"

	"github.com/go-redis/redis/v8"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type FeedModel struct {
	table types.Feed
}

func (FeedModel) GetFollowNewest(uid uint, offset, limit int) (list []*proto.UserFeed, err error) {
	var data []*types.Feed
	rst := db.Orm.Raw(`select * from feeds where user_id = ? order by created_at desc limit ?, ?`, uid, offset, limit).Find(&data)
	// 	rst := db.Orm.Raw(`select v.*
	// from
	// 	sns_relations s
	// left join feeds v on s.follow_id = v.user_id
	// where s.user_id = ?
	// order by v.created_at desc
	// limit ?, ?`, uid, offset, limit).Find(&data)
	if rst.Error != nil {
		return nil, rst.Error
	}
	var uids = make([]uint, 0, len(data))
	for _, v := range data {
		uids = append(uids, v.UserID)
	}
	userMap, err := UserModel{}.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserFeed, 0, len(data))
	for k, v := range data {
		uv := &proto.UserFeed{
			Feed: data[k], UserBaseInfo: userMap[v.UserID],
		}
		list = append(list, uv)
	}
	return
}

func (FeedModel) GetFriendNewest(uid uint, offset, limit int) (list []*proto.UserFeed, err error) {
	var data []*types.Feed
	rst := db.Orm.Raw(`select v.*
from 
	sns_relations s 
left join feeds v on s.follow_id = v.user_id
where s.user_id = ? and s.friend_flag > 0
order by v.created_at desc 
limit ?, ?`, uid, offset, limit).Find(&data)
	if rst.Error != nil {
		return nil, rst.Error
	}
	var uids = make([]uint, 0, len(data))
	for _, v := range data {
		uids = append(uids, v.UserID)
	}
	userMap, err := UserModel{}.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserFeed, 0, len(data))
	for k, v := range data {
		uv := &proto.UserFeed{
			Feed: data[k], UserBaseInfo: userMap[v.UserID],
		}
		list = append(list, uv)
	}
	return
}

func (m FeedModel) UserLike(uid uint, vid uint, undo bool) (err error) {
	ctx := context.Background()
	key := fmt.Sprintf(KeyFeedLike, uid)
	var rst *redis.IntCmd
	var exp clause.Expr
	ts := db.Redis.ZScore(ctx, key, strconv.Itoa(int(vid))).Val()
	if undo {
		//not like
		if ts == 0 {
			return nil
		}
		exp = gorm.Expr("like_num - 1")
		rst = db.Redis.ZRem(ctx, key, vid)
	} else {
		//had liked
		if ts > 0 {
			return nil
		}
		exp = gorm.Expr("like_num + 1")
		rst = db.Redis.ZAdd(ctx, key, &redis.Z{Member: vid, Score: float64(time.Now().Unix())})
	}
	if rst.Err() != nil {
		return rst.Err()
	}

	//todo 后期可以考虑不记录到数据库,只记录到缓存
	err = db.Orm.Model(&m.table).
		Where("id = ?", vid).
		UpdateColumn("like_num", exp).Error
	return
}

func (m FeedModel) FillUserLiked(uid uint, feeds []*proto.UserFeed) error {
	if uid == 0 || len(feeds) == 0 {
		return nil
	}
	ctx := context.Background()
	key := fmt.Sprintf(KeyFeedLike, uid)
	members := make([]string, 0, len(feeds))
	for _, v := range feeds {
		members = append(members, strconv.Itoa(int(v.Feed.ID)))
	}
	rst := db.Redis.ZMScore(ctx, key, members...)
	if rst.Err() != nil {
		return rst.Err()
	}
	for k, v := range rst.Val() {
		if v > 0 {
			feeds[k].Liked = true
		} else {
			feeds[k].Liked = false
		}
	}
	return nil
}
func (m FeedModel) FillUserStared(uid uint, feeds []*proto.UserFeed) error {
	if uid == 0 || len(feeds) == 0 {
		return nil
	}
	ctx := context.Background()
	key := fmt.Sprintf(KeyFeedStar, uid)
	members := make([]string, 0, len(feeds))
	for _, v := range feeds {
		members = append(members, strconv.Itoa(int(v.Feed.ID)))
	}
	rst := db.Redis.ZMScore(ctx, key, members...)
	if rst.Err() != nil {
		return rst.Err()
	}
	for k, v := range rst.Val() {
		if v > 0 {
			feeds[k].Stared = true
		} else {
			feeds[k].Stared = false
		}
	}
	return nil
}

func (m FeedModel) UserStar(uid uint, vid uint, undo bool) (err error) {
	ctx := context.Background()
	key := fmt.Sprintf(KeyFeedStar, uid)
	var rst *redis.IntCmd
	var exp clause.Expr
	ts := db.Redis.ZScore(ctx, key, strconv.Itoa(int(vid))).Val()
	if undo {
		if ts == 0 {
			return nil //没有收藏
		}
		exp = gorm.Expr("star_num - 1")
		rst = db.Redis.ZRem(ctx, key, vid)
	} else {
		if ts > 0 {
			return nil //已经收藏过
		}
		exp = gorm.Expr("star_num + 1")
		rst = db.Redis.ZAdd(ctx, key, &redis.Z{Member: vid, Score: float64(time.Now().Unix())})
	}
	if rst.Err() != nil {
		return rst.Err()
	}

	//todo 后期可以考虑不记录到数据库,只记录到缓存
	err = db.Orm.Model(&m.table).
		Where("id = ?", vid).
		UpdateColumn("star_num", exp).Error
	return
}

func (m FeedModel) StatsShare(uid, vid uint) (err error) {
	ctx := context.Background()
	key := fmt.Sprintf(KeyShareLimit, uid, vid)
	num := db.Redis.Incr(ctx, key).Val()
	if num > 10 {
		return errors.New("share limit reached")
	}
	ttl := db.Redis.TTL(ctx, key).Val()
	if ttl == -1 {
		db.Redis.Expire(ctx, key, 24*time.Hour)
	}
	err = db.Orm.Model(&m.table).Where("id = ?", vid).
		UpdateColumn("share_num", gorm.Expr("share_num + 1")).Error
	return
}

func (m FeedModel) StatsComment(vid uint) (err error) {
	err = db.Orm.Model(&m.table).Where("id = ?", vid).
		UpdateColumn("comment_num", gorm.Expr("comment_num + 1")).Error
	return
}

func (m FeedModel) GetUserLike(uid uint, start, stop int64) (v []*types.Feed, err error) {
	ctx := context.Background()
	key := fmt.Sprintf(KeyFeedLike, uid)
	rst, err := db.Redis.ZRange(ctx, key, start, stop).Result()
	if err != nil {
		return nil, err
	}
	if len(rst) > 0 {
		ids := make([]uint, 0, 10)
		for i := range rst {
			id, err := strconv.Atoi(rst[i])
			if err != nil {
				return nil, err
			}
			ids = append(ids, uint(id))
		}
		err = db.Orm.Model(&m.table).Where("id IN ?", ids).Find(&v).Error
	}
	return
}

func (m FeedModel) GetLatest(offset, limit int) (list []*proto.UserFeed, err error) {
	data := make([]*types.Feed, 0, 4)
	err = db.Orm.Model(&m.table).
		Offset(offset).Limit(limit).
		Order("created_at desc").
		Find(&data).Error
	var uids = make([]uint, 0, len(data))
	for _, v := range data {
		uids = append(uids, v.UserID)
	}
	userMap, err := UserModel{}.GetUsersBaseInfo(uids)
	if err != nil {
		return nil, err
	}
	list = make([]*proto.UserFeed, 0, len(data))
	for k, v := range data {
		uv := &proto.UserFeed{
			Feed: data[k], UserBaseInfo: userMap[v.UserID],
		}
		list = append(list, uv)
	}
	return
}

func (m FeedModel) SearchIntro(q string, offset, limit int) (v []*types.Feed, err error) {
	err = db.Orm.Model(&m.table).
		Where("intro LIKE ?", q).
		Order("created_at desc").
		Offset(offset).Limit(limit).
		Find(&v).Error
	return
}

// GetAll 获取用户所有公开的短视频列表(不包括秘密视频)
func (m FeedModel) GetAll(uid uint, offset, limit int) (v []*types.Feed, err error) {
	err = db.Orm.Model(&m.table).
		Where("user_id = ?", uid).
		Where("visible < ?", types.VisibleEnumSelf).
		Order("created_at desc").
		Offset(offset).Limit(limit).
		Find(&v).Error
	return
}

// GetSecret 获取用户秘密视频
func (m FeedModel) GetSecret(uid uint, offset, limit int) (v []*types.Feed, err error) {
	err = db.Orm.Model(&m.table).
		Where("user_id = ?", uid).
		Where("visible = ?", types.VisibleEnumSelf).
		Order("created_at desc").
		Offset(offset).Limit(limit).
		Find(&v).Error
	return
}

func (m FeedModel) Get(id uint) (v *types.Feed, err error) {
	v = &types.Feed{}
	err = db.Orm.Model(&m.table).Take(v, id).Error
	return
}

func (m FeedModel) Save(v *types.Feed) (err error) {
	return db.Orm.Save(v).Error
}

func (m FeedModel) Update(id uint, updates map[string]any) (err error) {
	return db.Orm.Model(&m.table).Where("id = ?", id).UpdateColumns(updates).Error
}

func (m FeedModel) Watch(uid uint, fid uint) (info *types.Feed, err error) {
	feedInfo, err := m.Get(fid)
	if err != nil {
		return nil, err
	}
	ctx := context.Background()
	key := fmt.Sprintf(keyWatchFeed, fid)
	rst := db.Redis.PFAdd(ctx, key, uid)
	if rst.Err() != nil {
		return nil, rst.Err()
	}
	count := uint(db.Redis.PFCount(ctx, key).Val())
	//需要更新
	if feedInfo.WatchNum < count {
		err = db.Orm.Model(&m.table).
			Where("id = ?", fid).
			UpdateColumn("watch_num", count).Error
		feedInfo.WatchNum = count
	}
	info = feedInfo
	return
}

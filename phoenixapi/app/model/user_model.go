package model

import (
	"context"
	"fmt"
	"log"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/pkg/db"
	"time"

	"gorm.io/gorm"
)

const (
	verifyCodeTimeout = 5 * time.Minute
)

type VerifyCodeCate uint8

const (
	_ VerifyCodeCate = iota
	VerifyCodeRegister
	VerifyCodeLogin
	VerifyCodeRetrievePwd

	VerifyCodeCateMax
)

type UserModel struct {
	table types.User
}

func (u UserModel) GetVerifyCode(cate VerifyCodeCate, phoneId string) (code string, err error) {
	ctx := context.Background()
	key := fmt.Sprintf(KeyVerifyCode, cate, phoneId)
	rst := db.Redis.Get(ctx, key)
	log.Println("GetVerifyCode:", rst)
	code, err = rst.Val(), rst.Err()
	return
}

func (u UserModel) SetVerifyCode(cate VerifyCodeCate, phoneId string, code string) error {
	ctx := context.Background()
	key := fmt.Sprintf(KeyVerifyCode, cate, phoneId)
	rst := db.Redis.SetEX(ctx, key, code, verifyCodeTimeout)
	log.Println("GetVerifyCode:", rst)
	err := rst.Err()
	return err
}

func (u UserModel) GetBaseInfo(uid uint) (baseInfo *proto.UserBaseInfo, err error) {
	baseInfo = &proto.UserBaseInfo{}
	err = db.Orm.Model(&u.table).
		Select([]string{
			"id as user_id",
			"username as user_name",
			"avatar",
			"gender",
		}).Where("id = ?", uid).Find(baseInfo).Error
	if err != nil {
		return nil, err
	}
	return
}

func (u UserModel) GetUsersBaseInfo(fids []uint) (userMap map[uint]*proto.UserBaseInfo, err error) {
	if len(fids) == 0 {
		return nil, nil
	}
	users := make([]*proto.UserBaseInfo, 0, len(fids))
	err = db.Orm.Model(&u.table).
		Select([]string{
			"id as user_id",
			"username as user_name",
			"avatar",
			"gender",
		}).Where("id IN ?", fids).Find(&users).Error
	if err != nil {
		return nil, err
	}
	userMap = make(map[uint]*proto.UserBaseInfo)
	for k, v := range users {
		userMap[v.UserID] = users[k]
	}
	return
}

func (u UserModel) Get(id uint) (user *types.User, err error) {
	user = &types.User{}
	err = db.Orm.Model(user).Take(user, id).Error
	if err != nil {
		user = nil
	}
	return
}

func (u UserModel) FindByPhoneID(phoneId string) (user *types.User, err error) {
	user = &types.User{}
	rst := db.Orm.Model(user).Find(user, "phone = ?", phoneId)
	err = rst.Error
	if err != nil {
		return nil, err
	}
	if rst.RowsAffected == 0 {
		return nil, nil
	}
	return
}

func (u UserModel) Save(user *types.User) error {
	return db.Orm.Save(user).Error
}

func (u UserModel) Update(uid uint, user *types.User) error {
	err := db.Orm.Model(user).
		Where("id = ?", uid).
		Updates(user).Error
	return err
}

func (u UserModel) UpdateColumn(uid uint, col string, change int) error {
	if change == 0 {
		return nil
	}
	val := ""
	if change < 0 {
		val = fmt.Sprintf("%s %d", col, change)
	} else {
		val = fmt.Sprintf("%s + %d", col, change)
	}
	return db.Orm.Model(&u.table).
		Where("id = ?", uid).
		Update(col, gorm.Expr(val)).Error
}

func (u UserModel) SearchName(q string, offset, limit int) (users []*types.User, err error) {
	users = make([]*types.User, 0, 2)
	err = db.Orm.Model(&u.table).
		Where("name LIKE ?", q).
		Offset(offset).Limit(limit).Find(&users).Error
	if err != nil {
		return nil, err
	}
	return
}

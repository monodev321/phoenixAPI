package app

import (
	"net/http"
	"path/filepath"
	"phoenix/app/api"
	"phoenix/app/api/admin"
	"phoenix/app/api/blog"
	"phoenix/app/api/film"
	"phoenix/app/api/im"
	"phoenix/app/api/mine"
	"phoenix/app/api/token"
	"phoenix/app/api/upload"
	"phoenix/pkg/config"

	"github.com/gin-gonic/gin"
)

func setupRouter(e *gin.Engine) {
	//i18n
	e.Use(I18n())
	v1 := e.Group("/v1")
	{
		// login
		{
			v1.POST("/oauth2/token", api.Token)                //查看别人的主页 [v] OK
			v1.POST("/oauth2/refresh-token", api.RefreshToken) //查看别人的主页 [v] OK
			v1.POST("/user/sms", api.SendSms)                  //查看别人的主页 [v] OK
			v1.POST("/user/tel-register", api.TelRegister)     //查看别人的主页 [v] OK
			v1.POST("/user/login", api.Login)                  //查看别人的主页 [v] OK
			v1.POST("/user/logout", api.Logout)                //查看别人的主页 [v] OK
			v1.POST("/admin/login", api.AdminLogin)
		}

	}
	filmApi := api.FilmApi{}
	adminGroup := v1.Group("/admin", adminJWT())
	{
		//upload
		adminGroup.POST("/upload", upload.MultUpload)
		// user
		adminGroup.POST("/user/list", admin.UserList)
		adminGroup.POST("/add", admin.AdministratorAdd)

		// fead
		adminGroup.POST("/feed/upload", admin.Upload)
		adminGroup.POST("/feed/uploadimg", admin.UploadImg)
		adminGroup.POST("/feed/info", admin.FeedInfo)
		adminGroup.POST("/feed/list", admin.FeedList)
		adminGroup.POST("/feed/create", admin.FeedCreate)
		adminGroup.POST("/feed/edit", admin.FeedEdit)
		adminGroup.POST("/feed/delete", admin.FeedDelete)

		// blog
		adminGroup.POST("/blog/add", blog.BlogAdd)
		adminGroup.POST("/blog/list", blog.BlogList)
		adminGroup.POST("/blog/info", blog.BlogInfo)
		adminGroup.POST("/blog/edit", blog.BlogEdit)
		adminGroup.POST("/blog/delete", blog.BlogDelete)
		adminGroup.POST("/blog/changefield", blog.BlogChangeField)
		//film

		adminGroup.POST("/films", filmApi.Films)
		adminGroup.GET("/film", filmApi.Film)
		adminGroup.POST("/film-edit", filmApi.FilmEdit)
		adminGroup.POST("/film-add", filmApi.FilmAdd)
		adminGroup.POST("/film-delete", filmApi.FilmDelete)
		adminGroup.POST("/film-change-field", filmApi.FilmChangeField)
	}
	userGroup := v1.Group("/user", authJWT())
	{
		userGroup.POST("referrer-code", api.ReferrerCode)
		userGroup.POST("my-referrer", api.MyReferrer)
		userGroup.POST("referrer-more", api.ReferrerMore)
		userGroup.POST("/change-tran-password", api.ChangeTranPassword)
		userGroup.POST("user-by-username", api.GetUserByUsername)
	}
	tokenGroup := v1.Group("/token", authJWT())
	{
		tokenGroup.POST("user-tokens", token.UserTokens)
		tokenGroup.POST("/token", token.Token)
		tokenGroup.POST("/transfer", token.Transfer)
		tokenGroup.POST("/pay", token.PayForDapp)
		tokenGroup.POST("/user-token", token.UserToken)
		tokenGroup.POST("/flow-record", token.FlowRecord)
		tokenGroup.POST("/user-flow", token.UserFlow)
		tokenGroup.POST("/user-frozen", token.UserFrozen)
		tokenGroup.POST("/frozen", token.Frozen)
		tokenGroup.POST("/frozen-tran", token.FrozenTran)
		tokenGroup.POST("/frozen-back", token.FrozenBack)
		tokenGroup.POST("/frozen-record", token.FrozenRecord)
		tokenGroup.POST("/unfrozen-record", token.UnFrozenRecord)
		tokenGroup.POST("/issue-flow", token.IssueFlow)
		tokenGroup.POST("/pledge", token.Pledge)
		tokenGroup.POST("/pledge-details", token.PledgeDetails)
	}
	mineGroup := v1.Group("/mine", authJWT())
	{
		mineGroup.POST("add-payment", mine.AddPayment)
		mineGroup.POST("get-payment", mine.GetPayment)
		mineGroup.POST("payment-log", mine.AddVip)
	}

	friendGroup := v1.Group("/friend", authJWT())
	{
		friendGroup.POST("/friend-request", im.FriendRequest)
		friendGroup.POST("/handle-friend-request", im.HandleFriendRequest)
		friendGroup.POST("/friend-request-list", im.FriendRequestList)
		friendGroup.POST("/friends", im.Friends)
		friendGroup.POST("/friend-by-uid", im.FriendByUid)
		friendGroup.POST("/friend-setting", im.FriendSetting)
		friendGroup.POST("/recover", im.RecoverFriend)
		friendGroup.POST("/delete", im.DeleteFriend)
		friendGroup.POST("/recommend", im.RecommendFriend)
	}

	r := e.Group("/api/v1")
	{

		user := api.UserApi{}
		{
			//public
			r.POST("/user/verifyCode", user.VerifyCode)   //获取验证码 [v] OK
			r.POST("/user/register", user.Register)       //注册 [v] OK
			r.POST("/user/login", user.Login)             //登陆 [v] OK
			r.POST("/user/logout", user.Logout)           //登出 [v] OK
			r.POST("/user/retrievePwd", user.RetrievePwd) //找回密码 [v] OK
		}
		sns := api.SnsApi{}
		{
			//public
			r.POST("/sns/profile", sns.Profile)            //查看别人的主页 [v] OK
			r.POST("/sns/userFeed/list", sns.UserFeedList) //作品列表 [v] OK
			r.POST("/sns/search/feed", sns.SearchFeed)     //用户搜索 [v] OK
			r.POST("/sns/search/user", sns.SearchUser)     //用户搜索 [v] OK

		}
		feed := api.FeedApi{}
		{
			//public
			r.POST("/feed/recommend", feed.Recommend) //推荐 [v] OK
			r.POST("/feed/info", feed.Info)           //短视频信息 [v] OK
			r.POST("/feed/upload", feed.Upload)       //上传 [v] OK
			// r.POST("/user/verifyCode", user.VerifyCode) //获取验证码 [v] OK
			// r.POST("/user/register", user.Register)     //注册 [v] OK
			// r.POST("/user/login", user.Login)           //登陆 [v] OK
			// r.POST("/user/logout", user.Logout)         //登出 [v] OK
			r.POST("/refresh-token2", filmApi.Refresh)
			r.POST("/user-info", filmApi.UserInfo)
			r.POST("/upload", upload.MultUploadFilm)
			r.POST("/user-films", filmApi.FrontFilms)
			r.POST("/film/top-recommend/list", film.TopRecommedList)
			r.POST("/film/ranking/list", film.RankingList)
			r.POST("/film/new-arrivals/list", film.NewArrivalsList)
			r.POST("/film/recommend/list", film.RecommedList)
			r.POST("/film/with-same-plot", film.WithSamePlot)
			r.POST("/film/like", film.LikeFilm)
			r.POST("/film/dislike", film.DislikeFilm)
			r.POST("/film/search", film.SearchFilmByName)
			r.POST("/film/filter", film.QuickFilter)

			// comment the film
			// r.POST("/film/comment/list", film.ListComments) //评论列表 [v] OK
		}

		//auth required
		s := r.Use(authJWT())
		{
			s.POST("/user/profile", user.Profile)            //我的个人主页 [v] OK
			s.POST("/profile/update", user.ProfileUpdate)    //更新个人主页 [v] OK
			s.POST("/profile/avatar", user.Avatar)           //更换头像 [v] OK
			s.POST("/profile/background", user.Background)   //更换背景 [v] OK
			s.POST("/user/report", user.Report)              //举报 [v]
			s.POST("/user/report/upload", user.ReportUpload) //举报上传 [v]
			s.POST("/user/block", user.Block)                //拉黑 [v]
			s.POST("/user/feedback", user.Feedback)          //反馈 [v]
		}
		p := r.Use(authJWT())
		{
			pf := api.PartFileApi{}
			p.POST("/partFile/create", pf.PartCreate)         //创建分片 [v] OK
			p.POST("/partFile/upload/:uuid", pf.PartUpload)   //上传分片 [v] OK
			p.GET("/partFile/info/:uuid", pf.PartUploadQuery) //查询分片 [v] OK
		}
		//sns friends/ follow/ follower
		{
			s.POST("/sns/search/follow", im.SearchFollow)    //关注搜索 [v] OK
			s.POST("/sns/follower/list", im.FollowerList)    //粉丝页面 [v] OK
			s.POST("/sns/follow/list", im.FollowList)        //关注页面 [v] OK
			s.POST("/sns/follow/feed", im.FollowFeed)        //关注的人的视频列表 [v] OK
			s.POST("/sns/friend/list", im.Friends)           //好友列表 [v] OK
			s.POST("/sns/friend/feed", im.FriendFeedList)    //好友短视频列表 [v] OK
			s.POST("/sns/userFeed/like", im.LikeFeedList)    //喜欢列表 [v] OK
			s.POST("/sns/follow", im.Follow)                 //关注(回关)/取消关注 [v] OK
			s.POST("/sns/removeFollower", im.RemoveFollower) //移除粉丝 [v]
			s.POST("/sns/remark", im.Remark)                 //设置备注名(关注后才能设置) [v] OK
			s.POST("/sns/setting", im.Setting)               //设置好友权限 [v] OK
		}
		//blog
		{
			s.POST("/blog/list", blog.BlogList)
			s.POST("/blog/info", blog.BlogInfo)
			s.POST("/blog/search", blog.SearchBlogByName)
		}

		//feed
		{
			s.POST("/feed/list", feed.List)                 //作品页面 [v] OK
			s.POST("/feed/like", feed.Like)                 //点赞,取消点赞 [v] OK
			s.POST("/feed/star", feed.Star)                 //收藏,取消收藏 [v] OK
			s.POST("/feed/create", feed.Create)             //发布短视频 [v] OK
			s.POST("/feed/share", feed.Share)               //分享(功能应该前端实现,这里只在成功后统计下次数) [v] OK
			s.POST("/feed/watch", feed.Watch)               //短视频观看计数 [v] OK
			s.POST("/feed/uninterested", feed.Uninterested) //不感兴趣 [v] OK
			s.POST("/feed/visible", feed.Visible)           //设置查看权限 [v] OK
		}

		// film with auth
		{
			s.POST("/film/comment/list", film.ListComments) //评论列表 [v] OK
			s.POST("/film/comment/post", film.PostComment)  //发表评论 [v] OK
			r.POST("/film/comment/like", film.LikeComment)  //点赞评论 [v] OK
		}

		//comment
		{
			comment := api.CommentApi{}
			s.POST("/comment/post", comment.Post) //发表评论 [v] OK
			s.POST("/comment/like", comment.Like) //点赞评论 [v] OK
			s.POST("/comment/list", comment.List) //评论列表 [v] OK
		}

		//live streaming
		{
			live := api.LiveApi{}
			s.POST("/live/verifyIdCard", live.VerifyIdCard) //实名认证 [v] OK
			s.POST("/live/create", live.Create)             //开直播 [v] OK
			s.POST("/live/list", live.FeedList)             //直播中的视频列表/主播列表 [v] OK
			s.POST("/live/close", live.Close)               //下播 [v] OK
			s.POST("/live/like", live.Like)                 //赞/取消点赞直播 [v] OK
		}

	}

	tool := e.Group("/api/tool")
	{
		tool.POST("/create-client", api.CreateOauthClient) //查看别人的主页 [v] OK
	}
	// swag.SetupSwag(e)

	resDir, err := filepath.Abs(config.AppCfg.App.ResourceDir)
	if err != nil {
		panic(err)
	}
	e.StaticFS("/res", http.Dir(resDir))
}

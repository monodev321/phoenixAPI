package app

import (
	"encoding/json"
	"github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
	"golang.org/x/text/language"
	"path"
	"path/filepath"
	"phoenix/pkg/config"
)

func I18n() gin.HandlerFunc {
	i18nPath, err := filepath.Abs(path.Join(config.AppCfg.App.I18nDir, "localize"))
	if err != nil {
		panic(err)
	}
	h := i18n.Localize(i18n.WithBundle(&i18n.BundleCfg{
		RootPath:        i18nPath,
		DefaultLanguage: language.Chinese,
		AcceptLanguage: []language.Tag{
			language.Chinese,
			language.English,
		},
		UnmarshalFunc:    json.Unmarshal,
		FormatBundleFile: "json",
	}))
	return h
}

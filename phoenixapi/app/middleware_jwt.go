package app

import (
	"phoenix/app/api"
	"phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"
	"strconv"

	"github.com/gin-gonic/gin"
)

func authJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken, ok := service.Oauth2.BearerAuth(c.Request)
		if !ok {
			api.ErrorMsg(c, api.TokenInvalid, werror.New(200, "token值不能为空"))
			return
		}
		uid, username, err := util.ParseJwtToken(accessToken, config.AppCfg.Oauth2.PublickKey)
		if err != nil {
			api.ErrorMsg(c, api.TokenInvalid, werror.New(200, "无效的Token"))
			return
		}
		userId, _ := strconv.ParseUint(uid.(string), 10, 64)
		c.Set("userId", uint(userId))
		c.Set("uid", uid.(string))
		c.Set("username", username.(string))
		c.Next()
		return
	}
}

func adminJWT() gin.HandlerFunc {
	return func(c *gin.Context) {
		//直接用token验证
		token, err := service.Oauth2.ValidationBearerToken(c.Request)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
		scope := token.GetScope()
		if scope != "administrator" {
			util.FailWithCode(c, werror.New(200, "无效的token"), 404)
			return
		}
		userId, _ := strconv.ParseUint(token.GetUserID(), 10, 64)
		c.Set("userId", uint(userId))
		c.Set("uid", token.GetUserID())
		c.Set("username", token.GetUsername())
		c.Next()
		return
	}
}

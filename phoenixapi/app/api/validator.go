package api

import (
	"github.com/go-playground/validator/v10"
)

type EmailRegisterReq struct {
	Username        string `form:"username" json:"username" binding:"required,email"`
	Password        string `form:"password" json:"password" binding:"required,gte=6,lte=100,passwordValid"`
	ConfirmPassword string `form:"confirmPassword" json:"confirmPassword" binding:"required,eqfield=Password"`
}

// 绑定模型获取验证错误的方法
func (req *EmailRegisterReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Username" {
			switch e.Tag() {
			case "required":
				return "请输入您的注册账户"
			case "email":
				return "注册账号必须为邮件账号"
			}
		}

		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须大于6位"
			case "lte":
				return "密码长度必须小于100位"
			case "passwordValid":
				return "密码必须包含字母和数字"
			}
		}

		if e.Field() == "ConfirmPassword" {
			switch e.Tag() {
			case "required":
				return "请输入确认密码"
			case "eqfield":
				return "两次输入密码需一致"
			}
		}
	}
	return "参数错误"
}

type TelRegisterReq struct {
	Username string `form:"username" json:"username" binding:"required,number"`
	Password string `form:"password" json:"password" binding:"required,gte=6,lte=100,passwordValid"`
	Referrer string `form:"referrer" json:"referrer" binding:"required"`
	Code     string `form:"code" json:"code" binding:"required,number"`
}

// 绑定模型获取验证错误的方法
func (req *TelRegisterReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Username" {
			switch e.Tag() {
			case "required":
				return "请输入您的注册账户"
			case "number":
				return "注册账号必须为手机号码"
			}
		}

		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须大于6位"
			case "lte":
				return "密码长度必须小于100位"
			case "passwordValid":
				return "密码必须包含字母和数字"
			}
		}

		if e.Field() == "Code" {
			switch e.Tag() {
			case "required":
				return "请输入验证码"
			case "number":
				return "验证码格式不正确"
			}
		}

		if e.Field() == "Referrer" {
			switch e.Tag() {
			case "required":
				return "请输入邀请码"
			}
		}
	}
	return "参数错误"
}

type FindPasswordReq struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required,gte=6,lte=100,passwordValid"`
	Code     string `form:"code" json:"code" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *FindPasswordReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Username" {
			switch e.Tag() {
			case "required":
				return "请输入您的注册账户"
			}
		}

		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须大于6位"
			case "lte":
				return "密码长度必须小于100位"
			case "passwordValid":
				return "密码必须包含字母和数字"
			}
		}

		if e.Field() == "Code" {
			switch e.Tag() {
			case "required":
				return "请输入验证码"
			}
		}
	}
	return "参数错误"
}

type VerifyReq struct {
	Code string `form:"code" json:"code" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *VerifyReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Code" {
			switch e.Tag() {
			case "required":
				return "验证码错误"
			}
		}
	}
	return "参数错误"
}

type LoginReq struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required,gte=6,lte=100,passwordValid"`
}

// 绑定模型获取验证错误的方法
func (req *LoginReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Username" {
			switch e.Tag() {
			case "required":
				return "请输入您的注册账户"
			case "email":
				return "注册账号必须为邮件账号"
			}
		}

		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须大于6位"
			case "lte":
				return "密码长度必须小于100位"
			case "passwordValid":
				return "密码必须包含字母和数字"
			}
		}
	}
	return "参数错误"
}

type RefreshTokenReq struct {
	RefreshToken string `form:"refresh_token" json:"refresh_token" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *RefreshTokenReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "RefreshToken" {
			switch e.Tag() {
			case "required":
				return "无效的token"
			}
		}
	}
	return "参数错误"
}

// 请求用户信息
type GetUserByUsernameReq struct {
	Username string `form:"username" json:"username" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *GetUserByUsernameReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Username" {
			switch e.Tag() {
			case "required":
				return "无效的用户名"
			}
		}
	}
	return "参数错误"
}

// 请求用户信息
type GetUserByUidReq struct {
	Uid int64 `form:"uid" json:"uid" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *GetUserByUidReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 请求用户信息
type GetSessionUserReq struct {
	Uid int64  `form:"uid" json:"uid" binding:"required"`
	Gid *int64 `form:"gid" json:"gid" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *GetSessionUserReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 修改转账密码
type ChangeTranPasswordReq struct {
	OldPassword     string `form:"oldPassword" json:"oldPassword" binding:"-"`
	Password        string `form:"password" json:"password" binding:"required,gte=6,lte=6"`
	ConfirmPassword string `form:"confirmPassword" json:"confirmPassword" binding:"required,eqfield=Password"`
}

// 绑定模型获取验证错误的方法
func (req *ChangeTranPasswordReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须为6位"
			case "lte":
				return "密码长度必须为6位"
			}
		}
		if e.Field() == "ConfirmPassword" {
			switch e.Tag() {
			case "eqfield":
				return "两次输入密码必须一致"
			}
		}
	}
	return "参数错误"
}

// 修改转账密码
type ChangePasswordReq struct {
	OldPassword     string `form:"oldPassword" json:"oldPassword" binding:"required"`
	Password        string `form:"password" json:"password" binding:"required,gte=6,lte=100,passwordValid"`
	ConfirmPassword string `form:"confirmPassword" json:"confirmPassword" binding:"required,eqfield=Password"`
}

// 绑定模型获取验证错误的方法
func (req *ChangePasswordReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须大于等于为6位"
			case "lte":
				return "密码长度必须下于100位"
			}
		}
		if e.Field() == "ConfirmPassword" {
			switch e.Tag() {
			case "eqfield":
				return "两次输入密码必须一致"
			}
		}
	}
	return "参数错误"
}

// 修改转账密码
type EditNicknameReq struct {
	Nickname string `form:"nickname" json:"nickname" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *EditNicknameReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 修改转账密码
type EditSignatureReq struct {
	Signature string `form:"signature" json:"signature" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *EditSignatureReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 修改转账密码
type EditAvatarReq struct {
	Avatar string `form:"avatar" json:"avatar" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *EditAvatarReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 修改转账密码
type EditNotifyReq struct {
	Notify string `form:"notify" json:"notify" binding:"required"`
}

func (req *EditNotifyReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 修改转账密码
type ReferrerMoreReq struct {
	Role   int   `form:"role" json:"role" binding:"required"`
	Offset int64 `form:"offset" json:"offset" binding:"required"`
}

func (req *ReferrerMoreReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

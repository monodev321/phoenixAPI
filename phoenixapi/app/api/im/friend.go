package im

import (
	"strconv"

	"phoenix/internal/im/model"
	"phoenix/internal/im/service"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func FriendRequest(c *gin.Context) {
	var req FriendRequestReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	friendReq := &model.FriendRequest{
		ToUid:        req.ToUid,
		ToUsername:   req.ToUsername,
		ToNickname:   req.ToNickname,
		ToAvatar:     req.ToAvatar,
		FromUid:      req.FromUid,
		FromUsername: req.FromUsername,
		FromNickname: req.FromNickname,
		FromAvatar:   req.FromAvatar,
		AddType:      req.AddType,
		Comment:      req.Comment,
	}
	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	friendReq.FromUid = uid
	id, err := service.FriendService.AddFriendRequest(friendReq)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	if id == 0 {
		util.SuccessWithMsg(c, "你们已经是好友", 0)
		return
	}
	// user.Uid = uid
	util.SuccessWithMsg(c, "success", id)
	return
}

func HandleFriendRequest(c *gin.Context) {
	var req HandleFriendRequestReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	id := req.Id
	isPass := false
	if req.IsPass {
		isPass = req.IsPass
	}
	uid, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	err := service.FriendService.HandleFriendRequest(uid, id, isPass)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", 0)
	return
}

func FriendRequestList(c *gin.Context) {
	uid, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	freqs, err := service.FriendService.FriendRequestList(uid)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", freqs)
	return
}

func Friends(c *gin.Context) {
	uid, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	freqs, err := service.FriendService.FriendList(uid)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", freqs)
	return
}

func FriendByUid(c *gin.Context) {
	var req FriendByUidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	friend, err := service.FriendService.FriendByUid(req.Uid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", friend)
}

func FriendSetting(c *gin.Context) {
	var req FriendSettingReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.FriendService.Setting(req.Fid, req.Field, req.Val, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

func RecoverFriend(c *gin.Context) {
	var req FriendIdReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.FriendService.Recover(req.Fid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

func DeleteFriend(c *gin.Context) {
	var req FriendIdReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.FriendService.Delete(req.Fid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

func RecommendFriend(c *gin.Context) {
	var req RecommendFriendReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	// @todo 判断是不是是好友，允许推荐吗，这个安全性，防止随意推荐
	err = service.FriendService.RecommendFriend(req.Uids, req.Msg, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

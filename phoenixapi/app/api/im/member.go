package im

import (
	"strconv"

	"phoenix/internal/im/model"
	"phoenix/internal/im/service"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func RemoveGroupMembers(c *gin.Context) {
	var req removeGroupMembersReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.MemberService.RemoveGroupMembers(req.Uids, req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type removeGroupMembersReq struct {
	Uids []int64 `form:"uids" json:"uids" binding:"required"`
	Gid  int64   `form:"gid" json:"gid" binding:"required"`
}

func (req removeGroupMembersReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func ExitGroupMember(c *gin.Context) {
	var req exitGroupMemberReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	println(req.Gid)
	err = service.MemberService.ExitGroupMember(req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type exitGroupMemberReq struct {
	Gid int64 `form:"gid" json:"gid" binding:"required"`
}

func (req exitGroupMemberReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func AddGroupMembers(c *gin.Context) {
	var req addGroupMembersReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.MemberService.AddGroupMembers(req.Members, req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type addGroupMembersReq struct {
	Gid     int64                `form:"gid" json:"gid" binding:"required"`
	Members []*model.GroupMember `form:"members" json:"members" binding:"required"`
}

func (req addGroupMembersReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func JoinGroupMembers(c *gin.Context) {
	var req joinGroupMembersReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.MemberService.JoinGroupMembers(req.Members, req.Gid, req.Referrer, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type joinGroupMembersReq struct {
	Gid      int64                `form:"gid" json:"gid" binding:"required"`
	Referrer int64                `form:"referrer" json:"referrer" binding:"required"`
	Members  []*model.GroupMember `form:"members" json:"members" binding:"required"`
}

func (req joinGroupMembersReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func MuteMember(c *gin.Context) {
	var req muteMemberReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.MemberService.MuteMember(req.IsMute, req.Uid, req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type muteMemberReq struct {
	Gid    int64  `form:"gid" json:"gid" binding:"required"`
	IsMute string `form:"isMute" json:"isMute" binding:"required"`
	Uid    int64  `form:"uid" json:"uid" binding:"required"`
}

func (req muteMemberReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func MarknameMember(c *gin.Context) {
	var req marknameMemberReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.MemberService.MarknameMember(req.Markname, req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type marknameMemberReq struct {
	Gid      int64  `form:"gid" json:"gid" binding:"required"`
	Markname string `form:"markname" json:"markname" binding:"required"`
}

func (req marknameMemberReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func MemberSetting(c *gin.Context) {
	var req memberSettingReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.MemberService.MemberSetting(req.Gid, req.Field, req.Val, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type memberSettingReq struct {
	Gid   int64  `form:"gid" json:"gid" binding:"required"`
	Field string `form:"field" json:"field" binding:"required"`
	Val   string `form:"val" json:"val" binding:"required"`
}

func (req memberSettingReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func GroupMember(c *gin.Context) {
	var req groupMemberReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	member, err := service.MemberService.GroupMember(req.Gid, req.Uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", member)
}

type groupMemberReq struct {
	Gid int64 `form:"gid" json:"gid" binding:"required"`
	Uid int64 `form:"uid" json:"uid" binding:"required"`
}

func (req groupMemberReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

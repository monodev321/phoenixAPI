package im

import (
	"strconv"

	"phoenix/internal/im/model"
	"phoenix/internal/im/service"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func CreateGroup(c *gin.Context) {
	var req CreateGroupReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "获取用户账号失败"))
		return
	}

	group, err := service.GroupService.CreateGroup(req.Members, uid_i)
	if err != nil && group == nil {
		util.FailWithMsg(c, err)
		return
	}
	if err != nil && group != nil {
		util.SuccessWithMsg(c, "old", group)
		return
	}
	util.SuccessWithMsg(c, "success", group)
}

type CreateGroupReq struct {
	Members []*model.GroupMember `form:"members" json:"members" binding:"required"`
}

func (req CreateGroupReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func GroupByGid(c *gin.Context) {
	var req groupByGidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	group, err := service.GroupService.GroupByGid(req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", group)
}

func GroupByGid2(c *gin.Context) {
	var req groupByGidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	group, err := service.GroupService.GroupByGid2(req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", group)
}

func GroupMembersByGid(c *gin.Context) {
	var req groupByGidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	group, err := service.GroupService.GroupMembersByGid(req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", group)
}

func GroupMembersByGid2(c *gin.Context) {
	var req groupByGidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	group, err := service.GroupService.GroupMembersByGid2(req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", group)
}

func DismissGroup(c *gin.Context) {
	var req groupByGidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.GroupService.DismissGroup(req.Gid, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type groupByGidReq struct {
	Gid int64 `form:"gid" json:"gid" binding:"required"`
}

func (req groupByGidReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func GroupSetting(c *gin.Context) {
	var req groupSettingReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.GroupService.GroupSetting(req.Gid, req.Field, req.Val, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type groupSettingReq struct {
	Gid   int64  `form:"gid" json:"gid" binding:"required"`
	Field string `form:"field" json:"field" binding:"required"`
	Val   string `form:"val" json:"val" binding:"required"`
}

func (req groupSettingReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func AssiganGroup(c *gin.Context) {
	var req assiganGroupReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	err = service.GroupService.AssiganGroup(req.Gid, req.Owner, req.Nickname, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
}

type assiganGroupReq struct {
	Gid      int64  `form:"gid" json:"gid" binding:"required"`
	Owner    int64  `form:"owner" json:"owner" binding:"required"`
	Nickname string `form:"nickname" json:"nickname" binding:"required"`
}

func (req assiganGroupReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func UserGroups(c *gin.Context) {
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	gids, err := service.GroupService.UserGroups(uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", gids)
}

func UserGroups2(c *gin.Context) {
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	groups, err := service.GroupService.UserGroups2(uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", groups)
}

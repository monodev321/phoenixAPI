package im

import (
	"github.com/go-playground/validator/v10"
)

// 添加好友请求
type FriendRequestReq struct {
	ToUid        int64  `json:"to_uid" form:"to_uid" binding:"required"`                // 申请人
	ToUsername   string `json:"to_username"  form:"to_username" binding:"required"`     // 用户名
	ToNickname   string `json:"to_nickname"  form:"to_nickname" binding:"required"`     // 用户昵称
	ToAvatar     string `json:"to_avatar" form:"to_avatar" binding:"required"`          // 用户头像
	FromUid      int64  `json:"from_uid" form:"from_uid" `                              // 申请人
	FromUsername string `json:"from_username"  form:"from_username" binding:"required"` // 用户名
	FromNickname string `json:"from_nickname"  form:"from_nickname" binding:"required"` // 用户昵称
	FromAvatar   string `json:"from_avatar" form:"from_avatar" binding:"required"`      // 用户头像
	AddType      int    `json:"add_type" form:"add_type" binding:"required"`            // 添加方式
	Comment      string `json:"comment" form:"comment" binding:"required"`              // 申请备注
}

func (req *FriendRequestReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 处理好友请求
type HandleFriendRequestReq struct {
	Id     int64 `json:"id" form:"id" binding:"required"`    // 申请人
	IsPass bool  `json:"is_pass" form:"is_pass" binding:"-"` // 申请人
}

// 绑定模型获取验证错误的方法
func (req *HandleFriendRequestReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type FriendSettingReq struct {
	Fid   int64  `form:"fid" json:"fid" binding:"required"`
	Field string `form:"field" json:"field" binding:"required"`
	Val   string `form:"val" json:"val" binding:"required"`
}

func (req FriendSettingReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type FriendIdReq struct {
	Fid int64 `form:"fid" json:"fid" binding:"required"`
}

func (req FriendIdReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type FriendByUidReq struct {
	Uid int64 `form:"uid" json:"uid" binding:"required"`
}

func (req FriendByUidReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type RecommendFriendReq struct {
	Uids []int64 `form:"uids" json:"uids" binding:"required"`
	Msg  string  `form:"msg" json:"msg" binding:"required"`
}

func (req RecommendFriendReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

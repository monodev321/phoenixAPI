package im

import (
	"strconv"

	"phoenix/internal/im/service"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func GroupApplyList(c *gin.Context) {

	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "获取用户账号失败"))
		return
	}

	applys, err := service.GroupApplyService.GroupApplyList(uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", applys)
}

func GroupApplyDeal(c *gin.Context) {
	var req groupApplyDealReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "获取用户账号失败"))
		return
	}

	err = service.GroupApplyService.GroupApplyDeal(uid_i, req.Id, *req.Status)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", nil)
}

type groupApplyDealReq struct {
	Id     int64 `form:"id" json:"id" binding:"required"`
	Status *int  `form:"status" json:"status" binding:"required"`
}

func (req groupApplyDealReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

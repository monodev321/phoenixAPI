package im

import (
	"fmt"
	"strconv"

	"phoenix/internal/im/message"
	"phoenix/internal/im/service"
	"phoenix/internal/im/tools"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func Pull(c *gin.Context) {
	uid := c.MustGet("uid").(string)
	msgs, err := service.MessageService.Pull(uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	// user.Uid = uid
	util.SuccessWithMsg(c, "success", msgs)
	return
}

func Ack(c *gin.Context) {
	var req ackReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	if len(req.Ids) == 0 {
		util.FailWithMsg(c, werror.New(100, "msg id 不能为空"))
		return
	}
	err := service.MessageService.Ack(req.Ids, uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	// user.Uid = uid
	util.SuccessWithMsg(c, "success", 0)
	return
}

type ackReq struct {
	Ids []string `json:"ids"  form:"ids" binding:"required"` //消息ids
}

func (req *ackReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func Push(c *gin.Context) {
	var req pushReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, _ := strconv.ParseUint(uid, 0, 64)
	data, err := service.MessageService.Push(req.Msgs, uid_i)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	// user.Uid = uid
	util.SuccessWithMsg(c, "success", data)
}

type pushReq struct {
	Msgs []string `json:"msgs"  form:"msgs" binding:"required"` //消息ids
}

func (req *pushReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func PushSingleMsg(c *gin.Context) {
	var req pushSingleMsgReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "获取当前账户错误"))
		return
	}

	if req.ChatType == "single" {
		//发送单独聊消息
		msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		if err != nil {
			util.FailWithMsg(c, werror.New(100, "生成消息id错误"))
			return
		}
		message.SendSingleMassage(req.MsgType, req.Msg, uint64(uid_i), uint64(req.ToId), msgid2, 0)
	} else {
		message.SendGroupMassage(req.MsgType, req.Msg, uint64(uid_i), uint64(req.ToId), 0)

	}
	util.SuccessWithMsg(c, "success", nil)
	return
}

type pushSingleMsgReq struct {
	MsgType  string `form:"msgType" json:"msgType" binding:"required"`
	Msg      string `form:"msg" json:"msg" binding:"required"`
	ToId     int64  `form:"toId" json:"toId" binding:"required"`
	ChatType string `form:"chatType" json:"chatType" binding:"required"`
}

func (req *pushSingleMsgReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "参数错误"
}

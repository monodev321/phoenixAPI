package im

import (
	"errors"
	"fmt"
	"log"
	"phoenix/app/api"
	"phoenix/app/service"
	imService "phoenix/internal/im/service"

	userService "phoenix/internal/user/service"
	"strconv"

	"phoenix/app/types/proto"
	"phoenix/pkg/util"
	"unicode/utf8"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type SnsApi struct {
	user service.UserService
	sns  service.SnsService
	feed service.FeedService
}

// Follow godoc
// @BasePath /api/v1
// @tags SNS
// @summary 关注/取消关注
// @schemes http
// @description 关注/取消关注
// @param id body proto.AnyID true "{关注的用户ID, 是否是取消操作}"
// @accept json
// @produce json
// @response 200 {object} proto.util.FailWithMsg
// @router /sns/follow [post]
func Follow(c *gin.Context) {
	var err error
	p := proto.AnyID64{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	userId, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if userId == p.ID {
		api.ErrorMsg(c, api.ErrFlag, errors.New("不能关注自己"))
		return
	}

	err = imService.FollowService.Follow(userId, p.ID, p.Undo)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	api.RespJSON(c, nil, nil)
}

// RemoveFollower godoc
// @BasePath /api/v1
// @tags SNS
// @summary 移除粉丝;可以重新去加
// @schemes http
// @description 移除粉丝;可以重新去加
// @param id body integer true "用户ID"
// @accept json
// @produce json
// @response 200 {object} proto.util.FailWithMsg
// @router /sns/removeFollower [post]
func RemoveFollower(c *gin.Context) {
	var err error
	p := proto.AnyID64{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	userId, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if userId == p.ID {
		api.ErrorMsg(c, api.ErrFlag, errors.New("不能关注自己"))
		return
	}

	err = imService.FollowService.Follow(userId, p.ID, true)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	api.RespJSON(c, nil, nil)
}

// Profile godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 查看个人主页
// @schemes http
// @description 查看个人主页
// @param id body proto.AnyID true "用户ID"
// @accept json
// @produce json
// @response 200 {object} proto.UserProfile
// @router /sns/profile [post]
func (api SnsApi) Profile(c *gin.Context) {
	var err error
	p := proto.AnyID{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId := c.GetUint("userId")
	user, err := api.user.Get(p.ID)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	relation := api.sns.UserRelation(userId, p.ID)
	resp := proto.UserProfile{
		User:     user,
		Relation: relation,
	}
	selfFeed, err := api.feed.FeedModel.GetAll(p.ID, 0, 10)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	resp.SelfFeed = selfFeed
	likeFeed, err := api.feed.GetUserLike(p.ID, 0, 10)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	resp.LikeFeed = likeFeed

	//when view self, list secret feeds
	if p.ID == userId {
		secFeed, err := api.feed.FeedModel.GetSecret(p.ID, 0, 10)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
		resp.SecretFeed = secFeed
	}
	util.SuccessWithMsg(c, resp, nil)
}

// UserFeedList godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 用户作品列表
// @schemes http
// @description 用户作品列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/userFeed/list [post]
func (api SnsApi) UserFeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}
	p.Fixed()

	resp, err := api.feed.GetUserAll(p.ID, p.Offset(), p.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, resp)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, resp)
		if err != nil {
			log.Println(err)
		}
	}
	util.SuccessWithMsg(c, resp, p)
}

// FriendFeedList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 好友短视频列表
// @schemes http
// @description 好友短视频列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/friend/feed [post]
func FriendFeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	p.Fixed()
	userId := p.ID
	resp, err := imService.FollowService.GetFriendNewest(userId, p.Limit, p.Offset())
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	var uids = make([]int64, 0, len(*resp))
	for k := range *resp {
		uids = append(uids, (*resp)[k].UserID)
	}
	userMap, err := userService.UserService.GetUsersBaseInfo(uids)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	var list []*proto.UserFeed1
	list = make([]*proto.UserFeed1, 0, len(*resp))
	for k, v := range *resp {
		uv := &proto.UserFeed1{
			Feed: v, UserBaseInfo: *userMap[k],
		}
		list = append(list, uv)
	}
	selfId, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if selfId > 0 {
		err = imService.FollowService.FillUserStared(selfId, list)
		if err != nil {
			log.Println(err)
		}
		err = imService.FollowService.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	api.RespJSON(c, list, p)
}

// LikeFeedList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户喜欢列表
// @schemes http
// @description 用户喜欢列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/userFeed/like [post]
func LikeFeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	p.Fixed()
	userId := p.ID
	// resp, err := api.feed.GetUserLike(userId, p.Offset(), p.Limit)
	start := int64(p.Offset())
	stop := int64(p.Offset() + p.Limit)
	data, err := imService.FollowService.GetUserLike(userId, start, stop)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	var uids = make([]int64, 0, len(data))
	for k := range data {
		uids = append(uids, (data)[k].UserID)
	}
	userMap, err := userService.UserService.GetUsersBaseInfo(uids)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	var list []*proto.UserFeed1
	list = make([]*proto.UserFeed1, 0, len(data))
	for k, v := range data {
		uf := &proto.UserFeed1{
			Feed:         *v,
			UserBaseInfo: *userMap[k],
			Liked:        true,
		}
		list = append(list, uf)
	}
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	selfId, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if selfId > 0 {
		err = imService.FollowService.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	api.RespJSON(c, list, p)
}

// Remark godoc
// @BasePath /api/v1
// @tags SNS
// @summary 对关注的人进行备注
// @schemes http
// @description 对关注的人进行备注
// @param id body integer true "关系ID"
// @param val body string true "备注名"
// @accept json
// @produce json
// @response 200 {object} types.SnsRelation
// @router /sns/remark [post]
func Remark(c *gin.Context) {
	var err error
	p := proto.AnyVal{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	if utf8.RuneCountInString(p.Val) > 50 {
		api.ErrorMsg(c, api.ErrFlag, nil)
		return
	}
	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	err = imService.FriendService.Setting(int64(p.ID), "friend_remarkname", p.Val, uid)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	rel, err := imService.FriendService.FriendByUid(int64(p.ID), uid)
	rel.FriendNickname = p.Val
	api.RespJSON(c, rel, nil)
}

// Setting godoc
// @BasePath /api/v1
// @tags SNS
// @summary 设置好友权限等操作
// @schemes http
// @description 设置好友权限等操作,可设置的值有:friend, close_friend, is_top, see_ta, see_me
// @param id body proto.SnsSetting true "setting"
// @accept json
// @produce json
// @response 200 {object} types.SnsRelation
// @router /sns/setting [post]
func Setting(c *gin.Context) {
	var err error
	p := proto.SnsSetting{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	if len(p.Data) == 0 {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)

	rel, err := imService.FriendService.FriendByUid(int64(p.ID), uid)

	sql := ""
	for k, v := range p.Data {
		switch k {
		case "friend":
			if v {
				sql = sql + fmt.Sprintf("`%s` = %d ", "relation", 1)
				rel.Relation = 1
			} else {
				sql = sql + fmt.Sprintf("`%s` = %d ", "relation", 0)
				rel.Relation = 0
			}
		case "close_friend":
			if rel.Relation > 0 {
				if v {
					sql = sql + fmt.Sprintf("`%s` = %d ", "relation", 2)
					rel.Relation = 2
				} else {
					sql = sql + fmt.Sprintf("`%s` = %d ", "relation", 1)
					rel.Relation = 1
				}
			}
		case "is_top":
			val := 0
			if v {
				val = 1
			}
			sql = sql + fmt.Sprintf("`%s` = %d ", "is_top", val)
			rel.IsTop = val
		case "see_ta":
			val := 0
			if v {
				val = 1
			}
			sql = sql + fmt.Sprintf("`%s` = %d ", "is_watch", val)
			rel.IsWatch = val
		case "see_me":
			val := 0
			if v {
				val = 1
			}
			sql = sql + fmt.Sprintf("`%s` = %d ", "is_allow_watch", val)
			rel.IsAllowWatch = val
		}
	}
	err = imService.FollowService.Settings(int64(p.ID), sql, uid)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	api.RespJSON(c, rel, nil)
}

// FollowerList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户粉丝列表
// @schemes http
// @description 用户粉丝列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.SnsUserListResp
// @router /sns/follower/list [post]
func FollowerList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	p.Fixed()
	userId := p.ID
	list, err := imService.FollowService.FollowerList(userId, p.Limit, p.Offset())

	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	var resp []*proto.SnsUser1
	if len(*list) > 0 {
		resp = make([]*proto.SnsUser1, 0, len(*list))

		uids := make([]int64, 0, len(*list))
		for k := range *list {
			uids = append(uids, (*list)[k].UserID)
		}
		userMap, err := userService.UserService.GetUsersBaseInfo(uids)
		if err != nil {
			api.ErrorMsg(c, api.ErrFlag, err)
			return
		}

		for k, v := range *list {
			su := &proto.SnsUser1{
				Relation: v,
				User:     *userMap[k],
			}
			resp = append(resp, su)
		}
	}
	api.RespJSON(c, resp, p)
}

// FollowList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户关注列表
// @schemes http
// @description 用户关注列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.SnsUserListResp
// @router /sns/follow/list [post]
func FollowList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	p.Fixed()
	userId := p.ID
	list, err := imService.FollowService.FollowList(userId, p.Limit, p.Offset())

	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	var resp []*proto.SnsUser1
	if len(*list) > 0 {
		resp = make([]*proto.SnsUser1, 0, len(*list))

		fids := make([]int64, 0, len(*list))
		for k := range *list {
			fids = append(fids, (*list)[k].FollowID)
		}
		userMap, err := userService.UserService.GetUsersBaseInfo(fids)
		if err != nil {
			api.ErrorMsg(c, api.ErrFlag, err)
			return
		}

		for k, v := range *list {
			su := &proto.SnsUser1{
				Relation: v,
				User:     *userMap[k],
			}
			resp = append(resp, su)
		}
	}
	api.RespJSON(c, resp, p)
}

// FollowFeed godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户关注的人的视频列表
// @schemes http
// @description 用户关注的人的视频列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/follow/feed [post]
func FollowFeed(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	p.Fixed()
	selfId, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	data, err := imService.FollowService.GetFollowNewest(selfId, p.Limit, p.Offset())
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	var uids = make([]int64, 0, len(*data))
	for k := range *data {
		uids = append(uids, (*data)[k].UserID)
	}
	userMap, err := userService.UserService.GetUsersBaseInfo(uids)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	var list []*proto.UserFeed1
	list = make([]*proto.UserFeed1, 0, len(*data))
	for k, v := range *data {
		uv := &proto.UserFeed1{
			Feed: v, UserBaseInfo: *userMap[k],
		}
		list = append(list, uv)
	}
	if selfId > 0 {
		err = imService.FollowService.FillUserStared(selfId, list)
		if err != nil {
			log.Println(err)
		}
		err = imService.FollowService.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	api.RespJSON(c, list, p)
}

// FriendList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户朋友列表
// @schemes http
// @description 用户朋友列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.SnsUserListResp
// @router /sns/friend/list [post]
func (api SnsApi) FriendList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}
	p.Fixed()
	userId := p.ID
	list, err := api.sns.GetUserFriend(userId, p.Offset(), p.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	var resp []*proto.SnsUser
	if len(list) > 0 {
		resp = make([]*proto.SnsUser, 0, len(list))
		fids := make([]uint, 0, len(list))
		for k := range list {
			fids = append(fids, list[k].FollowID)
		}
		userMap, err := api.user.GetUsersBaseInfo(fids)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
		for k, v := range list {
			su := &proto.SnsUser{
				Relation: list[k],
				User:     userMap[v.FollowID],
			}
			resp = append(resp, su)
		}
	}
	util.SuccessWithMsg(c, resp, p)
}

// SearchFollow godoc
// @BasePath /api/v1
// @tags SNS
// @summary 搜索关注的人
// @schemes http
// @description 搜索关注的人(需要登录)
// @param req body proto.SearchReq true "SearchReq"
// @accept json
// @produce json
// @response 200 {object} proto.UserSearchResp
// @router /sns/search/follow [post]
func SearchFollow(c *gin.Context) {
	var err error
	p := &proto.SearchReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	p.Fixed()
	userId := p.ID
	keywd := p.Keyword
	// keywd = "%" + keywd + "%"
	list, err := imService.FollowService.SearchUserFollow(userId, keywd, p.Offset(), p.Limit)
	if err != nil {
		api.ErrorMsg(c, api.ErrFlag, err)
		return
	}
	var resp []*proto.SnsUser1
	if len(*list) > 0 {
		resp = make([]*proto.SnsUser1, 0, len(*list))
		fids := make([]int64, 0, len(*list))
		for k := range *list {
			fids = append(fids, (*list)[k].FollowID)
		}
		log.Printf("%#v", (*list)[0])
		userMap, err := userService.UserService.GetUsersBaseInfo(fids)
		if err != nil {
			api.ErrorMsg(c, api.ErrFlag, err)
			return
		}
		for k, v := range *list {
			su := &proto.SnsUser1{
				Relation: v,
				User:     *userMap[k],
			}
			resp = append(resp, su)
		}
	}

	api.RespJSON(c, gin.H{"data": resp, "keyword": keywd}, &p.Paginate)
}

// SearchUser godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 搜索用户名
// @schemes http
// @description 搜索用户名
// @param req body proto.SearchReq true "SearchReq"
// @accept json
// @produce json
// @response 200 {object} proto.UserSearchResp
// @router /sns/search/user [post]
func (api SnsApi) SearchUser(c *gin.Context) {
	var err error
	p := &proto.SearchReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}
	p.Fixed()
	q := p.Keyword
	list, err := api.user.SearchUser(q, p.Offset(), p.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, gin.H{"data": list, "keyword": q}, &p.Paginate)
}

// SearchFeed godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 搜索短视频
// @schemes http
// @description 搜索短视频
// @param req body proto.SearchReq true "SearchReq"
// @accept json
// @produce json
// @response 200 {object} proto.FeedSearchResp
// @router /sns/search/feed [post]
func (api SnsApi) SearchFeed(c *gin.Context) {
	var err error
	p := &proto.SearchReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}
	p.Fixed()
	q := p.Keyword
	list, err := api.feed.SearchFeed(q, p.Offset(), p.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, list)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	util.SuccessWithMsg(c, gin.H{"data": list, "keyword": q}, &p.Paginate)
}

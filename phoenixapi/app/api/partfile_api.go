package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"log"
	"os"
	"path"
	"path/filepath"
	"phoenix/app/service"
	"phoenix/app/types/proto"
	"phoenix/pkg/config"
	"strconv"
	"strings"
)

type PartFileApi struct {
	file service.PartFileService
}

// PartUpload godoc
// @BasePath /api/v1
// @tags PartFile
// @summary 分片上传
// @schemes http
// @description 分片上传
// @param uuid query string true "分片UUID"
// @param part_num formData string true "当前分片编号"
// @param part_hash formData string true "当前分片的md5"
// @param file formData file true "文件"
// @accept mpfd
// @produce json
// @response 200 {object} any || proto.ErrorMsg
// @router /partFile/upload/:uuid [post]
func (api PartFileApi) PartUpload(c *gin.Context) {
	var err error
	numStr := c.PostForm("part_num")
	partNum, _ := strconv.Atoi(numStr)
	partHash := c.PostForm("part_hash")
	file, err := c.FormFile("file")
	uuid := c.Param("uuid")
	if uuid == "" || err != nil {
		errorMsg(c, errReqArgs, "uuid", uuid, "part_num", numStr, "part_hash", partHash, err)
		return
	}
	userId := c.GetUint("userId")
	uniqFile, err := api.file.FindUpload(uuid)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	if partNum > int(uniqFile.PartNum) {
		errorMsg(c, errReqArgs, "part_num:", partNum, uniqFile.PartNum)
		return
	}

	meta := uniqFile.GetPart(uint(partNum))
	if meta == nil {
		errorMsg(c, errReqArgs, "meta file not found")
		return
	}
	if meta.Size != uint(file.Size) {
		errorMsg(c, errReqArgs, "size:", meta.Size, uint(file.Size))
		return
	}
	log.Println("Upload partFile: ", file.Filename, file.Size, file.Header)
	ext := strings.ToLower(path.Ext(uniqFile.FileName))
	// 这个目录只放还没合并完成的: partFile/<userid>/<hash>.part_001
	fileUri := fmt.Sprintf("partFile/%d/%s.part_%03d", userId, uniqFile.Hash, partNum)
	saveFile, err := filepath.Abs(path.Join(config.AppCfg.App.ResourceDir, fileUri))
	if err != nil {
		errorMsg(c, errSaveUploadFile, err)
		return
	}
	switch ext {
	case ".mp4", ".mov", ".avi", ".wmv":
		os.MkdirAll(filepath.Dir(saveFile), 0750)
		err = c.SaveUploadedFile(file, saveFile)
		if err != nil {
			errorMsg(c, errSaveUploadFile, err, saveFile)
			return
		}
	default:
		errorMsg(c, errImageExtNotSupport, ext)
		return
	}
	meta.Hash = partHash
	err = api.file.MetaUpdate(uniqFile, meta)
	if err != nil {
		errorMsg(c, errPartFileUpload, err)
		return
	}
	err = api.file.MergeCheck(uniqFile)
	log.Println("merge check: ", err)
	respJSON(c, uniqFile, nil)
}

// PartUploadQuery godoc
// @BasePath /api/v1
// @tags PartFile
// @summary 查询分片
// @schemes http
// @description 查询分片
// @accept json
// @produce json
// @response 200 {object} types.PartFile
// @router /partFile/info/:uuid [get]
func (api PartFileApi) PartUploadQuery(c *gin.Context) {
	hash := c.Param("uuid")
	file, err := api.file.FindUpload(hash)
	if err != nil {
		errorMsg(c, errPartFileNotFound, err, hash)
		return
	}
	respJSON(c, file, nil)
}

// PartCreate godoc
// @BasePath /api/v1
// @tags PartFile
// @summary 创建分片上传
// @schemes http
// @description 创建分片上传
// @param req body proto.PartFileCreate true "req"
// @accept json
// @produce json
// @response 200 {object} types.PartFile
// @router /partFile/create/ [post]
func (api PartFileApi) PartCreate(c *gin.Context) {
	var err error
	p := proto.PartFileCreate{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.GetUint("userId")
	file, err := api.file.CreateUpload(userId, &p)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, file, nil)
}

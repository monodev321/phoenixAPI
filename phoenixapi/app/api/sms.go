package api

import (
	"fmt"
	"strings"

	"phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"phoenix/pkg/mail"

	"github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"go.biyong.com/telesign/send"
)

func SendSms(c *gin.Context) {
	var req sendSmsReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		// util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		errorMsg(c, errFlag, req.GetError(err.(validator.ValidationErrors)))
		return
	}
	tel := strings.Trim(req.Tel, " ")

	code, err := service.SmsService.AddCode(tel, "registerTel")
	if err != nil {
		errorMsg(c, errFlag, err)
		return
	}
	mssgTem, _ := i18n.GetMessage("smsTemplate")
	mssg := fmt.Sprintf(mssgTem, code)
	// mssg := fmt.Sprintf("您的币用验证码为:%s,将在30分钟后失效[BiYong]", code)
	println(mssg)
	err = send.SendMessage(tel, mssg, config.AppCfg.TelesignID, config.AppCfg.TelesignSecretKey)

	if err != nil {
		errorMsg(c, errFlag, err)
	} else {
		respJSON(c, nil, nil)
	}
}

type sendSmsReq struct {
	Tel string `json:"tel" binding:"required,number"`
}

func (req *sendSmsReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "手机号码不正确"
}

func SendEmailCode(c *gin.Context) {
	// number := strings.Trim(req.Password, " ")
	var req sendEmailCodeReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	email := strings.Trim(req.Email, " ")

	code, err := service.SmsService.AddCode(email, "findpasswordEmail")
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "发送失败,请重试"))
		return
	}
	mssg := fmt.Sprintf("您的币用验证码为:%s,将在30分钟后失效[BiYong]", code)
	// println(mssg)
	go mail.UserCode(mssg, email)

	if err != nil {
		util.FailWithMsg(c, err)
	} else {
		util.SuccessWithMsg(c, "register success", nil)
	}
}

type sendEmailCodeReq struct {
	Email string `json:"email" binding:"required"`
}

func (req *sendEmailCodeReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "参数错误"
}

func SendEmailActive(c *gin.Context) {

	var req sendEmailActiveReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	email := strings.Trim(req.Email, " ")
	err := service.SmsService.SendEmailActive(email)
	if err != nil {
		util.FailWithMsg(c, err)
	} else {
		util.SuccessWithMsg(c, "register success", nil)
	}
}

type sendEmailActiveReq struct {
	Email string `json:"email" binding:"required"`
}

func (req *sendEmailActiveReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "参数错误"
}

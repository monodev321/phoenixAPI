package api

import (
	"net/http"
	service "phoenix/app/service/film"
	"strconv"

	"github.com/gin-gonic/gin"
)

type FilmApi struct {
	film service.FilmService
}

func (api FilmApi) Films(c *gin.Context) {
	json := make(map[string]interface{}) //注意该结构接受的内容
	c.BindJSON(&json)
	offset := int(json["offset"].(float64))
	films, count, err := api.film.Films(offset)
	println(count)
	if err != nil {
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": films, "count": count})
}

func (api FilmApi) FrontFilms(c *gin.Context) {
	json := make(map[string]interface{}) //注意该结构接受的内容
	c.BindJSON(&json)
	offset := int(json["offset"].(float64))
	kindId := int(json["kindId"].(float64))
	plotId := json["plotId"].(string)
	regionId := int(json["regionId"].(float64))
	languageId := int(json["languageId"].(float64))
	timeId := int(json["timeId"].(float64))
	films, count, err := api.film.FrontFilms(offset, kindId, plotId, regionId, languageId, timeId)
	println(count)
	if err != nil {
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": films, "count": count})
}

func (api FilmApi) FilmEdit(c *gin.Context) {
	json := make(map[string]interface{}) //注意该结构接受的内容
	c.BindJSON(&json)
	err := api.film.FilmEdit(json)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"code": 0, "message": "success", "data": 1})
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": 1})
}

func (api FilmApi) FilmAdd(c *gin.Context) {
	json := make(map[string]interface{}) //注意该结构接受的内容
	c.BindJSON(&json)
	err := api.film.FilmAdd(json)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"code": 0, "message": "success", "data": 1})
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": 1})
}

func (api FilmApi) Film(c *gin.Context) {
	// json := make(map[string]interface{}) //注意该结构接受的内容
	// c.BindJSON(&json)
	// fmt.Printf("%+v", json)
	// id := int(json["id"].(int))
	id, err := strconv.Atoi(c.Query("id"))
	film, err := api.film.FilmById(id)
	if err != nil {
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": film})
}

func (api FilmApi) FilmChangeField(c *gin.Context) {
	json := make(map[string]interface{}) //注意该结构接受的内容
	c.BindJSON(&json)
	err := api.film.FilmChangeField(json)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"code": 0, "message": "success", "data": 1})
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": 1})
}

func (api FilmApi) FilmDelete(c *gin.Context) {
	json := make(map[string]interface{}) //注意该结构接受的内容
	c.BindJSON(&json)
	id := int(json["id"].(float64))
	// id, err := strconv.Atoi(c.Query("id"))
	err := api.film.DeleteFilm(id)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{"code": 0, "message": "删除失败", "data": 1})
		return
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success"})
}

func (api FilmApi) UserInfo(c *gin.Context) {
	data := map[string]interface{}{
		"uid": 1,
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": data})
}

func (api FilmApi) Refresh(c *gin.Context) {
	data := map[string]interface{}{
		"role":       "admin",
		"access":     "78789798",
		"createTime": 1664898994,
		"expiredAt":  2664898994,
		"refresh":    "MDZHYZHIZWITNDQYYI01ZWQ1LWIZM2QTZWVIM2E0NMM1MWNH",
		"scope":      "dapp",
		"tokenType":  "Bearer",
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": data})
}

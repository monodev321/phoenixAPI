package admin

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"phoenix/app/api"
	"phoenix/internal/admin/model"
	"phoenix/internal/admin/service"
	userservice "phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func BlogAdd(c *gin.Context) {
	var req BlogReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	userId, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	userInfo, _ := userservice.UserService.GetUserInfo(userId)
	newblog := &model.Blog{
		ID:          0,
		Title:       req.Title,
		Content:     req.Content,
		CoverUrl:    req.CoverUrl,
		UserID:      userId,
		Nickname:    userInfo.Nickname,
		Category:    req.Category,
		Language:    req.Language,
		ViewsNum:    0,
		IsTop:       0,
		IsRecommend: 0,
		Sort:        0,
		IsStatus:    0,
		CreatedAt:   0,
		UpdatedAt:   0,
	}
	blog, err := service.BlogService.Add(newblog)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", blog)
}

func BlogList(c *gin.Context) {
	var req BlogListReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	blogs, total, count, err := service.BlogService.BlogList(req.Category, req.Offset(), req.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"total_num": total,
		"count":     count,
		"blogs":     blogs,
	}
	util.SuccessWithMsg(c, "success", data)
}

func Upload1(c *gin.Context) {
	var err error
	// imgfname, _ := c.GetPostForm("imgname")
	imgfile, err := c.FormFile("imgfile")
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)

	log.Println("Upload image: ", imgfile.Filename, imgfile.Size, imgfile.Header)
	ext1 := strings.ToLower(path.Ext(imgfile.Filename))
	imgFileUrl := path.Join(config.AppCfg.App.ResourceDir, path.Join("blogs", strconv.Itoa(int(userId)), imgfile.Filename))
	fmt.Println(imgFileUrl)
	saveImgFile, err := filepath.Abs(imgFileUrl)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	switch ext1 {
	case ".jpg", ".png":
		os.MkdirAll(filepath.Dir(saveImgFile), 0750)
		err = c.SaveUploadedFile(imgfile, saveImgFile)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
	default:
		api.ErrorMsg(c, api.ErrFlag, errors.New("图片格式不支持"))
		return
	}
	api.RespJSON(c, gin.H{"cover_url": path.Join("/", imgFileUrl)}, nil)
}

func BlogInfo(c *gin.Context) {
	var err error
	var req IdReq
	if err = c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	Id := int64(req.ID)
	blogInfo, err := service.BlogService.GetBlogInfo(Id)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId := blogInfo.UserID
	userInfo, err := userservice.UserService.GetBaseInfo(userId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	resp := &model.BlogInfo{
		Blog:         blogInfo,
		UserBaseInfo: userInfo,
	}
	util.SuccessWithMsg(c, "success", resp)
}

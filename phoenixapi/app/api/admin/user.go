package admin

import (
	// "context"
	// "errors"

	// "strconv"
	// "strings"

	// "time"

	// "github.com/onsi/ginkgo/config"

	// "phoenix/internal/user/model"
	"phoenix/app/api"
	// "phoenix/internal/admin/model"
	adminservice "phoenix/internal/admin/service"
	// "phoenix/internal/user/service"
	// "phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	// "github.com/go-oauth2/oauth2/v4/store"
	"github.com/go-playground/validator/v10"
)

func UserList(c *gin.Context) {
	var req UserListInfo
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	status := req.Status
	username := req.Username
	offset := req.Offset()
	limit := req.Limit
	userlist, count, err := adminservice.UserService.UserList(status, username, offset, limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"users": userlist,
		"count": count,
	}
	api.RespJSON(c, data, nil)
	return
}

func AdministratorAdd(c *gin.Context) {
	var req GetUserByUidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	err := adminservice.UserService.AdministratorAdd(req.Uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	api.RespJSON(c, nil, nil)
}

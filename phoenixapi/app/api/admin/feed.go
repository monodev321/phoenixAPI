package admin

import (
	"errors"
	"log"
	"os"
	"path"
	"path/filepath"
	"phoenix/pkg/hash"
	"phoenix/pkg/werror"
	"time"

	// "phoenix/app/service"
	"phoenix/app/api"
	// "phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/internal/admin/model"
	"phoenix/internal/admin/service"
	userservice "phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func Upload(c *gin.Context) {
	var err error
	file, err := c.FormFile("file")
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)

	log.Println("Upload feed: ", file.Filename, file.Size, file.Header)
	ext := strings.ToLower(path.Ext(file.Filename))
	dirUri := path.Join("upload/feed", getNowYMD()+"/"+strconv.Itoa(int(userId)), hash.Md5(file.Filename)+ext)
	fileUri := path.Join(config.AppCfg.App.FilmDir, dirUri)
	saveFile, err := filepath.Abs(fileUri)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	switch ext {
	case ".mp4", ".mov", ".avi", ".wmv":
		os.MkdirAll(filepath.Dir(saveFile), 0750)
		err = c.SaveUploadedFile(file, saveFile)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
	default:
		api.ErrorMsg(c, api.ErrFlag, errors.New("视频格式不支持"))
		return
	}
	api.RespJSON(c, gin.H{"url": path.Join("/", dirUri)}, nil)
}

func getNowYMD() string {
	y := strconv.Itoa(time.Now().Year())  //年
	m := int2Str(int(time.Now().Month())) //月
	d := int2Str(time.Now().Day())
	return y + m + d
}
func int2Str(i int) string {
	s := strconv.Itoa(i)
	if i < 10 {
		return "0" + s
	}
	return s
}

func UploadImg(c *gin.Context) {
	var err error
	imgfname, _ := c.GetPostForm("imgname")
	imgfile, err := c.FormFile("imgfile")
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)

	log.Println("Upload image: ", imgfile.Filename, imgfile.Size, imgfile.Header)
	ext1 := strings.ToLower(path.Ext(imgfile.Filename))
	imgFileUrl := path.Join(config.AppCfg.App.ResourceDir, path.Join("feeds", strconv.Itoa(int(userId)), imgfname))
	saveImgFile, err := filepath.Abs(imgFileUrl)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	switch ext1 {
	case ".jpg", ".png":
		os.MkdirAll(filepath.Dir(saveImgFile), 0750)
		err = c.SaveUploadedFile(imgfile, saveImgFile)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
	default:
		api.ErrorMsg(c, api.ErrFlag, errors.New("图片格式不支持"))
		return
	}
	api.RespJSON(c, gin.H{"img_url": path.Join("/", imgFileUrl)}, nil)
}

// Create Feed
func FeedCreate(c *gin.Context) {
	var req FeedReq
	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	userId, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	userInfo, _ := userservice.UserService.GetUserInfo(userId)

	newFeed := model.Feed{
		UserID:   userId,
		Visible:  req.Visible,
		Cate:     req.Cate,
		Tag:      req.Tag,
		Location: req.Location,
		Nickname: userInfo.Nickname,
		Intro:    req.Intro,
		FeedUrl:  req.FeedUrl,
		CoverUrl: req.CoverUrl,
	}
	err := service.FeedService.Create(newFeed)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	resp := newFeed
	util.SuccessWithMsg(c, "success", resp)
}

// GET FeedList
func FeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}
	p.Fixed()
	//userId := c.MustGet("userId").(uint)
	feeds, total, err := service.FeedService.GetList(p.Offset(), p.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"total_num": total,
		"feeds":     feeds,
	}
	util.SuccessWithMsg(c, "success", data)
}

// GET FeedList By Uid

func FeedInfo(c *gin.Context) {
	var err error
	var req IdReq
	if err = c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	Id := int64(req.ID)
	feedInfo, err := service.FeedService.GetFeedInfo(Id)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId := feedInfo.UserID
	userInfo, err := userservice.UserService.GetBaseInfo(userId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	resp := &model.FeedInfo{
		Feed:         feedInfo,
		UserBaseInfo: userInfo,
	}
	util.SuccessWithMsg(c, "success", resp)
}

// Delete Feed
func FeedDelete(c *gin.Context) {
	var req IdReq
	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	Id := int64(req.ID)
	err := service.FeedService.DeleteFeedById(Id)

	if err != nil {
		util.FailWithMsg(c, werror.New(100, err.Error()))
		return
	}

	util.SuccessWithMsg(c, "success", nil)
}

// Edit Feed
func FeedEdit(c *gin.Context) {
	var req FeedReq
	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	body := &model.Feed{
		ID:        req.ID,
		Visible:   req.Visible,
		Cate:      req.Cate,
		Tag:       req.Tag,
		Location:  req.Location,
		Intro:     req.Intro,
		FeedUrl:   req.FeedUrl,
		CoverUrl:  req.CoverUrl,
		UpdatedAt: time.Now().Unix(),
	}

	feed, err := service.FeedService.EditFeedById(body)

	if err != nil {
		util.FailWithMsg(c, werror.New(100, err.Error()))
		return
	}

	util.SuccessWithMsg(c, "success", feed)
}

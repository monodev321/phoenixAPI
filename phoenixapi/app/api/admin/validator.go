package admin

import (
	"github.com/go-playground/validator/v10"
)

type LoginReq struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required,gte=6,lte=100,passwordValid"`
}

// 绑定模型获取验证错误的方法
func (req *LoginReq) GetError(err validator.ValidationErrors) string {
	for _, e := range err {
		if e.Field() == "Username" {
			switch e.Tag() {
			case "required":
				return "请输入您的注册账户"
			case "email":
				return "注册账号必须为邮件账号"
			}
		}

		if e.Field() == "Password" {
			switch e.Tag() {
			case "required":
				return "请输入密码"
			case "gte":
				return "密码长度必须大于6位"
			case "lte":
				return "密码长度必须小于100位"
			case "passwordValid":
				return "密码必须包含字母和数字"
			}
		}
	}
	return "参数错误"
}

type GetUserByUidReq struct {
	Uid int64 `form:"uid" json:"uid" binding:"required"`
}

// 绑定模型获取验证错误的方法
func (req *GetUserByUidReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type UserListInfo struct {
	Username string `form:"username" json:"username"`
	Status   int    `form:"status" json:"status" binding:"required"`
	PageNum  int    `form:"pagenum" json:"pagenum"`
	Limit    int    `form:"limit" json:"limit"`
}

const (
	defaultPageSize = 20
)

func (p *UserListInfo) Fixed() {
	if p.PageNum <= 0 {
		p.PageNum = 1
	}
	if p.Limit <= 0 || p.Limit >= 20 {
		p.Limit = defaultPageSize
	}
}
func (p *UserListInfo) Offset() int64 {
	return int64((p.PageNum - 1) * p.Limit)
}
func (req *UserListInfo) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type BlogReq struct {
	Title    string `form:"title" json:"title" binding:"required"`
	Content  string `form:"content" json:"content" binding:"required"`
	CoverUrl string `form:"coverurl" json:"coverurl"`
	Category string `form:"category" json:"category" binding:"required"`
	Language string `form:"language" json:"language"`
}

func (req *BlogReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type BlogListReq struct {
	Category string `form:"category" json:"category"`
	PageNum  int    `form:"pagenum" json:"pagenum"`
	Limit    int    `form:"limit" json:"limit"`
}

func (p *BlogListReq) Fixed() {
	if p.PageNum <= 0 {
		p.PageNum = 1
	}
	if p.Limit <= 0 || p.Limit >= 20 {
		p.Limit = defaultPageSize
	}
}
func (p *BlogListReq) Offset() int64 {
	return int64((p.PageNum - 1) * p.Limit)
}
func (req *BlogListReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type IdReq struct {
	ID int64 `form:"id" json:"id" binding:"required"`
}

func (req IdReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type FeedReq struct {
	ID       int64  `json:"id"`
	Visible  uint8  `form:"visible" json:"visible"` //public = 0, friend = 1, self = 2
	Cate     string `form:"cate" json:"cate" binding:"required"`
	Tag      string `form:"tag" json:"tag"`
	Location string `form:"location" json:"location"`
	Intro    string `form:"intro" json:"intro"`
	FeedUrl  string `form:"feedurl" json:"feedurl" binding:"required"`
	CoverUrl string `form:"coverurl"json:"coverurl" binding:"required"`
}

func (req *FeedReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

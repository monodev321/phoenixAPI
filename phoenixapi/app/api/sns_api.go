package api

import (
	"log"
	"phoenix/app/service"
	"phoenix/app/types/proto"
	"unicode/utf8"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type SnsApi struct {
	user service.UserService
	sns  service.SnsService
	feed service.FeedService
}

// Follow godoc
// @BasePath /api/v1
// @tags SNS
// @summary 关注/取消关注
// @schemes http
// @description 关注/取消关注
// @param id body proto.AnyID true "{关注的用户ID, 是否是取消操作}"
// @accept json
// @produce json
// @response 200 {object} proto.ErrorMsg
// @router /sns/follow [post]
func (api SnsApi) Follow(c *gin.Context) {
	var err error
	p := proto.AnyID{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	if userId == p.ID {
		return
	}
	_, err = api.user.Get(p.ID)
	if err != nil {
		errorMsg(c, errUserNotFound, err)
		return
	}

	err = api.sns.FollowUser(userId, p.ID, p.Undo)
	if err != nil {
		errorMsg(c, errOperationFailed, err)
		return
	}
	respJSON(c, nil, nil)
}

// RemoveFollower godoc
// @BasePath /api/v1
// @tags SNS
// @summary 移除粉丝;可以重新去加
// @schemes http
// @description 移除粉丝;可以重新去加
// @param id body integer true "用户ID"
// @accept json
// @produce json
// @response 200 {object} proto.ErrorMsg
// @router /sns/removeFollower [post]
func (api SnsApi) RemoveFollower(c *gin.Context) {
	var err error
	p := proto.AnyID{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	followerId := p.ID
	err = api.sns.FollowUser(followerId, userId, true)
	if err != nil {
		errorMsg(c, errRemoveFollower, err)
		return
	}
	respJSON(c, nil, nil)
}

// Profile godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 查看个人主页
// @schemes http
// @description 查看个人主页
// @param id body proto.AnyID true "用户ID"
// @accept json
// @produce json
// @response 200 {object} proto.UserProfile
// @router /sns/profile [post]
func (api SnsApi) Profile(c *gin.Context) {
	var err error
	p := proto.AnyID{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.GetUint("userId")
	user, err := api.user.Get(p.ID)
	if err != nil {
		errorMsg(c, errUserNotFound, err)
		return
	}
	relation := api.sns.UserRelation(userId, p.ID)
	resp := proto.UserProfile{
		User:     user,
		Relation: relation,
	}
	selfFeed, err := api.feed.FeedModel.GetAll(p.ID, 0, 10)
	if err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	resp.SelfFeed = selfFeed
	likeFeed, err := api.feed.GetUserLike(p.ID, 0, 10)
	if err != nil {
		errorMsg(c, errFindUserLikeFeed, err)
		return
	}
	resp.LikeFeed = likeFeed

	//when view self, list secret feeds
	if p.ID == userId {
		secFeed, err := api.feed.FeedModel.GetSecret(p.ID, 0, 10)
		if err != nil {
			errorMsg(c, errFindUserFeed, err)
			return
		}
		resp.SecretFeed = secFeed
	}
	respJSON(c, resp, nil)
}

// UserFeedList godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 用户作品列表
// @schemes http
// @description 用户作品列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/userFeed/list [post]
func (api SnsApi) UserFeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()

	resp, err := api.feed.GetUserAll(p.ID, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, resp)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, resp)
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, resp, p)
}

// FriendFeedList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 好友短视频列表
// @schemes http
// @description 好友短视频列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/friend/feed [post]
func (api SnsApi) FriendFeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := p.ID
	resp, err := api.feed.GetFriendNewest(userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, resp)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, resp)
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, resp, p)
}

// LikeFeedList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户喜欢列表
// @schemes http
// @description 用户喜欢列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/userFeed/like [post]
func (api SnsApi) LikeFeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := p.ID
	resp, err := api.feed.GetUserLike(userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserLiked(selfId, resp)
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, resp, p)
}

// Remark godoc
// @BasePath /api/v1
// @tags SNS
// @summary 对关注的人进行备注
// @schemes http
// @description 对关注的人进行备注
// @param id body integer true "关系ID"
// @param val body string true "备注名"
// @accept json
// @produce json
// @response 200 {object} types.SnsRelation
// @router /sns/remark [post]
func (api SnsApi) Remark(c *gin.Context) {
	var err error
	p := proto.AnyVal{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	if utf8.RuneCountInString(p.Val) > 10 {
		errorMsg(c, errSnsRemarkTooLong)
		return
	}
	rel, err := api.sns.Get(p.ID)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}
	err = api.sns.Update(p.ID, "remark", p.Val)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}
	rel.Remark = p.Val

	respJSON(c, rel, nil)
}

// Setting godoc
// @BasePath /api/v1
// @tags SNS
// @summary 设置好友权限等操作
// @schemes http
// @description 设置好友权限等操作,可设置的值有:friend, close_friend, is_top, see_ta, see_me
// @param id body proto.SnsSetting true "setting"
// @accept json
// @produce json
// @response 200 {object} types.SnsRelation
// @router /sns/setting [post]
func (api SnsApi) Setting(c *gin.Context) {
	var err error
	p := proto.SnsSetting{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	if len(p.Data) == 0 {
		errorMsg(c, errReqArgs, err)
		return
	}
	rel, err := api.sns.SnsModel.Get(p.ID)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}

	for k, v := range p.Data {
		switch k {
		case "friend":
			if v {
				rel.FriendFlag = 1
			} else {
				rel.FriendFlag = 0
			}
		case "close_friend":
			if rel.FriendFlag > 0 {
				if v {
					rel.FriendFlag = 2
				} else {
					rel.FriendFlag = 1
				}
			}
		case "is_top":
			rel.IsTop = v
		case "see_ta":
			rel.SeeTa = v
		case "see_me":
			rel.SeeMe = v
		}
	}
	err = api.sns.SnsModel.UpdateColumns(rel)
	if err != nil {
		errorMsg(c, errSnsRelationSetting, err)
		return
	}
	respJSON(c, rel, nil)
}

// FollowerList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户粉丝列表
// @schemes http
// @description 用户粉丝列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.SnsUserListResp
// @router /sns/follower/list [post]
func (api SnsApi) FollowerList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := p.ID
	list, err := api.sns.GetUserFollower(userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}
	var resp []*proto.SnsUser
	if len(list) > 0 {
		resp = make([]*proto.SnsUser, 0, len(list))
		uids := make([]uint, 0, len(list))
		for k := range list {
			uids = append(uids, list[k].UserID)
		}
		userMap, err := api.user.GetUsersBaseInfo(uids)
		if err != nil {
			errorMsg(c, errServerInternal, err)
			return
		}
		for k, v := range list {
			su := &proto.SnsUser{
				Relation: list[k],
				User:     userMap[v.UserID],
			}
			resp = append(resp, su)
		}
	}
	respJSON(c, resp, p)
}

// FollowList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户关注列表
// @schemes http
// @description 用户关注列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.SnsUserListResp
// @router /sns/follow/list [post]
func (api SnsApi) FollowList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := p.ID
	list, err := api.sns.GetUserFollow(userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}
	var resp []*proto.SnsUser
	if len(list) > 0 {
		resp = make([]*proto.SnsUser, 0, len(list))
		fids := make([]uint, 0, len(list))
		for k := range list {
			fids = append(fids, list[k].FollowID)
		}
		userMap, err := api.user.GetUsersBaseInfo(fids)
		if err != nil {
			errorMsg(c, errServerInternal, err)
			return
		}
		for k, v := range list {
			su := &proto.SnsUser{
				Relation: list[k],
				User:     userMap[v.FollowID],
			}
			resp = append(resp, su)
		}
	}
	respJSON(c, resp, p)
}

// FollowFeed godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户关注的人的视频列表
// @schemes http
// @description 用户关注的人的视频列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /sns/follow/feed [post]
func (api SnsApi) FollowFeed(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	selfId := c.GetUint("userId")
	list, err := api.feed.GetFollowNewest(selfId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, list)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, list, p)
}

// FriendList godoc
// @BasePath /api/v1
// @tags SNS
// @summary 用户朋友列表
// @schemes http
// @description 用户朋友列表
// @param Paginate body proto.Paginate true "Paginate"
// @accept json
// @produce json
// @response 200 {object} proto.SnsUserListResp
// @router /sns/friend/list [post]
func (api SnsApi) FriendList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := p.ID
	list, err := api.sns.GetUserFriend(userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}
	var resp []*proto.SnsUser
	if len(list) > 0 {
		resp = make([]*proto.SnsUser, 0, len(list))
		fids := make([]uint, 0, len(list))
		for k := range list {
			fids = append(fids, list[k].FollowID)
		}
		userMap, err := api.user.GetUsersBaseInfo(fids)
		if err != nil {
			errorMsg(c, errServerInternal, err)
			return
		}
		for k, v := range list {
			su := &proto.SnsUser{
				Relation: list[k],
				User:     userMap[v.FollowID],
			}
			resp = append(resp, su)
		}
	}
	respJSON(c, resp, p)
}

// SearchFollow godoc
// @BasePath /api/v1
// @tags SNS
// @summary 搜索关注的人
// @schemes http
// @description 搜索关注的人(需要登录)
// @param req body proto.SearchReq true "SearchReq"
// @accept json
// @produce json
// @response 200 {object} proto.UserSearchResp
// @router /sns/search/follow [post]
func (api SnsApi) SearchFollow(c *gin.Context) {
	var err error
	p := &proto.SearchReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := p.ID

	keywd := p.Keyword
	list, err := api.sns.SearchUserFollow(userId, keywd, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errSnsRelation, err)
		return
	}
	respJSON(c, gin.H{"data": list, "keyword": keywd}, &p.Paginate)
}

// SearchUser godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 搜索用户名
// @schemes http
// @description 搜索用户名
// @param req body proto.SearchReq true "SearchReq"
// @accept json
// @produce json
// @response 200 {object} proto.UserSearchResp
// @router /sns/search/user [post]
func (api SnsApi) SearchUser(c *gin.Context) {
	var err error
	p := &proto.SearchReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	q := p.Keyword
	list, err := api.user.SearchUser(q, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, gin.H{"data": list, "keyword": q}, &p.Paginate)
}

// SearchFeed godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags SNS
// @summary 搜索短视频
// @schemes http
// @description 搜索短视频
// @param req body proto.SearchReq true "SearchReq"
// @accept json
// @produce json
// @response 200 {object} proto.FeedSearchResp
// @router /sns/search/feed [post]
func (api SnsApi) SearchFeed(c *gin.Context) {
	var err error
	p := &proto.SearchReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	q := p.Keyword
	list, err := api.feed.SearchFeed(q, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, list)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, gin.H{"data": list, "keyword": q}, &p.Paginate)
}

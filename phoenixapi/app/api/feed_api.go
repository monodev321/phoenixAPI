package api

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"log"
	"os"
	"path"
	"path/filepath"
	"phoenix/app/service"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/pkg/config"
	"strconv"
	"strings"
)

type FeedApi struct {
	feed service.FeedService
	user service.UserService
}

// Info godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags Feed
// @summary 短视频信息
// @schemes http
// @description 短视频信息
// @param param body proto.AnyID true "feed id"
// @accept json
// @produce json
// @response 200 {object} proto.UserFeed
// @router /feed/info [post]
func (api FeedApi) Info(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBind(p); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	feedInfo, err := api.feed.Get(p.ID)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	userInfo, err := api.user.GetBaseInfo(feedInfo.UserID)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	resp := &proto.UserFeed{
		Feed:         feedInfo,
		UserBaseInfo: userInfo,
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, []*proto.UserFeed{resp})
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, []*proto.UserFeed{resp})
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, resp, nil)
}

// Watch godoc
// @BasePath /api/v1
// @tags Feed
// @summary 短视频观看计数
// @schemes http
// @description 短视频观看计数
// @param param body proto.AnyID true "feed id"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /feed/watch [post]
func (api FeedApi) Watch(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBind(p); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.GetUint("userId")
	if userId == 0 {
		errorMsg(c, errRequireLogin, userId)
		return
	}
	info, err := api.feed.Watch(userId, p.ID)
	if err != nil {
		errorMsg(c, errFeedWatch, err)
		return
	}
	respJSON(c, gin.H{"id": info.ID, "watch_num": info.WatchNum}, nil)
}

// Recommend godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags Feed
// @summary 短视频推荐
// @schemes http
// @description 短视频推荐
// @param param body proto.Paginate true "feed id, page"
// @accept json
// @produce json
// @response 200 {object} proto.FeedListResp
// @router /feed/recommend [post]
func (api FeedApi) Recommend(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := c.GetUint("userId")
	list, err := api.feed.GetRecommend(userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	selfId := c.GetUint("userId")
	if selfId > 0 {
		err = api.feed.FillUserStared(selfId, list)
		if err != nil {
			log.Println(err)
		}
		err = api.feed.FillUserLiked(selfId, list)
		if err != nil {
			log.Println(err)
		}
	}
	respJSON(c, list, p)
}

// Like godoc
// @BasePath /api/v1
// @tags Feed
// @summary 短视频点赞/取消
// @schemes http
// @description 短视频点赞/取消
// @param param body proto.AnyID true "feed id, undo"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /feed/like [post]
func (api FeedApi) Like(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	feed, err := api.feed.Get(p.ID)
	if err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	err = api.feed.UserLike(userId, p.ID, p.Undo)
	if err != nil {
		errorMsg(c, errFeedLike, err)
		return
	}
	err = api.user.UpdateLikeNum(feed.UserID, p.Undo)
	if err != nil {
		errorMsg(c, errFeedLike, err)
		return
	}

	respJSON(c, nil, nil)
}

// Star godoc
// @BasePath /api/v1
// @tags Feed
// @summary 短视频收藏/取消
// @schemes http
// @description 短视频收藏/取消
// @param param body proto.AnyID true "feed id, undo"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /feed/star [post]
func (api FeedApi) Star(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	_, err = api.feed.Get(p.ID)
	if err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	err = api.feed.UserStar(userId, p.ID, p.Undo)
	if err != nil {
		errorMsg(c, errFeedStar, err)
		return
	}
	respJSON(c, nil, nil)
}

// Share godoc
// @BasePath /api/v1
// @tags Feed
// @summary 短视频分享
// @schemes http
// @description 短视频分享
// @param param body proto.AnyID true "feed id, undo"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /feed/share [post]
func (api FeedApi) Share(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	_, err = api.feed.Get(p.ID)
	if err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	err = api.feed.StatsShare(userId, p.ID)
	if err != nil {
		errorMsg(c, errFeedShare, err)
		return
	}
	respJSON(c, nil, nil)
}

// Create godoc
// @BasePath /api/v1
// @tags Feed
// @summary 短视频发布
// @schemes http
// @description 短视频发布
// @param param body proto.CreateFeed true "publish feed"
// @accept json
// @produce json
// @response 200 {object} proto.UserFeed
// @router /feed/create [post]
func (api FeedApi) Create(c *gin.Context) {
	var err error
	p := &proto.CreateFeed{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	newFeed := &types.Feed{
		UserID:   userId,
		Visible:  p.Visible,
		Cate:     p.Cate,
		Tag:      p.Tag,
		Location: p.Location,
		Intro:    p.Intro,
		FeedUrl:  p.FeedUrl,
		CoverUrl: p.CoverUrl,
		BgmUrl:   p.BgmUrl,
	}
	if err = api.feed.Save(newFeed); err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	resp := newFeed
	respJSON(c, resp, nil)
}

// List godoc
// @BasePath /api/v1
// @tags Feed
// @summary 短视频列表
// @schemes http
// @description 短视频列表
// @param param body proto.FeedListReq true "list user feed"
// @accept json
// @produce json
// @response 200 {object} proto.UserFeed
// @router /feed/list [post]
func (api FeedApi) List(c *gin.Context) {
	var err error
	p := &proto.FeedListReq{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := c.MustGet("userId").(uint)
	feed := api.feed.FeedModel
	var resp []*types.Feed
	switch p.Visible {
	case uint8(types.VisibleEnumSelf):
		resp, err = feed.GetSecret(userId, p.Offset(), p.Limit)
	default:
		resp, err = feed.GetAll(userId, p.Offset(), p.Limit)
	}
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, resp, &p.Paginate)
}

// Uninterested godoc
// @BasePath /api/v1
// @tags Feed
// @summary 不感兴趣
// @schemes http
// @description 不感兴趣
// @param req body proto.UninterestedReq true "feedId and reason"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /feed/uninterested [post]
func (api FeedApi) Uninterested(c *gin.Context) {
	var err error
	p := proto.UninterestedReq{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	if _, err = api.feed.Get(p.FeedID); err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	err = api.feed.UninterestedFeed(userId, p.FeedID, p.Reason)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, nil, nil)
}

// Visible godoc
// @BasePath /api/v1
// @tags Feed
// @summary 设置显示权限
// @schemes http
// @description 设置显示权限
// @param req body proto.AnyID true "feedId and undo"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /feed/visible [post]
func (api FeedApi) Visible(c *gin.Context) {
	var err error
	p := proto.AnyID{}
	if err = c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	f, err := api.feed.Get(p.ID)
	if err != nil {
		errorMsg(c, errFeedNotFound, err)
		return
	}
	if f.UserID != userId {
		errorMsg(c, errFeedOwnUser, err)
		return
	}
	var expr types.VisibleEnum
	if p.Undo {
		expr = types.VisibleEnumPublic
	} else {
		expr = types.VisibleEnumSelf
	}
	if err = api.feed.Update(p.ID, map[string]any{"visible": expr}); err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}

	respJSON(c, nil, nil)
}

// Upload godoc
// @BasePath /api/v1
// @tags User
// @summary 上传视频(mp4)
// @schemes http
// @description 上传视频(mp4)
// @param name formData string true "文件名: <hash>.mp4"
// @param file formData file true "文件"
// @accept mpfd
// @produce json
// @response 200 {object} any || proto.ErrorMsg
// @router /feed/upload [post]
func (api FeedApi) Upload(c *gin.Context) {
	var err error
	fname, _ := c.GetPostForm("name")
	file, err := c.FormFile("file")
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.GetUint("userId")
	log.Println("Upload feed: ", file.Filename, file.Size, file.Header)
	ext := strings.ToLower(path.Ext(file.Filename))
	fileUri := path.Join(config.AppCfg.App.ResourceDir, path.Join("feeds", strconv.Itoa(int(userId)), fname))
	saveFile, err := filepath.Abs(fileUri)
	if err != nil {
		errorMsg(c, errSaveUploadFile, err)
		return
	}
	switch ext {
	case ".mp4", ".mov", ".avi", ".wmv":
		os.MkdirAll(filepath.Dir(saveFile), 0750)
		err = c.SaveUploadedFile(file, saveFile)
		if err != nil {
			errorMsg(c, errSaveUploadFile, err, saveFile)
			return
		}
	default:
		errorMsg(c, errImageExtNotSupport, ext)
		return
	}
	respJSON(c, gin.H{"url": filepath.Join("/", fileUri)}, nil)
}

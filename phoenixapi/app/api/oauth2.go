package api

import (
	"phoenix/internal/user/service"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/go-oauth2/oauth2/v4/models"
	"github.com/go-oauth2/oauth2/v4/store"
)

func CreateOauthClient(c *gin.Context) {
	info := &models.Client{
		Domain:      "http://127.0.0.1:9000",
		UserID:      1,
		Username:    "developer@coinuse.io",
		RedirectUrl: "http://127.0.0.1:9000/v1/developer/login",
		Type:        "developer",
		Scope:       "developer",
	}
	clientStore, err := store.NewClientStore()
	err = clientStore.Create(info)
	if err != nil {
		util.FailWithMsg(c, err)
	} else {
		util.SuccessWithMsg(c, "register success", info)
	}
}

func Authorize(c *gin.Context) {
	uid := c.MustGet("uid").(string)
	username := c.MustGet("username").(string)
	uuid := c.Request.FormValue("uuid")
	callType := c.Request.FormValue("callType")
	if uuid == "" {
		util.FailWithMsg(c, werror.New(100, "uuid不能为空"))
		return
	}
	if callType == "" {
		util.FailWithMsg(c, werror.New(100, "callType不能为空"))
		return
	}
	err := service.Oauth2.HandleAuthorizeRequest(c.Writer, c.Request, uid, username, uuid, callType)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
}

func Token(c *gin.Context) {
	err := service.Oauth2.HandleTokenRequest(c.Writer, c.Request)
	if err != nil {
		util.FailWithMsg(c, err)
	}
}

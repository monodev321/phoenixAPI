package api

import (
	"fmt"
	"log"
	"net/http"
	"phoenix/app/types/proto"

	"github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
)

type ErrCode string

var (
	errFailed             ErrCode = "failed"
	errRequireLogin       ErrCode = "require login"
	errServerInternal     ErrCode = "errServerInternal"
	errReqArgs            ErrCode = "errReqArgs"
	errOperationFailed    ErrCode = "errOperationFailed"
	errSendVerifyCode     ErrCode = "errSendVerifyCode"
	errUserNotFound       ErrCode = "errUserNotFound"
	errUserExist          ErrCode = "errUserExist"
	errUserProfileUpdate  ErrCode = "errUserProfileUpdate"
	errUserUploadImage    ErrCode = "errUserUploadImage"
	errUserImageSize      ErrCode = "errUserImageSize"
	errImageExtNotSupport ErrCode = "errImageExtNotSupport"
	errSaveUploadFile     ErrCode = "errSaveUploadFile"
	errPasswdNotMatch     ErrCode = "errPasswdNotMatch"
	errPasswdTooShort     ErrCode = "errPasswdTooShort"
	errCodeNotMatch       ErrCode = "errCodeNotMatch"
	errFindUserFeed       ErrCode = "errFindUserFeed"
	errFindUserLikeFeed   ErrCode = "errFindUserLikeFeed"
	errFeedLike           ErrCode = "errFeedLike"
	errFeedStar           ErrCode = "errFeedStar"
	errFeedShare          ErrCode = "errFeedShare"
	errFeedUpload         ErrCode = "errFeedUpload"
	errRemoveFollower     ErrCode = "errRemoveFollower"
	errSnsRelation        ErrCode = "errSnsRelation"
	errSnsRelationSetting ErrCode = "errSnsRelationSetting"
	errSnsRemarkTooLong   ErrCode = "errSnsRemarkTooLong"
	errCommentPost        ErrCode = "errCommentPost"
	errCommentNotFind     ErrCode = "errCommentNotFind"
	errCommentLike        ErrCode = "errCommentLike"
	errVerifyRealNameLen  ErrCode = "errVerifyRealNameLen"
	errVerifyIdNumber     ErrCode = "errVerifyIdNumber"
	errVerifyIdNumberYet  ErrCode = "errVerifyIdNumberYet"
	errCreateLive         ErrCode = "errCreateLive"
	errFindUserLive       ErrCode = "errFindUserLive"
	errCloseUserLive      ErrCode = "errCloseUserLive"
	errLikeLive           ErrCode = "errLikeLive"
	errFeedWatch          ErrCode = "errFeedWatch"
	errPartFileNotFound   ErrCode = "errPartFileNotFound"
	errPartFileUpload     ErrCode = "errPartFileUpload"
	errFeedNotFound       ErrCode = "errFeedNotFound"
	errFeedOwnUser        ErrCode = "errFeedOwnUser"
	errFlag               ErrCode = "errFlag"
	ErrFlag               ErrCode = "errFlag"
	TokenInvalid          ErrCode = "405"
	RefreshTokenInvalid   ErrCode = "406"
)

func errorMsg(c *gin.Context, code ErrCode, logInfo ...any) {
	log.Printf("msg:%s, info:%s \n", code, fmt.Sprintln(logInfo...))
	errMsg, err := i18n.GetMessage(string(code))
	if string(code) == "errFlag" {
		errMsg = (logInfo[0].(error)).Error()
	}

	if err != nil {
		log.Println(err)
	}
	c.AbortWithStatusJSON(http.StatusOK, gin.H{"error": errMsg})
}

func respJSON(c *gin.Context, data any, paginate *proto.Paginate) {
	rst := gin.H{
		"message": "success",
		"data":    data,
	}
	if paginate != nil {
		rst["paginate"] = paginate
	}
	c.AbortWithStatusJSON(http.StatusOK, rst)
}

func ErrorMsg(c *gin.Context, code ErrCode, logInfo ...any) {
	errorMsg(c, code, logInfo...)
}

func RespJSON(c *gin.Context, data any, paginate *proto.Paginate) {
	respJSON(c, data, paginate)
}

package token

import (
	"strconv"
	"strings"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"

	"phoenix/internal/token/constant"
	"phoenix/internal/token/model"
	"phoenix/internal/token/service"

	"phoenix/internal/user/dao"
)

func TokenList(c *gin.Context) {
	tokens, err := service.TokenService.TokenList()
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

func Token(c *gin.Context) {
	var req tokenReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	tokens, err := service.TokenService.Token(req.Id)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

// 转账请求
type tokenReq struct {
	Id int64 `json:"id"  binding:"required"` // 代币id
}

func (req *tokenReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func Transfer(c *gin.Context) {
	var req TransfernReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}

	if uid != req.Uid {
		util.FailWithMsg(c, werror.New(100, "您无权当前操作"))
		return
	}
	if req.FUid == req.Uid {
		util.FailWithMsg(c, werror.New(100, "不能给自己转账"))
		return
	}

	log1 := &model.TokenFlowLogs{
		TokenId:   req.TokenId,
		Uid:       req.Uid,
		FUid:      req.FUid,
		FUsername: req.FUsername,
		FNickname: req.FNickname,
		FAvatar:   req.FAvatar,
		Quantity:  req.Quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_OUT,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_DECREASE,
		Comment:   req.Comment,
		TranId:    "0",
		ProceId:   "0",
		Charge:    0,
	}

	log2 := &model.TokenFlowLogs{
		TokenId:   req.TokenId,
		Uid:       req.FUid,
		FUid:      req.Uid,
		FUsername: req.Username,
		FNickname: req.Nickname,
		FAvatar:   req.Avatar,
		Quantity:  req.Quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_IN,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_ADD,
		Comment:   req.Comment,
		TranId:    "0",
		ProceId:   "0",
		Charge:    0,
	}
	tranPassword := strings.Trim(req.TranPassword, " ")
	id, err := service.TokenService.Transfer(log1, log2, tranPassword, req.Symbol)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", id)
	return
}

func Pledge(c *gin.Context) {
	util.SuccessWithMsg(c, "success", "")
	return
}

func PledgeDetails(c *gin.Context) {
	util.SuccessWithMsg(c, "success", "")
	return
}

func PayForDapp(c *gin.Context) {
	var req PayForDappReq
	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}

	if uid != req.Uid {
		util.FailWithMsg(c, werror.New(100, "您无权当前操作"))
		return
	}
	if req.FUid == req.Uid {
		util.FailWithMsg(c, werror.New(100, "不能给自己转账"))
		return
	}

	comment := "支付: " + req.OrderSeq

	fuser, err := dao.UserDao.GetUserInfo(req.FUid)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取收款账户失败"))
		return
	}
	log1 := &model.TokenFlowLogs{
		TokenId:   req.TokenId,
		Uid:       req.Uid,
		FUid:      req.FUid,
		FUsername: fuser.Username,
		FNickname: fuser.Nickname,
		FAvatar:   fuser.AvatarUrl,
		Quantity:  req.Quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_PAY_OUT,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_DECREASE,
		Comment:   comment,
		TranId:    req.OrderSeq,
		ProceId:   "0",
		Charge:    0,
	}

	log2 := &model.TokenFlowLogs{
		TokenId:   req.TokenId,
		Uid:       req.FUid,
		FUid:      req.Uid,
		FUsername: req.Username,
		FNickname: req.Nickname,
		FAvatar:   req.Avatar,
		Quantity:  req.Quantity,
		LogType:   constant.TOKEN_FLOW_LOG_TYPE_PAY_IN,
		Operator:  constant.TOKEN_FLOW_LOG_OPERATOR_ADD,
		Comment:   comment,
		TranId:    req.OrderSeq,
		ProceId:   "0",
	}
	tranPassword := strings.Trim(req.TranPassword, " ")
	fuserFlowId, err := service.TokenService.Pay(log1, log2, tranPassword, req.Symbol)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", fuserFlowId)
	return
}

package token

import (
	"github.com/go-playground/validator/v10"
)

// 转账请求
type TransfernReq struct {
	TokenId      int64  `json:"tokenId"  binding:"required"`     // 代币id
	Uid          int64  `json:"uid" binding:"required"`          // 流水相关用户
	Username     string `json:"username" binding:"required"`     // 流水相关用户
	Nickname     string `json:"nickname" binding:"required"`     // 流水相关用户
	Avatar       string `json:"avatar" binding:"required"`       // 流水相关用户
	FUid         int64  `json:"fuid" binding:"required"`         // 流水相关用户
	FUsername    string `json:"fusername" binding:"required"`    // 流水相关用户
	FNickname    string `json:"fnickname" binding:"required"`    // 流水相关用户
	FAvatar      string `json:"favatar" binding:"required"`      // 流水相关用户
	TranPassword string `json:"tranPassword" binding:"required"` // 流水相关用户
	Quantity     uint64 `json:"quantity" binding:"required"`     // 数量
	Comment      string `json:"comment" binding:"required"`      // 流水说明
	Symbol       string `json:"symbol" binding:"required"`       // 流水说明
}

func (req *TransfernReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

// 转账请求
type PayForDappReq struct {
	TokenId      int64  `json:"tokenId"  binding:"required"`     // 代币id
	Uid          int64  `json:"uid" binding:"required"`          // 流水相关用户
	Username     string `json:"username" binding:"required"`     // 流水相关用户
	Nickname     string `json:"nickname" binding:"required"`     // 流水相关用户
	Avatar       string `json:"avatar" binding:"required"`       // 流水相关用户
	FUid         int64  `json:"fuid" binding:"required"`         // 流水相关用户
	TranPassword string `json:"tranPassword" binding:"required"` // 流水相关用户
	Quantity     uint64 `json:"quantity" binding:"required"`     // 数量
	Symbol       string `json:"symbol" binding:"required"`       // 流水说明
	OrderSeq     string `json:"orderSeq" binding:"required"`     // 流水说明
}

func (req *PayForDappReq) GetError(err validator.ValidationErrors) string {
	// println("=======")
	for _, e := range err {
		println(e.Field(), e.Tag())
	}
	return "参数错误"
}

package validator2

import (
	"github.com/go-playground/validator/v10"
)

type UserTokensReq struct {
	Offset *int64 `form:"offset" json:"offset" binding:"required"`
}

func (req *UserTokensReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

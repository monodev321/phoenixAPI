package token

import (
	"fmt"
	"strconv"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"

	"phoenix/internal/token/model"
	"phoenix/internal/token/service"
)

func Frozen(c *gin.Context) {

	var req frozenReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}
	if uid != req.Uid {
		util.FailWithMsg(c, werror.New(200, "您没有权限"))
		return
	}

	if uid == req.Fuid {
		util.FailWithMsg(c, werror.New(200, "不能转账给自己"))
	}

	frozen := &model.TokenFrozen{
		DappId:         req.DappId,
		Uid:            req.Uid,
		FUid:           req.Fuid,
		Quantity:       req.Quantity,
		FrozenQuantity: req.Quantity,
		OrderQuantity:  req.OrderQuantity,
		Charge:         *req.Charge,
		ChargeRate:     *req.ChargeRate,
		ChargeUser:     *req.ChargeUser,
		TranId:         req.TranId,
		CancelInterval: *req.CancelInterval,
		Comment:        req.Comment,
	}

	data, err := service.FrozenService.Frozen(frozen, req.TokenSymbol, req.TranPassword)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", data)
	return
}

// 添加好友请求
type frozenReq struct {
	TokenSymbol    string  `json:"tokenSymbol" binding:"required"`    // 申请人
	DappId         int64   `json:"dappId" binding:"required"`         // 申请人
	Uid            int64   `json:"uid" binding:"required"`            // 申请人
	Fuid           int64   `json:"fuid" binding:"required"`           // 申请人
	Quantity       uint64  `json:"quantity" binding:"required"`       // 申请人
	OrderQuantity  uint64  `json:"orderQuantity" binding:"required"`  // 申请人
	Charge         *uint64 `json:"charge" binding:"required"`         // 申请人
	ChargeRate     *int    `json:"chargeRate" binding:"required"`     // 申请人
	ChargeUser     *int    `json:"chargeUser" binding:"required"`     // 申请人
	TranId         string  `json:"tranId" binding:"required"`         // 申请人
	CancelInterval *int64  `json:"cancelInterval" binding:"required"` // 申请人
	Comment        string  `json:"comment" binding:"required"`        // 申请人
	TranPassword   string  `json:"tranPassword" binding:"required"`   // 申请人
}

func (req *frozenReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "参数错误"
}

func FrozenTran(c *gin.Context) {

	var req frozenTranReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}
	if uid == req.Fuid {
		util.FailWithMsg(c, werror.New(200, "不能转账给自己"))
	}
	err = service.FrozenService.FrozenTran(req.FrozenId, req.Fuid, req.Quantity, req.TranPassword, req.TranId, uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
	return
}

// 添加好友请求
type frozenTranReq struct {
	FrozenId     int64  `json:"frozenId" binding:"required"`      // 申请人
	Fuid         int64  `json:"fuid" binding:"required"`          // 申请人
	Quantity     uint64 `json:"orderQuantity" binding:"required"` // 申请人
	TranId       string `json:"tranId" binding:"required"`        // 申请人
	TranPassword string `json:"tranPassword" binding:"required"`  // 申请人
}

func (req *frozenTranReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func FrozenBack(c *gin.Context) {

	var req frozenBackReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}

	err = service.FrozenService.FrozenBack(req.FrozenId, req.Quantity, req.TranPassword, uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
	return
}

// 添加好友请求
type frozenBackReq struct {
	FrozenId     int64  `json:"frozenId" binding:"required"`     // 申请人
	Quantity     uint64 `json:"quantity" binding:"required"`     // 申请人
	TranPassword string `json:"tranPassword" binding:"required"` // 申请人
}

func (req *frozenBackReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func UserFrozen(c *gin.Context) {
	var req userFrozenReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	frozen, err := service.FrozenService.UserFrozen(req.Id)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", frozen)
	return
}

// 添加好友请求
type userFrozenReq struct {
	Id int64 `json:"id" binding:"required"`
}

func (req *userFrozenReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

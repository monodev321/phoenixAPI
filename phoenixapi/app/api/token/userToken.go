package token

import (
	"strconv"

	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"

	"phoenix/app/api/token/validator2"
	"phoenix/internal/token/service"
)

func UserTokens(c *gin.Context) {
	var req validator2.UserTokensReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	println(uid)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}
	tokens, err := service.UserTokenService.UserTokenList(uid, *req.Offset)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

func FlowRecord(c *gin.Context) {
	var req FlowRecordReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}

	tokens, err := service.UserTokenService.FlowRecord(uid, req.TokenId, *req.Id, *req.Operator, *req.IsPre, req.Rows)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

// 添加好友请求
type FlowRecordReq struct {
	TokenId  int64  `json:"tokenId" binding:"required"`  // 申请人
	Id       *int64 `json:"id" binding:"required"`       // 申请人
	Operator *int   `json:"operator" binding:"required"` // 申请人
	IsPre    *int   `json:"isPre" binding:"required"`    // 申请人
	Rows     int    `json:"rows" binding:"required"`     // 申请人
}

func (req *FlowRecordReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func UserFlow(c *gin.Context) {
	var req userFlowReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	flow, err := service.UserTokenService.UserFlow(req.FlowId)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", flow)
	return
}

// 添加好友请求
type userFlowReq struct {
	FlowId int64 `json:"flowId" binding:"required"`
}

func (req *userFlowReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func FrozenRecord(c *gin.Context) {
	var req frozenRecordReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}

	tokens, err := service.UserTokenService.FrozenRecord(uid, req.TokenId, *req.Id, *req.IsPre, req.Rows)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

// 添加好友请求
type frozenRecordReq struct {
	TokenId int64  `json:"tokenId" binding:"required"` // 申请人
	Id      *int64 `json:"id" binding:"required"`      // 申请人
	IsPre   *int   `json:"isPre" binding:"required"`   // 申请人
	Rows    int    `json:"rows" binding:"required"`    // 申请人
}

func (req *frozenRecordReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func UnFrozenRecord(c *gin.Context) {
	var req unFrozenRecordReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}

	tokens, err := service.UserTokenService.UnFrozenRecord(uid, req.FrozenId, *req.Id, req.Rows)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

// 添加好友请求
type unFrozenRecordReq struct {
	FrozenId int64  `json:"frozenId" binding:"required"` // 申请人
	Id       *int64 `json:"id" binding:"required"`       // 申请人
	Rows     int    `json:"rows" binding:"required"`     // 申请人
}

func (req *unFrozenRecordReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func UserToken(c *gin.Context) {
	var req userTokenReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	uid, err := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "获取当前用户失败"))
		return
	}
	token, err := service.UserTokenService.UserToken(uid, req.TokenId)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", token)

	return
}

// 添加好友请求
type userTokenReq struct {
	TokenId int64 `json:"tokenId" binding:"required"` // 申请人
}

func (req *userTokenReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

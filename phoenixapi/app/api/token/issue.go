package token

import (
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"

	"phoenix/internal/token/service"
)

func IssueFlow(c *gin.Context) {

	var req issueFlowReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	tokens, err := service.IssueService.IssueFlow(req.TokenId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", tokens)
	return
}

// 添加好友请求
type issueFlowReq struct {
	TokenId int64 `json:"tokenId" binding:"required"` // 申请人
}

func (req *issueFlowReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

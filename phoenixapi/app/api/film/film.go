package film

import (
	"fmt"
	"phoenix/pkg/util"

	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/internal/film/model"
	"phoenix/internal/film/service"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func TopRecommedList(c *gin.Context) {
	list, err := service.FilmService.TopRecommedList()
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", list)
	return
}

func RankingList(c *gin.Context) {
	list, err := service.FilmService.RankingList()
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", list)
	return
}

func NewArrivalsList(c *gin.Context) {
	list, err := service.FilmService.NewArrivalsList()
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", list)
	return
}

func RecommedList(c *gin.Context) {
	var req RecommendFilmListReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	req.Fixed()

	list, err := service.FilmService.RecommedList(req.Offset(), req.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", list)
	return
}

type RecommendFilmListReq struct {
	proto.Paginate
}

func (req *RecommendFilmListReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func WithSamePlot(c *gin.Context) {
	var req FilmWithIdReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	type FilmWithPlot struct {
		Film *types.Film   `json:"film"`
		List *[]model.Film `json:"list"`
	}
	var data FilmWithPlot

	film, err := service.FilmService.FilmById(req.FilmId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data.Film = film

	list, err := service.FilmService.SamePlotList(film.PlotId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data.List = list

	util.SuccessWithMsg(c, "success", data)
	return
}

type FilmWithIdReq struct {
	FilmId uint `json:"film_id" binding:"required"`
}

func (req *FilmWithIdReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func LikeFilm(c *gin.Context) {
	var req FilmWithIdReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	err := service.FilmService.LikeFilm(req.FilmId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", nil)
	return
}

func DislikeFilm(c *gin.Context) {
	var req FilmWithIdReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	err := service.FilmService.DislikeFilm(req.FilmId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", nil)
	return
}

func SearchFilmByName(c *gin.Context) {
	var req SearchByNameReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	req.Fixed()

	list, err := service.FilmService.SearchByName(req.Offset(), req.Limit, req.MovieName)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", list)
	return
}

type SearchByNameReq struct {
	MovieName string `json:"movie_name" binding:"required"`
	proto.Paginate
}

func (req *SearchByNameReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func ListComments(c *gin.Context) {
	var req CommentListByFilmReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	userId := c.MustGet("userId").(uint)
	fmt.Println(userId)
	list, count, err := service.FilmService.ListComments(req.FilmId, userId, req.Offset(), req.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"list":  list,
		"count": count,
	}
	util.SuccessWithMsg(c, "success", data)
	return
}

func PostComment(c *gin.Context) {
	var req proto.CreateComment
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, "参数错误"))
		return
	}

	userId := c.MustGet("userId").(uint)
	film, err := service.FilmService.FilmById(req.FilmID)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	replyID := uint(0)
	if req.ReplyID > 0 {
		reply, err := service.FilmService.GetCommentById(req.ReplyID)
		if err != nil {
			util.FailWithMsg(c, err)
			return
		}
		replyID = reply.ID
	}
	comment := &types.Comment{
		UserID:  userId,
		FeedID:  req.FeedID,
		FilmID:  req.FilmID,
		ReplyID: replyID,
		LikeNum: 0,
		Content: req.Content,
	}
	err = service.FilmService.CreateComment(comment)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	err = service.FilmService.StatsComment(film.Id)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", nil)
	return
}

type CommentListByFilmReq struct {
	proto.Paginate
	FilmId int `json:"film_id" binding:"required"`
}

func (req *CommentListByFilmReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func LikeComment(c *gin.Context) {
	var req proto.AnyID
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, "参数错误"))
		return
	}

	userId := c.MustGet("userId").(uint)
	comment, err := service.FilmService.GetCommentById(req.ID)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	err = service.FilmService.LikeComment(userId, comment.ID, req.Undo)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", nil)
	return
}

func QuickFilter(c *gin.Context) {
	var req proto.QuickFilterReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, "参数错误"))
		return
	}

	req.Fixed()

	list, err := service.FilmService.QuickFilter(req.Offset(), req.Limit, req)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}

	util.SuccessWithMsg(c, "success", list)
	return
}

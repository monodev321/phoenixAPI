package blog

import (
	"phoenix/internal/blog/dao"
	"phoenix/internal/blog/model"
	"phoenix/internal/blog/service"
	userservice "phoenix/internal/user/service"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

// Create Blog
func BlogAdd(c *gin.Context) {
	var req BlogReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	userId, _ := strconv.ParseInt(c.MustGet("uid").(string), 10, 64)
	userInfo, _ := userservice.UserService.GetUserInfo(userId)
	newblog := &model.Blog{
		ID:          0,
		Title:       req.Title,
		Content:     req.Content,
		CoverUrl:    req.CoverUrl,
		UserID:      userId,
		Nickname:    userInfo.Nickname,
		Category:    req.Category,
		CategoryId:  req.CategoryId,
		Language:    req.Language,
		LanguageId:  req.LanguageId,
		ViewsNum:    0,
		IsTop:       0,
		IsRecommend: 0,
		Sort:        0,
		IsStatus:    0,
		CreatedAt:   0,
		UpdatedAt:   0,
	}
	blog, err := service.BlogService.Add(newblog)
	if err != nil {
		util.FailWithMsg(c, err)
	}
	util.SuccessWithMsg(c, "success", blog)
}

// Get Blogs
func BlogList(c *gin.Context) {
	var req BlogListReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	blogs, total, count, err := service.BlogService.BlogList(req.Category, req.Offset(), req.Limit)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"total_num": total,
		"count":     count,
		"blogs":     blogs,
	}
	util.SuccessWithMsg(c, "success", data)
}

// Get Blog Info
func BlogInfo(c *gin.Context) {
	var err error
	var req IdReq
	if err = c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	Id := int64(req.ID)
	blogInfo, err := service.BlogService.GetBlogInfo(Id)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	userId := blogInfo.UserID
	userInfo, err := userservice.UserService.GetBaseInfo(userId)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	resp := &model.BlogInfo{
		Blog:         blogInfo,
		UserBaseInfo: userInfo,
	}
	util.SuccessWithMsg(c, "success", resp)
}

// Search Blog
func SearchBlogByName(c *gin.Context) {
	var req SearchByNameReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, err)
		return
	}

	req.Fixed()

	list, count, err := service.BlogService.SearchByName(req.Offset(), req.Limit, req.BlogName, req.Category)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"blogs": list,
		"count": count,
	}
	util.SuccessWithMsg(c, "success", data)
}

// Delete Blog
func BlogDelete(c *gin.Context) {
	var req IdReq
	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	ID := int64(req.ID)
	err := service.BlogService.DeleteBlogById(ID)

	if err != nil {
		util.FailWithMsg(c, werror.New(100, err.Error()))
		return
	}

	util.SuccessWithMsg(c, "success", nil)
}

// Update Blog
func BlogEdit(c *gin.Context) {
	var req BlogReq

	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	body := &model.Blog{
		ID:         req.ID,
		Title:      req.Title,
		Content:    req.Content,
		CoverUrl:   req.CoverUrl,
		Category:   req.Category,
		CategoryId: req.CategoryId,
		Language:   req.Language,
		LanguageId: req.LanguageId,
		UpdatedAt:  time.Now().Unix(),
	}

	blog, err := service.BlogService.EditBlogById(body)

	if err != nil {
		util.FailWithMsg(c, werror.New(100, err.Error()))
		return
	}

	util.SuccessWithMsg(c, "success", blog)
}

func BlogChangeField(c *gin.Context) {
	var req BlogChangeFieldReq

	if err := c.ShouldBindWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	body := &model.Blog{
		ID:        req.ID,
		IsTop:     req.IsTop,
		IsStatus:  req.IsStatus,
		Sort:      req.Sort,
		UpdatedAt: time.Now().Unix(),
	}
	blog, err := dao.BlogDao.BlogChangeField(body)

	if err != nil {
		util.FailWithMsg(c, werror.New(100, err.Error()))
		return
	}

	util.SuccessWithMsg(c, "success", blog)
}

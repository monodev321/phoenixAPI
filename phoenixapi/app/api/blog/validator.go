package blog

import (
	"phoenix/app/types/proto"

	"github.com/go-playground/validator/v10"
)

const (
	defaultPageSize = 20
)

type BlogReq struct {
	ID         int64  `json:"id"`
	Title      string `form:"title" json:"title" binding:"required"`
	Content    string `form:"content" json:"content" binding:"required"`
	CoverUrl   string `form:"coverurl" json:"coverurl"`
	Category   string `form:"category" json:"category" binding:"required"`
	CategoryId uint   `form:"categoryid" json:"categoryid" binding:"required"`
	LanguageId uint   `form:"languageid" json:"languageid" binding:"required"`
	Language   string `form:"language" json:"language" binding:"required"`
}

func (req *BlogReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type BlogListReq struct {
	Category string `form:"category" json:"category"`
	PageNum  int    `form:"pagenum" json:"pagenum"`
	Limit    int    `form:"limit" json:"limit"`
}

func (p *BlogListReq) Fixed() {
	if p.PageNum <= 0 {
		p.PageNum = 1
	}
	if p.Limit <= 0 || p.Limit >= 20 {
		p.Limit = defaultPageSize
	}
}
func (p *BlogListReq) Offset() int64 {
	return int64((p.PageNum - 1) * p.Limit)
}
func (req *BlogListReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type IdReq struct {
	ID int64 `form:"id" json:"id" binding:"required"`
}

func (req IdReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type SearchByNameReq struct {
	Category string `json:"category"`
	BlogName string `json:"blogName"`
	proto.Paginate
}

type BlogChangeFieldReq struct {
	ID       int64 `json:"id" binding:"required"`
	IsTop    uint  `form:"istop" json:"istop" binding:"required"`
	IsStatus uint  `form:"isstatus" json:"isstatus" binding:"required"`
	Sort     uint  `form:"sort" json:"sort"`
}

func (req *BlogChangeFieldReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

package api

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	validator "github.com/guanguans/id-validator"
	"phoenix/app/service"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"unicode/utf8"
)

type LiveApi struct {
	live service.LiveService
	user service.UserService
}

// VerifyIdCard godoc
// @BasePath /api/v1
// @tags Live
// @summary 实名验证
// @schemes http
// @description 实名验证
// @param param body proto.IdCardInfo true "name, idCard"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /live/verifyIdCard [post]
func (api LiveApi) VerifyIdCard(c *gin.Context) {
	req := &proto.IdCardInfo{}
	err := c.ShouldBindWith(req, binding.JSON)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	l := utf8.RuneCountInString(req.Name)
	if l < 2 || l > 5 {
		errorMsg(c, errVerifyRealNameLen, err)
		return
	}
	if !validator.IsValid(req.IdNumber, false) {
		errorMsg(c, errVerifyIdNumber, req.IdNumber)
		return
	}
	userId := c.MustGet("userId").(uint)
	user, err := api.live.GetUser(userId)
	if err != nil {
		errorMsg(c, errUserNotFound, err)
		return
	}
	if user.IdVerify {
		errorMsg(c, errVerifyIdNumberYet, err)
		return
	}
	if err = api.live.SaveIdNumber(userId, req.Name, req.IdNumber); err != nil {
		errorMsg(c, errVerifyIdNumber, err)
		return
	}
	respJSON(c, nil, nil)
}

// Create godoc
// @BasePath /api/v1
// @tags Live
// @summary 创建直播
// @schemes http
// @description 创建直播
// @param param body proto.CreateLive true "CreateLive"
// @accept json
// @produce json
// @response 200 {object} proto.UserLive
// @router /live/create [post]
func (api LiveApi) Create(c *gin.Context) {
	req := &proto.CreateLive{}
	err := c.ShouldBindWith(req, binding.JSON)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	lv := &types.Live{
		UserID:   userId,
		LikeNum:  0,
		Status:   types.LiveStatusLiving,
		Cate:     req.Cate,
		Title:    req.Title,
		FeedUrl:  req.FeedUrl,
		CoverUrl: req.CoverUrl,
		AdUrl:    req.AdUrl,
	}
	if err = api.live.Save(lv); err != nil {
		errorMsg(c, errCreateLive, err)
		return
	}
	userInfo, err := api.user.GetBaseInfo(userId)
	if err != nil {
		errorMsg(c, errUserNotFound, err)
		return
	}
	resp := &proto.UserLive{
		UserBaseInfo: userInfo,
		Live:         lv,
	}
	respJSON(c, resp, nil)
}

// FeedList godoc
// @BasePath /api/v1
// @tags Live
// @summary 直播列表, 也可以展示主播列表
// @schemes http
// @description 直播列表, 也可以展示主播列表,数据都是一样的
// @param id body proto.Paginate true "这里需要随便写一个ID,如自己的用户ID"
// @accept json
// @produce json
// @response 200 {object} proto.UserLive
// @router /live/list [post]
func (api LiveApi) FeedList(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	//userId := c.MustGet("userId").(uint)
	list, err := api.live.GetLivingList(p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errFindUserLive, err)
		return
	}
	respJSON(c, list, p)
}

// Close godoc
// @BasePath /api/v1
// @tags Live
// @summary 关闭直播
// @schemes http
// @description 关闭直播
// @param id body proto.AnyID true "直播ID"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /live/close [post]
func (api LiveApi) Close(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	live, err := api.live.Get(p.ID)
	if err != nil {
		errorMsg(c, errFindUserLive, err)
		return
	}
	if live.UserID != userId {
		errorMsg(c, errCloseUserLive)
		return
	}
	if err = api.live.CloseLiving(live.ID); err != nil {
		errorMsg(c, errCloseUserLive)
		return
	}
	live.Status = types.LiveStatusClosed
	respJSON(c, live, nil)
}

// Like godoc
// @BasePath /api/v1
// @tags Live
// @summary 点赞直播/取消点赞
// @schemes http
// @description 点赞直播/取消点赞
// @param id body proto.AnyID true "点赞的直播ID,是否是取消操作"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /live/like [post]
func (api LiveApi) Like(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	live, err := api.live.Get(p.ID)
	if err != nil {
		errorMsg(c, errFindUserLive, err)
		return
	}
	if live.UserID != userId || live.Status == types.LiveStatusClosed {
		errorMsg(c, errLikeLive)
		return
	}
	if err = api.live.Like(userId, live.ID, p.Undo); err != nil {
		errorMsg(c, errLikeLive)
		return
	}
	respJSON(c, nil, nil)
}

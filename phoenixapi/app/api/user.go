package api

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	// "github.com/onsi/ginkgo/config"

	"phoenix/internal/user/model"
	"phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-oauth2/oauth2/v4/store"
	"github.com/go-playground/validator/v10"
)

func EmailRegister(c *gin.Context) {
	var req EmailRegisterReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	user := &model.User{
		Password: strings.Trim(req.Password, " "),
		Email:    strings.Trim(req.Username, " "),
	}

	_, err := service.UserService.EmailRegister(c, user)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "register success", user)
	return
}

func TelRegister(c *gin.Context) {
	var req TelRegisterReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		errorMsg(c, errFlag, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	user := &model.User{
		Password: strings.Trim(req.Password, " "),
		Tel:      strings.Trim(req.Username, " "),
		Code:     strings.Trim(req.Referrer, " "),
	}
	code := strings.Trim(req.Code, " ")

	_, err := service.UserService.TelRegister(c, user, code)
	if err != nil {
		errorMsg(c, errFlag, err)
		return
	}
	respJSON(c, nil, nil)
	return
}

func FindPassword(c *gin.Context) {
	var req FindPasswordReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	username := strings.Trim(req.Username, " ")
	password := strings.Trim(req.Password, " ")
	code := strings.Trim(req.Code, " ")

	err := service.UserService.FindPassword(c, username, password, code)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "register success", nil)
	return
}

func Login(c *gin.Context) {
	var req LoginReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		errorMsg(c, errFlag, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	username := strings.Trim(req.Username, " ")

	// if !bson.IsObjectIdHex(username) {
	// 	username = coinHash.Md5WithSalt(username, username)
	// }

	data := map[string]string{
		"password":      strings.Trim(req.Password, " "),
		"username":      username,
		"grant_type":    "password",
		"scope":         "user",
		"client_id":     strconv.FormatInt(config.AppCfg.Oauth2.ID, 10),
		"client_secret": config.AppCfg.Oauth2.Secret,
	}
	res, reserr := util.Post("/v1/oauth2/token", data, "user", true)
	if reserr != nil {
		if sererr, ok := res["error"]; ok {
			if sererr == "wrong_user_or_password" {
				errorMsg(c, errFlag, werror.New(200, "用户名或者密码错误"))
				return
			}
			if sererr == "user_inactive" {
				errorMsg(c, "用户未激活", 401)
				return
			}
			if sererr == "user_block" {
				errorMsg(c, errFlag, werror.New(200, "用户被禁用"))
				return
			}
		}
		// fmt.Printf("%+v", reserr)
		errorMsg(c, errFlag, werror.New(200, "登陆失败，请重试"))
		return
	}
	user, err := service.UserService.GetUserByUsername(username)
	fmt.Printf("%+v", user)
	if err != nil {
		errorMsg(c, errFlag, werror.New(200, "登陆失败，请重试"))
		return
	}
	user.IsTranPassword = 0
	if user.TranPassword != "" {
		user.IsTranPassword = 1
		user.TranPassword = ""
	}
	data2 := map[string]interface{}{
		"auth": res,
		"user": *user,
	}
	respJSON(c, data2, nil)
	return
}

func ChangeTranPassword(c *gin.Context) {
	var req ChangeTranPasswordReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	data := map[string]string{
		"oldPassword":     strings.Trim(req.OldPassword, " "),
		"password":        strings.Trim(req.Password, " "),
		"confirmPassword": strings.Trim(req.ConfirmPassword, " "),
		"uid":             uid,
	}
	err := service.UserService.ChangeTranPassword(data)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "change tran password success", nil)
	return
}

func ChangePassword(c *gin.Context) {
	var req ChangePasswordReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	data := map[string]string{
		"oldPassword":     strings.Trim(req.OldPassword, " "),
		"password":        strings.Trim(req.Password, " "),
		"confirmPassword": strings.Trim(req.ConfirmPassword, " "),
		"uid":             uid,
	}
	err := service.UserService.ChangePassword(data)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "change tran password success", nil)
	return
}

func EditNickname(c *gin.Context) {
	var req EditNicknameReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	data := map[string]string{
		"nickname": strings.Trim(req.Nickname, " "),
		"uid":      uid,
	}
	err := service.UserService.EditNickname(data)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "修改昵称成功", nil)
	return
}

func EditSignature(c *gin.Context) {
	var req EditSignatureReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	data := map[string]string{
		"signature": strings.Trim(req.Signature, " "),
		"uid":       uid,
	}
	err := service.UserService.EditSignature(data)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "change tran password success", nil)
	return
}

func EditAvatar(c *gin.Context) {
	var req EditAvatarReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	data := map[string]string{
		"avatar": strings.Trim(req.Avatar, " "),
		"uid":    uid,
	}
	err := service.UserService.EditAvatar(data)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "change avatar success", nil)
	return
}

func EditNotify(c *gin.Context) {
	var req EditNotifyReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := c.MustGet("uid").(string)
	data := map[string]string{
		"notify": strings.Trim(req.Notify, " "),
		"uid":    uid,
	}
	err := service.UserService.EditNotify(data)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "change notify success", nil)
	return
}

func RefreshToken(c *gin.Context) {
	var req RefreshTokenReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	data := map[string]string{
		"grant_type":    "refresh_token",
		"refresh_token": req.RefreshToken,
		"client_id":     strconv.FormatInt(config.AppCfg.Oauth2.ID, 10),
		"client_secret": config.AppCfg.Oauth2.Secret,
	}
	println(req.RefreshToken)
	res, reserr := util.Post("/v1/oauth2/token", data, "user", true)
	fmt.Printf("%+v", res)
	if reserr != nil {
		if sererr, ok := res["error"]; ok {
			errorMsg(c, errFlag, errors.New(sererr.(string)))
			return
		}
		errorMsg(c, errFlag, reserr)
		return
	}
	respJSON(c, res, nil)
	return
}

func Info(c *gin.Context) {
	uid := c.MustGet("uid").(string)
	id, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(300, "没有用户id"))
		return
	}
	user, err := service.UserService.GetUserInfo(id)
	user.IsTranPassword = 0
	if user.TranPassword != "" {
		user.IsTranPassword = 1
		user.TranPassword = ""
	}
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "验证成功请", user)
	return

}

func GetUserByUid(c *gin.Context) {
	var req GetUserByUidReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	user, err := service.UserService.GetUserInfo(req.Uid)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	data := map[string]interface{}{
		"uid":       user.Uid,
		"username":  user.Username,
		"nickname":  user.Nickname,
		"avatar":    user.AvatarUrl,
		"signature": user.Signature,
	}
	util.SuccessWithMsg(c, "验证成功请", data)
	return
}

func GetUserByUsername(c *gin.Context) {
	var req GetUserByUsernameReq
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	res, err := service.UserService.GetUserByUsername(req.Username)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "", res)
	return

}

func Logout(c *gin.Context) {
	accessToken, ok := service.Oauth2.BearerAuth(c.Request)
	if !ok {
		util.FailWithMsg(c, werror.New(200, "无效的token"))
		return
	}
	tokenStore := store.NewDefaultStore()
	err := tokenStore.RemoveByAccess(context.Background(), accessToken)
	if err != nil {
		util.FailWithMsg(c, werror.New(200, "退出失败"))
		return
	}
	util.SuccessWithMsg(c, "您已经退出!", nil)
}

func ReferrerCode(c *gin.Context) {
	uid := int64(c.MustGet("userId").(uint))
	err, code := service.UserService.ReferrerCode(uid)
	fmt.Printf("%d %s", uid, code)
	if err != nil {
		errorMsg(c, errFlag, err)
		return
	}
	respJSON(c, code, nil)
	return
}

func MyReferrer(c *gin.Context) {

	uid := int64(c.MustGet("userId").(uint))
	err, referrer, referrerPath := service.UserService.MyReferrer(uid)
	if err != nil {
		errorMsg(c, errFlag, err)
		return
	}
	role1, _ := service.UserService.SubReferrers(referrerPath, 1, 0)
	role2, _ := service.UserService.SubReferrers(referrerPath, 2, 0)
	role3, _ := service.UserService.SubReferrers(referrerPath, 3, 0)
	data := map[string]any{
		"referrer": referrer,
		"role1":    role1,
		"role2":    role2,
		"role3":    role3,
	}
	respJSON(c, data, nil)
	return
}

func ReferrerMore(c *gin.Context) {
	var req ReferrerMoreReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		errorMsg(c, errFlag, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := int64(c.MustGet("userId").(uint))
	err, _, referrerPath := service.UserService.MyReferrer(uid)
	if err != nil {
		errorMsg(c, errFlag, err)
		return
	}
	roles, _ := service.UserService.SubReferrers(referrerPath, req.Role, req.Offset)

	respJSON(c, roles, nil)
	return
}

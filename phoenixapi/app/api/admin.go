package api

import (
	// "context"
	// "errors"

	"fmt"
	"strconv"
	"strings"

	"time"

	// "github.com/onsi/ginkgo/config"

	// "phoenix/internal/user/model"
	"phoenix/internal/admin/model"
	adminservice "phoenix/internal/admin/service"
	"phoenix/internal/user/service"
	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"

	// "github.com/go-oauth2/oauth2/v4/store"
	"github.com/go-playground/validator/v10"
)

func AdminLogin(c *gin.Context) {
	var req LoginReq
	//自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		errorMsg(c, errFlag, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	username := strings.Trim(req.Username, " ")

	data := map[string]string{
		"password":      strings.Trim(req.Password, " "),
		"username":      username,
		"grant_type":    "password",
		"scope":         "administrator",
		"client_id":     strconv.FormatInt(config.AppCfg.AdminOauth2.ID, 10),
		"client_secret": config.AppCfg.AdminOauth2.Secret,
	}
	fmt.Printf("%+v", data)
	res, reserr := util.Post("/v1/oauth2/token", data, "user", true)
	if reserr != nil {
		if sererr, ok := res["error"]; ok {
			if sererr == "wrong_user_or_password" {
				errorMsg(c, errFlag, werror.New(200, "用户名或者密码错误"))
				return
			}
			if sererr == "user_inactive" {
				errorMsg(c, "用户未激活", 401)
				return
			}
			if sererr == "user_block" {
				errorMsg(c, errFlag, werror.New(200, "用户被禁用"))
				return
			}
		}

		errorMsg(c, errFlag, werror.New(200, "登陆失败，请重试"))
		return
	}
	user, err := service.UserService.GetUserByUsername(username)

	if err != nil {
		errorMsg(c, errFlag, werror.New(200, "获取用户错误，请重试"))
		return
	}
	checkadmin, err := adminservice.AdministratorService.GetUser(user.ID)
	if err != nil || checkadmin == nil {
		errorMsg(c, errFlag, werror.New(200, "管理员不存在，请重试"))
		return
	}
	f := &model.Administrator{
		Uid:            user.ID,
		Scope:          res["scope"].(string),
		TokenType:      res["token_type"].(string),
		Access:         res["access_token"].(string),
		Refresh:        res["refresh_token"].(string),
		TokenCreateAt:  time.Now().Unix(),
		TokenExpiredAt: time.Now().Unix() + int64(res["expires_in"].(float64)),
	}
	err = adminservice.AdministratorService.UpdateUserToken(f)
	if err != nil {
		errorMsg(c, errFlag, werror.New(200, "更新失败"))
		return
	}
	admin, err := adminservice.AdministratorService.GetUser(user.ID)
	data1 := map[string]interface{}{
		"auth":  res,
		"admin": *admin,
	}
	respJSON(c, data1, nil)
	return
}

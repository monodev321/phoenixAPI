package api

import (
	"crypto/md5"
	"fmt"
	"log"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"phoenix/app/model"
	"phoenix/app/service"
	"phoenix/app/types"
	"phoenix/app/types/proto"
	"phoenix/pkg/config"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/i18n"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type UserApi struct {
	user     service.UserService
	feed     service.FeedService
	report   service.ReportService
	feedback service.FeedbackService
	sns      service.SnsService
}

// Login godoc
// @BasePath /api/v1
// @summary 登录
// @schemes http
// @tags NoAuth
// @tags User
// @description 用户请求登录；</br>使用验证码登录时，如果帐号不存在，会创建一个帐号; url上可携带邀请人的ID ?invite_id=123;
// @param LoginReq body proto.LoginReq true "LoginReq"
// @accept json
// @produce json
// @response 200 {object} types.User || proto.ErrorMsg
// @router /user/login [post]
func (api UserApi) Login(c *gin.Context) {
	req := proto.LoginReq{}
	err := c.ShouldBindWith(&req, binding.JSON)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	inviteId := c.Query("invite_id")
	userModel := model.UserModel{}
	user, err := userModel.FindByPhoneID(req.PhoneID)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}

	//user passwd login
	if req.Passwd != "" {
		if user == nil {
			errorMsg(c, errPasswdNotMatch, err)
			return
		}
		err = api.user.VerifyPasswd(user, req.Passwd)
		if err != nil {
			errorMsg(c, errPasswdNotMatch, err)
			return
		}
	} else if req.VerifyCode != "" { //use verifyCode
		code, err := api.user.GetVerifyCode(model.VerifyCodeLogin, req.PhoneID)
		println(req.VerifyCode, code)
		if err != nil || len(code) != 6 || code != req.VerifyCode {
			errorMsg(c, errCodeNotMatch, err, code, req.VerifyCode)
			return
		}
		if user == nil {
			user, err = api.user.CreateUser(req.PhoneID, "", defaultName(req.PhoneID), inviteId)
			if err != nil {
				errorMsg(c, errServerInternal, err)
				return
			}
		}
	} else {
		errorMsg(c, errReqArgs)
		return
	}
	auth := &model.AuthClaims{UserID: user.ID, UserName: user.Nickname}
	token, err := auth.CreateToken()
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, gin.H{"user": user, "auth": token}, nil)
}

// Logout godoc
// @BasePath /api/v1
// @tags User
// @summary 登出帐号
// @schemes http
// @description 登出帐号,清除session
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg || proto.ErrorMsg
// @router /user/logout [post]
func (api UserApi) Logout(c *gin.Context) {
	//sess := sessions.Default(c)
	//sess.Clear()
	//sess.Save()
	respJSON(c, nil, nil)
}

// Register godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags User
// @summary 注册帐号
// @schemes http
// @description 注册帐号, url上可携带邀请人的ID ?invite_id=123;
// @param LoginReq body proto.LoginReq true "LoginReq"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg || proto.ErrorMsg
// @router /user/register [post]
func (api UserApi) Register(c *gin.Context) {
	req := proto.LoginReq{}
	err := c.ShouldBindWith(&req, binding.JSON)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	code, err := api.user.GetVerifyCode(model.VerifyCodeRegister, req.PhoneID)
	if err != nil || code != req.VerifyCode {
		errorMsg(c, errCodeNotMatch, "code:", code, "req:", req.VerifyCode)
		return
	}
	userModel := model.UserModel{}
	user, err := userModel.FindByPhoneID(req.PhoneID)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	if user != nil {
		errorMsg(c, errUserExist, user, req)
		return
	}
	user, err = api.user.CreateUser(req.PhoneID, req.Passwd, defaultName(req.PhoneID), c.Query("invite_id"))
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, user, nil)
}

// VerifyCode godoc
// @BasePath /api/v1
// @tags NoAuth
// @tags User
// @summary 发送验证码
// @schemes http
// @description 向手机发送验证码；</br>测试时, 会在控制台打印验证码
// @param req body proto.VerifyReq true "phone_id and code cate"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg || proto.ErrorMsg
// @router /user/verifyCode [post]
func (api UserApi) VerifyCode(c *gin.Context) {
	req := &proto.VerifyReq{}
	err := c.ShouldBindWith(req, binding.JSON)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	getCode := func() string {
		rand.Seed(time.Now().UnixNano())
		var n = int(100000 + rand.Int31n(900000))
		return fmt.Sprintf("%6s", strconv.Itoa(n))
	}
	code := getCode()
	log.Printf("verify code for phone:%s, cate:%v is %s", req.PhoneID, req.Cate, code)
	if req.Cate >= uint(model.VerifyCodeCateMax) {
		errorMsg(c, errReqArgs, err)
		return
	}
	err = api.user.SetVerifyCode(model.VerifyCodeCate(req.Cate), req.PhoneID, code)
	if err != nil {
		errorMsg(c, errSendVerifyCode, err)
		return
	}
	// TODO send verify code to phone
	var data gin.H
	if config.AppMode == config.AppModeDev {
		data = gin.H{"code": code}
	}
	respJSON(c, data, nil)
}

// Profile godoc
// @BasePath /api/v1
// @tags User
// @summary 用户个人信息
// @schemes http
// @description 用户个人信息,包括基础信息, 作品, 喜欢
// @accept json
// @produce json
// @response 200 {object} proto.UserProfile || proto.ErrorMsg
// @router /user/profile [post]
func (api UserApi) Profile(c *gin.Context) {
	SnsApi{}.Profile(c)
}

// RetrievePwd godoc
// @BasePath /api/v1
// @tags User
// @summary 用户找回密码
// @schemes http
// @description 找回密码(不需要登录)
// @accept json
// @produce json
// @response 200 {object} proto.UserProfile || proto.ErrorMsg
// @router /user/retrievePwd [post]
func (api UserApi) RetrievePwd(c *gin.Context) {
	req := proto.LoginReq{}
	err := c.ShouldBindWith(&req, binding.JSON)
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	if req.VerifyCode == "" || req.Passwd == "" {
		errorMsg(c, errReqArgs, err)
		return
	}
	if len(req.Passwd) < 6 {
		errorMsg(c, errPasswdNotMatch, err)
		return
	}
	code, err := api.user.GetVerifyCode(model.VerifyCodeRetrievePwd, req.PhoneID)
	if err != nil || len(code) != 6 || code != req.VerifyCode {
		errorMsg(c, errCodeNotMatch, err, code, req.VerifyCode)
		return
	}
	userModel := model.UserModel{}
	user, err := userModel.FindByPhoneID(req.PhoneID)
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}

	newPwd := api.user.GenPasswdHash(req.PhoneID, req.Passwd)
	user.Passwd = newPwd
	err = api.user.Update(user.ID, &types.User{Passwd: newPwd})
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	auth := &model.AuthClaims{UserID: user.ID, UserName: user.Nickname}
	token, err := auth.CreateToken()
	if err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, gin.H{"user": user, "auth": token}, nil)
}

// ProfileUpdate godoc
// @BasePath /api/v1
// @tags User
// @summary 设置用户基础信息
// @schemes http
// @description 设置用户基础信息<br/> 包括 name, intro, birthday("1970-01-01"), local, school, sex(female/male/none)
// @param UpdateProfile body proto.UpdateProfile true "字段名:值"
// @accept json
// @produce json
// @response 200 {object} types.User || proto.ErrorMsg
// @router /profile/update [post]
func (api UserApi) ProfileUpdate(c *gin.Context) {
	p := proto.UpdateProfile{}
	if err := c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	user, err := api.user.Get(userId)
	if err != nil {
		errorMsg(c, errUserNotFound, err)
		return
	}
	if len(p.Keys) != len(p.Values) || len(p.Keys) == 0 {
		return
	}
	userChange := &types.User{}
	for i, k := range p.Keys {
		v := p.Values[i]
		switch k {
		case "name":
			userChange.Nickname = v
		case "intro":
			userChange.Intro = v
		case "birthday":
			userChange.BirthDay = v
		case "local":
			userChange.Local = v
		case "school":
			userChange.School = v
		//case "background":
		//	userChange.Background = v
		case "sex":
			if v == "female" || v == "male" || v == "none" {
				userChange.Gender = v
			}
		}
	}
	err = api.user.Update(user.ID, userChange)
	if err != nil {
		errorMsg(c, errUserProfileUpdate, err)
		return
	}
	user, _ = api.user.Get(userId)
	respJSON(c, user, nil)
}

// Avatar godoc
// @BasePath /api/v1
// @tags User
// @summary 上传并更换头像
// @schemes http
// @description 上传并更换头像
// @param avatar formData file true "头像文件名"
// @accept mpfd
// @produce json
// @response 200 {object} any || proto.ErrorMsg
// @router /profile/avatar [post]
func (api UserApi) Avatar(c *gin.Context) {
	file, err := c.FormFile("avatar")
	if err != nil {
		errorMsg(c, errUserUploadImage, err)
		return
	}
	if file.Size > 1024*1024 {
		errorMsg(c, errUserImageSize, "size out of 1M")
		return
	}
	userId := c.MustGet("userId").(uint)
	ext := strings.ToLower(path.Ext(file.Filename))
	fileUri := path.Join(config.AppCfg.App.ResourceDir, path.Join("avatar", fmt.Sprintf("avatar_%d%s", userId, ext)))
	imagePath, err := filepath.Abs(fileUri)
	if err != nil {
		errorMsg(c, errSaveUploadFile, err)
		return
	}
	switch ext {
	case ".png", ".jpg", ".jpeg":
		err = c.SaveUploadedFile(file, imagePath)
		if err != nil {
			errorMsg(c, errSaveUploadFile, err, imagePath)
			return
		}
	default:
		errorMsg(c, errImageExtNotSupport, ext)
		return
	}
	fileUri2 := path.Join("./uploads", path.Join("avatar", fmt.Sprintf("avatar_%d%s", userId, ext)))
	imageUrl := filepath.Join("/", fileUri2)
	err = api.user.Update(userId, &types.User{Avatar: imageUrl})
	if err != nil {
		errorMsg(c, errUserUploadImage, err)
		return
	}
	respJSON(c, gin.H{"avatar": imageUrl}, nil)
}

// Background godoc
// @BasePath /api/v1
// @tags User
// @summary 上传并更换背景
// @schemes http
// @description 上传并更换背景
// @param bg formData file true "文件名"
// @accept mpfd
// @produce json
// @response 200 {object} any || proto.ErrorMsg
// @router /profile/background [post]
func (api UserApi) Background(c *gin.Context) {
	file, err := c.FormFile("bg")
	if err != nil {
		errorMsg(c, errUserUploadImage, err)
		return
	}
	if file.Size > 4*1024*1024 {
		errorMsg(c, errUserImageSize, "size out of 4M")
		return
	}
	userId := c.MustGet("userId").(uint)
	ext := strings.ToLower(path.Ext(file.Filename))
	filePath := path.Join(config.AppCfg.App.ResourceDir, path.Join("background", fmt.Sprintf("bg_%d%s", userId, ext)))
	imagePath, err := filepath.Abs(filePath)
	if err != nil {
		errorMsg(c, errSaveUploadFile, err)
		return
	}
	switch ext {
	case ".png", ".jpg", ".jpeg":
		err = c.SaveUploadedFile(file, imagePath)
		if err != nil {
			errorMsg(c, errSaveUploadFile, err, imagePath)
			return
		}
	default:
		errorMsg(c, errImageExtNotSupport, ext)
		return
	}
	fileUri2 := path.Join("./uploads", path.Join("background", fmt.Sprintf("bg_%d%s", userId, ext)))
	imageUrl := filepath.Join("/", fileUri2)
	err = api.user.Update(userId, &types.User{Background: imageUrl})
	if err != nil {
		errorMsg(c, errUserUploadImage, err)
		return
	}
	respJSON(c, gin.H{"background": imageUrl}, nil)
}

// Report godoc
// @BasePath /api/v1
// @tags User
// @summary 举报
// @schemes http
// @description 举报
// @param req body types.Report true "report"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /user/report [post]
func (api UserApi) Report(c *gin.Context) {
	p := types.Report{}
	if err := c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	p.ID = 0
	p.FromUser = userId
	if err := api.report.Save(&p); err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, nil, nil)
}

// Block godoc
// @BasePath /api/v1
// @tags User
// @summary 拉黑
// @schemes http
// @description 拉黑: 不能被对方搜索, 并取消相互关注, 并且不能聊天
// @param userid body proto.AnyID true "userid"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /user/block [post]
func (api UserApi) Block(c *gin.Context) {
	p := proto.AnyID{}
	if err := c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	uId := c.MustGet("userId").(uint)
	fId := p.ID

	if p.Undo {
		if err := api.user.UnBlock(uId, fId); err != nil {
			errorMsg(c, errServerInternal, err)
			return
		}
	} else {
		api.sns.Follow(uId, fId, true)
		api.sns.Follow(fId, uId, true)
		if err := api.user.Block(uId, fId); err != nil {
			errorMsg(c, errServerInternal, err)
			return
		}
	}
	respJSON(c, nil, nil)
}

// Feedback godoc
// @BasePath /api/v1
// @tags User
// @summary 反馈
// @schemes http
// @description 反馈
// @param req body types.Feedback true "feedback"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /user/feedback [post]
func (api UserApi) Feedback(c *gin.Context) {
	p := types.Feedback{}
	if err := c.ShouldBindWith(&p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	p.ID = 0
	p.UserID = userId
	if err := api.feedback.Save(&p); err != nil {
		errorMsg(c, errServerInternal, err)
		return
	}
	respJSON(c, nil, nil)
}

// ReportUpload godoc
// @BasePath /api/v1
// @tags User
// @summary 上传举报图片
// @schemes http
// @description 上传举报图片
// @param files[] formData file true "文件"
// @accept mpfd
// @produce json
// @response 200 {object} any || proto.ErrorMsg
// @router /user/report/upload [post]
func (api UserApi) ReportUpload(c *gin.Context) {
	var err error
	form, err := c.MultipartForm()
	if err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	if len(form.File["files[]"]) == 0 {
		errorMsg(c, errReqArgs, "files[] empty")
		return
	}
	userId := c.GetUint("userId")
	urls := make([]string, 0, 2)
	for _, file := range form.File["files[]"] {
		log.Println("Upload report: ", file.Filename, file.Size, file.Header)
		ext := strings.ToLower(path.Ext(file.Filename))
		hx := md5.New()
		hx.Write([]byte(fmt.Sprintf("%d-%s-%d", userId, file.Filename, file.Size)))
		fileUri := path.Join(config.AppCfg.App.ResourceDir, fmt.Sprintf("report/%d/%x%s", userId, hx.Sum(nil), ext))
		saveFile, err := filepath.Abs(fileUri)
		if err != nil {
			errorMsg(c, errSaveUploadFile, err)
			return
		}
		switch ext {
		case ".png", ".jpg":
			os.MkdirAll(filepath.Dir(saveFile), 0750)
			err = c.SaveUploadedFile(file, saveFile)
			if err != nil {
				errorMsg(c, errSaveUploadFile, err, saveFile)
				return
			}
		default:
			errorMsg(c, errImageExtNotSupport, ext)
			return
		}
		url := filepath.Join("/", fileUri)
		urls = append(urls, url)
	}
	respJSON(c, gin.H{"urls": urls}, nil)
}

func (api UserApi) mustGetUser(c *gin.Context) *types.User {
	userId := c.MustGet("userId").(uint)
	user, err := api.user.Get(userId)
	if err != nil {
		errorMsg(c, errUserNotFound, err)
		panic(err)
	}
	return user
}

func defaultName(phoneId string) (name string) {
	name, _ = i18n.GetMessage("user")
	name = name + "-" + phoneId[len(phoneId)-4:]
	return name
}

package validator2

import (
	"github.com/go-playground/validator/v10"
)

type AddPaymentReq struct {
	Kind     string ` json:"kind" binding:"required"`
	Symbol   string ` json:"symbol" binding:"required"`
	Username string ` json:"username" binding:"required"`
	Amount   int    ` json:"amount" binding:"required"`
}

func (req *AddPaymentReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

type GetPaymentReq struct {
	Kind string ` json:"kind" binding:"required"`
}

func (req *GetPaymentReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

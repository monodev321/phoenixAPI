package validator2

import (
	"github.com/go-playground/validator/v10"
)

type AddVipReq struct {
	OrderId string ` json:"orderId" binding:"required"`
	TokenId int64  ` json:"tokenId" binding:"required"`
	Kind    string ` json:"kind" binding:"required"`
	Amount  int    ` json:"amount" binding:"required"`
}

func (req *AddVipReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

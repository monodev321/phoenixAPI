package mine

import (
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"phoenix/app/api/mine/validator2"
	"phoenix/internal/mine/service"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func AddVip(c *gin.Context) {

	var req validator2.AddVipReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	uid := int64(c.MustGet("userId").(uint))
	err := service.VipService.Add(req.OrderId, uid, req.TokenId, req.Kind, req.Amount)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", nil)
	return
}

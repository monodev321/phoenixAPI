package mine

import (
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"

	"phoenix/internal/mine/service"

	"phoenix/app/api/mine/validator2"
)

func AddPayment(c *gin.Context) {

	var req validator2.AddPaymentReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	id, err := service.PaymentService.Add(req.Symbol, req.Username, req.Amount, req.Kind)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", id)
	return
}

func GetPayment(c *gin.Context) {

	var req validator2.GetPaymentReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	payment, err := service.PaymentService.GetPaymentByKind(req.Kind)
	if err != nil {
		util.FailWithMsg(c, err)
		return
	}
	util.SuccessWithMsg(c, "success", payment)
	return
}

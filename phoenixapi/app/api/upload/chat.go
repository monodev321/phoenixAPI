package upload

import (

	// "github.com/onsi/ginkgo/config"

	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"phoenix/pkg/config"
	"phoenix/pkg/util"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

func Upload(c *gin.Context) {
	uid := c.MustGet("uid").(string)
	uid_i, err := strconv.ParseInt(uid, 10, 64)
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "获取当前账户错误"))
		return
	}

	uidDirName := (uid_i / 10000) + 1

	file, header, err := c.Request.FormFile("file")
	if err != nil || header.Filename == "" {
		util.FailWithMsg(c, werror.New(100, fmt.Sprintf("file err :  %s", err.Error())))
		return
	}
	dirType := strings.Trim(c.Request.FormValue("type"), " ")
	dirName := ""
	newFilename := ""
	switch dirType {
	case "avatar":
		dirName = "upload/avatar/" + strconv.FormatInt(uidDirName, 10)
		timeUnix := time.Now().UnixNano() / 100000
		newFilename = uid + "_" + strconv.FormatInt(timeUnix, 10)
		break
	case "group":
		dirName = "upload/gavatar/" + strconv.FormatInt(uidDirName, 10)
		timeUnix := time.Now().UnixNano() / 100000
		newFilename = uid + "_" + strconv.FormatInt(timeUnix, 10)
		break
	case "chat":
		dirName = "upload/chat/" + strconv.FormatInt(uidDirName, 10) + "/" + uid
		timeUnix := time.Now().UnixNano() / 1000
		newFilename = strconv.FormatInt(timeUnix, 10)
		break
	default:
		break
	}

	if dirName == "" {
		util.FailWithMsg(c, werror.New(100, "上传类型不正确"))
		return
	}
	uploadFileNameWithSuffix := path.Base(header.Filename)
	uploadFileType := path.Ext(uploadFileNameWithSuffix)
	saveDir := config.AppCfg.App.FilmDir + dirName
	localFileInfo, fileStatErr := os.Stat(saveDir)
	//目录不存在
	if fileStatErr != nil || !localFileInfo.IsDir() {
		//创建目录
		errByMkdirAllDir := os.MkdirAll(saveDir, 0755)
		if errByMkdirAllDir != nil {
			util.FailWithMsg(c, werror.New(100, "目标创建失败"))
			return
		}
	}
	out, err := os.Create(saveDir + "/" + newFilename + uploadFileType)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	_, err = io.Copy(out, file)

	if err != nil {
		util.FailWithMsg(c, werror.New(100, "文件上错错误"))
		return
	}
	filepath := "/" + dirName + "/" + newFilename + uploadFileType

	util.SuccessWithMsg(c, "success", filepath)
	return
}

func UploadChatImage(c *gin.Context) {
	var req uploadChatReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	// uid := c.MustGet("uid").(string)
	// uid_i, err := strconv.ParseInt(uid, 10, 64)
	// if err != nil {
	// 	util.FailWithMsg(c, werror.New(100, "获取当前账户错误"))
	// 	return
	// }

	thumb := strings.Trim(req.Thumb, " ")
	big := strings.Trim(req.Big, " ")
	ext := strings.Trim(req.Ext, " ")
	name := strings.Trim(req.Name, " ")
	path := strings.Trim(req.Path, " ")
	chatType := strings.Trim(req.Type, " ")
	// sendId := uint64(0)
	// if req.SendId != nil {
	// 	sendId = *req.SendId
	// }

	dirName := "upload" + path
	saveDir := config.AppCfg.App.FilmDir + dirName
	localFileInfo, fileStatErr := os.Stat(saveDir)
	//目录不存在
	if fileStatErr != nil || !localFileInfo.IsDir() {
		//创建目录
		errByMkdirAllDir := os.MkdirAll(saveDir, 0755)
		if errByMkdirAllDir != nil {
			util.FailWithMsg(c, werror.New(100, "目录创建失败"))
			return
		}
	}

	if thumb != "" {
		out, err := os.Create(saveDir + "/" + name + ext)
		if err != nil {
			log.Fatal(err)
		}
		defer out.Close()
		_, err = out.WriteString(thumb)

		if err != nil {
			util.FailWithMsg(c, werror.New(100, "缩略图文件上错错误"))
			return
		}

	}

	if big != "" {
		out, err := os.Create(saveDir + "/" + name + "_big" + ext)
		if err != nil {
			log.Fatal(err)
		}
		defer out.Close()
		_, err = out.WriteString(big)

		if err != nil {
			util.FailWithMsg(c, werror.New(100, "大文件上错错误"))
			return
		}

	}
	filepath := dirName + "/" + name + ext

	if chatType == "single" {
		//发送单独聊消息
		// msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		// if err != nil {
		// 	util.FailWithMsg(c, werror.New(100, "生成消息id错误"))
		// 	return
		// }
		// m := map[string]interface{}{"url": filepath, "isRemote": 1, "isBigRemote": 1, "status": 0}
		// mjson, _ := json.Marshal(m)
		// mString := string(mjson)
		// message.SendSingleMassage("singleImage", mString, uint64(uid_i), uint64(req.ToId), msgid2, sendId)
	} else {
		// m := map[string]interface{}{"url": filepath, "isRemote": 1, "isBigRemote": 1, "status": 0}
		// mjson, _ := json.Marshal(m)
		// mString := string(mjson)
		// message.SendGroupMassage("groupImage", mString, uint64(uid_i), uint64(req.ToId), sendId)

	}
	util.SuccessWithMsg(c, "success", filepath)
	return
}

type uploadChatReq struct {
	Thumb  string  `form:"thumb" json:"thumb" binding:"required"`
	Big    string  `form:"big" json:"big" binding:"required"`
	Path   string  `form:"path" json:"path" binding:"required"`
	Name   string  `form:"name" json:"name" binding:"required"`
	Ext    string  `form:"ext" json:"ext" binding:"required"`
	ToId   int64   `form:"toId" json:"toId" binding:"required"`
	Type   string  `form:"type" json:"type" binding:"required"`
	SendId *uint64 `form:"sendId" json:"sendId" `
}

func (req *uploadChatReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "参数错误"
}

func UploadChatVoice(c *gin.Context) {
	var req uploadChatVoiceReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	// uid := c.MustGet("uid").(string)
	// uid_i, err := strconv.ParseInt(uid, 10, 64)
	// if err != nil {
	// 	util.FailWithMsg(c, werror.New(100, "获取当前账户错误"))
	// 	return
	// }

	file := strings.Trim(req.File, " ")
	ext := strings.Trim(req.Ext, " ")
	name := strings.Trim(req.Name, " ")
	path := strings.Trim(req.Path, " ")
	chatType := strings.Trim(req.Type, " ")
	// sendId := uint64(0)
	if req.SendId != nil {
		// sendId = *req.SendId
	}

	dirName := "upload" + path
	saveDir := config.AppCfg.App.FilmDir + dirName
	localFileInfo, fileStatErr := os.Stat(saveDir)
	//目录不存在
	if fileStatErr != nil || !localFileInfo.IsDir() {
		//创建目录
		errByMkdirAllDir := os.MkdirAll(saveDir, 0755)
		if errByMkdirAllDir != nil {
			util.FailWithMsg(c, werror.New(100, "目录创建失败"))
			return
		}
	}

	if file != "" {
		out, err := os.Create(saveDir + name + ext)
		if err != nil {
			log.Fatal(err)
		}
		defer out.Close()
		_, err = out.WriteString(file)

		if err != nil {
			util.FailWithMsg(c, werror.New(100, "缩略图文件上错错误"))
			return
		}

	}

	filepath := dirName + name + ext

	if chatType == "single" {
		//发送单独聊消息
		// msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		// if err != nil {
		// 	util.FailWithMsg(c, werror.New(100, "生成消息id错误"))
		// 	return
		// }
		// m := map[string]interface{}{"url": filepath,
		// 	"isRemote": 1,
		// 	"status":   0,
		// 	"length":   req.Length}
		// mjson, _ := json.Marshal(m)
		// mString := string(mjson)
		//message.SendSingleMassage("singleVoice", mString, uint64(uid_i), uint64(req.ToId), msgid2, sendId)
	} else {
		// m := map[string]interface{}{"url": filepath,
		// 	"isRemote": 1,
		// 	"status":   0,
		// 	"length":   req.Length}
		//mjson, _ := json.Marshal(m)
		//mString := string(mjson)
		//message.SendGroupMassage("groupVoice", mString, uint64(uid_i), uint64(req.ToId), sendId)

	}
	util.SuccessWithMsg(c, "success", filepath)
	return
}

type uploadChatVoiceReq struct {
	File   string  `form:"file" json:"file" binding:"required"`
	Length float64 `form:"length" json:"length" binding:"required"`
	Path   string  `form:"path" json:"path" binding:"required"`
	Name   string  `form:"name" json:"name" binding:"required"`
	Ext    string  `form:"ext" json:"ext" binding:"required"`
	ToId   int64   `form:"toId" json:"toId" binding:"required"`
	Type   string  `form:"type" json:"type" binding:"required"`
	SendId *uint64 `form:"sendId" json:"sendId" `
}

func (req *uploadChatVoiceReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func UploadChatVideo(c *gin.Context) {
	var req uploadChatVideoReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}
	// uid := c.MustGet("uid").(string)
	// uid_i, err := strconv.ParseInt(uid, 10, 64)
	// if err != nil {
	// 	util.FailWithMsg(c, werror.New(100, "获取当前账户错误"))
	// 	return
	// }

	video := strings.Trim(req.Video, " ")
	image := strings.Trim(req.Image, " ")
	ext := strings.Trim(req.Ext, " ")
	name := strings.Trim(req.Name, " ")
	path := strings.Trim(req.Path, " ")
	chatType := strings.Trim(req.Type, " ")
	// sendId := uint64(0)
	if req.SendId != nil {
		// sendId = *req.SendId
	}

	dirName := "upload" + path
	saveDir := config.AppCfg.App.FilmDir + dirName
	localFileInfo, fileStatErr := os.Stat(saveDir)
	//目录不存在
	if fileStatErr != nil || !localFileInfo.IsDir() {
		//创建目录
		errByMkdirAllDir := os.MkdirAll(saveDir, 0755)
		if errByMkdirAllDir != nil {
			util.FailWithMsg(c, werror.New(100, "目录创建失败"))
			return
		}
	}

	// 视频
	// videoUrl := dirName + "/" + name + ext
	if !util.Exists(saveDir + "/" + name + ext) {
		if video != "" {
			out, err := os.Create(saveDir + "/" + name + ext)
			if err != nil {
				log.Fatal(err)
			}
			defer out.Close()
			_, err = out.WriteString(video)

			if err != nil {
				util.FailWithMsg(c, werror.New(100, "缩略图文件上错错误"))
				return
			}

		}
	}
	// 图片
	// imageSrc := dirName + "/" + name + ".jpg"
	if !util.Exists(saveDir + "/" + name + ".jpg") {
		if image != "" {
			out, err := os.Create(saveDir + "/" + name + ".jpg")
			if err != nil {
				log.Fatal(err)
			}
			defer out.Close()
			_, err = out.WriteString(image)

			if err != nil {
				util.FailWithMsg(c, werror.New(100, "缩略图文件上错错误"))
				return
			}

		}
	}

	filepath := dirName + "/" + name + ext

	if chatType == "single" {
		//发送单独聊消息
		// msgid2, err := strconv.ParseUint(tools.GetSnowflakeId(), 10, 64)
		// if err != nil {
		// 	util.FailWithMsg(c, werror.New(100, "生成消息id错误"))
		// 	return
		// }
		// m := map[string]interface{}{"video": videoUrl, "image": imageSrc, "isVideoRemote": 1, "isImageRemote": 1, "status": 0}
		// mjson, _ := json.Marshal(m)
		// mString := string(mjson)
		// message.SendSingleMassage("singleVideo", mString, uint64(uid_i), uint64(req.ToId), msgid2, sendId)
	} else {
		// m := map[string]interface{}{"video": videoUrl, "image": imageSrc, "isVideoRemote": 1, "isImageRemote": 1, "status": 0}
		// mjson, _ := json.Marshal(m)
		// mString := string(mjson)
		// message.SendGroupMassage("groupVideo", mString, uint64(uid_i), uint64(req.ToId), sendId)

	}
	util.SuccessWithMsg(c, "success", filepath)
	return
}

type uploadChatVideoReq struct {
	Video  string  `form:"video" json:"video" binding:"required"`
	Image  string  `form:"image" json:"image" binding:"required"`
	Path   string  `form:"path" json:"path" binding:"required"`
	Name   string  `form:"name" json:"name" binding:"required"`
	Ext    string  `form:"ext" json:"ext" binding:"required"`
	ToId   int64   `form:"toId" json:"toId" binding:"required"`
	Type   string  `form:"type" json:"type" binding:"required"`
	SendId *uint64 `form:"sendId" json:"sendId" `
}

func (req *uploadChatVideoReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

func DownloadChat(c *gin.Context) {
	var req downloadChatReq
	// //自定义错误提示
	if err := c.ShouldBindBodyWith(&req, binding.JSON); err != nil {
		util.FailWithMsg(c, werror.New(100, req.GetError(err.(validator.ValidationErrors))))
		return
	}

	f, err := os.Open(config.AppCfg.App.FilmDir + req.Path)
	if err != nil {
		util.FailWithCode(c, werror.New(100, "文件路径错误"), 404)
	}
	bytes, err := ioutil.ReadAll(f)
	if err != nil {
		util.FailWithMsg(c, werror.New(100, "读取文件错误"))
	}
	str := string(bytes[0:])
	util.SuccessWithMsg(c, "success", str)
	return
}

type downloadChatReq struct {
	Path string `form:"path" json:"path" binding:"required"`
}

func (req *downloadChatReq) GetError(err validator.ValidationErrors) string {
	return "参数错误"
}

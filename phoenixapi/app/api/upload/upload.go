package upload

import (
	"errors"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"phoenix/pkg/config"
	"phoenix/pkg/hash"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func MultUploadFilm(c *gin.Context) {
	nums, _ := strconv.Atoi(c.Request.FormValue("nums"))
	fileList := make([]string, 0, nums)
	println(nums)
	md5 := c.Request.FormValue("md5")
	for i := 0; i < nums; i++ {
		file, header, err := c.Request.FormFile("files" + strconv.Itoa(i))
		fmt.Printf("%+v ;%+v ; => \n", header.Filename, err)
		if err != nil || header.Filename == "" {
			c.AbortWithStatusJSON(http.StatusOK, gin.H{"error": err.Error()})
			return
		}
		dirType := strings.Trim(c.Request.FormValue("type"), " ")
		filename, err := uploadFileFilm(dirType, header.Filename, file, md5)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusOK, gin.H{"error": err.Error()})
			return
		}
		fileList = append(fileList, filename)
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": fileList})
	return
}

func MultUpload(c *gin.Context) {
	nums, _ := strconv.Atoi(c.Request.FormValue("nums"))
	fileList := make([]string, 0, nums)
	println(nums)
	md5 := c.Request.FormValue("md5")
	for i := 0; i < nums; i++ {
		file, header, err := c.Request.FormFile("files" + strconv.Itoa(i))
		fmt.Printf("%+v ;%+v ; => \n", header.Filename, err)
		if err != nil || header.Filename == "" {
			c.AbortWithStatusJSON(http.StatusOK, gin.H{"error": err.Error()})
			return
		}
		dirType := strings.Trim(c.Request.FormValue("type"), " ")
		filename, err := uploadFile(dirType, header.Filename, file, md5)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusOK, gin.H{"error": err.Error()})
			return
		}
		fileList = append(fileList, filename)
	}
	c.JSON(http.StatusOK, gin.H{"code": 1, "message": "success", "data": fileList})
	return
}

type multUploadReq struct {
	Files string `form:"files" json:"files" binding:"required"`
}

func (req *multUploadReq) GetError(err validator.ValidationErrors) string {
	fmt.Printf("%+v", err)
	return "参数错误"
}

func uploadFile(dirType string, filename string, file multipart.File, md5 string) (string, error) {
	dirName := ""
	uploadFileNameWithSuffix := path.Base(filename)
	uploadFileType := path.Ext(uploadFileNameWithSuffix)
	newFilename := hash.Md5(filename) + uploadFileType

	switch dirType {
	case "blogCover":
		dirName = "/upload/blog-cover/" + getNowYMD() + "/" + md5
		break
	case "editor":
		dirName = "/upload/editor/" + getNowYMD() + "/" + md5
		break
	case "help":
		dirName = "/upload/help/" + getNowYMD() + "/" + md5
		break
	case "feed":
		dirName = "/upload/feed/" + getNowYMD() + "/" + md5
		break
	default:
		break
	}

	if dirName == "" {
		return "", errors.New("上传类型不正确")
	}

	saveDir := config.AppCfg.App.FilmDir + dirName
	localFileInfo, fileStatErr := os.Stat(saveDir)
	//目录不存在
	if fileStatErr != nil || !localFileInfo.IsDir() {
		//创建目录
		errByMkdirAllDir := os.MkdirAll(saveDir, 0755)
		if errByMkdirAllDir != nil {
			return "", errors.New("目标创建失败")
		}
	}
	out, err := os.Create(saveDir + "/" + newFilename)
	if err != nil {
		log.Fatal(err)
		return "", errors.New("创建文件失败")
	}
	defer out.Close()
	_, err = io.Copy(out, file)

	if err != nil {
		return "", errors.New("文件上错错误")
	}
	filepath := dirName + "/" + newFilename
	return filepath, nil
}

func uploadFileFilm(dirType string, filename string, file multipart.File, md5 string) (string, error) {
	dirName := ""
	newFilename := ""

	switch dirType {
	case "Film":
		dirName = "/upload/film/" + getNowYMD() + "/" + md5
		newFilename = "thumb2.jpg"
		break
	case "FilmCover":
		dirName = "/upload/film/" + getNowYMD() + "/" + md5
		newFilename = "cover2.jpg"
		break
	default:
		break
	}

	if dirName == "" {
		return "", errors.New("上传类型不正确")
	}
	// uploadFileNameWithSuffix := path.Base(filename)
	// uploadFileType := path.Ext(uploadFileNameWithSuffix)
	saveDir := config.AppCfg.App.FilmDir + dirName
	localFileInfo, fileStatErr := os.Stat(saveDir)
	//目录不存在
	if fileStatErr != nil || !localFileInfo.IsDir() {
		//创建目录
		errByMkdirAllDir := os.MkdirAll(saveDir, 0755)
		if errByMkdirAllDir != nil {
			return "", errors.New("目标创建失败")
		}
	}
	out, err := os.Create(saveDir + "/" + newFilename)
	if err != nil {
		log.Fatal(err)
		return "", errors.New("创建文件失败")
	}
	defer out.Close()
	_, err = io.Copy(out, file)

	if err != nil {
		return "", errors.New("文件上错错误")
	}
	filepath := dirName + "/" + newFilename
	return filepath, nil
}

func getNowYMD() string {
	y := strconv.Itoa(time.Now().Year())  //年
	m := int2Str(int(time.Now().Month())) //月
	d := int2Str(time.Now().Day())
	return y + m + d
}

func int2Str(i int) string {
	s := strconv.Itoa(i)
	if i < 10 {
		return "0" + s
	}
	return s
}

package api

import (
	"phoenix/app/service"
	"phoenix/app/types"
	"phoenix/app/types/proto"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

type CommentApi struct {
	comment service.CommentService
	feed    service.FeedService
}

// Like godoc
// @BasePath /api/v1
// @tags Comment
// @summary 评论点赞
// @schemes http
// @description 评论点赞
// @param id body proto.AnyID true "comment id"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /comment/like [post]
func (api CommentApi) Like(c *gin.Context) {
	var err error
	p := &proto.AnyID{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	comment, err := api.comment.GetOne(p.ID)
	if err != nil {
		errorMsg(c, errCommentNotFind, err)
		return
	}
	err = api.comment.Like(userId, comment.ID, p.Undo)
	if err != nil {
		errorMsg(c, errCommentLike, err)
		return
	}
	respJSON(c, nil, nil)
}

// Post godoc
// @BasePath /api/v1
// @tags Comment
// @summary 评论发表
// @schemes http
// @description 评论发表
// @param CreateComment body proto.CreateComment true "create comment"
// @accept json
// @produce json
// @response 200 {object} proto.SuccessMsg
// @router /comment/post [post]
func (api CommentApi) Post(c *gin.Context) {
	var err error
	p := &proto.CreateComment{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	userId := c.MustGet("userId").(uint)
	feed, err := api.feed.Get(p.FeedID)
	if err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	replyID := uint(0)
	if p.ReplyID > 0 {
		reply, err := api.comment.GetOne(p.ReplyID)
		if err != nil {
			errorMsg(c, errCommentPost, err)
			return
		}
		replyID = reply.ID
	}
	comment := &types.Comment{
		UserID:  userId,
		FeedID:  p.FeedID,
		ReplyID: replyID,
		LikeNum: 0,
		Content: p.Content,
	}
	err = api.comment.Save(comment)
	if err != nil {
		errorMsg(c, errCommentPost, err)
		return
	}
	err = api.feed.StatsComment(feed.ID)
	if err != nil {
		errorMsg(c, errCommentPost, err)
		return
	}
	respJSON(c, comment, nil)
}

// List godoc
// @BasePath /api/v1
// @tags Comment
// @summary 评论列表
// @schemes http
// @description 评论发表
// @param param body proto.Paginate true "list comment"
// @accept json
// @produce json
// @response 200 {object} []proto.UserComment
// @router /comment/list [post]
func (api CommentApi) List(c *gin.Context) {
	var err error
	p := &proto.Paginate{}
	if err = c.ShouldBindWith(p, binding.JSON); err != nil {
		errorMsg(c, errReqArgs, err)
		return
	}
	p.Fixed()
	userId := c.MustGet("userId").(uint)
	_, err = api.feed.Get(p.ID)
	if err != nil {
		errorMsg(c, errFindUserFeed, err)
		return
	}
	list, err := api.comment.GetList(p.ID, userId, p.Offset(), p.Limit)
	if err != nil {
		errorMsg(c, errCommentNotFind, err)
		return
	}
	respJSON(c, list, p)
}

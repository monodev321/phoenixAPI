### phoenix community
    phoenix community api server

+ requirement:
  - go1.18+
  - redis 6
  - mysql 8

+ build:
  ```
  debug:
    go build -v -x -tags debug -o build/phoenixServer phoenix/cmd/phoenixServer

  product:
  go build -v -ldflags="-s -w" -tags prod -o build/phoenixServer phoenix/cmd/phoenixServer
  ``

   go run main.go -c app-dev.yaml -m dev 
    go build -v -ldflags="-s -w" -tags prod -o build/phoenixServer phoenix/cmd/phoenixServer
    
  docs install:
    go get -u github.com/swaggo/swag/cmd/swag
    go get -u github.com/swaggo/gin-swagger
    go get -u github.com/swaggo/files
  docs gen:
      swag init
      http://127.0.0.1:8000/docs/index.html
  ```

## 运行im connet 和task

go run main.go --c app-dev.yaml --m conn
go run main.go --c app-dev.yaml --m task
go run main.go --c app-dev.yaml --m api
go run main-api.go --c app-dev.yaml --m api


## 测试

sudo docker run -d --name phoenix-api --network phoenix  -p 8000:8000 --restart=always -v /home/azureuser/data/api/upload:/var/app/upload: -v ~/data/leveldb:/data/database docker.biyong001.com/pre/phoenix-api:1.0.0-56 /var/app/phoenixServer --c /var/app/app-pre.yaml --m api

docker pull docker.biyong001.com/pre/phoenix-api:1.0.0-6

sudo docker run  --restart=always --network phoenix  -v ~/data/api:/phoenix --name film-api -p 8000:8000 -d golang:1.18-stretch /phoenix/run.sh

图片：

sudo docker run  --restart=always --network phoenix  -v ~/data/api:/usr/share/nginx/html:ro --name film-image -p 8080:80 -d nginx

## 测试
docker run  --restart=always  -v /Users/villian/app/video/api:/usr/share/nginx/html:ro --name phoenix-web -p 8555:80 -d nginx

# How to run
copy .app-dev.yaml.example as .app-dev.yaml and change configurations.

Run:
```
go run main.go --c app-dev.yaml --m api
```

Enjoy!
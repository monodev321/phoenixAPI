package util

import (
	"encoding/json"
	"fmt"

	"phoenix/pkg/config"
	"phoenix/pkg/werror"

	"github.com/valyala/fasthttp"
)

func Get(url string, host string, isHostCfg bool) interface{} {
	if isHostCfg {
		url = config.AppCfg.ServerHost[host] + url
	} else {
		url = host + url
	}

	// status, resp, err := fasthttp.Get(nil, url)
	// if err != nil {
	// 	fmt.Println("请求失败:", err.Error())
	// 	return nil
	// }

	// if status != fasthttp.StatusOK {
	// 	fmt.Println("请求没有成功:", status)
	// 	return nil
	// }

	return url
}

func Post(url string, data map[string]string, host string, isHostCfg bool) (map[string]interface{}, error) {
	if isHostCfg {
		url = config.AppCfg.ServerHost[host] + url
	} else {
		url = host + url
	}
	// println(url)
	// 填充请求参数
	args := &fasthttp.Args{}
	for k, v := range data {
		args.Add(k, v)
	}
	status, resp, err := fasthttp.Post(nil, url, args)
	if err != nil {
		// fmt.Println("请求失败:", err.Error())
		// @todo 这边可以记录请求结果
		return nil, werror.FunWrap(err, 200, "")
	}
	var res map[string]interface{}
	err = json.Unmarshal(resp, &res)
	if status != fasthttp.StatusOK {
		//当返回非200的时候看看有错误提示吗
		if err == nil {
			return res, werror.New(300, "解析返回结果失败")
		}
		return nil, werror.New(200, "user服务器响应500错误")
	}

	if err != nil {
		return nil, werror.New(300, "解析返回结果失败")
	}
	return res, nil
}

func PostJson(url string, data map[string]interface{}, host string, isHostCfg bool) (map[string]interface{}, error) {
	if isHostCfg {
		url = config.AppCfg.ServerHost[host] + url
	} else {
		url = host + url
	}

	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req) // 用完需要释放资源
	req.Header.SetContentType("application/json")
	req.Header.SetMethod("POST")
	req.SetRequestURI(url)

	reqjson, _ := json.Marshal(data)

	var res1 map[string]interface{}
	err := json.Unmarshal(reqjson, &res1)

	if err != nil {
		return nil, err
	}

	req.SetBody(reqjson)

	resp := fasthttp.AcquireResponse()
	defer fasthttp.ReleaseResponse(resp) // 用完需要释放资源

	if err := fasthttp.Do(req, resp); err != nil {
		fmt.Println("请求失败:", err.Error())
		return nil, err
	}

	var res map[string]interface{}
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		return nil, werror.New(300, "解析返回结果失败")
	}
	fmt.Printf("%+v", res)
	return res, nil
}

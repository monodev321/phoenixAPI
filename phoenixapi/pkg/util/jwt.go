package util

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

func ParseJwtToken(tokenStr string, pubKey string) (interface{}, interface{}, error) {

	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		buf := []byte(pubKey)
		// 2 pem解码
		block, _ := pem.Decode(buf) // 解码

		// 3 x509解析得到公钥
		pubInterface, _ := x509.ParsePKIXPublicKey(block.Bytes)
		// 断言类型转换
		pubKey := pubInterface.(*rsa.PublicKey)
		return pubKey, nil
	})

	if err != nil {
		return nil, nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims["sub"], claims["username"], nil
	} else {
		return nil, nil, nil
	}
}

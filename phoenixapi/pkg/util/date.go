package util

import (
	"strconv"
	"time"
)

func int2Str(i int) string {
	s := strconv.Itoa(i)
	if i < 10 {
		return "0" + s
	}
	return s
}

func GetYearMonth(timestamp int64) string {
	y := strconv.Itoa(time.Unix(timestamp, 0).Year())  //年
	m := int2Str(int(time.Unix(timestamp, 0).Month())) //月
	return y + m
}

func GetNowYM() string {
	y := strconv.Itoa(time.Now().Year())  //年
	m := int2Str(int(time.Now().Month())) //月
	return y + m
}

func GetNowYMD() string {
	y := strconv.Itoa(time.Now().Year())  //年
	m := int2Str(int(time.Now().Month())) //月
	d := int2Str(time.Now().Day())
	return y + m + d
}

package util

import (
	"regexp"

	"github.com/go-playground/validator/v10"
)

func Password(field validator.FieldLevel) bool {
	if str, ok := field.Field().Interface().(string); ok {

		// nl := regexp.MustCompile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,10}$") //测试用
		nl := regexp.MustCompile("[^0-9A-Za-z!-~]+") //是否包含之外的字符，true包含 false不包含
		if nl.MatchString(str) {
			return false
		}

		l := regexp.MustCompile("[a-zA-Z]+") //必须包含字母 true包含
		if !l.MatchString(str) {
			return false
		}

		n := regexp.MustCompile("[0-9]+") //必须包含数字  true包含
		if !n.MatchString(str) {
			return false
		}
		return true
	}
	return false
}

package util

import (
	"fmt"
	"phoenix/app/types"
	"testing"
)

func TestConst(t *testing.T) {
	var a, b, c, d = types.RelationTypeNone, types.RelationTypeFollow, types.RelationTypeFollower, types.RelationTypeFriend
	fmt.Println(a, b, c, d)
}

func TestNameLen(t *testing.T) {
	var a, b, c, d = "abc", "中国", "张三李四", "张三李四王五"
	fmt.Println(len(a), len(b), len(c), len(d))
}

/**
 * Created by lock
 * Date: 2019-10-06
 * Time: 23:30
 */
package util

import (
	"net/http"

	"phoenix/pkg/logger"
	"phoenix/pkg/werror"

	"github.com/gin-gonic/gin"
)

const (
	CodeSuccess      = 0
	CodeFail         = 1
	CodeUnknownError = -1
	CodeSessionError = 40000
)

var MsgCodeMap = map[int]string{
	CodeUnknownError: "unKnow error",
	CodeSuccess:      "success",
	CodeFail:         "fail",
	CodeSessionError: "Session error",
}

func SuccessWithMsg(c *gin.Context, msg interface{}, data interface{}) {
	ResponseWithCode(c, CodeSuccess, msg, data)
}

func FailWithMsg(c *gin.Context, msg interface{}) {
	err := werror.ResWrap(msg)
	logger.Logger.Info(err.LogError())
	ResponseWithCode(c, CodeFail, err.Error(), nil)
}

func FailWithCode(c *gin.Context, msg interface{}, code int) {
	err := werror.ResWrap(msg)
	logger.Logger.Info(err.LogError())
	ResponseWithCode(c, code, err.Error(), nil)
}

func ResponseWithCode(c *gin.Context, msgCode int, msg interface{}, data interface{}) {
	if msg == nil {
		if val, ok := MsgCodeMap[msgCode]; ok {
			msg = val
		} else {
			msg = MsgCodeMap[-1]
		}
	}

	c.AbortWithStatusJSON(http.StatusOK, gin.H{
		"code":    msgCode,
		"message": msg,
		"data":    data,
	})
}

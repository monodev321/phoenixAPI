package mail

import (
	"strconv"

	"phoenix/pkg/config"
	"phoenix/pkg/logger"
	"phoenix/pkg/werror"

	"gopkg.in/gomail.v2"
)

func SendMail(mailTo []string, subject string, body string) error {

	mailConn := map[string]string{
		"user":        config.AppCfg.Email.User,
		"pass":        config.AppCfg.Email.Pass,
		"host":        config.AppCfg.Email.Host,
		"port":        config.AppCfg.Email.Port,
		"addressName": config.AppCfg.Email.AddressName,
	}

	port, _ := strconv.Atoi(mailConn["port"]) //转换端口类型为int

	m := gomail.NewMessage()

	m.SetHeader("From", m.FormatAddress(mailConn["user"], mailConn["addressName"])) //这种方式可以添加别名，即“XX官方”
	m.SetHeader("To", mailTo...)                                                    //发送给多个用户
	m.SetHeader("Subject", subject)                                                 //设置邮件主题
	m.SetBody("text/html", body)                                                    //设置邮件正文

	d := gomail.NewDialer(mailConn["host"], port, mailConn["user"], mailConn["pass"])

	err := d.DialAndSend(m)
	return err

}

func Register(str string, username string) error {
	//定义收件人
	mailTo := []string{
		username,
	}
	subject := `币用激活邮件`
	link := config.AppCfg.Host + "/v1/oauth2/verify?code=" + str
	body := `您好，欢迎使用本应用，请点击以下链接激活：<a href="` + link + `"  target="_blank">` + link + `</a><br>如果不能点击,请复制到浏览器打开`

	err := SendMail(mailTo, subject, body)
	if err != nil {
		return werror.New(300, "邮件发送失败，请检查邮件账号是否存在")
	}
	logger.Logger.Info(username + ":邮件发送成功")
	return nil

}
func UserCode(str string, username string) error {
	//定义收件人
	mailTo := []string{
		username,
	}
	subject := `币用验证码`
	body := str

	err := SendMail(mailTo, subject, body)
	if err != nil {
		return werror.New(300, "邮件发送失败，请检查邮件账号是否存在")
	}
	logger.Logger.Info(username + ":邮件发送成功")
	return nil

}

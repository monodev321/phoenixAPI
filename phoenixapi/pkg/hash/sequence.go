package hash

import (
	"strconv"
	"sync"
)

/**
 * 使用结构体代替类
 */
type Sequence struct {
	value int
}

/**
 * 建立私有变量
 */
var sequenceInstance *Sequence

var once sync.Once

func GetSequenceInstance() *Sequence {
	if sequenceInstance == nil {
		once.Do(func() {
			sequenceInstance = new(Sequence)
			sequenceInstance.value = 1

		})
	}
	return sequenceInstance
}

func (s *Sequence) GetSequenceValue() string {
	str := strconv.Itoa(s.value)
	zero := ""
	for i := 0; i < 5-len(str); i++ {
		zero = zero + "0"
	}
	r := zero + str
	if s.value < 99999 {
		s.value = s.value + 1
	} else {
		s.value = 1
	}

	return r
}

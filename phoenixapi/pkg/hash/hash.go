package hash

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"gopkg.in/mgo.v2/bson"

	"phoenix/pkg/config"

	uuid "github.com/satori/go.uuid"
)

func BsonId() string {
	return bson.NewObjectId().Hex()
}

func Md5(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

func Md5WithSalt(str string, salt string) string {
	m5 := md5.New()
	m5.Write([]byte(str))
	m5.Write([]byte(string(salt)))
	st := m5.Sum(nil)
	return hex.EncodeToString(st)
}

func GetSha256(str string) string {
	h := sha256.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

func GetSha256WithSalt(str string, salt string) string {
	s := []byte(str)
	key := []byte(salt)
	m := hmac.New(sha256.New, key)
	m.Write(s)
	signature := hex.EncodeToString(m.Sum(nil))
	return signature
}

func GetSha256ByConf(str string) string {
	salt := config.AppCfg.Salt
	return GetSha256WithSalt(str, salt)
}

func CheckSha256(str string, hash string) bool {
	newHash := GetSha256ByConf(str)
	if newHash == hash {
		return true
	}
	return false
}

// 产生hash验证码
func GenerateHashCode(str string) string {
	rand.Seed(time.Now().UnixNano())
	str = str + strconv.Itoa(rand.Intn(10000))
	str = GetSha256ByConf(str)
	data := []byte(str)
	has := md5.Sum(data)
	md5str := fmt.Sprintf("%x", has)
	return md5str
}

// 产生code验证码
func GenerateCode6() string {
	rand.Seed(time.Now().UnixNano())
	str := strconv.Itoa(rand.Intn(899999) + 100000)
	return str
}

func GetUuid() string {
	id := uuid.NewV4()
	return id.String()
}

func int2Str(i int) string {
	s := strconv.Itoa(i)
	if i < 10 {
		return "0" + s
	}
	return s
}

func nanosecond2Str(i int) string {
	s := strconv.Itoa(i)
	zero := ""
	for i := 0; i < 9-len(s); i++ {
		zero = zero + "0"
	}
	r := zero + s
	return r[0:6]
}

func severId2Str(severId string) string {
	zero := ""
	for i := 0; i < 5-len(severId); i++ {
		zero = zero + "0"
	}
	r := zero + severId
	return r[0:5]
}

func GetOrderSequence(severId string) string {
	y := strconv.Itoa(time.Now().Year())  //年
	m := int2Str(int(time.Now().Month())) //月
	d := int2Str(time.Now().Day())        //日
	h := int2Str(time.Now().Hour())       //小时
	s := int2Str(time.Now().Minute())     //分钟
	i := int2Str(time.Now().Second())     //秒
	mi := nanosecond2Str(time.Now().Nanosecond())
	seq := GetSequenceInstance().GetSequenceValue()
	severId = severId2Str(severId)
	return y + m + d + h + s + i + mi + severId + seq //纳秒
}

//go:build debug
// +build debug

package swag

import (
	"github.com/gin-gonic/gin"
	sf "github.com/swaggo/files"
	gs "github.com/swaggo/gin-swagger"
	"phoenix/docs"
)

func SetupSwag(e *gin.Engine) {
	//swagger docs
	swag := docs.SwaggerInfo
	swag.BasePath = "/api/v1"
	swag.Title = "Phoenix Api Server doc"
	swag.Description = `
	1. 所有返回结果都是JSON格式,并且状态是200,不是200就是其它问题
	2. 当有错误时,会返回: {"error":"<error message>"}
	3. 当正确处理时,会返回: {
		"message":"success",//固定值
		"data":nil, 		//看接口情况,结构不同 
		"paginate":{		//有分页时,会有这个属性
			"id":0,				//看情况,传对应的ID
			"page_num":10,		//当前第几页(从1开始)
			"limit":10 			//每页多少条[1,20]
		}
	}`
	e.GET("/docs/*any", gs.WrapHandler(sf.Handler))
}

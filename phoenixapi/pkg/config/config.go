package config

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sync"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	"gopkg.in/yaml.v3"
)

const (
	AppModeDev  = "dev"
	AppModeProd = "prod"
)

var once sync.Once
var BaseDir string

const (
	TopicName             = "coinuseim"
	ChannelName           = "msg"
	SuccessReplyCode      = 0
	FailReplyCode         = 1
	SuccessReplyMsg       = "success"
	QueueName             = "gochat_sub"
	RedisBaseValidTime    = 86400
	RedisPrefix           = "gochat_"
	RedisRoomPrefix       = "gochat_room_"
	RedisRoomOnlinePrefix = "gochat_room_online_count_"
	MsgVersion            = 1
	OpSingleSend          = 2 // single user
	OpGroupSend           = 3 // send to room
	OpRoomCountSend       = 4 // get online user count
	OpRoomInfoSend        = 5 // send info to room
)

var (
	SessionKey     = "sess_id"
	SessionMaxConn = 10
	TokenSecret    = []byte("pH0en1xTkScr")
)

var (
	AppMode = "" //app mode, one of `dev`,`prod`
	AppCfg  *appConfig
)

type appConfig struct {
	App struct {
		Listen      string `yaml:"listen"`
		Mode        string `yaml:"mode"`
		ImageDir    string `yaml:"image_dir"`
		FilmDir     string `yaml:"film_dir"`
		ImageUrl    string `yaml:"image_url"`
		ResourceDir string `yaml:"resource_dir"`
		I18nDir     string `yaml:"i18n_dir"`
	} `yaml:"app"`
	Mysql struct {
		DSN string `yaml:"dsn"`
	} `yaml:"mysql"`
	Redis struct {
		DSN string `yaml:"dsn"`
	} `yaml:"redis"`

	AppPort           int               `yaml:"appPort"`
	GrpcAddr          string            `yaml:"grpcAddr"`
	ENV               string            `yaml:"env"`
	Salt              string            `yaml:"salt"`
	Host              string            `yaml:"host"`
	Signature         string            `yaml:"signature"`
	AvatarBase        string            `yaml:"avatarBase"`
	UploadBaseDir     string            `yaml:"uploadBaseDir"`
	AvatarGroupBase   string            `appName:"avatarGroupBase"`
	TelesignID        string            `yaml:"telesignID"`
	TelesignSecretKey string            `yaml:"telesignSecretKey"`
	Email             Email             `yaml:"email"`
	Oauth2            Oauth2            `yaml:"oauth2"`
	DappOauth2        DappOauth2        `yaml:"dappOauth2"`
	AdminOauth2       AdminOauth2       `yaml:"adminOauth2"`
	ServerHost        map[string]string `yaml:"serverHost"`
	HostConfig        map[string]string `yaml:"hostConfig"`
	ImConnect         ConnectConfig     `yaml:"ImConnect"`
	ImTask            TaskConfig        `yaml:"ImTask"`
	Taskservice       []Taskservice     `yaml:"taskservice"`
	LevelDB           string            `yaml:"levelDB"`
	RpcAddr           string            `yaml:"rpcAddr"`
	ConnRpc           string            `yaml:"connRpc"`
}

type Email struct {
	User        string `yaml:"user"`
	Pass        string `yaml:"pass"`
	Host        string `yaml:"host"`
	Port        string `yaml:"port"`
	AddressName string `yaml:"addressName"`
}

type Oauth2 struct {
	ID         int64  `yaml:"id"`
	Secret     string `yaml:"secret"`
	PublickKey string `yaml:"publickKey"`
}

type DappOauth2 struct {
	ID          int64  `yaml:"id"`
	Secret      string `yaml:"secret"`
	PublickKey  string `yaml:"publickKey"`
	RedirectUri string `yaml:"redirectUri"`
}

type AdminOauth2 struct {
	ID          int64  `yaml:"id"`
	Secret      string `yaml:"secret"`
	PublickKey  string `yaml:"publickKey"`
	RedirectUri string `yaml:"redirectUri"`
}

type Taskservice struct {
	Time  int      `yaml:"time"`
	Num   int      `yaml:"num"`
	Tasks []string `yaml:"tasks"`
}

type ConnectRpcAddress struct {
	Address string `yaml:"address"`
}

type ConnectBucket struct {
	CpuNum        int    `yaml:"cpuNum"`
	Channel       int    `yaml:"channel"`
	Room          int    `yaml:"room"`
	SrvProto      int    `yaml:"svrProto"`
	RoutineAmount uint64 `yaml:"routineAmount"`
	RoutineSize   int    `yaml:"routineSize"`
}

type ConnectWebsocket struct {
	Bind string `yaml:"bind"`
}

type ConnectConfig struct {
	ConnectRpcAddress ConnectRpcAddress `yaml:"connectRpcAddress"`
	ConnectBucket     ConnectBucket     `yaml:"connectBucket"`
	ConnectWebsocket  ConnectWebsocket  `yaml:"connectWebsocket"`
	LevelDB           string            `yaml:"levelDB"`
	SendMsgDB         string            `yaml:"sendMsgDB"`
	RpcAddr           string            `yaml:"taskRpc"`
	ApiRpc            string            `yaml:"apiRpc"`
}

type TaskBase struct {
	CpuNum       int `yaml:"cpuNum"`
	PushChan     int `yaml:"pushChan"`
	PushChanSize int `yaml:"pushChanSize"`
}

type TaskConfig struct {
	TaskBase          TaskBase `yaml:"taskBase"`
	TaskIncomeDB      string   `yaml:"taskIncomeDB"`
	TaskUserSessionDB string   `yaml:"taskUserSessionDB"`
	TaskUserMsgDB     string   `yaml:"taskUserMsgDB"`
	TaskSingleMsgDB   string   `yaml:"taskSingleMsgDB"`
	TaskGroupMsgDB    string   `yaml:"taskGroupMsgDB"`
	RpcAddr           string   `yaml:"rpcAddr"`
	ApiRpc            string   `yaml:"apiRpc"`
	ConnRpc           string   `yaml:"connRpc"`
}

func init() {
	GetMode()

	cfgFile := viper.GetString("c")
	err := Init(cfgFile)
	fmt.Printf("%+v  as \n", AppCfg)
	if err != nil {
		fmt.Printf("%+v asas \n", err)
		return
	}
}
func Init(f string) error {
	file, err := filepath.Abs(f)
	if err != nil {
		return err
	}
	content, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	AppCfg = &appConfig{}
	err = yaml.Unmarshal(content, AppCfg)
	AppMode = AppCfg.App.Mode
	if AppModeProd != AppMode {
		AppMode = AppModeDev
	}
	return err
}

func GetMode() {
	pflag.String("env", "local", "运行环境")
	pflag.String("m", "api", "assign run module")
	pflag.String("c", "app-dev.yaml", "配置文件")
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)
}

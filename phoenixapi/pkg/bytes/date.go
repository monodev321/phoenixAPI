package btytes

import (
	"encoding/binary"
	"fmt"

	"phoenix/internal/im/tools"
	"strconv"
	"time"
)

func GetYMD(days int) uint32 {
	duration := fmt.Sprintf("-%dh", 24*days)
	h, _ := time.ParseDuration(duration)
	time2 := time.Now().Add(h)
	date := time2.Format("20060102")
	ymd, _ := strconv.Atoi(date)
	return uint32(ymd)
}

func YmdToBytes(days int) []byte {
	ymd := GetYMD(days)
	var field1 []byte = make([]byte, 4)
	binary.BigEndian.PutUint32(field1, uint32(ymd))
	return field1
}

func YmdByMid(mid uint64) uint32 {
	timestamp := tools.GetTimeBySnowflakeId(int64(mid)) / 1000
	time2 := time.Unix(int64(timestamp), 0)
	date := time2.Format("20060102")
	ymd, _ := strconv.Atoi(date)
	return uint32(ymd)
}

func YmdBySendid(mid uint64) uint32 {
	timestamp := mid / 100000
	time2 := time.Unix(int64(timestamp), 0)
	date := time2.Format("20060102")
	ymd, _ := strconv.Atoi(date)
	return uint32(ymd)
}

func TimestampByMid(mid uint64) int64 {
	timestamp := tools.GetTimeBySnowflakeId(int64(mid)) / 1000
	return timestamp
}

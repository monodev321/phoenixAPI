package btytes

import (
	"bytes"
	"encoding/binary"
)

func ConnectClientMsgKey(date uint32, uid uint64, sendid uint64) []byte {
	var field0 []byte = make([]byte, 4)
	var field1 []byte = make([]byte, 8)
	var field2 []byte = make([]byte, 8)
	binary.BigEndian.PutUint32(field0, date)
	binary.BigEndian.PutUint64(field1, uid)
	binary.BigEndian.PutUint64(field2, sendid)
	var key bytes.Buffer
	key.Write(field0)
	key.Write(field1)
	key.Write(field2)
	return key.Bytes()
}

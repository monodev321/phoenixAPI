package nsq

import (
	"sync"
	"time"

	"github.com/nsqio/go-nsq"
)

var once sync.Once

// 消费者
type NSQProducer struct {
	producer *nsq.Producer
	adds     []string
	topic    string
}

var nsqProducerInstance *NSQProducer

func GetProducerInstance(adds []string, topic string, addIndex int) *NSQProducer {
	if nsqProducerInstance == nil {
		once.Do(func() {
			nsqProducerInstance = new(NSQProducer)
			nsqProducerInstance.adds = adds
			nsqProducerInstance.topic = topic
			nsqProducerInstance.initProducer(addIndex)
		})
	}
	return nsqProducerInstance
}

// 初始化生产者
func (p *NSQProducer) initProducer(i int) {
	var err error
	j := i % len(p.adds)
	p.producer, err = nsq.NewProducer(p.adds[j], nsq.NewConfig())
	if err != nil {
		panic(err)
	}
}

//发布消息
func (p *NSQProducer) publish(message []byte) error {
	var err error
	if p.producer != nil {
		if message == nil { //不能发布空串，否则会导致error
			return nil
		}
		err = p.producer.Publish(p.topic, message) // 发布消息
		return err
	}
	return err
}

func (p *NSQProducer) Send(message []byte) {
	// i := 0
	// for err := p.publish(message); err != nil; err = p.publish(message) {
	// 	p.initProducer(i)
	// 	i = i + 1
	// }
}

//发布消息
func (p *NSQProducer) Stop() {
	p.producer.Stop()
}

type nsqConsumer struct {
}

var nsqConsumerInstance *nsqConsumer

func GetConsumerInstance() *nsqConsumer {
	if nsqConsumerInstance == nil {
		once.Do(func() {
			nsqConsumerInstance = new(nsqConsumer)

		})
	}
	return nsqConsumerInstance
}

//@todo 处理nsq 集群
func (client *nsqConsumer) InitConsumer(topic string, channel string, address []string, handle nsq.Handler) {
	cfg := nsq.NewConfig()
	cfg.LookupdPollInterval = time.Second * 20     //设置重连时间
	c, err := nsq.NewConsumer(topic, channel, cfg) // 新建一个消费者
	if err != nil {
		panic(err)
	}
	c.SetLogger(nil, 0)  //屏蔽系统日志
	c.AddHandler(handle) // 添加消费者接口

	//建立多个nsqd连接
	if err := c.ConnectToNSQDs([]string{address[0]}); err != nil {
		panic(err)
	}
}

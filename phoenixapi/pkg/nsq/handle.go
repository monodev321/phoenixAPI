//Nsq接收测试
package nsq

import (
	"fmt"

	"github.com/nsqio/go-nsq"
)

// 消费者
type ConsumerT struct{}

func (*ConsumerT) HandleMessage(msg *nsq.Message) error {
	fmt.Println("receive", msg.NSQDAddress, "message:", string(msg.Body))
	return nil
}

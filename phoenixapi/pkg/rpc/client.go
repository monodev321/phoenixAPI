package rpc

import (
	"github.com/smallnest/rpcx/client"
	"sync"
)

var RpcClients sync.Map

func init() {
	//RpcClients = make(map[string]*client.OneClient, 10) // 需要修改并发
}

func GetRpcClient(path string) (clientRpc *client.OneClient) {
	_clientRpc, ok := RpcClients.Load(path)
	if ok {
		clientRpc = _clientRpc.(*client.OneClient)
		return
	}
	d, _ := client.NewPeer2PeerDiscovery(path, "")
	clientRpc = client.NewOneClient(client.Failtry, client.RandomSelect, d, client.DefaultOption)
	RpcClients.Store(path, clientRpc)
	return
}

func CloseRpcClient() {
	RpcClients.Range(func(key, value interface{}) bool {
		name := key.(string)
		client := value.(*client.OneClient)
		client.Close()
		RpcClients.Delete(name)
		return true
	})
}

func DelRpcClient(path string) {
	_clientRpc, ok := RpcClients.Load(path)
	if ok {
		clientRpc := _clientRpc.(*client.OneClient)
		clientRpc.Close()
	}
	RpcClients.Delete(path)
}

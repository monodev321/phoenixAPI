package werror

import (
	"errors"
	"fmt"
	"runtime"
	"strings"
)

type ResError struct {
	code  int32 // 100 表示参数不符合规范  200表示响应参数不符合规范（比如数据库调用）  300表示函数执行错误 500表示未知系统错误
	msg   string
	where string
}

//对于主动报错的地方
func New(code int32, msg string) *ResError {
	// 获取代码位置, 代码就不贴了, 不是重点.
	where := caller(2)
	return &ResError{code: code, msg: msg, where: where}
}

func (e *ResError) Error() string {
	return fmt.Sprintf("%s", e.msg)
}
func (e *ResError) GetCode() int32 {
	return e.code
}
func (e *ResError) LogError() string {
	return fmt.Sprintf("code = %d ; msg = %s; where = %s", e.code, e.msg, e.where)
}

//函数调用出错的地方，用于返回
func FunWrap(err interface{}, code int32, msg string) *ResError {
	_err := assert(err)
	var where string
	switch t := _err.(type) {
	case *ResError:
		// 继承where和code
		where = t.where
		code = t.code
		// 拼接上之前的错误
		msg = msg + ":: " + t.msg
	default:
		where = stack()
		msg = msg + ":: " + _err.Error()
	}

	return &ResError{code: code, msg: msg, where: where}
}

//未捕获的系统错误
func ResWrap(err interface{}) *ResError {
	_err := assert(err)
	var where string
	var msg string
	var code int32 = 500
	switch t := _err.(type) {

	case *ResError:
		// 继承where和code
		where = t.where
		code = t.code
		// 拼接上之前的错误
		msg = t.msg
	default:
		where = stack()
		msg = _err.Error()
	}

	return &ResError{code: code, msg: msg, where: where}
}

// Stack 获取堆栈信息
func stack() string {
	var pc = make([]uintptr, 12)
	n := runtime.Callers(3, pc)

	var build strings.Builder
	for i := 1; i < n; i++ {
		f := runtime.FuncForPC(pc[i] - 1)
		file, line := f.FileLine(pc[i] - 1)
		// n := strings.Index(file, name)
		if n != -1 {
			s := fmt.Sprintf(" %s:%d \n", file, line)
			build.WriteString(s)
		}
	}
	return build.String()
}

func assert(r interface{}) error {
	var err error
	switch x := r.(type) {
	case string:
		err = errors.New(x)
	case error:
		err = x
	default:
		err = errors.New("未知错误")
	}
	return err
}

//获取函数的未知
func caller(skip int) string {
	var build strings.Builder
	pc, file, line, _ := runtime.Caller(skip)
	pcName := runtime.FuncForPC(pc).Name() //获取函数名

	s := fmt.Sprintf(" %s:%d %s", file, line, pcName)
	build.WriteString(s)
	return build.String()
}

// func WrapRPCError(err error) error {
// 	if err == nil {
// 		return nil
// 	}
// 	e, _ := status.FromError(err)
// 	s := &spb.Status{
// 		Code:    int32(e.Code()),
// 		Message: e.Message(),
// 		Details: []*any.Any{
// 			{
// 				TypeUrl: TypeUrlStack,
// 				Value:   []byte{'1'},
// 			},
// 		},
// 	}
// 	return status.FromProto(s).Err()
// }

// func GetErrorStack(s *status.Status) string {
// 	pbs := s.Proto()
// 	for i := range pbs.Details {
// 		if pbs.Details[i].TypeUrl == TypeUrlStack {
// 			return "As"
// 		}
// 	}
// 	return ""
// }

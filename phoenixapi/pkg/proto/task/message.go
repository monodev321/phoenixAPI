package task

type Msg struct {
	Msg []byte
}

type Reply struct {
	C int
}

type HttpPullMessage struct {
	Uid uint64
}

type HttpPullMessageReply struct {
	Code     int64
	Messages map[string]string
}

type HttpPullMessageAck struct {
	Uid uint64
	Ids []string
}

type HttpPullMessageAckReply struct {
	Code int64
}

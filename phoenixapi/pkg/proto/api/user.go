package api

type UserLoginArgs struct {
	Uid int64
}

type UserLoginrReply struct {
	Code     int
	Token    string
	PushType int32 // 0无 1 fcm 2 anps
}

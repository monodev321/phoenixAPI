package api

type GroupMemberArgs struct {
	Gid int64
}

type GroupMemberReply struct {
	Code int
	Uids string
}

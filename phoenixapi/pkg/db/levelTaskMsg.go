package db

import (
	"encoding/binary"
	"fmt"

	"phoenix/pkg/config"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"
)

var TaskMsgLevelDB *leveldb.DB

type levelDBTaskMsg struct{}

var LevelDBTaskMsg = new(levelDBTaskMsg)

var TaskChan chan int

func (db *levelDBTaskMsg) Init2() {
	o := &opt.Options{}
	var err error
	TaskMsgLevelDB, err = leveldb.OpenFile(config.AppCfg.ImTask.TaskIncomeDB, o)
	TaskChan = make(chan int, 500)
	if err != nil {
		panic(err)
	}
}

func (db *levelDBTaskMsg) AddMessage(mid int64, msg []byte) error {
	var key2 []byte = make([]byte, 8) // toid
	binary.BigEndian.PutUint64(key2, uint64(mid))
	err := TaskMsgLevelDB.Put(key2, msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
		return err
	}
	TaskChan <- 1
	return nil
}

func (db *levelDBTaskMsg) GetMessage() (iter iterator.Iterator) {
	iter = TaskMsgLevelDB.NewIterator(util.BytesPrefix([]byte{}), nil)
	return iter
}

func (db *levelDBTaskMsg) DelMessage(key []byte) error {
	err := TaskMsgLevelDB.Delete(key, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

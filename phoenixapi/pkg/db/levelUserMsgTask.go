package db

import (
	"fmt"

	"phoenix/pkg/config"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	Bybytes "phoenix/pkg/bytes"
)

var LevelDBUserMsgTask *leveldb.DB

type userMsgTaskLevelDB struct{}

var UserMsgTaskLevelDB = new(userMsgTaskLevelDB)

func (db *userMsgTaskLevelDB) Init2() {
	o := &opt.Options{}
	var err error
	LevelDBUserMsgTask, err = leveldb.OpenFile(config.AppCfg.ImTask.TaskUserMsgDB, o)
	if err != nil {
		panic(err)
	}
}

func (db *userMsgTaskLevelDB) AddMessage(key []byte, msg []byte) error {
	err := LevelDBUserMsgTask.Put(key, msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *userMsgTaskLevelDB) GetMessages(prefix []byte) (iter iterator.Iterator) {
	// key := "u_"
	iter = LevelDBUserMsgTask.NewIterator(util.BytesPrefix(prefix), nil)
	// iter = TaskMsgLevelDB.NewIterator(nil, nil)
	return iter
}

func (db *userMsgTaskLevelDB) GetByKey(key []byte) ([]byte, error) {
	val, err := LevelDBUserMsgTask.Get(key, nil)
	if err == leveldb.ErrNotFound {
		return val, nil
	}
	return val, err
}

func (db *userMsgTaskLevelDB) DelMessage(key []byte) error {
	err := LevelDBUserMsgTask.Delete(key, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *userMsgTaskLevelDB) DelExpired(days int) error {
	key := Bybytes.YmdToBytes(days)
	iter := LevelDBUserMsgTask.NewIterator(util.BytesPrefix(key), nil)
	for iter.Next() {
		err := LevelDBUserMsgTask.Delete(iter.Key(), nil)
		if err != nil {
			fmt.Printf("删除消息: %+v\n", err)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		fmt.Printf("删除消息处理 %+v \n", err)
	}
	return err
}

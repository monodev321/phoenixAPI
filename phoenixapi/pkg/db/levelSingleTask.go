package db

import (
	"fmt"

	"phoenix/pkg/config"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	Bybytes "phoenix/pkg/bytes"
)

var LevelDBSingleTask *leveldb.DB

type singleTaskLevelDB struct{}

var SingleTaskLevelDB = new(singleTaskLevelDB)

func (db *singleTaskLevelDB) Init2() {
	o := &opt.Options{}
	var err error
	LevelDBSingleTask, err = leveldb.OpenFile(config.AppCfg.ImTask.TaskSingleMsgDB, o)
	if err != nil {
		panic(err)
	}
}

func (db *singleTaskLevelDB) AddMessage(key []byte, msg []byte) error {
	err := LevelDBSingleTask.Put(key, msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *singleTaskLevelDB) GetMessages() (iter iterator.Iterator) {
	// key := "u_"
	// iter = LevelDBSingleTask.NewIterator(util.BytesPrefix([]byte(key)), nil)
	iter = LevelDBSingleTask.NewIterator(nil, nil)
	return iter
}

func (db *singleTaskLevelDB) GetByKey(key []byte) ([]byte, error) {
	val, err := LevelDBSingleTask.Get(key, nil)
	if err == leveldb.ErrNotFound {
		return val, nil
	}
	return val, err
}

func (db *singleTaskLevelDB) DelMessage(key []byte) error {
	err := LevelDBSingleTask.Delete(key, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *singleTaskLevelDB) DelExpired(days int) error {
	key := Bybytes.YmdToBytes(days)
	iter := LevelDBSingleTask.NewIterator(util.BytesPrefix(key), nil)
	for iter.Next() {
		err := LevelDBSingleTask.Delete(iter.Key(), nil)
		if err != nil {
			fmt.Printf("删除消息: %+v\n", err)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		fmt.Printf("删除消息处理 %+v \n", err)
	}
	return err
}

package db

import (
	"phoenix/pkg/config"

	"fmt"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
)

var LevelDBApi *leveldb.DB

type apiLevelDB struct{}

var ApiLevelDB = new(apiLevelDB)

func (db *apiLevelDB) Init2() {
	o := &opt.Options{}
	var err error
	LevelDBApi, err = leveldb.OpenFile(config.AppCfg.LevelDB, o)

	if err != nil {
		panic(err)
	}
}

func (db *apiLevelDB) Add(key string, msg []byte) error {
	err := LevelDBApi.Put([]byte(key), msg, nil)
	return err
}

func (db *apiLevelDB) Del(key string) error {
	err := LevelDBApi.Delete([]byte(key), nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
		return err
	}
	return nil
}

func (db *apiLevelDB) Get(key string) (data string, err error) {
	val, err := LevelDBApi.Get([]byte(key), nil)
	data = string(val)
	return data, err
}

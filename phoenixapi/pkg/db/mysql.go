package db

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"phoenix/pkg/config"
)

var (
	Orm *gorm.DB
)

func InitMySql(dsn string) error {
	db, err := gorm.Open(mysql.Open(dsn))
	if err != nil {
		return err
	}
	if config.AppMode == config.AppModeDev {
		Orm = db.Debug()
	}
	return nil
}

package db

import (
	"fmt"

	"phoenix/pkg/config"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	Bybytes "phoenix/pkg/bytes"
)

var LevelDBGroupTask *leveldb.DB

type groupTaskLevelDB struct{}

var GroupTaskLevelDB = new(groupTaskLevelDB)

func (db *groupTaskLevelDB) Init2() {
	o := &opt.Options{}
	var err error
	LevelDBGroupTask, err = leveldb.OpenFile(config.AppCfg.ImTask.TaskGroupMsgDB, o)
	if err != nil {
		panic(err)
	}
}

func (db *groupTaskLevelDB) AddMessage(key []byte, msg []byte) error {
	err := LevelDBGroupTask.Put(key, msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *groupTaskLevelDB) GetMessages() (iter iterator.Iterator) {
	// key := "u_"
	// iter = LevelDBGroupTask.NewIterator(util.BytesPrefix([]byte(key)), nil)
	iter = LevelDBGroupTask.NewIterator(nil, nil)
	return iter
}

func (db *groupTaskLevelDB) GetByKey(key []byte) ([]byte, error) {
	val, err := LevelDBGroupTask.Get(key, nil)
	if err == leveldb.ErrNotFound {
		return val, nil
	}
	return val, err
}

func (db *groupTaskLevelDB) DelMessage(key []byte) error {
	err := LevelDBGroupTask.Delete(key, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *groupTaskLevelDB) DelExpired(days int) error {
	key := Bybytes.YmdToBytes(days)
	iter := LevelDBGroupTask.NewIterator(util.BytesPrefix(key), nil)
	for iter.Next() {
		err := LevelDBGroupTask.Delete(iter.Key(), nil)
		if err != nil {
			fmt.Printf("删除消息: %+v\n", err)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		fmt.Printf("删除消息处理 %+v \n", err)
	}
	return err
}

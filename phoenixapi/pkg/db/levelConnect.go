package db

import (
	"fmt"

	"phoenix/pkg/config"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	Bybytes "phoenix/pkg/bytes"
)

var LevelDBConnect *leveldb.DB

type connectLevelDB struct{}

var ConnectLevelDB = new(connectLevelDB)

var ConnChan chan int

func (db *connectLevelDB) Init2() {
	o := &opt.Options{}
	var err error
	LevelDBConnect, err = leveldb.OpenFile(config.AppCfg.ImConnect.LevelDB, o)
	ConnChan = make(chan int, 500)
	// defer LevelDBConnect.Close()
	if err != nil {
		panic(err)
	}
}

func (db *connectLevelDB) AddMessage(key string, msg []byte) error {
	// 添加时间
	key2 := "u_" + key
	// fmt.Printf("AddMessage call key: %s\n", key2)
	err := LevelDBConnect.Put([]byte(key2), msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	ConnChan <- 1
	return err
}

func (db *connectLevelDB) AddMessage2(key []byte, msg []byte) error {
	err := LevelDBConnect.Put(key, msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	ConnChan <- 1
	return err
}

func (db *connectLevelDB) GetMessage() (iter iterator.Iterator) {
	// key := "u_"
	// iter = LevelDBConnect.NewIterator(util.BytesPrefix([]byte(key)), nil)
	iter = LevelDBConnect.NewIterator(nil, nil)
	return iter
}

func (db *connectLevelDB) GetByKey(key []byte) ([]byte, error) {
	val, err := LevelDBConnect.Get(key, nil)
	if err == leveldb.ErrNotFound {
		return val, nil
	}
	return val, err
}

func (db *connectLevelDB) DelMessage(key []byte) error {
	err := LevelDBConnect.Delete(key, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *connectLevelDB) DelExpired(days int) error {
	key := Bybytes.YmdToBytes(days)
	iter := LevelDBConnect.NewIterator(util.BytesPrefix(key), nil)
	for iter.Next() {
		err := LevelDBConnect.Delete(iter.Key(), nil)
		if err != nil {
			fmt.Printf("删除消息: %+v\n", err)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		fmt.Printf("删除消息处理 %+v \n", err)
	}
	return err
}

package db

import (
	"context"
	"github.com/go-redis/redis/v8"
)

var (
	Redis *redis.Client
)

func InitRedis(dsn string) error {
	opt, err := redis.ParseURL(dsn)
	if err != nil {
		return err
	}
	rdb := redis.NewClient(opt)
	_, err = rdb.Ping(context.Background()).Result()
	if err != nil {
		return err
	}
	Redis = rdb
	return nil
}

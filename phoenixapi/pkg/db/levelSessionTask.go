package db

import (
	"fmt"

	"phoenix/pkg/config"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/iterator"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"github.com/syndtr/goleveldb/leveldb/util"

	Bybytes "phoenix/pkg/bytes"
)

var LevelDBSessionTask *leveldb.DB

type sessionTaskLevelDB struct{}

var SessionTaskLevelDB = new(sessionTaskLevelDB)

func (db *sessionTaskLevelDB) Init2() {
	o := &opt.Options{}
	var err error
	LevelDBSessionTask, err = leveldb.OpenFile(config.AppCfg.ImTask.TaskUserSessionDB, o)
	if err != nil {
		panic(err)
	}
}

func (db *sessionTaskLevelDB) AddMessage(key []byte, msg []byte) error {
	err := LevelDBSessionTask.Put(key, msg, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *sessionTaskLevelDB) GetMessages() (iter iterator.Iterator) {
	// key := "u_"
	// iter = LevelDBSessionTask.NewIterator(util.BytesPrefix([]byte(key)), nil)
	iter = LevelDBSessionTask.NewIterator(nil, nil)
	return iter
}

func (db *sessionTaskLevelDB) GetByKey(key []byte) ([]byte, error) {
	val, err := LevelDBSessionTask.Get(key, nil)
	if err == leveldb.ErrNotFound {
		return val, nil
	}
	return val, err
}

func (db *sessionTaskLevelDB) DelMessage(key []byte) error {
	err := LevelDBSessionTask.Delete(key, nil)
	if err != nil {
		fmt.Printf("call: %+v\n", err)
	}
	return err
}

func (db *sessionTaskLevelDB) DelExpired(days int) error {
	key := Bybytes.YmdToBytes(days)
	iter := LevelDBSessionTask.NewIterator(util.BytesPrefix(key), nil)
	for iter.Next() {
		err := LevelDBSessionTask.Delete(iter.Key(), nil)
		if err != nil {
			fmt.Printf("删除消息: %+v\n", err)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		fmt.Printf("删除消息处理 %+v \n", err)
	}
	return err
}

package db

import (
	"fmt"
	"phoenix/app/types"
	"phoenix/pkg/config"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var DBCli *sqlx.DB

func init() {
	Init()
}

func Init() {
	var err error

	fmt.Printf("%+v \n", config.AppCfg)
	DBCli, err = sqlx.Open("mysql", config.AppCfg.Mysql.DSN)
	if err != nil {
		panic(err)
	}

	mysqlDsn := config.AppCfg.Mysql.DSN
	err = InitMySql(mysqlDsn)
	if err != nil {
		panic(fmt.Errorf("init mysql: %s", err))
	}
	err = Orm.AutoMigrate(types.MigrateTables...)
	if err != nil {
		panic(fmt.Errorf("auto migrate table: %s", err))
	}

	redisDsn := config.AppCfg.Redis.DSN
	err = InitRedis(redisDsn)
	if err != nil {
		panic(fmt.Errorf("init redis: %s", err))
	}
}
